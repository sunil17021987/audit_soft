TRUNCATE TABLE `agentfeedback`
TRUNCATE TABLE `audit_remarks`
TRUNCATE TABLE `audit`;


UPDATE audit_remarks
        INNER JOIN
    audit ON audit.id = audit_remarks.audit_id 
SET 
    audit_remarks.feedback_status_id = 'Pending'
WHERE
    audit_remarks.feedback_status_id ='NA' AND audit.score < 90;


SELECT e.emp_name,COUNT(a.id) as AM_total,COUNT(a.id) as fatal FROM audit as a
  INNER JOIN employee as e ON e.emp_id=a.employee_id_AM
 INNER JOIN audit_remarks as ar ON ar.audit_id=a.id
WHERE ar.Fatal_Reason!=0 AND a.date=CURDATE()
 GROUP BY a.employee_id_AM


SELECT e.emp_name,COUNT(a.id) as AM_total,(SELECT COUNT(arr.Fatal_Reason) FROM audit_remarks as arr WHERE arr.Fatal_Reason!=0 AND arr.employee_id_AM=e.emp_id AND $where1) as fatal FROM audit as a INNER JOIN employee as e ON e.emp_id=a.employee_id_AM $where GROUP BY a.employee_id_AM


**************************Reports***********************************
SELECT e.emp_name,COUNT(a.id) as AM_total,AVG(a.score) AS score,(SELECT COUNT(arr.Fatal_Reason) FROM audit_remarks as arr WHERE arr.Fatal_Reason!=0 AND arr.employee_id_AM=e.emp_id) as fatal FROM audit as a INNER JOIN employee as e ON e.emp_id=a.employee_id_AM GROUP BY a.employee_id_AM

SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_AM WHERE MONTH(a.date)= MONTH(CURRENT_DATE()) AND YEAR(a.date)= YEAR(CURRENT_DATE()) GROUP BY a.employee_id_AM 


SELECT a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id WHERE YEAR(a.date)='2019' AND MONTH(a.date)='09' GROUP BY WEEK(a.date)




SELECT  `ag`.`emp_id`, `a`.`id`, `a`.`score`, `a`.`date`, `ag`.`emp_id`, `ag`.`name`, `ag`.`process`, `ag`.`gender`, `ag`.`batch_no`, `e`.`emp_name` as `employee_id_TL`, `ee`.`emp_name` as `employee_id_AM`, `eee`.`emp_name` as `auditByName` FROM `agentfeedback` as `af` JOIN `audit` as `a` ON `a`.`id` = `af`.`audit_id` JOIN `agents` as `ag` ON `ag`.`id` = `a`.`agents_id` JOIN `employee` as `eee` ON `eee`.`emp_id` = `a`.`audit_by` JOIN `employee` as `e` ON `e`.`emp_id` = `ag`.`employee_id_TL` JOIN `employee` as `ee` ON `ee`.`emp_id` = `ag`.`employee_id_AM` WHERE `a`.`date` >= '2019-09-01' AND `a`.`date` <= '2019-10-10' AND `af`.`Accepted` = 'No'


select lt_feedback_id,complaints_consumer_name,consumer1.consumer_name,non_ivrs_calling.consumer_name,complaints_ivrs,consumer_ivrs,complaints_called_mobile,complaints_consumer_mobile,consumer_mobile,non_ivrs_calling.mobile,non_ivrs_calling.mobile2,lt_feedback_answer,circle_name from lt_consumer_feedback left join non_ivrs_calling on lt_consumer_feedback.lt_feedback_non_ivrs_calling=non_ivrs_calling.complaints_id left join complaints on lt_consumer_feedback.lt_feedback_complaints_id=complaints.complaints_id left join consumer1 on lt_consumer_feedback.lt_feedback_consumer_id=consumer1.consumer_id left join circle on lt_consumer_feedback.lt_feedback_circle_id=circle.circle_id where lt_consumer_feedback.lt_feedback_question_id=2 group by lt_consumer_feedback.lt_feedback_id;

**************************************************************************************************************************************************************************
delete from complaints_escalation where complaints_escalation.escalation_complaint_id in (SELECT complaints_id FROM Magnum_jabalpur.complaints_closed) ;
delete from complaints_escalation where complaints_escalation.escalation_id in (SELECT escalation_id FROM complaints_escalation_closed  ) order by escalation_id ASC limit 50000 ;


delete from complaints_history where complaints_history.complaints_history_id in (SELECT complaints_history_id FROM complaints_history_closed  ) order by complaints_history_id ASC limit 10000;



delete from complaints_question where complaints_question.complaint_question_id in (SELECT complaint_question_id FROM complaints_question_closed) order by complaint_question_id ASC limit 50000;

551614



select complaints_id,complaints_consumer_name,complaints_called_mobile,complaints_ivrs,complaints_history_type,region_name,circle_name,division_name,sub_division_name,distributed_center_name,category_main_name,category_sub_name,complaints_created_date,complaints_remark from Magnum_jabalpur.complaints
left join complaints_history on complaints.complaints_id=complaints_history.complaints_history_complaint_id 
left join region on complaints.complaints_region=region.region_id
left join circle on complaints.complaints_circle=circle.circle_id
left join division on complaints.complaints_division=division.division_id
left join sub_division on complaints.complaints_sub_division=sub_division.sub_division_id
left join distributed_center on complaints.complaints_dc=distributed_center.distributed_center_id
left join category_main on complaints.complaints_main_category=category_main.category_main_id
left join category_sub on complaints.complaints_sub_category=category_sub.category_sub_id
where  complaints_history.complaints_history_status  in (1)  and complaints.complaints_main_category in (12)  and complaints.complaints_created_date between '2019-10-01 00:00:00' and '2019-11-31 00:00:00'   group by complaints.complaints_id;



select tb1.*  from  lt_consumer_feedback as tb1 where tb1.lt_feedback_question_id = 11 and tb1.lt_feedback_complaints_id IN (select tb2.lt_feedback_complaints_id from lt_consumer_feedback as tb2 where tb2.lt_feedback_question_id =2 and tb2.lt_feedback_answer='हाँ'  )

select tb1.*  from  lt_consumer_feedback as tb1 where
tb1.`lt_feedback_complaints_id`!=0 and
tb1.lt_feedback_question_id = 11 and tb1.lt_feedback_complaints_id IN (select tb2.lt_feedback_complaints_id from lt_consumer_feedback as tb2 where tb2.lt_feedback_question_id =2 and tb2.lt_feedback_answer='हाँ'  )
 ORDER BY `lt_feedback_id` DESC

DELETE form lt_consumer_feedback where lt_feedback_id IN (select lt_feedback_id  from  lt_consumer_feedback as tb1 where
tb1.`lt_feedback_complaints_id`!=0 and
tb1.lt_feedback_question_id = 11 and tb1.lt_feedback_complaints_id IN (select tb2.lt_feedback_complaints_id from lt_consumer_feedback as tb2 where tb2.lt_feedback_question_id =2 and tb2.lt_feedback_answer='हाँ'  )
 )

select lt_feedback_id  from  lt_consumer_feedback as tb1 where
tb1.`lt_feedback_complaints_id`!=0 and
tb1.lt_feedback_question_id = 11 and tb1.lt_feedback_complaints_id IN (select tb2.lt_feedback_complaints_id from lt_consumer_feedback as tb2 where tb2.lt_feedback_question_id =2 and tb2.lt_feedback_answer='हाँ'  )
 ORDER BY `lt_feedback_id` DESC



DELETE from lt_consumer_feedback where lt_feedback_id IN (select lt_feedback_id  from  lt_consumer_feedback as tb1 where
tb1.`lt_feedback_complaints_id`!=0 and
tb1.lt_feedback_question_id = 11 and tb1.lt_feedback_complaints_id IN (select tb2.lt_feedback_complaints_id from lt_consumer_feedback as tb2 where tb2.lt_feedback_question_id =2 and tb2.lt_feedback_answer='हाँ'  )
 )


SELECT c.complaints_id, FROM complaints as c

SELECT ch.complaints_history_id,ch.complaints_history_status,ch.complaints_history_complaint_id,DATE_FORMAT(ch.complaints_history_created_date,'%Y-%m-%d') history_date,ch.complaints_history_users_id,u.users_empid,u.users_first_name,u.users_last_name  FROM
 complaints_history ch
INNER JOIN complaints as c ON c.complaints_id = ch.complaints_history_complaint_id
INNER JOIN users as u ON u.users_id=ch.complaints_history_users_id
 where complaints_history_status=4 AND ch.complaints_history_created_date BETWEEN DATE(NOW()) - INTERVAL 20 DAY AND DATE(NOW()) GROUP by ch.complaints_history_users_id

CHATURVEDI=9425629154
-----------------------------




---------------------------LT FEEDBACK----------------------------
SELECT lt.lt_feedback_id,lt.lt_feedback_question,q.q_name,lt.lt_feedback_answer,c.consumer_ivrs,c.consumer_mobile,c.consumer_name,c.consumer_meter_number,lm.loc_region_name,lm.loc_circle_name,lm.loc_division_name,lm.loc_sub_division_name,lm.loc_dc_name,lm.loc_district_name
 FROM lt_consumer_feedback as lt
INNER JOIN lt_consumer_feedback_questions as q ON q.q_id = lt.lt_feedback_question_id       
INNER JOIN consumer as c ON c.consumer_id=lt.lt_feedback_consumer_id
INNER JOIN locations_master as lm ON lm.loc_id = c.consumer_location_id
 WHERE lt.lt_feedback_answer='नहीं'


---------------------------------------------------

SELECT co.* 
FROM complaints as co
INNER JOIN consumer as c ON c.consumer_id = co.complaints_consumer_id
where c.HV_cons!=0 AND date(co.complaints_created_date)>='2019-11-01' AND date(co.complaints_created_date)<='2019-11-24' 
*******************************************************************************************************************************************
SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id WHERE a.date >='2019-11-01' AND a.date <= '2019-11-26' AND YEAR(a.date)= YEAR(CURRENT_DATE()) AND DATEDIFF(CURRENT_DATE(),ag.DOJ) >=91 

CID=796630
210901
Magnum Group
Neha
Ahirwar
MSD19734
0
0
NULL
0
NULL
MSD19734

SELECT 
    MONTH(created_datetime) AS month, 
    WEEK(created_datetime) AS week, 
    DATE(created_datetime) AS date,
    COUNT(id) AS count
 FROM  users 
 WHERE date(created_datetime) BETWEEN '2017-05-01' AND '2017-12-31'
 GROUP BY month
 ORDER BY created_datetime


SELECT MONTH(a.date) AS monthof,WEEK(a.date)as week,date(a.date) AS dateof,COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id WHERE a.date >='2019-11-01' AND a.date <= '2019-12-02' GROUP BY week ORDER BY a.date

CEIL((SUM(score)/SUM(score_able))*100) as av 



SELECT cm.category_main_name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score) as tscore,SUM(score_able) as tscore_able, CEIL((SUM(score)/SUM(score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN category_main as cm ON cm.category_main_id =ar.category_main_id WHERE a.date >='2019-11-01' AND a.date <= '2019-11-30' GROUP BY ar.category_main_id


SELECT `a`.`id`, `a`.`audit_time`, `ed`.`msd_ID` as `empMSDID`, `ed`.`emp_name` as `audit_by`, `ag`.`emp_id`, `ag`.`name`, `e`.`emp_name` as `employee_id_TL`, `ee`.`emp_name` as `employee_id_AM`, `ag`.`DOJ`, DATEDIFF(a.date, ag.DOJ) as AON, `ag`.`process`, `ar`.`Call_Date`, `ar`.`Time_Of_Call`, `ar`.`Calling_Number`, `ar`.`CLI_Number`, `ar`.`Call_Dur_Min`, `ar`.`Call_Dur_Sec`, `ar`.`QME_Remarks`, `a`.`date`, `cm`.`category_main_name`, `cs`.`category_sub_name`, `cm_crr`.`category_main_name` as `category_main_id_crr`, `cs_crr`.`category_sub_name` as `category_sub_id_crr`, `ar`.`Consumers_Concern`, `ar`.`r_g_y_a`, `ar`.`QME_Remarks`, `ar`.`p_s_if_a`, `ar`.`t_d_b_a`, `ar`.`c_t_a_p_p`, `ar`.`Fatal_Reason`, IF(ar.Opening = "0", "GREEN", ar.Opening) as Opening, IF(ar.ActiveListening = "0", "GREEN", ar.ActiveListening) as ActiveListening, IF(ar.Probing = "0", "GREEN", ar.Probing) as Probing, IF(ar.Customer_Engagement = "0", "GREEN", ar.Customer_Engagement) as Customer_Engagement, IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required, IF(ar.Understanding = "0", "GREEN", ar.Understanding) as Understanding, IF(ar.Professionalism = "0", "GREEN", ar.Professionalism) as Professionalism, IF(ar.Politeness = "0", "GREEN", ar.Politeness) as Politeness, IF(ar.Hold_Procedure = "0", "GREEN", ar.Hold_Procedure) as Hold_Procedure, IF(ar.Closing = "0", "GREEN", ar.Closing) as Closing, IF(ar.Opening = "0", 10, 0) as OpeningScored, IF(ar.ActiveListening = "0", 6, 0) as ActiveListeningScored, IF(ar.Probing = "0", 10, 0) as ProbingScored, IF(ar.Customer_Engagement = "0", 6, 0) as Customer_EngagementScored, IF(ar.Empathy_where_required = "0", 5, 0) as Empathy_where_requiredScored, IF(ar.Understanding = "0", 6, 0) as UnderstandingScored, IF(ar.Professionalism = "0", 10, 0) as ProfessionalismScored, IF(ar.Politeness = "0", 10, 0) as PolitenessScored, IF(ar.Hold_Procedure = "0", 5, 0) as Hold_ProcedureScored, IF(ar.Closing = "0", 7, 0) as ClosingScored, IF(ar.Closing = "0", 7, 0) as ClosingScored, `ar`.`Correct_smaller`, `ar`.`Accurate_Complete`, `ar`.`Fatal_Reason`, `a`.`score`, `sf`.`name` as `shift_name`, `ct`.`name` as `call_type` FROM `audit` as `a` JOIN `employee` as `e` ON `e`.`emp_id` = `a`.`employee_id_TL` JOIN `employee` as `ee` ON `ee`.`emp_id` = `a`.`employee_id_AM` JOIN `employee` as `ed` ON `ed`.`emp_id` = `a`.`audit_by` JOIN `agents` as `ag` ON `ag`.`id` = `a`.`agents_id` JOIN `shifts` as `sf` ON `sf`.`id` = `ag`.`shift_id` JOIN `audit_remarks` as `ar` ON `ar`.`audit_id` = `a`.`id` JOIN `category_main` as `cm` ON `cm`.`category_main_id` = `ar`.`category_main_id` JOIN `category_sub` as `cs` ON `cs`.`category_sub_id` = `ar`.`category_sub_id` JOIN `category_main` as `cm_crr` ON `cm_crr`.`category_main_id` = `ar`.`category_main_id_crr` JOIN `category_sub` as `cs_crr` ON `cs_crr`.`category_sub_id` = `ar`.`category_sub_id_crr` JOIN `call_types` as `ct` ON `ct`.`id` = `ar`.`Call_Type` WHERE `a`.`agents_id` = '2' AND `a`.`date` >= '2019-12-01' AND `a`.`date` <= '2019-12-05'


SELECT * FROM `complaints` WHERE `complaints`.`complaints_id` != '' AND date(`complaints`.`complaints_created_date`) >= '2019-12-01 0:00 ' AND date(`complaints`.`complaints_created_date`) <= ' 2019-12-06 23:59' ORDER BY `complaints_id`

SELECT *
FROM `complaints`
WHERE `complaints`.`complaints_id` != '' AND date(complaints.complaints_created_date) >= '2019-12-01 ' AND date(complaints.complaints_created_date) <= ' 2019-12-06'
ORDER BY `complaints_id` DESC


*****************************************

date_of, Inbound_StaffedDuration,Inbound_ReadyDuration,Inbound_BreakDuration,Inbound_IdleTime,Inbound_HoldTime,Inbound_NumberOfcallsOffered,Inbound_NumberofCallanswered,Inbound_AbandonedPer,Inbound_AHT,Outbound_StaffedDuration,Outbound_ReadyDuration,Outbound_BreakDuration,Outbound_IdleTime,Outbound_HoldTime,Outbound_NumberOfCallsDial,Outbound_NumberofCallsconnected,Outbound_ConnectivityPer,Outbound_AHT
------
SELECT u.users_first_name,u.users_last_name,u.users_mobile,d.division_name,dc.distributed_center_name,u.users_type
FROM
 users as u
 INNER JOIN division as d ON d.division_id=u.users_division_id
 INNER JOIN distributed_center as dc ON dc.distributed_center_id=u.users_distributed_center_id
 WHERE u.users_type!=1
