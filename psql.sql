-- mysql functions
DELIMITER $$
CREATE FUNCTION calcMarksLoss(scoreable INT, score int) RETURNS int
BEGIN
  DECLARE z int;
  SET z = scoreable-score;
  RETURN z;
END$$
DELIMITER ;


-- mysql PROCEDURE

DELIMITER $$
CREATE PROCEDURE showAudits()
BEGIN
  SELECT id,score_able,score FROM audit;
END$$
DELIMITER ;

CALL showAudits() \G

SHOW PROCEDURE STATUS WHERE db = 'audit_soft';
DROP PROCEDURE IF EXISTS showAudits;

ALTER TABLE agent_performance DROP INDEX date_of;


-----------------------------------------------------------
SELECT
count(*) as records,
e.msd_ID as msd,e.emp_name as am,
ap.date_of,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_StaffedDuration))),'%H:%i') as Inbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_ReadyDuration))),'%H:%i') as Inbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_BreakDuration))),'%H:%i') as Inbound_BreakDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_IdleTime))),'%H:%i') as Inbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_HoldTime))),'%H:%i') as Inbound_HoldTime,
FORMAT(AVG(ap.Inbound_NumberOfcallsOffered),2) as Inbound_NumberOfcallsOffered,
FORMAT(AVG(ap.Inbound_NumberofCallanswered),2) as Inbound_NumberofCallanswered,
FORMAT(AVG(ap.Inbound_AbandonedPer),2) as Inbound_AbandonedPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Inbound_AHT))),'%H:%i') as Inbound_AHT,


TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_StaffedDuration))),'%H:%i') as Outbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_ReadyDuration))),'%H:%i') as Outbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_BreakDuration))),'%H:%i') as Outbound_BreakDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_IdleTime))),'%H:%i') as Outbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_HoldTime))),'%H:%i') as Outbound_HoldTime,
FORMAT(AVG(ap.Outbound_NumberOfCallsDial),2) as Outbound_NumberOfCallsDial,
FORMAT(AVG(ap.Outbound_NumberofCallsconnected),2) as Outbound_NumberofCallsconnected,
FORMAT(AVG(ap.Outbound_ConnectivityPer),2) as Outbound_ConnectivityPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(ap.Outbound_AHT))),'%H:%i') as Outbound_AHT

FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_AM
WHERE e.status=1 AND date_of='2020-05-01' GROUP BY e.emp_id
------------------------

SELECT
ap.*,
e.msd_ID as msd,e.emp_name as am,
ap.date_of
FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_AM
WHERE e.status=1 AND Inbound_ReadyDuration >0

------------------------------------------------

SELECT
count(*) as records,
e.msd_ID as msd,e.emp_name as agents_msd,
ap.date_of,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Inbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Inbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Inbound_BreakDuration,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration > 0,ap.Inbound_IdleTime,NULL)),2) as Inbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Inbound_HoldTime,
SUM(ap.Inbound_NumberOfcallsOffered) as Inbound_NumberOfcallsOffered,
SUM(ap.Inbound_NumberofCallanswered) as Inbound_NumberofCallanswered,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AbandonedPer,NULL)),2) as Inbound_AbandonedPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AHT,'00:00:00')))),'%H:%i:%s') as Inbound_AHT,

TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Outbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Outbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Outbound_BreakDuration,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_IdleTime,NULL)),2) as Outbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Outbound_HoldTime,
SUM(ap.Outbound_NumberOfCallsDial) as Outbound_NumberOfCallsDial,
SUM(ap.Outbound_NumberofCallsconnected) as Outbound_NumberofCallsconnected,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ConnectivityPer,NULL)),2) as Outbound_ConnectivityPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_AHT,'00:00:00')))),'%H:%i:%s') as Outbound_AHT

FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_AM
WHERE $where GROUP BY e.emp_id
----------------------------------
SELECT count(*) as records, a.emp_id as msd,a.name as agents_msd, ap.date_of, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_StaffedDuration,NULL)))),'%H:%i:%s') as Inbound_StaffedDuration, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_ReadyDuration,NULL)))),'%H:%i:%s') as Inbound_ReadyDuration, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_BreakDuration,NULL)))),'%H:%i:%s') as Inbound_BreakDuration, FORMAT(AVG(IF(ap.Inbound_ReadyDuration > 0,ap.Inbound_IdleTime,NULL)),2) as Inbound_IdleTime, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_HoldTime,NULL)))),'%H:%i:%s') as Inbound_HoldTime, SUM(ap.Inbound_NumberOfcallsOffered) as Inbound_NumberOfcallsOffered, SUM(ap.Inbound_NumberofCallanswered) as Inbound_NumberofCallanswered, FORMAT(AVG(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AbandonedPer,NULL)),2) as Inbound_AbandonedPer, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AHT,NULL)))),'%H:%i:%s') as Inbound_AHT, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_StaffedDuration,NULL)))),'%H:%i:%s') as Outbound_StaffedDuration, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ReadyDuration,NULL)))),'%H:%i:%s') as Outbound_ReadyDuration, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_BreakDuration,NULL)))),'%H:%i:%s') as Outbound_BreakDuration, FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_IdleTime,NULL)),2) as Outbound_IdleTime, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_HoldTime,NULL)))),'%H:%i:%s') as Outbound_HoldTime, SUM(ap.Outbound_NumberOfCallsDial) as Outbound_NumberOfCallsDial, SUM(ap.Outbound_NumberofCallsconnected) as Outbound_NumberofCallsconnected, FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ConnectivityPer,NULL)),2) as Outbound_ConnectivityPer, TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_AHT,NULL)))),'%H:%i:%s') as Outbound_AHT FROM agent_performance as ap INNER JOIN agents as a ON a.id=ap.agents_id INNER JOIN employee as e ON e.emp_id=a.employee_id_TL WHERE e.status=1 AND a.employee_id_TL = 18 AND ap.date_of ='2020-06-04' AND ap.Inbound_ReadyDuration>0 GROUP BY a.id

SELECT  a.emp_id as msd,a.name as agents_msd, ap.date_of,
FROM agent_performance as ap INNER JOIN agents as a ON a.id=ap.agents_id INNER JOIN employee as e ON e.emp_id=a.employee_id_TL WHERE e.status=1 AND a.employee_id_TL = 18 AND ap.date_of ='2020-06-04';
-------------------------------------------------------------

--Proceduress===================

 -------------------------------------------------
DELIMITER $$
CREATE PROCEDURE agentsRec()
BEGIN
SELECT a.id,a.emp_id,a.name,e.emp_name,ee.emp_name
 FROM agents as a
 INNER JOIN employee as e ON e.emp_id=a.employee_id_TL
 INNER JOIN employee as ee ON ee.emp_id=a.employee_id_AM
WHERE a.id=5;
END $$
DELIMITER ;

call agentsRec();$$
-----------------without parameter---------------------------------
delimiter //
create procedure agentsRecords() 
 begin 
  SELECT a.id,a.emp_id,a.name,e.emp_name,ee.emp_name
 FROM agents as a
 INNER JOIN employee as e ON e.emp_id=a.employee_id_TL
 INNER JOIN employee as ee ON ee.emp_id=a.employee_id_AM
WHERE a.id=5;
 end //
delimiter ;
call agentsRecords();
--------------------With parameter()------------------------------
delimiter //
create procedure agentsRecordsId(IN agent_id integer) 
 begin 
  SELECT a.id,a.emp_id,a.name,e.emp_name,ee.emp_name
 FROM agents as a
 INNER JOIN employee as e ON e.emp_id=a.employee_id_TL
 INNER JOIN employee as ee ON ee.emp_id=a.employee_id_AM
WHERE a.id=agent_id;
 end //
delimiter ;
call agentsRecordsId(8);
--------------------INOUT-------------------------------------------------------

delimiter //
create procedure agents_gender(INOUT mfgender integer, IN emp_gender integer)  
                      begin 
                      select COUNT(gender) 
                         INTO mfgender FROM agents where gender = emp_gender;   
                      end //
delimiter ;
call agents_gender(@M,1);
select @M;
-------------------------------------------------------------
https://www.w3resource.com/mysql/mysql-triggers.php
-----------------------------------------------------------
delimiter //
create trigger before_users_delete before delete 
 ON users for each ROW
 begin
INSERT INTO `users_logs` (first_name,last_name,email,password,phone,created,modified,status) VALUES (old.first_name,old.last_name,old.email,old.password,old.phone,old.created,old.modified,old.status);
 end//
delimiter ;

-------------------------insert trigger--------------------------------------

delimiter //
create trigger after_users_insert after insert 
 ON users for each ROW
 begin
INSERT INTO `users_logs_insert` (users_id,first_name,last_name,email,password,phone,created,modified,status) VALUES (new.id,new.first_name,new.last_name,new.email,new.password,new.phone,new.created,new.modified,new.status);
 end//
delimiter ;
----------------------before update--------------------------------------------------------
delimiter //
create trigger before_users_update after update 
 ON users for each ROW
 begin
INSERT INTO `users_logs_insert`(users_id,first_name,last_name,email,password,phone,created,modified,status) VALUES (old.users_id,old.first_name,old.last_name,old.email,old.password,old.phone,old.created,old.modified,old.status);
 end//
delimiter ;

-------------------------------- Create Multiple Triggers | Follows, Precedes Keyword--------------------------------------------------------------
delimiter //
create trigger before_users_log_update after update 
 ON users for each ROW follows before_users_update
 begin
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(old.users_id,USER());
 end//
delimiter ;

-----------------------------------------------------------------
--MySQL Event Tutorials for Beginners #1-------------
--1. ON wondows task schudler
--2. on Linus cronjob

--use of events
--1. Optmizing database tables, cleaning up logs,archiving/ backups,or generating comples reprts in off time
--open phpmyadmin
--1. set global event_scheduler = on  
--show processlist

--One time events
--
CREEATE EVENT IF NOT EXISTS event_01
ON SCHEDULE AT current_timestamp
DO 
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(111,''eventexecute);

------------in 20 second---delted after run-------------------
CREATE EVENT IF NOT EXISTS event_02
ON SCHEDULE AT current_timestamp + INTERVAL 20 SECOND
DO 
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(111,'eventexecut 20 sec');
------------in 40 second---Preserved after run-------------------
CREATE EVENT IF NOT EXISTS event_03
ON SCHEDULE AT current_timestamp + INTERVAL 40 SECOND
DO 
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(111,'eventexecut_3 not preserve 20 sec');
------------------------------------------------------------------------------------------
CREATE EVENT IF NOT EXISTS event_04
ON SCHEDULE AT current_timestamp + INTERVAL 50 SECOND
ON COMPLETION PRESERVE
DO 
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(111,'eventexecut_4 preserve 20 sec');

--Recurring events
CREATE EVENT IF NOT EXISTS event_05
ON SCHEDULE every 1 SECOND
STARTS current_timestamp
ENDS current_timestamp + INTERVAL 50 SECOND
ON COMPLETION PRESERVE
DO 
INSERT INTO `users_logs_insert`(users_id,first_name) VALUES(111,'eventexecut_4 preserve 20 sec');

==========================================================================================================
Rest API JWT token
==========================================================================================================

------------------START TRANSACTION;--------------------------------------------------------------------------
START TRANSACTION;
DELETE FROM `process_summery_hourly_outbound` WHERE DATE(interval_start)='2020-11-18';
SELECT * FROM `process_summery_hourly_outbound` WHERE DATE(interval_start)='2020-11-18';
COMMIT;
ROLLBACK;


SELECT * FROM `complaints` WHERE `complaints`.`complaints_main_category` IN (12,13,15,16,18,19,23) AND `complaints`.`complaints_consumer_gr_no` IN ('JFE16','JFE50','JFE74') AND `complaints`.`complaints_consumer_loc_no` IN (1445005) AND `complaints`.`complaints_dc` IN (479,453,477,482) AND `complaints`.`complaints_id` != '' and `complaints`.`complaints_current_status` IN (1,3,5) ORDER BY `complaints_id` DESC

