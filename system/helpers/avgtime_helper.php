<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function avg_of_times($arraydata){
 $times =$arraydata;

$totaltime = 0;
foreach($times as $time){
        $timestamp = strtotime($time);
        $totaltime += $timestamp;
}

$average_time = ($totaltime/count($times));

echo date('H:i:s',$average_time);
    
}

function avg_of_decimal($arraydata){
 $times =$arraydata;

$totaltime = 0;
foreach($times as $time){
        $timestamp = $time;
        $totaltime += $timestamp;
}

$average_time = ($totaltime/count($times));

echo number_format($average_time,2).'%';
    
}

?>