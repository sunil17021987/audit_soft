<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Superwiser_report_model');
        $this->load->model('LoginAuth_model');
        $this->load->model('Common_model');
        $this->load->library('mylibrary');
        //ini_set('memory_limit', '-1');
        $this->load->library("Excel");

        if ($this->LoginAuth_model->loggedin() == FALSE) {
            redirect(base_url());
        }
    }


    public function index() {

        $this->load->view('superwiser/components/header');
        $data['main_category'] = $this->Common_model->loop("category_main",array("category_main_status="=>1)); 
        $condition = "";

        $data['circle_list']=$this->Common_model->loop("circle",array("circle_status="=>1));

        $data['category_wise_complain']=$this->Superwiser_report_model->get_category_wise_complaint($condition);
         $cond ="";
        $data['region_wise_complain']=$this->Superwiser_report_model->get_region_wise_complaint();


           if(isset($_GET['category']) && $_GET['category']!='') {
             $cond = " AND complaints.complaints_main_category='".$_GET['category']."'";
             $data['category_sub'] = $this->Common_model->loop("category_sub",array("category_sub_status="=>1,"category_sub_main_id="=>$_GET['category'])); 

            }

          if(isset($_GET['subcategory']) && $_GET['subcategory']!='') {

              $cond .= " AND complaints.complaints_sub_category='".$_GET['subcategory']."'";
          }
          if(isset($_GET['date']) && $_GET['date']!='') {
                   $dtr = explode("To",$_GET['date']);
             $cond .= " AND DATE(complaints.complaints_created_date) between '$dtr[0]' and '$dtr[1]'";

              }

       
             $data['circle_wise_complain']=$this->Superwiser_report_model->get_circle_wise_complaint($cond);

        $this->load->view('superwiser/report/report', $data);

    }

function month_wise_report(){
         $condition = $this->input->post('condition');     
         $cat = $this->input->post('mcategory'); 
         $data['month_wise_complain']=$this->Superwiser_report_model->get_month_wise_complaint($condition,$cat);
         $this->load->view('superwiser/report/month_wise_report', $data);

    }


   

    function circle_wise_category_report(){
        $condition = $this->input->post('condition');
       
        $data['category_wise_complain']=$this->Superwiser_report_model->get_category_wise_complaint($condition);
         $this->load->view('superwiser/report/cat_report', $data);

    }


    public function add_con_consumer() {
        $data['main_category'] = $this->Superwiser_report_model->get_main_category();
        $data['district'] = $this->Superwiser_report_model->get_district_by_circle();
        $this->load->view('agent/complain/add_complain_non_consumer', $data);
    }

    public function get_foc_and_dc_by_grampanchayat() {
        $gram_panchayat = $this->input->post('grampanchayat');
        $data['main_category'] = $this->Superwiser_report_model->get_foc_by_grampanchayat();
        $data['main_category'] = $this->Superwiser_report_model->get_main_category();
    }

    public function get_sub_category() {
        $main_category_id = $this->input->post('main_category_id');

        $data['sub_category'] = $this->Superwiser_report_model->get_sub_category($main_category_id);

        echo json_encode($data);
    }

    public function save_complain() {
        
        $foc = null;
        $this->Superwiser_report_model->_table_name = "complaints"; // table name
        $this->Superwiser_report_model->_primary_key = "complaints_id"; // $id
        $consumer_id = $this->input->post('complaints_consumer_id');
        $data = $this->Superwiser_report_model->array_from_post(array('complaints_lt_ht_type', 'complaints_consumer_id', 'complaints_consumer_name', 'complaints_consumer_mobile', 'complaints_ivrs', 'complaints_region', 'complaints_circle', 'complaints_division', 'complaints_sub_division', 'complaints_dc', 'complaints_urban_rular', 'complaints_district', 'complaints_main_category', 'complaints_sub_category', 'complaints_remark'));
        $data['complaints_created_by'] = $this->session->userdata('users_id');
        $data['complaints_type'] = 1;
        $type = $this->input->post('complaints_urban_rular');
        if ($type == 1) {
            $data['complaints_city'] = $this->input->post('complaints_city');
            $data['complaints_area'] = $this->input->post('complaints_area');
            $foc = $this->Superwiser_report_model->get_foc_by_area($data['complaints_area']);
            $address = $data['complaints_city'] . ', ' . $data['complaints_area'];
        } else {
            $data['complaints_block'] = $this->input->post('complaints_block');
            $data['complaints_panchayat'] = $this->input->post('complaints_panchayat');
            $address = $data['complaints_block'] . ', ' . $data['complaints_panchayat'];
            $foc = $this->Superwiser_report_model->get_foc_by_grampanchayat($data['complaints_panchayat']);
        }
        $new_complain_id = $this->Superwiser_report_model->save($data);
        $this->session->set_flashdata('complain_id', $new_complain_id);
        if (!empty($foc)) {
            $this->Superwiser_report_model->_table_name = "complaints"; // table name
            $this->Superwiser_report_model->_primary_key = "complaints_id"; // $id
            $assign['complaints_assign_officer_id'] = $foc->foc_id;
            $this->Superwiser_report_model->save($assign, $new_complain_id);
            $consumer_name = $this->input->post('complaints_consumer_name');
            $consumer_mobile = $this->input->post('complaints_consumer_mobile');
            $msg = urlencode("आप के लिए नई शिकायत आई है शिकायत क्रमांक-$new_complain_id है, उपभोक्ता का नाम- $consumer_name , मोबाइल-$consumer_mobile धन्यवाद।");
            $this->mylibrary->send_sms_to_foc($foc->foc_mobile, $new_complain_id, $msg);
            $this->mylibrary->send_sms_to_caller($consumer_mobile, $new_complain_id);
        }

        redirect("agent/Consumer/consumer_info/$consumer_id");
    }

    public function get_foc_je_by_grampanchayat() {
        $gram_panchayat_id = $this->input->post('gram_panchayat_id');
        $data['foc_list'] = $this->Superwiser_report_model->get_foc_by_grampanchayat($gram_panchayat_id);
        $data['je_list'] = $this->Superwiser_report_model->get_je_by_dc($data['foc_list']->foc_dc);
        echo json_encode($data);
    }

    public function get_foc_je_by_area() {
        $area_id = $this->input->post('area_id');
        $data['foc_list'] = $this->Superwiser_report_model->get_foc_by_area($area_id);
        $data['je_list'] = $this->Superwiser_report_model->get_je_by_dc($data['foc_list']->foc_dc);
        echo json_encode($data);
    }

    public function data_table_complain_list_condition() {
        $condition = $this->input->post('condition');
        $list = $this->Superwiser_report_model->get_datatables($condition);
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $v_list) {
            $no++;
            $row = array();
            $row[] = $v_list->complaints_id;
            $row[] = "<span style='color:green'><b>I: &nbsp;&nbsp;</b></span> " . $v_list->complaints_consumer_mobile . "<br> <span style='color:#01c4ff'><b>M:</b></span> " . $v_list->complaints_ivrs;

            $row[] = $v_list->category_main_name;
            $row[] = $v_list->category_sub_name;
            if ($v_list->complaints_sub_category == 17) {
                $row[] = "Not Assigned";
            } else {
                $row[] = $this->Superwiser_report_model->getvalue('users_first_name', "users", array("users_id=" => $v_list->complaints_assign_officer_id)) . ' ' . $this->Superwiser_report_model->getvalue('users_last_name', "users", array("users_id=" => $v_list->complaints_assign_officer_id)) . " <b>[</b>" . $this->Superwiser_report_model->getvalue("users_mobile", "users", array("users_id=" => $v_list->complaints_assign_officer_id)) . "<b>]</b>";
            }
            $row[] = $v_list->complaints_created_date;
            $row[] = $v_list->complaints_last_updated_date;
            if ($v_list->complaints_current_status == 1) {
                $row[] = "<span class='tag tag-red'>Open</span>";
            } else {
                if ($v_list->complaints_current_status == 2) {
                    $row[] = "<span class='tag tag-yellow'>In Progress</span>";
                } else {
                    if ($v_list->complaints_current_status == 3) {
                        $row[] = "<span class='tag tag-orange'>Attended</span>";
                    } else {
                        if ($v_list->complaints_current_status == 4) {
                            $row[] = "<span class='tag tag-green'>Closed</span>";
                        } else {
                            if ($v_list->complaints_current_status == 5) {
                                $row[] = "<span class='tag tag-gray-dark'>Re-open</span>";
                            } else {
                                if ($v_list->complaints_current_status == 6) {
                                    $row[] = "<span class='tag tag-gray-dark'>Force Closed</span>";
                                }
                            }
                        }
                    }
                }
            }
            $row[] = '<a href="javascript:void(0)" onclick="complaints_details(' . $v_list->complaints_id . ')" title="Complaints Details" class="btn btn-icon btn-sm btn-primary btn-secondary text-black" style="background-color:#000;color:#fff">
                                                       <i class="fa fa-eye"></i></a>' . " <a type='button' title='History' class='btn btn-icon btn-sm btn-primary btn-secondary' onclick='load_log_list(" . $v_list->complaints_id . ");' style='color:#fff;background-color:#ff5600'><i class='fa fa-history'></i></a>" . ' <a href="javascript:void(0)" onclick="complaints_FollowUp(' . $v_list->complaints_id . ')" title="Complaints FollowUp" class="btn btn-sm btn-icon btn-primary btn-secondary" style="color:#fff;background-color:#2babab"><i class="fa fa-phone-square"></a>';
//            $row[] = "<a href='" . base_url() . "index.php/agent/FollowUp/view/" . $v_list->complaints_id . "' class='btn btn-pill btn-info' style='color:#FFFFFF'>View</a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Superwiser_report_model->count_all(),
            "recordsFiltered" => $this->Superwiser_report_model->count_filtered($condition),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

//        VIEW COMPLAIN
    public function view($complain_id) {
        $data['info'] = $this->Superwiser_report_model->get_complain_details($complain_id);
        $data['history'] = $this->Superwiser_report_model->get_complain_history($complain_id);
        $data['questions'] = $this->Superwiser_report_model->get_complain_questions($complain_id);

        // echo "<pre>";
        //  print_r($data['info']);
        // die;
        $this->load->view('agent/followup/view_complain', $data);
    }

//        END VIEW COMPLAIN
    //        ACTION ON COMPLAIN
    // public function action($complain_id) {
    public function action() {
        $action = $this->input->post('action');
        $remark = $this->input->post('remark');
        $complaint_id = $this->input->post('complaints_id');
        $data = array(
            "complaints_remark" => $remark,
            "complaints_current_status" => $action
        );
        $this->Superwiser_report_model->_table_name = "complaints"; // table name
        $this->Superwiser_report_model->_primary_key = "complaints_id"; // $id
        $data['complaints_current_status'] = $action;
        $this->Common_model->update("complaints", $data, array("complaints_id=" => $complaint_id));

        $this->Superwiser_report_model->_table_name = "complaints_history"; // table name
        $this->Superwiser_report_model->_primary_key = "complaints_history_id"; // $id
        $histroy['complaints_history_complaint_id'] = $complaint_id;
        $histroy['complaints_history_type'] = "Action And Remark";
        $histroy['complaints_history_followup_by'] = "Agent";
        $histroy['complaints_history_remark'] = $remark;
        $histroy['complaints_history_status'] = $action;
        $this->Common_model->insert("complaints_history", $histroy);
        $this->session->set_flashdata('actmessage', 'Complaint Status Update Successfully');

        redirect("agent/FollowUp/view/" . $complaint_id);
    }

    public function get_comp_details($comp_id) {
        $consumer_id = $this->Common_model->getvalue('complaints_consumer_id', "complaints", "complaints_id='$comp_id'");


        if ($consumer_id != 0) {

            $query = $this->db->query("SELECT * FROM `complaints` INNER JOIN category_main ON category_main.category_main_id=complaints_main_category LEFT JOIN consumer ON consumer.consumer_id=complaints_consumer_id LEFT JOIN category_sub ON category_sub.category_sub_id=complaints_sub_category LEFT JOIN category_sub_sub ON category_sub_sub.category_sub_sub_id=complaints_sub_sub_category INNER JOIN region ON region.region_id=complaints_region INNER JOIN circle ON circle.circle_id=complaints_circle LEFT JOIN division ON division.division_id=complaints_division LEFT JOIN sub_division ON sub_division.sub_division_id=complaints_sub_division LEFT JOIN distributed_center ON distributed_center.distributed_center_id=complaints_dc LEFT JOIN district ON district.district_id=consumer_district LEFT JOIN city ON city.city_id=consumer_city  LEFT JOIN block ON block.block_id=consumer_block LEFT JOIN gram_panchayat ON gram_panchayat.gram_panchayat_id=consumer_grampanchyat LEFT JOIN village ON village.village_id=consumer_village  LEFT JOIN area ON area.area_id=consumer_area LEFT JOIN colony ON colony.colony_id=consumer_colony LEFT JOIN foc_center ON foc_center.foc_center_dc_id=consumer_dc   WHERE complaints_id=$comp_id");
        } else {
            $query = $this->db->query("SELECT * FROM `complaints` INNER JOIN category_main ON category_main.category_main_id=complaints_main_category  LEFT JOIN category_sub ON category_sub.category_sub_id=complaints_sub_category LEFT JOIN category_sub_sub ON category_sub_sub.category_sub_sub_id=complaints_sub_sub_category INNER JOIN region ON region.region_id=complaints_region INNER JOIN circle ON circle.circle_id=complaints_circle LEFT JOIN division ON division.division_id=complaints_division LEFT JOIN sub_division ON sub_division.sub_division_id=complaints_sub_division LEFT JOIN distributed_center ON distributed_center.distributed_center_id=complaints_dc LEFT JOIN district ON district.district_id=complaints_district LEFT JOIN city ON city.city_id=complaints_city  LEFT JOIN block ON block.block_id=complaints_block LEFT JOIN gram_panchayat ON gram_panchayat.gram_panchayat_id=complaints_panchayat LEFT JOIN village ON village.village_id=complaints_village  LEFT JOIN area ON area.area_id=complaints_area LEFT JOIN colony ON colony.colony_id=complaints_colony LEFT JOIN foc_center ON foc_center.foc_center_dc_id=complaints_dc   WHERE complaints_id=$comp_id");
        }
        $data['info'] = $query->row();

        $data['questions'] = $this->Superwiser_report_model->get_complain_questions($comp_id);

        $this->load->view('agent/complain/complaint_details', $data);
    }

    function get_comp_FollowUp($comp_id) {
        $data['complaint_id'] = $comp_id;
        $this->load->view('agent/followup/complaint_followup', $data);
    }

    function download_reports(){

        $this->load->view('superwiser/components/header');
        $this->load->view('special/download_reports');
        $this->load->view('superwiser/components/footer');

    }

      function download_whatsapp_report($from_date,$to_date){
          @ini_set('memory_limit', '-1');
          @ini_set('max_execution_time', 0);


    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $object->getActiveSheet()->setTitle('Whatsapp Report');

    $table_columns = array("ID","Name","MobileNumber","Whatsapp Mobile","IVRS","Address","Location","Feedback","Agent Name","Agent MSD","Date");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:K1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('ff5600');

      $column++;
    }


     $data = $this->Common_model->reports("DATE(updated_date) between '".$from_date."' and '".$to_date."' ");
    
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->whatsapp_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->address);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->location);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->feedback);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->updated_date );
    

      $excel_row++;
      $k++;
    }
    $name = "Whatsapp_".date("Y-m-d").".xlsx";
    ob_clean();
        $objWriter = new PHPExcel_Writer_Excel2007($object);

   // $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


  function download_pan_report($from_date,$to_date){
      @ini_set('memory_limit', '-1');
      @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Name","MobileNumber","IVRS","Address","TDS Amount","Pan Card","Feedback","Agent Name","Agent MSD","Date");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:K1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('ff5600');
      $column++;
    }


     $data = $this->Common_model->pan_reports("DATE(updated_date) between '$from_date' and '$to_date' ");
     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->pan_card_cons_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->pan_card_MOB_NO);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->pan_card_consumer_no);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pan_card_LOC_DESC_SUBDN);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->pan_card_TOT_SD_HELD);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->pancard_no);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->feedback);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->updated_date );
    

      $excel_row++;
      $k++;
    }
    $name = "PanCard_".date("Y-m-d").".xls";
    ob_clean();
    $objWriter = new PHPExcel_Writer_Excel2007($object);
   // $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


     function download_pensionar_report($from_date,$to_date){
      @ini_set('memory_limit', '-1');
      @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Name","MobileNumber","Question","Answer","Agent Name","Agent MSD","Date");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:K1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->pensionar_reports("DATE(pensionar_feedback.created_date) between '$from_date' and '$to_date' ");
     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->pensionar_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->pensionar_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->pq_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pensionar_feedback_option);

      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->created_date );
    

      $excel_row++;
      $k++;
    }
    $name = "Pensionar_".date("Y-m-d").".xls";
    ob_clean();
    $objWriter = new PHPExcel_Writer_Excel2007($object);
   // $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }




    function all_reports(){

        $date1 = $this->input->post("from_date");
        $date2 = $this->input->post("to_date");


       if($this->input->post('campaign')=='1'){
       
            $this->download_whatsapp_report($date1,$date2); //call function
       }
       else if($this->input->post('campaign')=='2'){
            $this->download_pan_report($date1,$date2);
       }
       else if($this->input->post('campaign')=='3'){
            $this->download_pensionar_report($date1,$date2);
       }
       else if($this->input->post('campaign')=='4'){
            $this->download_prs_report($date1,$date2);
       }
        else if($this->input->post('campaign')=='5'){
            $this->download_nonivrs_report($date1,$date2);
       }
        else if($this->input->post('campaign')=='6'){
            
            $this->download_htprs_report($date1,$date2);
       }


    }

    function complaints_reports(){
        $data['categories'] = $this->Common_model->loop("category_main",array("merge_show="=>1)); 
        $this->load->view('superwiser/components/header');
        $this->load->view('special/download_complaints_report',$data);
        $this->load->view('superwiser/components/footer');

    }

    function download_complaints_reports(){
        $date1 = $this->input->post("from_date");
        $date2 = $this->input->post("to_date");
        $status = $this->input->post("status");
       

       if($this->input->post('complaint_type')=='1'){
         $category = $this->input->post("category_type");
         $status = $this->input->post("status_type");

        $this->download_consumer_complaint_report($date1,$date2,$category,$status); //call function
       }
       else if($this->input->post('complaint_type')=='2'){
            $dtr_status = $this->input->post("dtr_status");

            $this->download_dtr_complaint_report($date1,$date2,$dtr_status);
       }
       else if($this->input->post('complaint_type')=='3'){
           $this->download_emp_complaint_report($date1,$date2);
       }
       else if($this->input->post('complaint_type')=='4'){
            $this->download_contractor_complaint_report($date1,$date2);
       }
       else if($this->input->post('complaint_type')=='5'){
           $this->download_corruption_complaint_report($date1,$date2);
       }
       else if($this->input->post('complaint_type')=='6'){
           $this->download_information_report($date1,$date2);
       }
       else if($this->input->post('complaint_type')=='7'){
           $this->download_agent_history_report($date1,$date2);
       }
     else if($this->input->post('complaint_type')=='8'){
           $this->download_foc_complaint_report($date1,$date2);
       } 
     else if($this->input->post('complaint_type')=='9'){
           $this->download_web_complaint_report($date1,$date2);
       } 
     else if($this->input->post('complaint_type')=='10'){
           $this->download_lt_survey_report($date1,$date2);
       } 

     else if($this->input->post('complaint_type')=='11'){
           $this->download_onupdate_complaint_report($date1,$date2,$status);
       } 

    }


 function download_onupdate_complaint_report($from_date,$to_date,$status){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("complaints_id","Region","Circle","Division","Subdivision","DC","Consumer Name","Consumer Mobile","Consumer Called Mobile","Consumer IVRS","Category","Subcategory","Created Date","Status","Agent Name","Remark","Came From");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:R1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->complaint_report_on_update("DATE(complaints_history_created_date) between '$from_date' and '$to_date' and complaints_history_status='$status'");
     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
        

         
        $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->complaints_history_users_id));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->complaints_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->complaints_consumer_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->complaints_consumer_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->complaints_called_mobile);

      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->complaints_ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->category_sub_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->complaints_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $cstatus);

      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $created_by->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->complaints_history_remark);
      

      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->complaints_came_from);
      


      $excel_row++;
      $k++;
    }
    $name = "ComplaintsreportUpdate_".date("Y-m-d").".csv";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


     function download_consumer_complaint_report($from_date,$to_date,$category,$status){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Region","Circle","Division","Subdivision","DC","Consumer Name","Consumer Mobile","Consumer IVRS","Category","Subcategory","Assign To","Assign To","Created Date","Status","Agent Name","Agent MSD","Remark","Call Not Pick by Officer","Call not pick by consumer","Reminder","Call not Connect to consumer","Call not Connect to Officer","Came From","Consumer Called Mobile","complaints_type","Bill Month","consumer_type","attended_date");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:R1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }

    $categories = implode(",",$category);
    $status_s = implode(",",$status);

    
     $data = $this->Common_model->consumer_complaint_report("DATE(complaints_created_date) between '$from_date' and '$to_date' AND complaints_main_category IN ($categories) and complaints_current_status IN ($status_s)");
// echo $this->db->last_query();
//     die();
    
     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
        if($row->complaints_main_category==17){
           $lineman = $this->Common_model->selectfetch("lineman_name,lineman_mobile","gang_lineman",array("lineman_gang_id="=>$row->complaints_assign_gang_id));

            $officer = "Gang - ".$lineman->lineman_name;
            $officer_mob = "Gang - ".$lineman->lineman_mobile;
        }
        else{
             if($row->users_type==7)
                $complaint_officer = "JE";
              else if($row->users_type==6)
                $complaint_officer = "AE";
               else if($row->users_type==5)
                $complaint_officer = "EE";
               else if($row->users_type==4)
                $complaint_officer = "SE";
                else if($row->users_type==3)
                $complaint_officer = "CE";

           $off = $this->Common_model->selectfetch("users_first_name,users_last_name,users_mobile","users",array("users_id="=>$row->complaints_assign_officer_id));


             $officer = "officer - ".$off->users_first_name." ".$off->users_last_name;
             $officer_mob = "Officer - ".$off->users_mobile;
        }

        if($row->complaints_current_status==1)
            $status = "Open";
        else if($row->complaints_current_status==3)
            $status = "Attended";
        else if($row->complaints_current_status==4)
            $status = "Close";
        else if($row->complaints_current_status==6)
            $status = "Force Close";
        else if($row->complaints_current_status==5)
            $status = "Reopen";

        $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->complaints_created_by));
       
//        ************************************************
       
        $region_name = $this->Common_model->selectfetch("region_name","region",array("region_id="=>$row->complaints_region));
       $circle_name = $this->Common_model->selectfetch("circle_name","circle",array("circle_id="=>$row->complaints_circle));
        $division_name = $this->Common_model->selectfetch("division_name","division",array("division_id="=>$row->complaints_division));
         $sub_division_name = $this->Common_model->selectfetch("sub_division_name","sub_division",array("sub_division_id="=>$row->complaints_sub_division));
          $distributed_center_name = $this->Common_model->selectfetch("distributed_center_name","distributed_center",array("distributed_center_id="=>$row->complaints_dc));
           $category_main_name = $this->Common_model->selectfetch("category_main_name","category_main",array("category_main_id="=>$row->complaints_main_category));
            $category_sub_name = $this->Common_model->selectfetch("category_sub_name","category_sub",array("category_sub_id="=>$row->complaints_sub_category));
       
//        ************************************************
        
        if($row->complaints_type==1)
          $consumer_type="Ivrs Alloted";
        else if($row->complaints_type==2)
          $consumer_type="Ivrs Not Alloted";


        if($row->complaints_urban_rular==1)
          $consumer_geo_type ="Urban";
        else if($row->complaints_urban_rular==2)
           $consumer_geo_type ="Rural";
         else
           $consumer_geo_type="Not Found";

$dbquery=$this->db->query("SELECT complaints_history_created_date FROM complaints_history WHERE complaints_history_complaint_id=$row->complaints_id AND complaints_history_status=3 ORDER BY complaints_history_id ASC LIMIT 1");
 $complaintshistoryattendeddate=$dbquery->row();
 //echo $this->db->last_query();
// die();  
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->complaints_id);
      
       $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $region_name->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $circle_name->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $division_name->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $sub_division_name->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $distributed_center_name->distributed_center_name);
//      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->region_name);
//      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->circle_name);
//      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->division_name);
//      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->sub_division_name);
//      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->complaints_consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->complaints_consumer_mobile);
     
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->complaints_ivrs);
      
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $category_main_name->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $category_sub_name->category_sub_name);
      
      
      
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $officer);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $officer_mob);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->complaints_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $status);

      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $created_by->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, str_replace("=","-",$row->complaints_remark));
      $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->callnotpickedbyofficer);

      $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->callnotpickedbyconsumer);

      $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row->complaints_reminders);
      $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $row->callnotconnectToconsumer);
      $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row->callnotpickedbyconsumer);

      $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row->complaints_came_from);
       $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row->complaints_called_mobile);
       $object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $consumer_type);
     

      if($row->ccb_tag==1)
        $tag = 'Current Month';
      elseif($row->ccb_tag==2)
        $tag = 'Previous Month';
      else
        $tag = 'NA';


      $object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $tag);
      $object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $consumer_geo_type);
   
  $object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, ($complaintshistoryattendeddate->complaints_history_created_date)?$complaintshistoryattendeddate->complaints_history_created_date:'');
      $excel_row++;
      $k++;
    }
    $name = "Complaintsreport_".date("Y-m-d").".csv";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }



    //// DTR Report

    function download_dtr_complaint_report($from_date,$to_date,$status){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Region","Circle","Division","Subdivision","DC","Consumer Name","Consumer Mobile","Consumer Alt Mobile","Consumer IVRS","Category","Subcategory","Assign To","Assign To","Created Date","Status","reminder","callnotpickbyconsumer","callnotpickbyofficer","Agent Name","Agent MSD","Udistrict","city","Area","colony","Rdistrict","block","Grampanchyat","Village");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }
  $status_s = implode(",",$status);


     $data = $this->Common_model->dtr_complaint_report("DATE(dtr_complain_date) between '$from_date' and '$to_date' and dtr_complain_status IN ($status_s)");
 //  echo $this->db->last_query();
//die();

     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       

        if($row->dtr_complain_status==1)
            $status = "Open";
        else if($row->dtr_complain_status==2)
            $status = "Under Inspection";
        else if($row->dtr_complain_status==3)
            $status = "Changed";
        else if($row->dtr_complain_status==4)
            $status = "Repaired";
        else if($row->dtr_complain_status==5)
            $status = "Wrong Complaint";
        else if($row->dtr_complain_status==6)
            $status = "Close";
        else if($row->dtr_complain_status==11)
            $status = "Force Close";
       else if($row->dtr_complain_status==18)
            $status = "Arrear Pending";

       else if($row->dtr_complain_status==7)
            $status = "Reopen";
        else{
 $status = "NO";

}
        

        $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->dtr_complain_writen_by));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->dtr_complain_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->dtr_complain_consumer_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->dtr_complain_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->dtr_complain_alt_mobile);

      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->dtr_complain_ivrs);

      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->category_sub_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->users_mobile);

      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->dtr_complain_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $status);
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->consumerreminder);
      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->callnotpickedbyconsumer);
      $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->callnotpickedbyofficer);

      $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $created_by->users_name);
    
         
          
        

        
         

    if($row->dtr_complain_location_type==1){
        $urban  = $this->Common_model->selectfetch("loc_district_name,loc_city_name,loc_area_name,loc_colony_name","locations_master",array("loc_colony_id="=>$row->dtr_complain_colony));

         $dit1 = $urban->loc_district_name;
         $city = $urban->loc_city_name;
         $area = $urban->loc_area_name;
         $colony = $urban->loc_colony_name;

     } else {
         $dit1 = "";
         $city = "";
         $area = "";
         $colony = "";

     }

     if($row->dtr_complain_location_type==2){
         $rural  = $this->Common_model->selectfetch("loc_district_name,loc_block_name,loc_gp_name,loc_village_name","locations_master",array("loc_village_id="=>$row->dtr_complain_village));
         $dit2 = $rural->loc_district_name;
         $block = $rural->loc_block_name;
         $gp = $rural->loc_gp_name;
         $village = $rural->loc_village_name;

     } else {
         $dit2 = "";
         $block = "";
         $gp = "";
         $village = "";

     }
        $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $dit1);
        $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $city);
        $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $area);
        $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $colony);

        $object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $dit2);
        $object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $block);
        $object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $gp);
        $object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, $village);

      $excel_row++;
      $k++;
    }
    $name = "dtr_complaint_report_".date("Y-m-d").".xls";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


  /// Employee Report


    function download_emp_complaint_report($from_date,$to_date){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Employee Name","Employee Mobile","Subcategory","Assign To","Assign To","Created Date","Status","Agent Name","Agent MSD");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->emp_complaint_report("DATE(emp_complaint_created_date) between '$from_date' and '$to_date' ");

  // echo "<pre>";
  //    print_r($data);
  //    exit;


     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       

        if($row->emp_complaint_status==1)
            $status = "Open";
        else if($row->emp_complaint_status==3)
            $status = "Attended";
        else if($row->emp_complaint_status==4)
            $status = "Close";
        else if($row->emp_complaint_status==5)
            $status = "Reopen";
        else if($row->emp_complaint_status==6)
            $status = "Force Close";
        

        $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->emp_complaint_created_by));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->emp_complaint_employee_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->emp_complaint_employee_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->category_sub_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->employee_emp_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->employee_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->emp_complaint_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $status);

      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $created_by->users_name);
    

      $excel_row++;
      $k++;
    }
    $name = "Employee_report_".date("Y-m-d").".xls";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


    // End Employee Report



  function download_corruption_complaint_report($from_date,$to_date){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Victim Name","Victim Mobile","Corrupted Emp Name","Corrupted Emp Mob","Corrupted Emp Dept","Subcategory","Assign To","Assign To","Created Date","Status","Agent Name","Agent MSD");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->corruption_complaint_report("DATE(emp_complaint_created_date) between '$from_date' and '$to_date' ");

     //   echo "<pre>";
     // print_r($data);
     //  exit;
 
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       

        if($row->emp_complaint_status==1)
            $status = "Open";
        else if($row->emp_complaint_status==3)
            $status = "Attended";
        else if($row->emp_complaint_status==4)
            $status = "Close";
        else if($row->emp_complaint_status==5)
            $status = "Reopen";
        else if($row->emp_complaint_status==6)
            $status = "Force Close";
        
        

      $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->emp_complaint_created_by));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->emp_complaint_employee_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->emp_complaint_employee_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->currpt_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->currpt_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->currpt_dept);

      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->category_sub_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->assign_name);
      
       $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->assign_mob);

      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->emp_complaint_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $status);

      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $created_by->users_name);
    

      $excel_row++;
      $k++;
    }
    $name = "Corruption_report_".date("Y-m-d").".xls";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


    // End Employee Report


  function download_information_report($from_date,$to_date){
      @ini_set('memory_limit', '-1');
      @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    //$table_columns = array("ID","Name","MobileNumber","Description","Type","Status","Date","Agent Name","Agent MSD");
$table_columns = array("ID","Category","Sub Category","MobileNumber","Description","Date","Agent Name","Agent MSD");


    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:K1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('ff5600');
      $column++;
    }


     //$data = $this->Common_model->information_reports("DATE(info_feedback_created_date) between '$from_date' and '$to_date' ");
$data = $this->Common_model->information_reports_table("DATE(date_time) between '$from_date' and '$to_date' ");
//echo $this->db->last_query();
//die();
     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, 'Information');
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->category_sub_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->complaints_called_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->complaints_remark);
      
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->date_time);

      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->users_name);    

      $excel_row++;
      $k++;
    }
    $name = "Information_".date("Y-m-d").".xls";
    ob_clean();
    $objWriter = new PHPExcel_Writer_Excel2007($object);
   // $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }


   function download_contractor_complaint_report($from_date,$to_date){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Contractor","Contractor Mobile","Office Type","Office Name","Subcategory","Assign To","Assign To","Created Date","Status","Agent Name","Agent MSD");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->contractor_complaint_report("DATE(con_complaint_created_date) between '$from_date' and '$to_date' ");

       // echo "<pre>";
       //  print_r($data);
       //   exit;
 
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       

        if($row->con_complaint_status==1)
            $status = "Open";
        else if($row->con_complaint_status==3)
            $status = "Attended";
        else if($row->con_complaint_status==4)
            $status = "Close";
        else if($row->con_complaint_status==5)
            $status = "Reopen";
        else if($row->con_complaint_status==6)
            $status = "Force Close";

        if($row->con_complaint_authority_office=='RAO'){
           $authority_office = "RAO";
           $authority_name = $this->Common_model->getvalue("rao_name","rao_list",array("rao_id="=>$row->con_complaint_authority_name));
         }
         else if($row->con_complaint_authority_office=='Corporate'){
           $authority_office = "Corporate";
           $authority_name = $this->Common_model->getvalue("corporate_name","corporate_list",array("corporate_id="=>$row->con_complaint_authority_name));
         }
         else if($row->con_complaint_authority_office=='Region'){
           $authority_office = "Region";
           $authority_name = $this->Common_model->getvalue("region_name","region",array("region_id="=>$row->con_complaint_authority_name));
         }
         else if($row->con_complaint_authority_office=='Circle'){
           $authority_office = "Circle";
           $authority_name = $this->Common_model->getvalue("circle_name","circle",array("circle_id="=>$row->con_complaint_authority_name));
         }
         
         else if($row->con_complaint_authority_office=='Division'){
           $authority_office = "Division";
           $authority_name = $this->Common_model->getvalue("division_name","division",array("division_id="=>$row->con_complaint_authority_name));
         }
      

      $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->con_complaint_created_by));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->con_complaint_contractor_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->con_complaint_contractor_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $authority_office);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $authority_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->category_sub_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->users_first_name." ".$row->users_last_name);
      
       $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->users_mobile);

      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->con_complaint_created_by);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $status);

      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $created_by->users_name);
    

      $excel_row++;
      $k++;
    }
    $name = "Contractor_report_".date("Y-m-d").".xls";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }

    /// PRS Report Section

    function download_prs_report($from_date,$to_date){

           @ini_set('memory_limit', '-1');
           @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Group No","Diary No","Consumer Id","Name","Address","mobile","category","Tariff","Fidder","DTR","Meter No","Pole No","Arrear","Amount To Be Paid","Deposit","last Payment Date","Last Month Read","Due Date","Aspected Payment Date","Remark","Tagging","Bill Not Pay","Call Back Date","Agent Name","Agent MSD","Date","division_name","distributed_center_name");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->prs_reports("DATE(prs.prs_call_date) between '$from_date' and '$to_date' ");
   
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {

        if($row->prs_consumer_tagging==1)
             $tag = "Wrong Number / who did not pickup";
         else if($row->prs_consumer_tagging==2)
             $tag = "Made Payment as per Campaign";
         else if($row->prs_consumer_tagging==3)
             $tag = "Who did not pay";
         else if($row->prs_consumer_tagging==4)
             $tag = "Intimation given to consumer";
      
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->prs_group_no);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prs_diary_no);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->prs_consumer_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->prs_consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->prs_consumer_address);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->prs_consumer_mobile);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->prs_consumer_address_category);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->prs_consumer_tariff);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->prs_consumer_fidder);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->prs_consumer_dtr_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->prs_consumer_meter_number);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->prs_consumer_pole_number);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->prs_consumer_arrear);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->prs_amount_to_be_paid);

      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->prs_consumer_security_deposit);
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->prs_consumer_last_payment_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->prs_consumer_last_month_read);
      $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->prs_due_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->prs_aspected_payment);
      $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row->prs_remark);
      $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $tag);
      $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row->billnotpay);
      $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row->prs_call_back_date);

      $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row->users_first_name.' '.$row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $row->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $row->prs_call_date);
       $object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $row->division_name);
        $object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, $row->distributed_center_name);
    

      $excel_row++;
      $k++;
    }
    $name = "prs_".date("Y-m-d").".csv";
    ob_clean();
   // $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter  = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');



    }
    
     function download_nonivrs_report($from_date,$to_date){

           @ini_set('memory_limit', '-1');
           @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Region Name","Circle Name","Division Name","Sub Division Name","Distributed center name","Complaints Id","Consumer Name","IVRS","Mobile","mobile2","New Mobile","Taging","Description","Updated Date","type");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->nonivrs_reports("DATE(non.updated_date) between '$from_date' and '$to_date' ");
 
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
$tag='';

        if($row->taging==1)
             $tag = "Wrong Number / who did not pickup";
         else if($row->taging==2)
             $tag = "Made Payment as per Campaign";
         else if($row->taging==3)
             $tag = "Who did not pay";
         else if($row->taging==4)
             $tag = "Intimation given to consumer";
         else if($row->taging==5)
             $tag = "IVRS / Mobile no. Already registered.";

 
      if($row->status==1)
         $type = "By Calling";
      else 
         $type = "By SMS";

      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->complaints_id);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->mobile2);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->new_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->taging);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->description);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->updated_date);
          $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $type);


      $excel_row++;
      $k++;
    }
    $name = "NonIVRS_".date("Y-m-d").".csv";
    ob_clean();
   // $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter  = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');



    }

function download_htprs_report($from_date,$to_date){

           @ini_set('memory_limit', '-1');
           @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","REGION NAME","CIR_NAME","DIV_NAME","IVRS","Name","CONS_ADDRRESS","Bill_mobile_no","Email","BILL_DATE","DUE_DATE_CASH","CURRENT_MONTH_BILL","FeedBack","Description","Updated Date");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Z1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->htprs_reports("DATE(h.updated_date) between '$from_date' and '$to_date' ");
   
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {

//        if($row->taging==1)
//             $tag = "Wrong Number / who did not pickup";
//         else if($row->taging==2)
//             $tag = "Made Payment as per Campaign";
//         else if($row->taging==3)
//             $tag = "Who did not pay";
//         else if($row->taging==4)
//             $tag = "Intimation given to consumer";
      
    
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $k);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->REGION_NAME);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->CIR_NAME);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->DIV_NAME);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->ACC_ID);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->NAME);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->CONS_ADDR);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->bill_mobile_no);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->E_MAIL_ID);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->BILL_DATE);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->DUE_DATE_CASH);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->HT_CURRENT_MONTH_BILL);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->taging);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->description);

      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->updated_date);

    

      $excel_row++;
      $k++;
    }
    $name = "HTPRS_".date("Y-m-d").".csv";
    ob_clean();
   // $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter  = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');



    }

    /// Agent History Report

  function download_agent_history_report($from_date,$to_date){

          @ini_set('memory_limit', '-1');
          @ini_set('max_execution_time', 0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Complaint Id","History Type","Follow Up","Remark","Status","Created Date","First Name","Last Name","Usernanme","category","Subcategory");

    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:J1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->agent_history_complaint_report("DATE(complaints_history_created_date) between '$from_date' and '$to_date' ");


     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
        

        if($row->complaints_history_status==1)
            $status = "Open";
        else if($row->complaints_history_status==3)
            $status = "Attended";
        else if($row->complaints_history_status==4)
            $status = "Close";
        else if($row->complaints_history_status==6)
            $status = "Force Close";
        else if($row->complaints_history_status==5)
            $status = "Reopen";
        else if($row->complaints_history_status==8)
            $status = "Reminder";
        else if($row->complaints_history_status==9)
            $status = "Not Pick By Officer";
        else if($row->complaints_history_status==10)
            $status = "Not pickup by Consumer";
        else if($row->complaints_history_status==11)
            $status = " Call not connected to the consumer ";
        else if($row->complaints_history_status==12)
            $status = " Call not connected to the Officer ";
         else if($row->complaints_history_status==17)
            $status = "Consumer Called For Status";
        else if($row->complaints_history_status==20)
            $status = "Open complaint feedback";
       else if($row->complaints_history_status==16)
            $status = "Wrong DC";

      
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->complaints_history_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->complaints_history_complaint_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->complaints_history_type);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->complaints_history_followup_by);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->complaints_history_remark);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $status);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->complaints_history_created_date);

      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->users_first_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->users_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->category_sub_name);


      $excel_row++;
      $k++;
    }
    $name = "AhentHistoryReport_".date("Y-m-d").".csv";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');


 }
/// End Agent History Report
  function download_foc_complaint_report($from_date,$to_date){

          @ini_set('memory_limit', '-1');
          @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Consumer Name","Consumer Mobile","Ivrs","Status","Address","Category","SubCategory","Region","Circle","Division","Subdivision","DC","FOC","complaints created date","Remark","Foc incharge Name","Foc Incharge Mobile");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:S1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->foc_complaint_reports("DATE(complaints.complaints_created_date) between '$from_date' and '$to_date' ");
// echo "<pre>";
//      print_r($data);
// exit;
     $excel_row = 2;
    foreach($data as $row)
    {
      
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->complaints_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->complaints_consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->complaints_consumer_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->complaints_ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->complaints_status);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->complaints_address);

      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->category_sub_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->foc_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->complaints_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, str_replace("=","-",$row->complaints_remark));
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->inch_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->users_mobile);
     
      $excel_row++;
    }
    $name = "FOC_Complaints_".date("Y-m-d").".xls";
    ob_clean();
    $objWriter = new PHPExcel_Writer_Excel2007($object);
    //$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');

    }

  function download_web_complaint_report($from_date,$to_date){

          @ini_set('memory_limit', '-1');
          @ini_set('max_execution_time', 0);


    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

    $table_columns = array("ID","Consumer Name","Consumer Mobile","Ivrs","Status","Address","Category","SubCategory","Region","Circle","Division","Subdivision","DC","FOC","complaints created date","Remark","Foc incharge Name","Foc Incharge Mobile");

    $column = 0;
        

    foreach($table_columns as $field)
    {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:S1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->web_complaint_reports("and DATE(complaints.complaints_created_date) between '$from_date' and '$to_date' ");
// echo "<pre>";
//      print_r($data);
// exit;
     $excel_row = 2;
    foreach($data as $row)
    {
      
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->complaints_id);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->complaints_consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->complaints_consumer_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->complaints_ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->complaints_status);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->complaints_address);

      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->category_main_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->category_sub_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->distributed_center_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->foc_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->complaints_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, str_replace("=","-",$row->complaints_remark));
      $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->inch_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->users_mobile);
     
      $excel_row++;
    }
    $name = "web_Complaints_".date("Y-m-d").".xls";
    ob_clean();
    $objWriter = new PHPExcel_Writer_Excel2007($object);
    //$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');

    }


/// Survey Report

 function download_lt_survey_report($from_date,$to_date){

         @ini_set('memory_limit', '-1');
         @ini_set('max_execution_time', 0);

        error_reporting(0);

    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);

$table_columns = array("Complaint ID","Question","Answer","Created Date","Consumer Name","Consumer Mobile","Consumer Called Mob","IVRS","Region","Circle","Division","Sub Division","DC","Agent Name","Agent MSD");
    $column = 0;
        

    foreach($table_columns as $field)
    {
$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
      $object->getActiveSheet()->getStyle('A1:Q1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('007cf9');
      $column++;
    }


     $data = $this->Common_model->lt_consumer_survey("DATE(lt_feedback_created_date) between '$from_date' and '$to_date' AND lt_feedback_complaints_id!=0");
 //echo  $this->db->last_query();
//die();

     
     $excel_row = 2;
     $k=1;
    foreach($data as $row)
    {
       

     $created_by = $this->Common_model->selectfetch("users_first_name,users_last_name,users_name","users",array("users_id="=>$row->lt_feedback_agent_id));
       
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->lt_feedback_complaints_id);

      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->lt_feedback_question);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->lt_feedback_answer);

      $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->lt_feedback_created_date);
      $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->complaints_consumer_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->complaints_consumer_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->complaints_called_mobile);
      $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->complaints_ivrs);
      $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->region_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->circle_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->sub_division_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->distributed_center_name);

      $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $created_by->users_first_name.' '.$created_by->users_last_name);
      $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $created_by->users_name);
    

      $excel_row++;
      $k++;
    }
    $name = "lt_survey_report_".date("Y-m-d").".xls";
    ob_clean();
  //  $objWriter = new PHPExcel_Writer_Excel2007($object);
    $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$name.'"');
    $objWriter->save('php://output');
    }

	function complaints_reports_1() {
        $start = $this->input->post('from_date');
        $end = $this->input->post('to_date');
        $data['division_data'] = $this->Superwiser_report_model->division_count($start,$end);
        $data['categories'] = $this->db->get('category_main')->result_array();
        $this->load->view('superwiser/components/header');
        $this->load->view('special/download_complaints_report_1', $data);
        $this->load->view('superwiser/components/footer');
    }}

?>