<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ftp_agents_model extends CI_Model {

    public $table = 'ftp_agents';
    public $id = 'id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_all_by_join() {

        $this->db->select('f.id,f.file_name,f.dateof,f.filename,f.status,a.name');
        $this->db->from('ftp_agents as f');
        $this->db->join('agents as a', 'a.id = f.upload_by');


        $query = $this->db->get();
        return $query->result();
    }
     function get_all_by_join_id($id) {

        $this->db->select('f.id,f.file_name,f.dateof,f.filename,f.status,a.name');
        $this->db->from('ftp_agents as f');
        $this->db->join('agents as a', 'a.id = f.upload_by');

        $this->db->where('f.upload_by', $id);
        $query = $this->db->get();
        return $query->result();
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('upload_by', $q);
        $this->db->or_like('file_name', $q);
        $this->db->or_like('filename', $q);
        $this->db->or_like('fileurl', $q);
        $this->db->or_like('dateof', $q);
        $this->db->or_like('status', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('upload_by', $q);
        $this->db->or_like('file_name', $q);
        $this->db->or_like('filename', $q);
        $this->db->or_like('fileurl', $q);
        $this->db->or_like('dateof', $q);
        $this->db->or_like('status', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Ftp_agents_model.php */
/* Location: ./application/models/Ftp_agents_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-11-15 21:38:12 */
