<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agents_model extends CI_Model {

    public $table = 'agents';
    public $id = 'id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_all_formar() {
        $this->db->select("id,emp_id,name,employee_id_TL,employee_id_AM,shift_id");
        $this->db->where('status', '1');
        $this->db->order_by($this->id, 'ASC');
        return $this->db->get($this->table)->result_array();
    }
     function get_am_tl_by_agent_id($id) {
        $this->db->select("id,emp_id,employee_id_TL,employee_id_AM");
        $this->db->where('id', $id);
       
        return $this->db->get($this->table)->row();
    }

    function get_all_by_joins($status) {

        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status");
        $this->db->from('agents as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        //$this->db->order_by($this->id, $this->order);
        if ($status != '') {
            $this->db->where('a.status', $status);
        }
        return $this->db->get()->result();
    }
     function get_all_by_joinsbystatus($status) {

        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status,(case when (a.status = '1') THEN 'Active' ELSE 'Close' END) as statusof");
        $this->db->from('agents as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        //$this->db->order_by($this->id, $this->order);
        if ($status != '') {
            $this->db->where('a.status', $status);
        }
        return $this->db->get()->result();
    }

    function get_active_close() {
        $this->db->select("SUM(case when a.status = '1' then 1 else 0 end) as totalactive,SUM(case when a.status = '0' then 1 else 0 end) as totalclose");
        $this->db->from('agents as a');
        return $this->db->get()->row();
    }

    function get_all_by_joins_notlogintoday() {
        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.shift_id as shiftid,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status,a.lastLogin");
        $this->db->from('agents as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->order_by('a.shift_id', 'DESC');
        $this->db->where('a.status', '1');
        //  $this->db->where('a.lastLogin!=',date('Y-m-d'));
        return $this->db->get()->result();
    }

    function get_all_by_joins_byID($id) {
        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status");
        $this->db->from('agents as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->order_by($this->id, $this->order);
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
     function get_all_by_joins_byID_supervises($id) {
        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status");
        $this->db->from('supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->order_by($this->id, $this->order);
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }

    function countMonthly($id = '') {
        $this->db->select("count(a.id),a.date,AVG(a.score) as score");
        $this->db->from('audit as a');
        $this->db->where('a.agents_id', $id);
        $this->db->group_by('MONTH(a.date)');

        return $this->db->get()->result();
    }
    function countMonthly_tl($id = '') {
        $this->db->select("count(a.id),a.date,AVG(a.score) as score");
        $this->db->from('audit_tl as a');
        $this->db->where('a.agents_id', $id);
        $this->db->group_by('MONTH(a.date)');

        return $this->db->get()->result();
    }

    function get_audit_byidAll($id = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('a.agents_id', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        return $this->db->get()->result();
    }
     function get_audit_byidAll_supervises($id = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('a.agents_id', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        return $this->db->get()->result();
    }
     function get_audit_byidAll_tl($id = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_tl as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('a.agents_id', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        return $this->db->get()->result();
    }

    function get_fb_status($audit_id) {
        $this->db->select('af.Auditor,af.Accepted,af.date_of');
        $this->db->from('agentfeedback as af');
        $this->db->where('af.audit_id', $audit_id);
        
        return $this->db->get()->row();
    }

    function get_audit_byidAllBymonth($month) {
        $id = $this->session->userdata('id');
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('MONTH(a.date)', date('m', strtotime($month)));
        $this->db->where('a.agents_id', $id);


        return $this->db->get()->result();
    }
      function get_audit_byidAllBymonth_tl($month) {
        $id = $this->session->userdata('id');
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_tl as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('MONTH(a.date)', date('m', strtotime($month)));
        $this->db->where('a.agents_id', $id);


        return $this->db->get()->result();
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
     // get data by id
    function get_all_agents_by_tl($id) {
        $results='';
        $arraylist = array();
          $this->db->select('id');
        $this->db->where('employee_id_TL', $id);
         $this->db->where('status','1');
       $results = $this->db->get($this->table)->result();
       foreach ($results as $value) {
          $arraylist[]=$value->id; 
       }
       return $arraylist;
    }
    
   

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('emp_id', $q);
        $this->db->or_like('name', $q);
        $this->db->or_like('employee_id_TL', $q);
        $this->db->or_like('employee_id_AM', $q);
        $this->db->or_like('shift_id', $q);
        $this->db->or_like('DOJ', $q);
        $this->db->or_like('process', $q);
        $this->db->or_like('gender', $q);
        $this->db->or_like('batch_no', $q);
        $this->db->or_like('status', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('emp_id', $q);
        $this->db->or_like('name', $q);
        $this->db->or_like('employee_id_TL', $q);
        $this->db->or_like('employee_id_AM', $q);
        $this->db->or_like('shift_id', $q);
        $this->db->or_like('DOJ', $q);
        $this->db->or_like('process', $q);
        $this->db->or_like('gender', $q);
        $this->db->or_like('batch_no', $q);
        $this->db->or_like('status', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    function updateAlignment($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
     function get_agents_namebyid($id) {
        $this->db->select("a.name");
        $this->db->from('agents as a');
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
     function get_ID_supervises($id) {
        $this->db->select("a.id");
        $this->db->from('supervises as a');
       
        $this->db->where('a.emp_id', $id);
        return $this->db->get()->row();
    }
     function get_all_by_joins_byID_supervises_dash($id) {
        $this->db->select("a.id,a.emp_id,a.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id,a.DOJ,a.process, (case when (a.gender = 1) THEN 'Male' ELSE 'Female' END) as gender,a.batch_no,a.status");
        $this->db->from('supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->order_by($this->id, $this->order);
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
     function countMonthly_supervisess($id = '') {
        $this->db->select("count(a.id),a.date,AVG(a.score) as score");
        $this->db->from('audit_supervises as a');
        $this->db->where('a.agents_id', $id);
        $this->db->group_by('MONTH(a.date)');

        return $this->db->get()->result();
    }
    function get_audit_byidAll_supervisess($id = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('a.agents_id', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        return $this->db->get()->result();
    }
    
      function get_audit_byidAllBymonth_supervisess($month,$id) {
       // $id = $this->session->userdata('id');
        $this->db->select('a.id,a.audit_time,
            ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             ar.QME_Remarks,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
              ar.Consumers_Concern,
              ar.r_g_y_a,
              ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
            
                 ');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');



        $this->db->where('MONTH(a.date)', date('m', strtotime($month)));
        $this->db->where('a.agents_id', $id);


        return $this->db->get()->result();
    }
     function get_fb_status_supervisess($audit_id) {
        $this->db->select('af.Auditor,af.Accepted,af.date_of');
        $this->db->from('agentfeedback as af');
        $this->db->where('af.audit_id', $audit_id);
        return $this->db->get()->row();
    }
    
     function insertagentsperformance($data) {
        $this->db->insert('agent_performance', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
      function get_all_tl_of_AM($id) {
          $this->db->select('e.emp_id,emp_name,msd_ID');
        $this->db->from('agents as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->where('a.employee_id_AM', $id);
         $this->db->where('a.status', '1');
          $this->db->where('e.status', '1');
         $this->db->group_by('a.employee_id_TL');
        return $this->db->get()->result();
    }
    function get_all_am_of_AM() {
          $this->db->select('e.emp_id,emp_name,msd_ID');
        $this->db->from('agents as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_AM');
        $this->db->where('e.designation','4');
         $this->db->where('a.status', '1');
          $this->db->where('e.status', '1');
        $this->db->group_by('a.employee_id_AM');
        return $this->db->get()->result();
    }

}

/* End of file Agents_model.php */
/* Location: ./application/models/Agents_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-08-02 12:35:42 */
