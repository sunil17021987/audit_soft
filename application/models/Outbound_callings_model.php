<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Outbound_callings_model extends CI_Model
{

    public $table = 'outbound_callings';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('patient_full_name', $q);
	$this->db->or_like('patient_mobile_number', $q);
	$this->db->or_like('address', $q);
	$this->db->or_like('district', $q);
	$this->db->or_like('block', $q);
	$this->db->or_like('call_status', $q);
	$this->db->or_like('q1', $q);
	$this->db->or_like('q2', $q);
	$this->db->or_like('q3', $q);
	$this->db->or_like('q4', $q);
	$this->db->or_like('q5', $q);
	$this->db->or_like('q6', $q);
	$this->db->or_like('q7', $q);
	$this->db->or_like('q8', $q);
	$this->db->or_like('q9', $q);
	$this->db->or_like('created_date', $q);
	$this->db->or_like('updated_date', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('patient_full_name', $q);
	$this->db->or_like('patient_mobile_number', $q);
	$this->db->or_like('address', $q);
	$this->db->or_like('district', $q);
	$this->db->or_like('block', $q);
	$this->db->or_like('call_status', $q);
	$this->db->or_like('q1', $q);
	$this->db->or_like('q2', $q);
	$this->db->or_like('q3', $q);
	$this->db->or_like('q4', $q);
	$this->db->or_like('q5', $q);
	$this->db->or_like('q6', $q);
	$this->db->or_like('q7', $q);
	$this->db->or_like('q8', $q);
	$this->db->or_like('q9', $q);
	$this->db->or_like('created_date', $q);
	$this->db->or_like('updated_date', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    
      function date_wise_count($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE DATE(o.created_date) >='$date_fromf' AND DATE(o.created_date) <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE DATE(o.created_date) ='$currdate'";
        }

//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//
//            $where = $where . " AND a.audit_by =$audit_by";
//        }
        $query = $this->db->query(""
                . "SELECT
                    DATE_FORMAT(created_date, '%W %M %e %Y') as createdDate,
                count(id) as total, 
                 100 * COUNT(o.call_status) / COUNT(o.call_status) as 'total_connected %',
                SUM(IF(o.call_status=1,1,0)) AS connected,
                 100 * SUM(IF(o.call_status='1', 1, 0)) / COUNT(o.call_status) as 'connectedPer',
                 SUM(IF(o.call_status!=1,1,0)) AS notconnected,
                 100 * SUM(IF(o.call_status!='1', 1, 0)) / COUNT(o.call_status) as 'notConnectedPer'
                FROM outbound_callings as o $where
                GROUP BY YEAR(o.created_date),MONTH(o.created_date),DAY(o.created_date)
                ");
        $rows = $query->result();
        return $rows;
    }
      function block_wise_count($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE DATE(o.created_date) >='$date_fromf' AND DATE(o.created_date) <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE DATE(o.created_date) ='$currdate'";
        }

//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//
//            $where = $where . " AND a.audit_by =$audit_by";
//        }
        $query = $this->db->query("
                 SELECT
                   b.category_sub_name as block_name,
                   COUNT(IF(o.call_status='1', 1, NULL)) as 'PatientInHospital',
                   COUNT(IF(o.call_status='2', 1, NULL)) as 'PatientInHomeIsolation',
                   COUNT(IF(o.call_status='3', 1, NULL)) as 'PatientInQuarantineCenter',
                   COUNT(IF(o.call_status='4', 1, NULL)) as 'NeedDoctorAssistance',
                   COUNT(IF(o.call_status='5', 1, NULL)) as 'NeedMedicines',
                   COUNT(IF(o.call_status='6', 1, NULL)) as 'PatientHasRecovered',
                    COUNT(IF(o.call_status='7', 1, NULL)) as 'MobileSwitchedOff',
                     COUNT(IF(o.call_status='8', 1, NULL)) as 'WrongNumber',
                       COUNT(IF(o.call_status='9', 1, NULL)) as 'OutOfNetwork'
                       
                FROM category_sub as b 
                LEFT JOIN outbound_callings as o ON o.block=b.category_sub_id
                $where
                GROUP BY b.category_sub_id
                ");
        $rows = $query->result();
        return $rows;
    }
     function count_date_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY YEAR(a.date),MONTH(a.date),DAY(a.date)");
        $rows = $query->result();
        return $rows;
    }
    
     function getRecordsByaAgent($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE DATE(o.created_date) >='$date_fromf' AND DATE(o.created_date) <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE DATE(o.created_date) !=''";
        }

        if ($audit_by != '') {

            $where = $where . " AND o.agents_id =$audit_by";
        }
        $query = $this->db->query("
                 SELECT
                 o.*,
                 ct.name as call_type_name,
                 ag.name as agent_name,
                 c.category_main_name as district_name,
                   cs.category_sub_name as block_name
                   FROM outbound_callings as o 
                INNER JOIN agents as ag ON ag.id=o.agents_id
                INNER JOIN category_main as c ON c.category_main_id=o.district
                INNER JOIN category_sub as cs ON cs.category_sub_id=o.block
                INNER JOIN call_types as ct ON ct.id=o.call_status
                $where
                
                ");
        $rows = $query->result();
        return $rows;
    }
    

}

/* End of file Outbound_callings_model.php */
/* Location: ./application/models/Outbound_callings_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2022-01-22 09:33:52 */
