<?php
defined('BASEPATH') OR exit('NO Direct Access Allowed');
class Login_model extends CI_Model {
    public function validate() {
        $email = $this->security->xss_clean($this->input->post('emailid'));
        $password = $this->security->xss_clean($this->input->post('password'));
       
            $sql = "select * from employee where msd_ID = '" . $email . "' and password = '" . md5($password) . "' AND status=1 limit 1";
            $query = $this->db->query($sql);            
            if ($query->num_rows()==1) {
                $row = $query->row();
                $data = array(
                    'reg_id' => $row->emp_id,
                    'emailid' => $row->email,
                    'emp_name' => $row->emp_name,
                     'validated' => true,
                     'type' =>$row->designation,
                    'msd_ID'=>$row->msd_ID,
                    'designation'=>$row->designation,
                     'department'=>$row->department,
                    'image'=>$row->image,
                    'category'=>$row->category,
                );
                $this->session->set_userdata($data);
                return $row;
            }
    }
    
    public function agentvalidate() {
        $email = $this->security->xss_clean($this->input->post('emailid'));
        $password = $this->security->xss_clean($this->input->post('password'));
       
            $sql = "select * from agents where emp_id = '" . $email . "' and password = '" . $password . "' AND status='1' limit 1";
            $query = $this->db->query($sql);            
            if ($query->num_rows()==1) {
                $row = $query->row();
                $data = array(
                    'id' => $row->id,
                    'emp_id' => $row->emp_id,
                    'name' => $row->name,
                    'validated' => true
                    );
                $this->session->set_userdata($data);
                return $row;
            }
    }



    public function read_member_information($id) {



        $condition = "emailid =" . "'" . $id . "'";

        $this->db->select('*');

        $this->db->from('members');

        $this->db->where($condition);

        $this->db->limit(1);

        $query = $this->db->get();



        if ($query->num_rows() == 1) {

            return $query->result();

        } else {

            return false;

        }

    }
    
     // update data
    function update($id, $data) {
        $this->db->where('emp_id', $id);
        $this->db->update('employee', $data);
    }
     function updateAgent($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('agents', $data);
    }



}



?>