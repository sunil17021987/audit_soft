<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee_model extends CI_Model {

    public $table = 'employee';
    public $id = 'emp_id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    public function get_all_notlogintoday(){
          $this->db->where('status','1');
         $this->db->where('lastLogin!=', date('Y-m-d'));
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result(); 
    }
    function get_all_TL() {
        $this->db->where('designation', '1');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_all_AM() {
        $this->db->where('designation', '4');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_staff_by_task($ids) {
        $this->db->order_by($this->id, $this->order);
        $this->db->where('emp_id in (' . $ids . ')');
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
     function get_by_id_msd_id($id) {
          $this->db->select('emp_id,msd_ID,designation');
        $this->db->where('msd_ID', $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_id1($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }

    function get_emails_by_ids($ids) {
        $this->db->select('GROUP_CONCAT(email) email');
        $condi = "emp_id in (" . $ids . ")";
        $this->db->where($condi);
        $result = $this->db->get('employee')->row();
        //echo $this->db->last_query();        
        // $emails=(array) $result[0];
        //print_r($emails);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('emp_id', $q);
        $this->db->or_like('emp_name', $q);
        $this->db->or_like('gender', $q);
        $this->db->or_like('dob', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('mobile', $q);
        $this->db->or_like('category', $q);
        $this->db->or_like('qualification', $q);
        $this->db->or_like('join_date', $q);
        $this->db->or_like('department', $q);
        $this->db->or_like('designation', $q);
        $this->db->or_like('matial_status', $q);
        $this->db->or_like('nationality', $q);
        $this->db->or_like('experience', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('created_at', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('emp_id', $q);
        $this->db->or_like('emp_name', $q);
        $this->db->or_like('gender', $q);
        $this->db->or_like('dob', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('mobile', $q);
        $this->db->or_like('category', $q);
        $this->db->or_like('qualification', $q);
        $this->db->or_like('join_date', $q);
        $this->db->or_like('department', $q);
        $this->db->or_like('designation', $q);
        $this->db->or_like('matial_status', $q);
        $this->db->or_like('nationality', $q);
        $this->db->or_like('experience', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('created_at', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function Login_staffapi($data) {
        $condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";

        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get($this->table);

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    function read_information_api($email = '') {
        $this->db->where('email', $email);
        return $this->db->get($this->table)->row();
    }

    function Enquiry_add($data) {

        $this->db->insert('demo', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function batch_bycourse($id = 0) {
        $this->db->where('c_id', $id);
        return $this->db->get('batch')->result();
    }

    function task_bystaff($id = 0) {
        $this->db->where('staff_id', $id);
        $this->db->where('status', 1);
        return $this->db->get('task')->result();
    }

    function get_idms($Login_ID) {
        //$this->db->where('Login_ID', $Login_ID);
        //  return $this->db->get('idms')->row();

        $this->db->select('a.id,a.emp_id,a.name,a.DOJ,a.process,a.gender,a.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id');
        $this->db->from('agents as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
         $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
         $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->where('a.emp_id', "$Login_ID");
         $this->db->where('a.status','1');
        return $this->db->get()->row();
    }
     function get_idms_supervises($Login_ID) {
        //$this->db->where('Login_ID', $Login_ID);
        //  return $this->db->get('idms')->row();

        $this->db->select('a.id,a.emp_id,a.name,a.DOJ,a.process,a.gender,a.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id');
        $this->db->from('supervises as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
         $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
         $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->where('a.emp_id', "$Login_ID");
        return $this->db->get()->row();
    }
    
      function get_idms_cli_QA($Login_ID) {
       $this->db->select('ar.audit_id');
        $this->db->from('audit_remarks as ar');
        $this->db->where('ar.CLI_Number', "$Login_ID");
        return $this->db->get()->row();
    }
    function get_idms_cli_TL($Login_ID) {
       $this->db->select('ar.audit_id');
        $this->db->from('audit_remarks_tl as ar');
        $this->db->where('ar.CLI_Number', "$Login_ID");
        return $this->db->get()->row();
    }
    
    
     function get_idms_byid($id) {
         $this->db->select('a.id,a.emp_id,a.name,a.DOJ,a.process,a.gender,a.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id');
        $this->db->from('agents as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
         $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
         $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->where('a.id', "$id");
        return $this->db->get()->row();
    }
     function get_idms_byid_supervises($id) {
         $this->db->select('a.id,a.emp_id,a.name,a.DOJ,a.process,a.gender,a.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,s.name as shift_id');
        $this->db->from('supervises as a');
         $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
         $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
         $this->db->join('shifts as s', 's.id = a.shift_id');
        $this->db->where('a.id', "$id");
        return $this->db->get()->row();
    }

}

/* End of file Employee_model.php */
/* Location: ./application/models/Employee_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-10-24 16:31:23 */
/* http://harviacode.com */