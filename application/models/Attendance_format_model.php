<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attendance_format_model extends CI_Model
{

    public $table = 'attendance_format';
    public $id = 'id';
    public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('employeeName', $q);
	$this->db->or_like('TLName', $q);
	$this->db->or_like('agent_Status', $q);
	$this->db->or_like('batch', $q);
	$this->db->or_like('AMName', $q);
	$this->db->or_like('Date_of', $q);
	$this->db->or_like('RosterShift', $q);
	$this->db->or_like('Login', $q);
	$this->db->or_like('Attendance', $q);
	$this->db->or_like('MSD_ID', $q);
	$this->db->or_like('created_date', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('employeeName', $q);
	$this->db->or_like('TLName', $q);
	$this->db->or_like('agent_Status', $q);
	$this->db->or_like('batch', $q);
	$this->db->or_like('AMName', $q);
	$this->db->or_like('Date_of', $q);
	$this->db->or_like('RosterShift', $q);
	$this->db->or_like('Login', $q);
	$this->db->or_like('Attendance', $q);
	$this->db->or_like('MSD_ID', $q);
	$this->db->or_like('created_date', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    
          function get_all_bydateagent($date_fromf = '', $date_tof = '')
    {
           $id = $this->session->userdata('emp_id');
            $this->db->where('MSD_ID',$id);
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('Date_of >=', $date_fromf);
            $this->db->where('Date_of <=', $date_tof);
        }else{
             $this->db->where('MONTH(Date_of)', date('m'));
         }
        $this->db->order_by('Date_of', $this->order);
        return $this->db->get($this->table)->result();
    }
     function insertagentsperformance($data) {
        $this->db->insert('attendance_format', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
      function get_max_date() {
        $this->db->select('MAX(Date_of) as date_of');
        $this->db->from($this->table);
        return $this->db->get()->row();
    }
    
 function get_all_bydate_delete($date_fromf = '', $date_tof = '')
    {
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('Date_of >=', $date_fromf);
            $this->db->where('Date_of <=', $date_tof);
        }
      $this->db->delete($this->table);
       
    }
         function get_all_bydate($date_fromf = '', $date_tof = '')
    {
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('Date_of >=', $date_fromf);
            $this->db->where('Date_of <=', $date_tof);
        }else{
           //  $this->db->where('MONTH(date_of)', date('m'));
         }
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    

}

/* End of file Attendance_format_model.php */
/* Location: ./application/models/Attendance_format_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2020-10-16 08:51:16 */
