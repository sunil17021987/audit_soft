<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Audit_model extends CI_Model {

    public $table = 'audit';
    public $id = 'id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }
 function get_CLI_Number($Calling_Number, $agents_id) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
        return $this->db->get('audit_remarks')->row();
    }
    function get_CLI_Number_bycli($Calling_Number, $agents_id,$CLI_Number) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
          $this->db->where('CLI_Number', $CLI_Number);
        return $this->db->get('audit_remarks')->row();
    }
    function get_CLI_Number_tl($Calling_Number, $agents_id) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
        
        return $this->db->get('audit_remarks_tl')->row();
    }
     function get_CLI_Number_tl_bycli($Calling_Number, $agents_id,$CLI_Number) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
         $this->db->where('CLI_Number', $CLI_Number);
        return $this->db->get('audit_remarks_tl')->row();
    }
    function get_CLI_Number_am($Calling_Number, $agents_id) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
        return $this->db->get('audit_remarks_am')->row();
    }
     function get_CLI_Number_am_bycli($Calling_Number, $agents_id,$CLI_Number) {
        $this->db->select('Calling_Number');
        $this->db->where('Calling_Number', $Calling_Number);
        $this->db->where('agents_id', $agents_id);
        $this->db->where('CLI_Number', $CLI_Number);
        return $this->db->get('audit_remarks_am')->row();
    }
    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('agents_id', $q);
        $this->db->or_like('employee_id_AM', $q);
        $this->db->or_like('employee_id_TL', $q);
        $this->db->or_like('score', $q);
        $this->db->or_like('date', $q);
        $this->db->or_like('status', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('agents_id', $q);
        $this->db->or_like('employee_id_AM', $q);
        $this->db->or_like('employee_id_TL', $q);
        $this->db->or_like('score', $q);
        $this->db->or_like('date', $q);
        $this->db->or_like('status', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
       function insert_supervises($data) {
        $this->db->insert('audit_supervises', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
     function insert_tl($data) {
        $this->db->insert('audit_tl', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
     function insert_AM($data) {
        $this->db->insert('audit_am', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
          return $id;
    }
    
     function update_of_tl($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update('audit_tl', $data);
          return $id;
    }
     function update_of_am($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update('audit_am', $data);
          return $id;
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function count_audit_by() {
        $id = $this->session->userdata('reg_id');
        $this->db->where('audit_by', $id);
        $this->db->where('date', date('Y-m-d'));
        return $this->db->from($this->table)->count_all_results();
    }

    function count_TodaysAudit_by($agents_id) {
        $this->db->where('agents_id', $agents_id);
        $this->db->where('date', date('Y-m-d'));
        return $this->db->from($this->table)->count_all_results();
    }
     function count_TodaysAudit_by_supervises($agents_id) {
        $this->db->where('agents_id', $agents_id);
        $this->db->where('date', date('Y-m-d'));
        return $this->db->from('audit_supervises')->count_all_results();
    }

    function count_audit_hundrepercent($date_fromf = '', $date_tof = '', $audit_by = '') {
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
           // $this->db->where('date >=', $date_fromf);
           // $this->db->where('date <=', $date_tof);
        } else {
           // $this->db->where('date', date('Y-m-d'));
        }
        $lastday=date('Y-m-d',strtotime("-1 days"));

        $this->db->where('date=',"$lastday");
        $this->db->where('score=',100);
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('audit_by', $audit_by);
//        }

//        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('employee_id_AM', $audit_by);
//        }

        return $this->db->from($this->table)->count_all_results();
    }
      function count_audit_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date >=', $date_fromf);
            $this->db->where('date <=', $date_tof);
        } else {
            $this->db->where('date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('audit_by', $audit_by);
//        }

//        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('employee_id_AM', $audit_by);
//        }

        return $this->db->from($this->table)->count_all_results();
    }

    function count_audit_AM($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $where1 = "";
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
            $where1 = " arr.date >='$date_fromf' AND arr.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
            $where1 = " arr.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }

        // $query = $this->db->query("SELECT e.emp_name,COUNT(a.id) as AM_total FROM audit as a  INNER JOIN employee as e ON e.emp_id=a.employee_id_AM $where GROUP BY a.employee_id_AM");
        $query = $this->db->query("SELECT e.emp_name,COUNT(a.id) as AM_total,AVG(a.score) AS score,(SELECT COUNT(arr.Fatal_Reason) FROM audit_remarks as arr WHERE arr.Fatal_Reason!=0 AND arr.employee_id_AM=e.emp_id AND $where1) as fatal FROM audit as a INNER JOIN employee as e ON e.emp_id=a.employee_id_AM $where GROUP BY a.employee_id_AM");
        $rows = $query->result();
        return $rows;
    }

    function count_week_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        //$currdateyear = date('Y');
        // $currdatemnth = date('m');
        // $currdatemnth = '09';
        // $where = "WHERE YEAR(a.date)='$currdateyear' AND MONTH(a.date)='$currdatemnth'";
       // $query = $this->db->query("SELECT WEEK(NOW())as week, a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY WEEK(ADDDATE( a.date , WEEKDAY(NOW()) ))");
         $query = $this->db->query("SELECT MONTH(a.date) AS monthof,WEEK(a.date)as week,date(a.date) AS dateof,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able ,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av , SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY week ORDER BY a.date");
        $rows = $query->result();
        return $rows;
    }

    function count_AM_WISES($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av , SUM(a.score) as totalscore,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_AM $where GROUP BY a.employee_id_AM");
        $rows = $query->result();
        return $rows;
    }
 function count_TL_WISES_API($date_fromf = '',$employee_id_TL = '',$designation='') {
        $where = "";

       // $designation = $this->session->userdata('designation');
        if ($date_fromf != '' ) {
            $where = "WHERE YEAR(a.date) =YEAR('$date_fromf') AND MONTH(a.date) = MONTH('$date_fromf')";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE YEAR(a.date) = YEAR('$currdate') AND MONTH(a.date) = MONTH('$currdate')";
        }
       if ($employee_id_TL != '' && $employee_id_TL != '0' && $designation=='1') {

            $where = $where . " AND a.employee_id_TL =$employee_id_TL";
        }
         if ($employee_id_TL != '' && $employee_id_TL != '0' && $designation=='4') {

            $where = $where . " AND a.employee_id_AM =$employee_id_TL";
        }
         if ($employee_id_TL != '' && $employee_id_TL != '0' && $designation=='2') {

            $where = $where . " AND a.audit_by =$employee_id_TL";
        }
         if ($employee_id_TL != '' && $employee_id_TL != '0' && $designation=='3') {

            $where = $where . " AND a.audit_by =$employee_id_TL";
        }
        

//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//
//            $where = $where . " AND a.audit_by =$audit_by";
//        }
        $query = $this->db->query("SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_TL $where ");
        $rows = $query->result();
        return $rows;
    }
    function count_TL_WISES($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_TL $where GROUP BY a.employee_id_TL");
        $rows = $query->result();
        return $rows;
    }

    function count_shiftwise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT ag.shift_id,s.name as shift_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av  ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id INNER JOIN shifts as s ON s.id = ag.shift_id $where GROUP BY ag.shift_id");
        $rows = $query->result();
        return $rows;
    }

    function count_date_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY YEAR(a.date),MONTH(a.date),DAY(a.date)");
        $rows = $query->result();
        return $rows;
    }

    function count_Tenure_wise_0_30($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }

        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >=0 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 30");
        $rows = $query->row();
        return $rows;
    }

    function count_Tenure_wise_90($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND  DATEDIFF(ar.Call_Date,ag.DOJ) > 90");
        $rows = $query->row();
        return $rows;
    }

    function count_Tenure_wise_30_60($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id  $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >= 31 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 60");
        $rows = $query->row();
        return $rows;
    }

    function count_Tenure_wise_60_90($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,  SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >=61 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 90");
        $rows = $query->row();
        return $rows;
    }

    function count_Category_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT cm.category_main_name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able, ROUND(((SUM(score)/SUM(score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN category_main as cm ON cm.category_main_id =ar.category_main_id $where GROUP BY ar.category_main_id");
        $rows = $query->result();
        return $rows;
    }

    function count_Auditor_Wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id =a.audit_by $where GROUP BY a.audit_by");
        $rows = $query->result();
        return $rows;
    }

    function count_Call_type_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {
            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT ct.name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN call_types as ct ON ct.id =ar.Call_Type $where GROUP BY ar.Call_Type");
        $rows = $query->result();
        return $rows;
    }

    function count_audit_TL($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $where1 = "";
        $designation = $this->session->userdata('designation');
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
            $where1 = " arr.date >='$date_fromf' AND arr.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
            $where1 = " arr.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }

        $query = $this->db->query("SELECT e.emp_name,COUNT(a.id) as AM_total ,AVG(a.score) AS score,(SELECT COUNT(arr.Fatal_Reason) FROM audit_remarks as arr WHERE arr.Fatal_Reason!=0 AND arr.employee_id_TL=e.emp_id AND $where1) as fatal FROM audit as a  INNER JOIN employee as e ON e.emp_id=a.employee_id_TL $where GROUP BY a.employee_id_TL");
        $rows = $query->result();
        return $rows;
    }

    function count_auditFatal_Allbydate_old($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date >=', $date_fromf);
            $this->db->where('date <=', $date_tof);
        } else {
            $this->db->where('date', date('Y-m-d'));
        }
        if ($audit_by != '') {
            //  $this->db->where('audit_by', $audit_by);
        }
        $this->db->where('Fatal_Reason!=', '0');
        return $this->db->from('audit_remarks')->count_all_results();
    }

    function count_auditFatal_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.Fatal_Reason!=', '0');


        $this->db->select("COUNT(ar.Fatal_Reason) as total");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        return$this->db->get()->row();
    }

    function actionCloseButnotActionByAgents($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = '';
        if ($date_fromf != '' && $date_tof != '') {

            $where = " AND date >='$date_fromf' AND date <= '$date_tof'";
        } else {

            $where = " AND date = CURRENT_DATE()";
        }
        $designation = $this->session->userdata('designation');
       
        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//
//            $where = $where . " AND employee_id_TL =$audit_by";
//        }
//        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//
//            $where = $where . " AND employee_id_AM =$audit_by";
//        }

        $query = "SELECT audit_id FROM audit_remarks WHERE audit_id NOT IN (SELECT audit_id FROM agentfeedback) AND feedback_status_id='Close' $where";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    function count_auditScore_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date >=', $date_fromf);
            $this->db->where('date <=', $date_tof);
        } else {
            $this->db->where('date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('employee_id_AM', $audit_by);
//        }

        $this->db->select("AVG(score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(score)/SUM(score_able))*100) as av ");

        $this->db->from('audit');
        return$this->db->get()->row();
    }

    function count_auditFeedback_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.feedback_status_id', 'Pending');


        $this->db->select("COUNT(ar.feedback_status_id) as total");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        return$this->db->get()->row();
    }

    function getAllPengings($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.feedback_status_id', 'Pending');


        $this->db->select("ag.emp_id,ag.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.Call_Date,eee.emp_name as auditByName,a.score,a.date");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
        $this->db->join('employee as e', 'e.emp_id = ar.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ar.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = ar.agents_id');

        return$this->db->get()->result();
    }

    function getAllTotalAudits($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        // $this->db->where('ar.feedback_status_id', 'Pending');


        $this->db->select("ag.emp_id,ag.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.Call_Date,eee.emp_name as auditByName,a.score,a.date");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
        $this->db->join('employee as e', 'e.emp_id = ar.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ar.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = ar.agents_id');

        return$this->db->get()->result();
    }
    
     function getAllTotalAudits_hundreds($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
         //   $this->db->where('ar.date >=', $date_fromf);
          //  $this->db->where('ar.date <=', $date_tof);
        } else {
          //  $this->db->where('ar.date', date('Y-m-d'));
        }
         $lastday=date('Y-m-d',strtotime("-1 days"));

        $this->db->where('a.date=',"$lastday");
            $this->db->where('a.score=', 100);
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        // $this->db->where('ar.feedback_status_id', 'Pending');


        $this->db->select("ag.emp_id,ag.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.Call_Date,eee.emp_name as auditByName,a.score,a.date,sh.name as shiftname");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
        $this->db->join('employee as e', 'e.emp_id = ar.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ar.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = ar.agents_id');
         $this->db->join('shifts as sh', 'sh.id = ag.shift_id');

        return$this->db->get()->result();
    }

    function getAllTotalFatals($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.Fatal_Reason!=', '0');


        $this->db->select("ag.emp_id,ag.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.Call_Date,eee.emp_name as auditByName,a.score");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
        $this->db->join('employee as e', 'e.emp_id = ar.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ar.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = ar.agents_id');

        return$this->db->get()->result();
    }

    function getAllNoAction($audit_id) {
        $this->db->where('a.id', $audit_id);
        $this->db->select("ag.emp_id,ag.name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.Call_Date,eee.emp_name as auditByName,a.score");
        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
        $this->db->join('employee as e', 'e.emp_id = ar.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ar.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = ar.agents_id');
        return$this->db->get()->row();
    }

    function count_auditFeedbackclose_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.feedback_status_id', 'Close');


        $this->db->select("COUNT(ar.feedback_status_id) as total");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        return$this->db->get()->row();
    }

    function count_auditNotAck_CSA_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {
         if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        } else {
            $this->db->where('a.date', date('Y-m-d'));
        }
        $this->db->where('af.Auditor', 'No');
        $this->db->select("ag.emp_id,a.id,a.score,a.date,ag.emp_id,ag.name,ag.process,ag.gender,ag.batch_no,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,eee.emp_name as auditByName");
        $this->db->from('agentfeedback as af');
        $this->db->join('audit as a', 'a.id = af.audit_id');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
          $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
         $this->db->join('employee as e', 'e.emp_id = ag.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ag.employee_id_AM');
        return$this->db->get()->result();
        
        
    }

    function count_auditNotAccepted($date_fromf = '', $date_tof = '', $audit_by = '') {
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        } else {
            $this->db->where('a.date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('af.Accepted', 'No');
        $this->db->select("ag.emp_id,a.id,a.score,a.date,ag.emp_id,ag.name,ag.process,ag.gender,ag.batch_no,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,eee.emp_name as auditByName");
        $this->db->from('agentfeedback as af');
        $this->db->join('audit as a', 'a.id = af.audit_id');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
          $this->db->join('employee as eee', 'eee.emp_id = a.audit_by');
         $this->db->join('employee as e', 'e.emp_id = ag.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = ag.employee_id_AM');
        return$this->db->get()->result();
    }

    function get_All_audit_by($date_fromf = '', $date_tof = '', $audit_by = '', $Calling_Number = '') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.CLI_Number');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
         $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        if ($id != '') {
            // $this->db->where('a.audit_by', $id);
        }
         if ($Calling_Number == '') {
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }else{
             $this->db->where('MONTH(a.date)', date('m'));
         }
        }
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
//         if ($Calling_Number != '') {
//             $where = "ar.Calling_Number=$Calling_Number OR ar.CLI_Number='$Calling_Number'";
//            $this->db->or_where($where);
//        }
         if(!empty($Calling_Number)) {
        $this->db->group_start();
        $this->db->like('ar.Calling_Number', $Calling_Number);
        $this->db->or_like('ar.CLI_Number', $Calling_Number);
        $this->db->group_end();
    }
        
        
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
    
    function get_All_audit_by_tl($date_fromf = '', $date_tof = '', $audit_by = '') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        if ($id != '') {
            // $this->db->where('a.audit_by', $id);
        }
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
      function get_All_audit_by_AM($date_fromf = '', $date_tof = '', $audit_by = '') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM');
        $this->db->from('audit_am as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        if ($id != '') {
            // $this->db->where('a.audit_by', $id);
        }
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }

    function get_audit_by($date_fromf = '', $date_tof = '',$Calling_Number = '') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        if ($id != '') {
            $this->db->where('a.audit_by', $id);
        }
          if ($Calling_Number == '') {
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
          }
//         if ($Calling_Number != '') {
//             $where = "ar.CLI_Number='$Calling_Number'";
//            $this->db->where($where);
//        }
         if(!empty($Calling_Number)) {
        $this->db->group_start();
        $this->db->like('ar.Calling_Number', $Calling_Number);
        $this->db->or_like('ar.CLI_Number', $Calling_Number);
        $this->db->group_end();
    }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
     function get_audit_by_tl($date_fromf = '', $date_tof = '',$Calling_Number='') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
         $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        if ($id != '') {
            $this->db->where('a.audit_by', $id);
        }
         if ($Calling_Number == '') {
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
         }
//        if ($Calling_Number != '') {
//             $where = "ar.CLI_Number='$Calling_Number'";
//            $this->db->where($where);
//        }
         if(!empty($Calling_Number)) {
        $this->db->group_start();
        $this->db->like('ar.Calling_Number', $Calling_Number);
        $this->db->or_like('ar.CLI_Number', $Calling_Number);
        $this->db->group_end();
    }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }

    function get_audit_byid($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.audit_by,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
     function get_audit_byid_supervises($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.audit_by,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
    
     function get_audit_byid_tl_tl($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.audit_by,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks_tl as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
    
     function get_audit_byid_of_am($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.score,a.audit_by,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit_am as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks_am as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
     function get_audit_byid_am($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit_am as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks_am as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
    
    
     function get_audit_byid_TL($id = '') {
//           $id=$this->session->userdata('reg_id');
        $this->db->select('a.id,a.agents_id,a.audit_by,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,
                ar.Call_Date,ar.Time_Of_Call,ar.Calling_Number,ar.CLI_Number,ar.Call_Dur_Min,ar.Call_Dur_Sec,ar.Call_Type,ar.Todays_Audit_Count,ar.category_main_id,
ar.category_sub_id,ar.category_main_id_crr,ar.category_sub_id_crr,ar.Consumers_Concern,ar.r_g_y_a,ar.QME_Remarks,
ar.p_s_if_a,ar.t_d_b_a,ar.c_t_a_p_p,ar.Opening,ar.ActiveListening,ar.Probing,ar.Customer_Engagement,ar.Empathy_where_required,
ar.Understanding,ar.Professionalism,ar.Politeness,ar.Hold_Procedure,ar.Closing,ar.Correct_smaller,ar.Accurate_Complete,
ar.Fatal_Reason,ar.date,ar.status,ar.feedback_status_id
                 ');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('audit_remarks_tl as ar', 'ar.audit_id = a.id');

        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }

    function get_audit_byidAll($audit_by = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
             ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(ar.Call_Date,ag.DOJ) as AON,
             ag.process,
             ag.batch_no,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
             ar.Consumers_Concern,
             ar.r_g_y_a,
            ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            ar.feedback_status_id,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
                      
                 ');
        $this->db->from('audit as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        // $this->db->where('a.audit_by', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
    function get_audit_byidAll_tl($audit_by = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
             ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ag.batch_no,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
             ar.Consumers_Concern,
             ar.r_g_y_a,
            ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            ar.feedback_status_id,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
                      
                 ');
        $this->db->from('audit_tl as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_tl as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        // $this->db->where('a.audit_by', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
    
     function get_audit_byidAll_AM($audit_by = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
             ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             edp.emp_name as audit_parent_name,
             ag.DOJ,
             DATEDIFF(a.date,ag.DOJ) as AON,
             ag.process,
             ag.batch_no,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
             ar.Consumers_Concern,
             ar.r_g_y_a,
            ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            ar.feedback_status_id,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
                      
                 ');
        $this->db->from('audit_am as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('employee as edp', 'edp.emp_id = a.audit_parent_id');
        $this->db->join('agents as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_am as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        // $this->db->where('a.audit_by', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }

    function get_count_bycallDuration($start_time, $end_time) {
        $this->db->select('count(id) as total');
        $this->db->from('audit');
        $audit_by = $this->session->userdata('reg_id');
        if ($this->session->userdata('category') != '1') {
            $this->db->where('audit_by', $audit_by);
        }
        $this->db->where('callDuration >= ', $start_time);
        $this->db->where('callDuration <= ', $end_time);
        $this->db->where('YEAR(date)', date('Y'));
        $this->db->where('MONTH(date)', date('m'));
        return $this->db->get()->row();
    }
    
    
     function get_All_audit_by_supervises($date_fromf = '', $date_tof = '', $audit_by = '', $Calling_Number = '') {
        $id = '';
        $id = $this->session->userdata('reg_id');
        $this->db->select('a.id,a.score,a.date,ag.emp_id,ag.name,ag.DOJ,ag.process,ag.gender,ag.batch_no,a.employee_id_TL as TL_id,a.employee_id_AM as AM_id,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,ar.CLI_Number');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
         $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');
        if ($id != '') {
            // $this->db->where('a.audit_by', $id);
        }
         if ($Calling_Number == '') {
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }else{
             $this->db->where('MONTH(a.date)', date('m'));
         }
        }
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
//         if ($Calling_Number != '') {
//             $where = "ar.Calling_Number=$Calling_Number OR ar.CLI_Number='$Calling_Number'";
//            $this->db->or_where($where);
//        }
         if(!empty($Calling_Number)) {
        $this->db->group_start();
        $this->db->like('ar.Calling_Number', $Calling_Number);
        $this->db->or_like('ar.CLI_Number', $Calling_Number);
        $this->db->group_end();
    }
        
        
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
    
     function get_audit_byidAll_Supervises($audit_by = '', $date_fromf = '', $date_tof = '') {
        $this->db->select('a.id,a.audit_time,
             ed.msd_ID as empMSDID,
             ed.emp_name as audit_by,
             ag.emp_id,
             ag.name,
             e.emp_name as employee_id_TL,
             ee.emp_name as employee_id_AM,
             ag.DOJ,
             DATEDIFF(ar.Call_Date,ag.DOJ) as AON,
             ag.process,
             ag.batch_no,
             ar.Call_Date,
             ar.Time_Of_Call,
             ar.Calling_Number,
             ar.CLI_Number,
             ar.Call_Dur_Min,
             ar.Call_Dur_Sec,
             a.date,
             cm.category_main_name,
             cs.category_sub_name,
             cm_crr.category_main_name as category_main_id_crr,
             cs_crr.category_sub_name as category_sub_id_crr,
             ar.Consumers_Concern,
             ar.r_g_y_a,
            ar.QME_Remarks,
            ar.p_s_if_a,
            ar.t_d_b_a,
            ar.c_t_a_p_p,
            ar.Fatal_Reason,
            IF(ar.Opening = "0", "GREEN",ar.Opening) as Opening,
            IF(ar.ActiveListening = "0", "GREEN",ar.ActiveListening) as ActiveListening,
            IF(ar.Probing = "0", "GREEN",ar.Probing) as Probing,
            IF(ar.Customer_Engagement = "0", "GREEN",ar.Customer_Engagement) as Customer_Engagement,
            IF(ar.Empathy_where_required = "0", "GREEN", ar.Empathy_where_required) as Empathy_where_required,
            IF(ar.Understanding = "0", "GREEN",ar.Understanding) as Understanding,
            IF(ar.Professionalism = "0", "GREEN",ar.Professionalism) as Professionalism,
            IF(ar.Politeness = "0", "GREEN",ar.Politeness) as Politeness,
            IF(ar.Hold_Procedure = "0", "GREEN",ar.Hold_Procedure) as Hold_Procedure,
            IF(ar.Closing = "0", "GREEN",ar.Closing) as Closing,
            
            IF(ar.Opening = "0", 10,0) as OpeningScored,
            IF(ar.ActiveListening = "0",6,0) as ActiveListeningScored,
            IF(ar.Probing = "0",10,0) as ProbingScored,
            IF(ar.Customer_Engagement = "0",6,0) as Customer_EngagementScored,
            IF(ar.Empathy_where_required = "0",5,0) as Empathy_where_requiredScored,
            IF(ar.Understanding = "0",6,0) as UnderstandingScored,
            IF(ar.Professionalism = "0",10,0) as ProfessionalismScored,
            IF(ar.Politeness = "0",10,0) as PolitenessScored,
            IF(ar.Hold_Procedure = "0",5,0) as Hold_ProcedureScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            IF(ar.Closing = "0",7,0) as ClosingScored,
            ar.Correct_smaller,
            ar.Accurate_Complete,
            ar.Fatal_Reason,
            ar.feedback_status_id,
            a.score,
            sf.name as shift_name,
            ct.name as call_type
                      
                 ');
        $this->db->from('audit_supervises as a');
        $this->db->join('employee as e', 'e.emp_id = a.employee_id_TL');
        $this->db->join('employee as ee', 'ee.emp_id = a.employee_id_AM');
        $this->db->join('employee as ed', 'ed.emp_id = a.audit_by');
        $this->db->join('supervises as ag', 'ag.id = a.agents_id');
        $this->db->join('shifts as sf', 'sf.id = ag.shift_id');
        $this->db->join('audit_remarks_supervises as ar', 'ar.audit_id = a.id');
        $this->db->join('category_main as cm', 'cm.category_main_id = ar.category_main_id');
        $this->db->join('category_sub as cs', 'cs.category_sub_id = ar.category_sub_id');
        $this->db->join('category_main as cm_crr', 'cm_crr.category_main_id = ar.category_main_id_crr');
        $this->db->join('category_sub as cs_crr', 'cs_crr.category_sub_id = ar.category_sub_id_crr');
        $this->db->join('call_types as ct', 'ct.id = ar.Call_Type');
        if ($audit_by != '') {
            $this->db->where('a.audit_by', $audit_by);
        }
        // $this->db->where('a.audit_by', $id);
        if ($date_fromf != '') {
            $this->db->where('a.date >=', $date_fromf);
            $this->db->where('a.date <=', $date_tof);
        }
        $this->db->order_by('a.id', 'DESC');
        return $this->db->get()->result();
    }
    
     function update_supervises($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update('audit_supervises', $data);
          return $id;
    }
    
    
      function api_count_week_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        //$currdateyear = date('Y');
        // $currdatemnth = date('m');
        // $currdatemnth = '09';
        // $where = "WHERE YEAR(a.date)='$currdateyear' AND MONTH(a.date)='$currdatemnth'";
       // $query = $this->db->query("SELECT WEEK(NOW())as week, a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY WEEK(ADDDATE( a.date , WEEKDAY(NOW()) ))");
         $query = $this->db->query("SELECT MONTH(a.date) AS monthof,WEEK(a.date)as week,date(a.date) AS dateof,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able ,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av , SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY week ORDER BY a.date");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_AM_WISES($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av , SUM(a.score) as totalscore,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_AM $where GROUP BY a.employee_id_AM");
        $rows = $query->result();
        return $rows;
    }
     function api_count_shiftwise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT ag.shift_id,s.name as shift_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av  ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id INNER JOIN shifts as s ON s.id = ag.shift_id $where GROUP BY ag.shift_id");
        $rows = $query->result();
        return $rows;
    }
     function api_count_Category_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT cm.category_main_name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able, ROUND(((SUM(score)/SUM(score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN category_main as cm ON cm.category_main_id =ar.category_main_id $where GROUP BY ar.category_main_id");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_TL_WISES($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name, COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id=a.employee_id_TL $where GROUP BY a.employee_id_TL");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_Tenure_wise_0_30($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }

        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >=0 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 30");
        $rows = $query->row();
        return $rows;
    }

    function api_count_Tenure_wise_90($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND  DATEDIFF(ar.Call_Date,ag.DOJ) > 90");
        $rows = $query->row();
        return $rows;
    }

    function api_count_Tenure_wise_30_60($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id  $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >= 31 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 60");
        $rows = $query->row();
        return $rows;
    }

    function api_count_Tenure_wise_60_90($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,  SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN agents as ag ON ag.id=a.agents_id $where AND DATEDIFF(ar.Call_Date,ag.DOJ) >=61 AND DATEDIFF(ar.Call_Date,ag.DOJ) <= 90");
        $rows = $query->row();
        return $rows;
    }
      function api_count_date_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT a.date as day,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id $where GROUP BY YEAR(a.date),MONTH(a.date),DAY(a.date)");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_Auditor_Wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";

        $designation ='';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {

            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT e.emp_name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(a.score)/SUM(a.score_able))*100) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN employee as e ON e.emp_id =a.audit_by $where GROUP BY a.audit_by");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_Call_type_wise($date_fromf = '', $date_tof = '', $audit_by = '') {
        $where = "";
        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $where = "WHERE a.date >='$date_fromf' AND a.date <= '$date_tof'";
        } else {
            $currdate = date('Y-m-d');
            $where = $where . "WHERE a.date ='$currdate'";
        }

        if ($audit_by != '' && $designation != '1' && $designation != '4') {
            $where = $where . " AND a.audit_by =$audit_by";
        }
        $query = $this->db->query("SELECT ct.name,COUNT(a.id) as total,AVG(a.score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,ROUND(((SUM(a.score)/SUM(a.score_able))*100),2) as av ,SUM(IF(ar.Fatal_Reason!=0,1,0)) AS fatal FROM audit a INNER JOIN audit_remarks as ar ON ar.audit_id=a.id INNER JOIN call_types as ct ON ct.id =ar.Call_Type $where GROUP BY ar.Call_Type");
        $rows = $query->result();
        return $rows;
    }
    
     function api_count_audit_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {
        $designation = '';
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date >=', $date_fromf);
            $this->db->where('date <=', $date_tof);
        } else {
            $this->db->where('date', date('Y-m-d'));
        }
        $designation = $this->session->userdata('designation');
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('audit_by', $audit_by);
//        }

//        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('employee_id_AM', $audit_by);
//        }

        return $this->db->from($this->table)->count_all_results();
    }
    
      function api_count_auditFatal_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('ar.date >=', $date_fromf);
            $this->db->where('ar.date <=', $date_tof);
        } else {
            $this->db->where('ar.date', date('Y-m-d'));
        }
        $designation = '';
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('a.audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('a.employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('a.employee_id_AM', $audit_by);
//        }

        $this->db->where('ar.Fatal_Reason!=', '0');


        $this->db->select("COUNT(ar.Fatal_Reason) as total");

        $this->db->from('audit_remarks as ar');
        $this->db->join('audit as a', 'a.id = ar.audit_id');
        return$this->db->get()->row();
    }
    
      function api_count_auditScore_Allbydate($date_fromf = '', $date_tof = '', $audit_by = '') {

        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date >=', $date_fromf);
            $this->db->where('date <=', $date_tof);
        } else {
            $this->db->where('date', date('Y-m-d'));
        }
        $designation = '';
//        if ($audit_by != '' && $designation != '1' && $designation != '4') {
//            $this->db->where('audit_by', $audit_by);
//        }

        //        BY TL id LOGIN
//        if ($this->session->userdata('designation') == '1') {
//            $this->db->where('employee_id_TL', $audit_by);
//        }
        //        BY AM id LOGIN
//        if ($this->session->userdata('designation') == '4') {
//            $this->db->where('employee_id_AM', $audit_by);
//        }

        $this->db->select("AVG(score) AS score,SUM(score)as t_score,SUM(score_able) as score_able,CEIL((SUM(score)/SUM(score_able))*100) as av ");

        $this->db->from('audit');
        return$this->db->get()->row();
    }
    

}

/* End of file Audit_model.php */
/* Location: ./application/models/Audit_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-08-06 08:47:06 */
