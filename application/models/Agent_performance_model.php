<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_performance_model extends CI_Model
{

    public $table = 'agent_performance';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
     function get_all_bydate($date_fromf = '', $date_tof = '')
    {
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date_of >=', $date_fromf);
            $this->db->where('date_of <=', $date_tof);
        }else{
           //  $this->db->where('MONTH(date_of)', date('m'));
         }
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
      function get_all_bydate_delete($date_fromf = '', $date_tof = '')
    {
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date_of >=', $date_fromf);
            $this->db->where('date_of <=', $date_tof);
        }
      $this->db->delete($this->table);
       
    }
    
      function get_all_bydateagent($date_fromf = '', $date_tof = '')
    {
           $id = $this->session->userdata('id');
            $this->db->where('agents_id',$id);
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date_of >=', $date_fromf);
            $this->db->where('date_of <=', $date_tof);
        }else{
             $this->db->where('MONTH(date_of)', date('m'));
         }
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    //SELECT MAX(date_of) FROM `agent_performance`
     function get_max_date() {
        $this->db->select('MAX(date_of) as date_of');
        $this->db->from($this->table);
        return $this->db->get()->row();
    }
     function get_all_bydateagent_TL($date_fromf = '', $date_tof = '',$tags ='',$date_of='')
    {
         $where = "agents_id IN($tags)";
         $this->db->where($where);
           // $this->db->where('agents_id',$id);
           if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('date_of >=', $date_fromf);
            $this->db->where('date_of <=', $date_tof);
        }else{
             $this->db->where('date_of',$date_of);
         }
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    
     function agentPerformancebyQA_AVG_AM($date_fromf = '', $date_tof = '',$date_of='') {
         $where='e.status=1 ';
          if ($date_fromf != '' && $date_tof != '') {
           
            $where.=" AND ap.date_of >='$date_fromf' AND ap.date_of <='$date_tof'";
        }else{
             $where.="AND ap.date_of ='$date_of'";
           
         }
       $query = $this->db->query("SELECT
count(*) as records,
e.msd_ID as msd,e.emp_name as agents_msd,
ap.date_of,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_StaffedDuration,NULL)))),'%H:%i:%s') as Inbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_ReadyDuration,NULL)))),'%H:%i:%s') as Inbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_BreakDuration,NULL)))),'%H:%i:%s') as Inbound_BreakDuration,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration > 0,ap.Inbound_IdleTime,NULL)),2) as Inbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_HoldTime,NULL)))),'%H:%i:%s') as Inbound_HoldTime,
SUM(ap.Inbound_NumberOfcallsOffered) as Inbound_NumberOfcallsOffered,
SUM(ap.Inbound_NumberofCallanswered) as Inbound_NumberofCallanswered,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AbandonedPer,NULL)),2) as Inbound_AbandonedPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AHT,NULL)))),'%H:%i:%s') as Inbound_AHT,

TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_StaffedDuration,NULL)))),'%H:%i:%s') as Outbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ReadyDuration,NULL)))),'%H:%i:%s') as Outbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_BreakDuration,NULL)))),'%H:%i:%s') as Outbound_BreakDuration,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_IdleTime,NULL)),2) as Outbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_HoldTime,NULL)))),'%H:%i:%s') as Outbound_HoldTime,
SUM(ap.Outbound_NumberOfCallsDial) as Outbound_NumberOfCallsDial,
SUM(ap.Outbound_NumberofCallsconnected) as Outbound_NumberofCallsconnected,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ConnectivityPer,NULL)),2) as Outbound_ConnectivityPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_AHT,NULL)))),'%H:%i:%s') as Outbound_AHT

FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_AM
WHERE $where GROUP BY e.emp_id");

       return  $query->result();
    }
    
    
     function agentPerformancebyQA_AVG_TL($date_fromf = '',$date_tof = '',$date_of='',$employee_id_AM='' ) {
         $where='e.status=1 ';
           if ($employee_id_AM != '') {
           
            $where.=" AND a.employee_id_AM = $employee_id_AM";
        }
          if ($date_fromf != '' && $date_tof != '') {
           
            $where.=" AND ap.date_of >='$date_fromf' AND ap.date_of <='$date_tof'";
        }else{
             $where.=" AND ap.date_of ='$date_of'";
           
         }
       $query = $this->db->query("SELECT
count(*) as records,
e.msd_ID as msd,e.emp_name as agents_msd,
ap.date_of,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Inbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Inbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Inbound_BreakDuration,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration > 0,ap.Inbound_IdleTime,0)),2) as Inbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Inbound_HoldTime,
SUM(ap.Inbound_NumberOfcallsOffered) as Inbound_NumberOfcallsOffered,
SUM(ap.Inbound_NumberofCallanswered) as Inbound_NumberofCallanswered,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AbandonedPer,0)),2) as Inbound_AbandonedPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AHT,NULL)))),'%H:%i:%s') as Inbound_AHT,

TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Outbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Outbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Outbound_BreakDuration,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_IdleTime,0)),2) as Outbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Outbound_HoldTime,
SUM(ap.Outbound_NumberOfCallsDial) as Outbound_NumberOfCallsDial,
SUM(ap.Outbound_NumberofCallsconnected) as Outbound_NumberofCallsconnected,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ConnectivityPer,'00:00:00')),2) as Outbound_ConnectivityPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_AHT,NULL)))),'%H:%i:%s') as Outbound_AHT

FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_TL
WHERE $where GROUP BY e.emp_id");

       return  $query->result();
    }
 function agentPerformancebyQA_AVG_agentswise($date_fromf = '',$date_tof = '',$date_of='',$employee_id_TL='' ) {
         $where='e.status=1 ';
           if ($employee_id_TL != '') {
           
            $where.=" AND a.employee_id_TL = $employee_id_TL";
        }
          if ($date_fromf != '' && $date_tof != '') {
           
            $where.=" AND ap.date_of >='$date_fromf' AND ap.date_of <='$date_tof'";
        }else{
             $where.=" AND ap.date_of ='$date_of'";
           
         }
       $query = $this->db->query("SELECT
count(*) as records,
a.emp_id as msd,a.name as agents_msd,
ap.date_of,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Inbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Inbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Inbound_BreakDuration,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration > 0,ap.Inbound_IdleTime,NULL)),2) as Inbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Inbound_HoldTime,
SUM(ap.Inbound_NumberOfcallsOffered) as Inbound_NumberOfcallsOffered,
SUM(ap.Inbound_NumberofCallanswered) as Inbound_NumberofCallanswered,
FORMAT(AVG(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AbandonedPer,NULL)),2) as Inbound_AbandonedPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Inbound_ReadyDuration>0,ap.Inbound_AHT,'00:00:00')))),'%H:%i:%s') as Inbound_AHT,

TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_StaffedDuration,'00:00:00')))),'%H:%i:%s') as Outbound_StaffedDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ReadyDuration,'00:00:00')))),'%H:%i:%s') as Outbound_ReadyDuration,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_BreakDuration,'00:00:00')))),'%H:%i:%s') as Outbound_BreakDuration,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_IdleTime,NULL)),2) as Outbound_IdleTime,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_HoldTime,'00:00:00')))),'%H:%i:%s') as Outbound_HoldTime,
SUM(ap.Outbound_NumberOfCallsDial) as Outbound_NumberOfCallsDial,
SUM(ap.Outbound_NumberofCallsconnected) as Outbound_NumberofCallsconnected,
FORMAT(AVG(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_ConnectivityPer,NULL)),2) as Outbound_ConnectivityPer,
TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(IF(ap.Outbound_ReadyDuration>0,ap.Outbound_AHT,'00:00:00')))),'%H:%i:%s') as Outbound_AHT

FROM agent_performance as ap
INNER JOIN agents as a ON a.id=ap.agents_id
INNER JOIN employee as e ON e.emp_id=a.employee_id_TL
WHERE $where GROUP BY a.id");

       return  $query->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('agents_msd', $q);
	$this->db->or_like('date_of', $q);
	$this->db->or_like('Inbound_StaffedDuration', $q);
	$this->db->or_like('Inbound_ReadyDuration', $q);
	$this->db->or_like('Inbound_BreakDuration', $q);
	$this->db->or_like('Inbound_IdleTime', $q);
	$this->db->or_like('Inbound_HoldTime', $q);
	$this->db->or_like('Inbound_NumberOfcallsOffered', $q);
	$this->db->or_like('Inbound_NumberofCallanswered', $q);
	$this->db->or_like('Inbound_AbandonedPer', $q);
	$this->db->or_like('Inbound_AHT', $q);
	$this->db->or_like('Outbound_StaffedDuration', $q);
	$this->db->or_like('Outbound_ReadyDuration', $q);
	$this->db->or_like('Outbound_BreakDuration', $q);
	$this->db->or_like('Outbound_IdleTime', $q);
	$this->db->or_like('Outbound_HoldTime', $q);
	$this->db->or_like('Outbound_NumberOfCallsDial', $q);
	$this->db->or_like('Outbound_NumberofCallsconnected', $q);
	$this->db->or_like('Outbound_ConnectivityPer', $q);
	$this->db->or_like('Outbound_AHT', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('agents_msd', $q);
	$this->db->or_like('date_of', $q);
	$this->db->or_like('Inbound_StaffedDuration', $q);
	$this->db->or_like('Inbound_ReadyDuration', $q);
	$this->db->or_like('Inbound_BreakDuration', $q);
	$this->db->or_like('Inbound_IdleTime', $q);
	$this->db->or_like('Inbound_HoldTime', $q);
	$this->db->or_like('Inbound_NumberOfcallsOffered', $q);
	$this->db->or_like('Inbound_NumberofCallanswered', $q);
	$this->db->or_like('Inbound_AbandonedPer', $q);
	$this->db->or_like('Inbound_AHT', $q);
	$this->db->or_like('Outbound_StaffedDuration', $q);
	$this->db->or_like('Outbound_ReadyDuration', $q);
	$this->db->or_like('Outbound_BreakDuration', $q);
	$this->db->or_like('Outbound_IdleTime', $q);
	$this->db->or_like('Outbound_HoldTime', $q);
	$this->db->or_like('Outbound_NumberOfCallsDial', $q);
	$this->db->or_like('Outbound_NumberofCallsconnected', $q);
	$this->db->or_like('Outbound_ConnectivityPer', $q);
	$this->db->or_like('Outbound_AHT', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Agent_performance_model.php */
/* Location: ./application/models/Agent_performance_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2020-04-30 10:48:06 */
