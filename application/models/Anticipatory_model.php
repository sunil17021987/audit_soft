<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Anticipatory_model extends CI_Model
{

    public $table = 'anticipatory';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
     function get_all_by_agent_id($id=NULL,$date_fromf = '', $date_tof = '')
    {
         if($id!=''){
         $this->db->where('an.agents_id',$id);
         }
        if ($date_fromf != '' && $date_tof != '') {
            $this->db->where('DATE(an.issue_date_time) >=', $date_fromf);
            $this->db->where('DATE(an.issue_date_time) <=', $date_tof);
        }
        
        $this->db->select("an.id,CONCAT(ag.name,'(',ag.emp_id,')') as name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,eee.emp_name as approved_by,an.mobile_no,DATE_FORMAT(an.issue_start,'%D %b %h:%i:%p') as issue_start,DATE_FORMAT(an.issue_end,'%D %b %h:%i:%p') as issue_end,at.name as issue_type,an.issue_remarks,
          CASE an.is_approved
          WHEN '0' THEN 'Pending'
          WHEN '1' THEN 'No'
          WHEN '2' THEN 'Yes'
          ELSE 'Error'
          END as is_approved,
          an.extensionNo,
                an.approved_remarks,DATE_FORMAT(an.approved_date_time,'%D %b %h:%i:%p') as approved_date_time,DATE_FORMAT(an.issue_date_time,'%D %b %h:%i:%p') as issue_date_time");
        $this->db->from('anticipatory as an');
        $this->db->join('agents as ag', 'ag.id = an.agents_id','left');
        $this->db->join('employee as e', 'e.emp_id = an.employee_id_TL','left');
        $this->db->join('employee as ee', 'ee.emp_id = an.employee_id_AM','left');
        $this->db->join('employee as eee', 'eee.emp_id = an.approved_by','left');
        $this->db->join('anticipatory_status as at', 'at.id = an.issue_type','left');
        $this->db->order_by('an.issue_date_time', $this->order);
        return$this->db->get()->result();
    }
     function get_all_by_agent_id_inserted_id($lastId,$id)
    {
          $this->db->where('an.id',$lastId);
         $this->db->where('an.agents_id',$id);
        $this->db->select("an.id,CONCAT(ag.name,'(',ag.emp_id,')') as name,e.emp_name as employee_id_TL,ee.emp_name as employee_id_AM,eee.emp_name as approved_by,an.mobile_no,DATE_FORMAT(an.issue_start,'%D %b %h:%i:%p') as issue_start,DATE_FORMAT(an.issue_end,'%D %b %h:%i:%p') as issue_end,at.name as issue_type,an.issue_remarks,
            CASE an.is_approved
          WHEN '0' THEN 'Pending'
          WHEN '1' THEN 'No'
          WHEN '2' THEN 'Yes'
          ELSE 'Error'
          END as is_approved,
      an.approved_remarks,DATE_FORMAT(an.approved_date_time,'%D %b %h:%i:%p') as approved_date_time,DATE_FORMAT(an.issue_date_time,'%D %b %h:%i:%p') as issue_date_time");
      
        $this->db->from('anticipatory as an');
        $this->db->join('agents as ag', 'ag.id = an.agents_id','left');
        $this->db->join('employee as e', 'e.emp_id = an.employee_id_TL','left');
        $this->db->join('employee as ee', 'ee.emp_id = an.employee_id_AM','left');
        $this->db->join('employee as eee', 'eee.emp_id = an.approved_by','left');
        $this->db->join('anticipatory_status as at', 'at.id = an.issue_type','left');
       //  $this->db->order_by('an.issue_date_time', $this->order);
        return$this->db->get()->row();
    }

    // get data by id
    
     function get_pendings_count()
    {
        $this->db->where('is_approved','0');
         $this->db->from($this->table);
          return $this->db->count_all_results();
    }
    
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('employee_id_AM', $q);
	$this->db->or_like('employee_id_TL', $q);
	$this->db->or_like('mobile_no', $q);
	$this->db->or_like('issue_start', $q);
	$this->db->or_like('issue_end', $q);
	$this->db->or_like('issue_type', $q);
	$this->db->or_like('issue_remarks', $q);
	$this->db->or_like('approved_by', $q);
	$this->db->or_like('is_approved', $q);
	$this->db->or_like('approved_remarks', $q);
	$this->db->or_like('approved_date_time', $q);
	$this->db->or_like('issue_date_time', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('employee_id_AM', $q);
	$this->db->or_like('employee_id_TL', $q);
	$this->db->or_like('mobile_no', $q);
	$this->db->or_like('issue_start', $q);
	$this->db->or_like('issue_end', $q);
	$this->db->or_like('issue_type', $q);
	$this->db->or_like('issue_remarks', $q);
	$this->db->or_like('approved_by', $q);
	$this->db->or_like('is_approved', $q);
	$this->db->or_like('approved_remarks', $q);
	$this->db->or_like('approved_date_time', $q);
	$this->db->or_like('issue_date_time', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
        $lastId = $this->db->insert_id();
        return $lastId;
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Anticipatory_model.php */
/* Location: ./application/models/Anticipatory_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2020-08-06 07:41:02 */
