<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Audit_remarks_model extends CI_Model
{

    public $table = 'audit_remarks';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get data by id
    function get_by_audit_id($id)
    {  $this->db->select('feedback_status_id');
        $this->db->where('audit_id', $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('audit_id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('employee_id_TL', $q);
	$this->db->or_like('employee_id_AM', $q);
	$this->db->or_like('Call_Date', $q);
	$this->db->or_like('Time_Of_Call', $q);
	$this->db->or_like('Calling_Number', $q);
	$this->db->or_like('CLI_Number', $q);
	$this->db->or_like('Call_Dur_Min', $q);
	$this->db->or_like('Call_Dur_Sec', $q);
	$this->db->or_like('Call_Type', $q);
	$this->db->or_like('Todays_Audit_Count', $q);
	$this->db->or_like('category_main_id', $q);
	$this->db->or_like('category_sub_id', $q);
	$this->db->or_like('category_main_id_crr', $q);
	$this->db->or_like('category_sub_id_crr', $q);
	$this->db->or_like('Consumers_Concern', $q);
	$this->db->or_like('r_g_y_a', $q);
	$this->db->or_like('QME_Remarks', $q);
	$this->db->or_like('p_s_if_a', $q);
	$this->db->or_like('c_t_a_p_p', $q);
	$this->db->or_like('Opening', $q);
	$this->db->or_like('ActiveListening', $q);
	$this->db->or_like('Probing', $q);
	$this->db->or_like('Customer_Engagement', $q);
	$this->db->or_like('Empathy_where_required', $q);
	$this->db->or_like('Understanding', $q);
	$this->db->or_like('Professionalism', $q);
	$this->db->or_like('Politeness', $q);
	$this->db->or_like('Hold_Procedure', $q);
	$this->db->or_like('Closing', $q);
	$this->db->or_like('Correct_smaller', $q);
	$this->db->or_like('Accurate_Complete', $q);
	$this->db->or_like('Fatal_Reason', $q);
	$this->db->or_like('date', $q);
	$this->db->or_like('status', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('audit_id', $q);
	$this->db->or_like('agents_id', $q);
	$this->db->or_like('employee_id_TL', $q);
	$this->db->or_like('employee_id_AM', $q);
	$this->db->or_like('Call_Date', $q);
	$this->db->or_like('Time_Of_Call', $q);
	$this->db->or_like('Calling_Number', $q);
	$this->db->or_like('CLI_Number', $q);
	$this->db->or_like('Call_Dur_Min', $q);
	$this->db->or_like('Call_Dur_Sec', $q);
	$this->db->or_like('Call_Type', $q);
	$this->db->or_like('Todays_Audit_Count', $q);
	$this->db->or_like('category_main_id', $q);
	$this->db->or_like('category_sub_id', $q);
	$this->db->or_like('category_main_id_crr', $q);
	$this->db->or_like('category_sub_id_crr', $q);
	$this->db->or_like('Consumers_Concern', $q);
	$this->db->or_like('r_g_y_a', $q);
	$this->db->or_like('QME_Remarks', $q);
	$this->db->or_like('p_s_if_a', $q);
	$this->db->or_like('c_t_a_p_p', $q);
	$this->db->or_like('Opening', $q);
	$this->db->or_like('ActiveListening', $q);
	$this->db->or_like('Probing', $q);
	$this->db->or_like('Customer_Engagement', $q);
	$this->db->or_like('Empathy_where_required', $q);
	$this->db->or_like('Understanding', $q);
	$this->db->or_like('Professionalism', $q);
	$this->db->or_like('Politeness', $q);
	$this->db->or_like('Hold_Procedure', $q);
	$this->db->or_like('Closing', $q);
	$this->db->or_like('Correct_smaller', $q);
	$this->db->or_like('Accurate_Complete', $q);
	$this->db->or_like('Fatal_Reason', $q);
	$this->db->or_like('date', $q);
	$this->db->or_like('status', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
         $insert_id = $this->db->insert_id();
        return $insert_id;
    }
     function insert_supervises($data)
    {
        $this->db->insert('audit_remarks_supervises', $data);
         $insert_id = $this->db->insert_id();
        return $insert_id;
    }
     function insert_tl($data)
    {
        $this->db->insert('audit_remarks_tl', $data);
         $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function insert_am($data)
    {
        $this->db->insert('audit_remarks_am', $data);
         $insert_id = $this->db->insert_id();
        return $insert_id;
    }

     function update_by_audit_id($id, $data)
    {
        $this->db->where('audit_id', $id);
        $this->db->update($this->table, $data);
    }
    
     function update_by_audit_id_of_am($id, $data)
    {
        $this->db->where('audit_id', $id);
        $this->db->update('audit_remarks_am', $data);
    }
     function update_by_audit_id_of_tl($id, $data)
    {
        $this->db->where('audit_id', $id);
        $this->db->update('audit_remarks_tl', $data);
    }
    
    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    
    // get data by id
    function get_CLI_Number($id)
    {  $this->db->select('CLI_Number');
        $this->db->where('CLI_Number', $id);
        return $this->db->get($this->table)->row();
    }
     function get_cliduplicate_supervises($id)
    {  $this->db->select('CLI_Number');
        $this->db->where('CLI_Number', $id);
        return $this->db->get('audit_supervises')->row();
    }
    
    
     function update_by_audit_id_supervises($id, $data)
    {
        $this->db->where('audit_id', $id);
        $this->db->update('audit_remarks_supervises', $data);
    }
    

}

/* End of file Audit_remarks_model.php */
/* Location: ./application/models/Audit_remarks_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-08-06 08:47:35 */
