<?php
require APPPATH . 'libraries/REST_Controller.php';
class Qms extends REST_Controller {

    public function __construct() {
       parent::__construct();
      // $this->load->database();
         $this->load->model('Dashborad_model');
    }
    //uno / card ser/pay/ pos
    public function dashboard_post() {
        $week_wise='';
        $datef1=trim($this->input->post('from'));
         $datet1=trim($this->input->post('to'));
           $date_fromf = date('Y-m-d', strtotime($datef1));
           $date_tof = date('Y-m-d', strtotime($datet1));
         $audit_by='';
        
 $week_wise = $this->Audit_model->api_count_week_wise($date_fromf, $date_tof, $audit_by);
  $amwises = $this->Audit_model->api_count_AM_WISES($date_fromf, $date_tof, $audit_by);
   $shift_wise = $this->Audit_model->api_count_shiftwise($date_fromf, $date_tof, $audit_by);
     $category_wise = $this->Audit_model->api_count_Category_wise($date_fromf, $date_tof, $audit_by);
       $tl_wise = $this->Audit_model->api_count_TL_WISES($date_fromf, $date_tof, $audit_by);
      $tenure_wise_60_90= $this->Audit_model->api_count_Tenure_wise_60_90($date_fromf, $date_tof, $audit_by);
      $tenure_wise_30_60= $this->Audit_model->api_count_Tenure_wise_30_60($date_fromf, $date_tof, $audit_by);
      $tenure_wise_90= $this->Audit_model->api_count_Tenure_wise_90($date_fromf, $date_tof, $audit_by);
       $tenure_wise_0_30=$this->Audit_model->api_count_Tenure_wise_0_30($date_fromf, $date_tof, $audit_by);
      $audit_date_wise= $this->Audit_model->api_count_date_wise($date_fromf, $date_tof, $audit_by);
       $auditor_Wise=$this->Audit_model->api_count_Auditor_Wise($date_fromf, $date_tof, $audit_by);
      $call_type_wise= $this->Audit_model->api_count_Call_type_wise($date_fromf, $date_tof, $audit_by);
      $total_audits=$this->Audit_model->api_count_audit_Allbydate($date_fromf, $date_tof, $audit_by);
      $total_fatals= $this->Audit_model->api_count_auditFatal_Allbydate($date_fromf, $date_tof, $audit_by);
      $quality_score= $this->Audit_model->api_count_auditScore_Allbydate($date_fromf, $date_tof, $audit_by);
 // $data['query']= $this->db->last_query();
      
   $data['total_audits']=$total_audits;
    $data['total_fatals']=$total_fatals;
    $data['quality_score']=$quality_score;
  $data['week_wise']=$week_wise;
  $data['amwises']=$amwises;
   $data['shift_wise']=$shift_wise;
     $data['category_wise']=$category_wise;
      $data['tl_wise']=$tl_wise;
       $data['tenure_wise_60_90']=$tenure_wise_60_90;
        $data['tenure_wise_30_60']=$tenure_wise_30_60;
         $data['tenure_wise_90']=$tenure_wise_90;
          $data['tenure_wise_0_30']=$tenure_wise_0_30;
           $data['audit_date_wise']=$audit_date_wise;
            $data['auditor_Wise']=$auditor_Wise;
             $data['call_type_wise']=$call_type_wise;
            
           
 $this->response($data, REST_Controller::HTTP_OK);
        }
   
    
    
	public function employee_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("employee", ['emp_id' => $id])->row_array();
        }else{
            $data = $this->db->get("employee")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->input->post();
        $this->db->insert('items',$input);
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('items', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('items', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}