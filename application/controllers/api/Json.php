<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Json extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Register_model');
         $this->load->model('Audit_model');
         $this->load->model('Employee_model');
       // $this->load->library('form_validation');
    }

    public function index() {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'register/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'register/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'register/index.html';
            $config['first_url'] = base_url() . 'register/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Register_model->total_rows($q);
        $register = $this->Register_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'register_data' => $register,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('register/register_list', $data);
    }

    public function read($id) {
        $row = $this->Register_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'firstName' => $row->firstName,
                'email' => $row->email,
                'password' => $row->password,
                'confirmPassword' => $row->confirmPassword,
                'address' => $row->address,
                'skills' => $row->skills,
                'acceptTerms' => $row->acceptTerms,
                'dateof' => $row->dateof,
            );
            $this->load->view('register/register_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('register'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('register/create_action'),
            'id' => set_value('id'),
            'firstName' => set_value('firstName'),
            'email' => set_value('email'),
            'password' => set_value('password'),
            'confirmPassword' => set_value('confirmPassword'),
            'address' => set_value('address'),
            'skills' => set_value('skills'),
            'acceptTerms' => set_value('acceptTerms'),
            'dateof' => set_value('dateof'),
        );
        $this->load->view('register/register_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'firstName' => $this->input->post('firstName', TRUE),
                'email' => $this->input->post('email', TRUE),
                'password' => $this->input->post('password', TRUE),
                'confirmPassword' => $this->input->post('confirmPassword', TRUE),
                'address' => $this->input->post('address', TRUE),
                'skills' => $this->input->post('skills', TRUE),
                'acceptTerms' => $this->input->post('acceptTerms', TRUE),
                'dateof' => $this->input->post('dateof', TRUE),
            );

            $this->Register_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('register'));
        }
    }

    public function createRegister() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');

        $blogpost = json_decode(file_get_contents('php://input'), true);
      // echo$blogpost['firstName'];
      
       
        if (!empty($blogpost)) {
            $blogdata = array(
                'firstName' => $blogpost['firstName'],
                'email' => $blogpost['email'],
                'password' => $blogpost['password'],
                'confirmPassword' => $blogpost['confirmPassword'],
                'address' => $blogpost['address'],
                'skills' => $blogpost['skills'],
                'acceptTerms' => $blogpost['acceptTerms']
            );
            $data = array();
            $id = $this->Register_model->insert($blogdata);

            $data['record'] = $this->Register_model->get_by_id($id);
            $data['status'] = 'success';
        } else {
            $data = array();
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function getUsers() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');
         $data = array();
            $login = $this->Register_model->get_all();
            if (!empty($login)) {
                $data['record'] = $login;
                $data['status'] = true;
            } else {
                $data['record'] = '';
                $data['status'] = false;
            }
              $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function createLogin() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');

        $blogpost = json_decode(file_get_contents('php://input'), true);
        $login = '';
        if (!empty($blogpost)) {
            $blogdata = array(
                'email' => $blogpost['email'],
                'password' => $blogpost['password'],
            );
            $data = array();
            $login = $this->Register_model->dologin($blogpost['email'], $blogpost['password']);
            if (!empty($login)) {
                $data['record'] = $login;
                $data['status'] = true;
            } else {
                $data['record'] = '';
                $data['status'] = false;
            }
        } else {
            $data = array();
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function studentdelete() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');

        $id = json_decode(file_get_contents('php://input'), true);
      
        $login = '';
        if (!empty($id)) {
           
         $this->Register_model->delete($id);
        }
            $login = $this->Register_model->get_all();
            if (!empty($login)) {
                $data['record'] = $login;
                $data['status'] = true;
            } else {
                $data['record'] = '';
                $data['status'] = false;
            }
              $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }




    public function update($id) {
        $row = $this->Register_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('register/update_action'),
                'id' => set_value('id', $row->id),
                'firstName' => set_value('firstName', $row->firstName),
                'email' => set_value('email', $row->email),
                'password' => set_value('password', $row->password),
                'confirmPassword' => set_value('confirmPassword', $row->confirmPassword),
                'address' => set_value('address', $row->address),
                'skills' => set_value('skills', $row->skills),
                'acceptTerms' => set_value('acceptTerms', $row->acceptTerms),
                'dateof' => set_value('dateof', $row->dateof),
            );
            $this->load->view('register/register_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('register'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'firstName' => $this->input->post('firstName', TRUE),
                'email' => $this->input->post('email', TRUE),
                'password' => $this->input->post('password', TRUE),
                'confirmPassword' => $this->input->post('confirmPassword', TRUE),
                'address' => $this->input->post('address', TRUE),
                'skills' => $this->input->post('skills', TRUE),
                'acceptTerms' => $this->input->post('acceptTerms', TRUE),
                'dateof' => $this->input->post('dateof', TRUE),
            );

            $this->Register_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('register'));
        }
    }

    public function delete($id) {
         $data = array();
        $row = $this->Register_model->get_by_id($id);

        if ($row) {
            $this->Register_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('register'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('register'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('firstName', 'firstname', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'confirmpassword', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('skills', 'skills', 'trim|required');
        $this->form_validation->set_rules('acceptTerms', 'acceptterms', 'trim|required');
        $this->form_validation->set_rules('dateof', 'dateof', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
      public function tlQuality() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');

      
        
        $blogpost = json_decode(file_get_contents('php://input'), true);
        $login = '';
        $designation='';
        $employee_id_TL='';
        if (!empty($_GET['date'])) {
           
                 $date = date('Y-m-d', strtotime( $_GET['date']));
                 if( $_GET['msd']!='0' &&  $_GET['msd']!=''){
                   $msdid = $_GET['msd'];
                   $row = $this->Employee_model->get_by_id_msd_id($msdid);
                   $designation=$row->designation;
                   $employee_id_TL=$row->emp_id;
                  }
            $tlwises = $this->Audit_model->count_TL_WISES_API($date,$employee_id_TL,$designation);
           
                $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($tlwises);
                                    foreach ($tlwises as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    $amtotal;
                                    $tlwises_total_per = ($t_score != 0) ? ceil(($t_score / $score_able) * 100)  : '0';
             
             
             
             
             
        }
         if (!empty($tlwises_total_per)) {
                $data['record'] = $tlwises_total_per;
                $data['status'] = true;
            } else {
                $data['record'] = '';
                $data['status'] = false;
            }
         $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        
        
        
      }
    
    

}