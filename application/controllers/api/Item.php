<?php

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . '/libraries/CreatorJwt.php';

class Item extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
          $this->load->model('login_model');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->objOfJwt = new CreatorJwt();
        header('Content-Type: application/json');
    }

    public function loginToken_post() {
        
        $row = $this->login_model->validate();
        if ($row > '0') {
        $tokenData['emp_id'] = $row->emp_id;
        $tokenData['msd_ID'] = $row->msd_ID;
        $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
        $jwtToken = $this->objOfJwt->GenerateToken($tokenData);
        echo json_encode(array('Token' => $jwtToken));
         
         
        } else {
              echo json_encode(array('Token' =>"Authorization is faild !"));
        }
        
        
        

    }

    public function verify_post() {

        try {
            $received_Token = $this->input->request_headers();
            $jwtData = $this->objOfJwt->DecodeToken($received_Token['Authorization']);

            $data['records'] = $this->db->get("category")->result();
            $data['auth'] = $jwtData;


            $this->response($data, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            http_response_code('401');
            echo json_encode(array("status" => false, "message" => $e->getMessage()));
            exit;
        }
    }

    public function index_get($id = 0) {

        $received_Token = $this->input->request_headers('Authorization');
        $jwtData = $this->objOfJwt->DecodeToken($received_Token['Token']);
        echo json_encode($jwtData);
        die();
        try {

            if (!empty($id)) {
                $data['records'] = $this->db->get_where("category", ['category_id' => $id])->row_array();
            } else {
                $data['records'] = $this->db->get("category")->result();
                $data['auth'] = $received_Token;
            }

            $this->response($data, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            http_response_code('401');
            echo json_encode(array("status" => false, "message" => $e->getMessage()));
            exit;
        }
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_post() {
        $input = $this->input->post();
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            // show errors
            $this->response(array(
                "status" => 0,
                "message" => validation_errors(),
                "data" => ''
                    ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {


            $postdata = array(
                "category_name" => $this->security->xss_clean($this->input->post('category_name'))
            );
            // die();
            // $postdata= json_decode(file_get_contents("php://input"));
            if (isset($postdata)) {

//         $input=array(
//             "category_name"=>$postdata->category_name
//         );
                $this->db->insert('category', $postdata);
                $this->response(array(
                    "status" => 1,
                    "message" => "sucess",
                    "data" => $input
                        ), REST_Controller::HTTP_OK);
            } else {

                $this->response(array(
                    "status" => 0,
                    "message" => "error",
                    "data" => ''
                        ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('category_name', 'category_name', 'trim|required');
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_put($id) {
        $input = $this->put();
        $this->db->update('items', $input, array('id' => $id));

        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_delete($id) {
        $this->db->delete('items', array('id' => $id));

        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }

}
