<?php
if (!defined('BASEPATH'))
    exit('No Direct Script Access Allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Dashborad_model');
        $this->load->model('Designation_model');
        $this->load->model('Department_model');
        $this->load->model('Audit_model');
        $this->load->model('Employee_model');
        $this->load->model('Agents_model');
        $this->load->model('Outbound_callings_model');
        
        $this->check_auth();
    }

    public function check_auth() {
        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login'));
    }
    
    public function dashboard_outbound()
    {
           $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $employees = $this->Employee_model->get_all();
        $bydaterange = $this->input->post('bydaterange', TRUE);
        if ($this->session->userdata('category') == '1') {
            $audit_by = $this->input->post('audit_by', TRUE);
        } else {
            $audit_by = $this->session->userdata('reg_id');
        }


        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }

    
        $audits = $this->Outbound_callings_model->date_wise_count($date_fromf, $date_tof, $audit_by);
       $blockwises= $this->Outbound_callings_model->block_wise_count($date_fromf, $date_tof, $audit_by);

        $data = array(
            'audits' => $audits,
            'blockwises'=>$blockwises,
           
            'bydaterange' => $bydaterange,
            
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;


        if ($this->uri->segment(1) == 'dashboard') {
            $data['admission'] = "active";
        }

  
        $data['content'] = 'dash/home_outbound';
        $this->load->view('common/master', $data);   
    }
    

    public function index() {
        $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $employees = $this->Employee_model->get_all();
        $bydaterange = $this->input->post('bydaterange', TRUE);
        if ($this->session->userdata('category') == '1') {
            $audit_by = $this->input->post('audit_by', TRUE);
        } else {
            $audit_by = $this->session->userdata('reg_id');
        }


        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }

        $agents = $this->Agents_model->get_active_close();
       //  echo $this->db->last_query();
        $audits = $this->Audit_model->count_audit_Allbydate($date_fromf, $date_tof, $audit_by);
        $hundredperscre=$this->Audit_model->count_audit_hundrepercent($date_fromf, $date_tof, $audit_by);
        $fatals = $this->Audit_model->count_auditFatal_Allbydate($date_fromf, $date_tof, $audit_by);
        $noActin = $this->Audit_model->actionCloseButnotActionByAgents($date_fromf, $date_tof, $audit_by);
        // echo $this->db->last_query();


        $score = $this->Audit_model->count_auditScore_Allbydate($date_fromf, $date_tof, $audit_by);
         //echo $this->db->last_query();
        $feedback = $this->Audit_model->count_auditFeedback_Allbydate($date_fromf, $date_tof, $audit_by);
        $feedbackclose = $this->Audit_model->count_auditFeedbackclose_Allbydate($date_fromf, $date_tof, $audit_by);
        $fnotackbyCSA = $this->Audit_model->count_auditNotAck_CSA_Allbydate($date_fromf, $date_tof, $audit_by);

        $notAccepted = $this->Audit_model->count_auditNotAccepted($date_fromf, $date_tof, $audit_by);

        $auditsAm = $this->Audit_model->count_audit_AM($date_fromf, $date_tof, $audit_by);
        
        $auditsTl = $this->Audit_model->count_audit_TL($date_fromf, $date_tof, $audit_by);

        $allPengings = $this->Audit_model->getAllPengings($date_fromf, $date_tof, $audit_by);
        $totalAudits = $this->Audit_model->getAllTotalAudits($date_fromf, $date_tof, $audit_by);
         $totalAuditshundredrec = $this->Audit_model->getAllTotalAudits_hundreds($date_fromf, $date_tof, $audit_by);
       //  print_r($totalAuditshundred);
        $totalFatals = $this->Audit_model->getAllTotalFatals($date_fromf, $date_tof, $audit_by);


        $week_wise = $this->Audit_model->count_week_wise($date_fromf, $date_tof, $audit_by);
       // print_r($week_wise);
        // echo$this->db->last_query();
        $amwises = $this->Audit_model->count_AM_WISES($date_fromf, $date_tof, $audit_by);
       // echo $this->db->last_query();
        $shiftwises = $this->Audit_model->count_shiftwise($date_fromf, $date_tof, $audit_by);

        $date_wises = $this->Audit_model->count_date_wise($date_fromf, $date_tof, $audit_by);
        $tlwises = $this->Audit_model->count_TL_WISES($date_fromf, $date_tof, $audit_by);
        $count_Tenure_wise_0_30 = $this->Audit_model->count_Tenure_wise_0_30($date_fromf, $date_tof, $audit_by);
        $count_Tenure_wise_30_60 = $this->Audit_model->count_Tenure_wise_30_60($date_fromf, $date_tof, $audit_by);
       // echo$this->db->last_query();
        $count_Tenure_wise_90 = $this->Audit_model->count_Tenure_wise_90($date_fromf, $date_tof, $audit_by);
        
        $count_Tenure_wise_60_90 = $this->Audit_model->count_Tenure_wise_60_90($date_fromf, $date_tof, $audit_by);
       
        $count_Category_wise = $this->Audit_model->count_Category_wise($date_fromf, $date_tof, $audit_by);
       // echo$this->db->last_query();
        $count_Auditor_Wise = $this->Audit_model->count_Auditor_Wise($date_fromf, $date_tof, $audit_by);
        $count_Call_type_wise = $this->Audit_model->count_Call_type_wise($date_fromf, $date_tof, $audit_by);


        $data = array(
            'audits' => $audits,
            'hundredperscre'=>$hundredperscre,
            'bydaterange' => $bydaterange,
            'audit_by' => $audit_by,
            'employees' => $employees,
            'fatals' => $fatals->total,
            'score' => $score,
            'feedbackpending' => $feedback->total . '|' . $feedbackclose->total,
            'fnotackbyCSA' => count($fnotackbyCSA),
            'fnotackbyCSARec' => $fnotackbyCSA,
            'notAccepted' => count($notAccepted),
            'notAcceptedRec' => $notAccepted,
            'auditsAm' => $auditsAm,
            'auditsTl' => $auditsTl,
            'allPengings' => $allPengings,
            'week_wises' => $week_wise,
            'amwises' => $amwises,
            'shiftwises' => $shiftwises,
            'date_wises' => $date_wises,
            'tlwises' => $tlwises,
            'count_Tenure_wise_0_30' => $count_Tenure_wise_0_30,
            'count_Tenure_wise_30_60' => $count_Tenure_wise_30_60,
            'count_Tenure_wise_90' => $count_Tenure_wise_90,
            'count_Tenure_wise_60_90' => $count_Tenure_wise_60_90,
            'count_Category_wise' => $count_Category_wise,
            'count_Auditor_Wise' => $count_Auditor_Wise,
            'count_Call_type_wise' => $count_Call_type_wise,
            'totalAudits' => $totalAudits,
             'totalAuditshundredrec' => $totalAuditshundredrec ,
            'totalFatals' => $totalFatals,
            'noActin' => count($noActin),
            'noActinrecords' => $noActin,
            'agents'=>$agents
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;


        if ($this->uri->segment(1) == 'dashboard') {
            $data['admission'] = "active";
        }

        $data['callModelTotalAudits'] = 'dash/callModelTotalAudits';
        $data['totalAuditshundred']='dash/callModelTotalAudits_hundred';
        $data['callModelTotalFatals'] = 'dash/callModelTotalFatals';
        $data['addmodel'] = 'dash/allPengings';
        $data['modelnotAccepted'] = 'dash/callModelNotAccepted';
        $data['modelnotAcknologed'] = 'dash/callModelNotAcknologed';

        $data['callModelTotalNoActin'] = 'dash/callModelTotalNoActin';
        $data['content'] = 'dash/home';
        $this->load->view('common/master', $data);
    }

}

?>
