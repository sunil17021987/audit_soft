<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_sub extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Category_sub_model');
        $this->load->library('form_validation');
    }
    public function get_subcategories() {

        $category_sub_main_id = $_POST['category_main_id'];


        $records = $this->Category_sub_model->get_all_by_category_sub_main_id($category_sub_main_id);
        $strig = '<option value="">Select Sub Categories</option>';
        if ($records) {

            foreach ($records as $row) {
                $strig = $strig . '<option value="' . $row->category_sub_id . '">' . $row->category_sub_name . '</option>';
            }
        } else {
            
        }
        echo $strig;
    }
    public function index()
    {
        $category_sub = $this->Category_sub_model->get_all();

        $data = array(
            'category_sub_data' => $category_sub
        );

          $data['content'] = 'category_sub/category_sub_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Category_sub_model->get_by_id($id);
        if ($row) {
            $data = array(
		'category_sub_id' => $row->category_sub_id,
		'category_sub_main_id' => $row->category_sub_main_id,
		'category_sub_name' => $row->category_sub_name,
	    );
             $data['content'] = 'category_sub/category_sub_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('category_sub/create_action'),
	    'category_sub_id' => set_value('category_sub_id'),
	    'category_sub_main_id' => set_value('category_sub_main_id'),
	    'category_sub_name' => set_value('category_sub_name'),
	);
        $data['content'] = 'category_sub/category_sub_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'category_sub_main_id' => $this->input->post('category_sub_main_id',TRUE),
		'category_sub_name' => $this->input->post('category_sub_name',TRUE),
	    );

            $this->Category_sub_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('category_sub'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Category_sub_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('category_sub/update_action'),
		'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
		'category_sub_main_id' => set_value('category_sub_main_id', $row->category_sub_main_id),
		'category_sub_name' => set_value('category_sub_name', $row->category_sub_name),
	    );
            $data['content'] = 'category_sub/category_sub_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('category_sub_id', TRUE));
        } else {
            $data = array(
		'category_sub_main_id' => $this->input->post('category_sub_main_id',TRUE),
		'category_sub_name' => $this->input->post('category_sub_name',TRUE),
	    );

            $this->Category_sub_model->update($this->input->post('category_sub_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('category_sub'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Category_sub_model->get_by_id($id);

        if ($row) {
            $this->Category_sub_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('category_sub'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('category_sub_main_id', 'category sub main id', 'trim|required');
	$this->form_validation->set_rules('category_sub_name', 'category sub name', 'trim|required');

	$this->form_validation->set_rules('category_sub_id', 'category_sub_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "category_sub.xls";
        $judul = "category_sub";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Main Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Name");

	foreach ($this->Category_sub_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_main_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_name);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Category_sub.php */
/* Location: ./application/controllers/Category_sub.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2022-01-22 11:45:16 */
