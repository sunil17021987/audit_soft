<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('login_model');
        $this->load->library('session');
    }
public function check_auth()
    {
      
         $login_type = $this->session->userdata('validated');
         if($login_type == TRUE)
              redirect (base_url('dashboard'));
                    
    }
    public function index($msg = NULL) {
          $this->check_auth();
        $data['msg'] = $msg;
        $this->load->view('login', $data);
    }

    public function process() {
        $result = $this->login_model->validate();
        if ($result > '0') {
            $data=array('lastLogin'=>date('Y-m-d'));
            $this->login_model->update($result->emp_id, $data);
            redirect('dashboard');
        } else {
            $msg = '<font color=red>Invalid MSDID and/or password.</font><br />';
            $this->index($msg);
        }
    }
     public function agents($msg = NULL) {
          $this->check_auth();
        $data['msg'] = $msg;
        $this->load->view('agents', $data);
    }
    
     public function agentprocess() {
        $result = $this->login_model->agentvalidate();
        if ($result > '0') {
             $data=array('lastLogin'=>date('Y-m-d'));
            $this->login_model->updateAgent($result->id, $data);
            redirect('DashboardAgent');
        } else {
            $msg = '<font color=red>Invalid MSDID and/or password.</font><br />';
            $this->agents($msg);
        }
    }

      public function logout() {
        $this->session->unset_userdata('reg_id');
        $this->session->unset_userdata('emailid');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('validated');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('msd_ID');
         $this->session->unset_userdata('category');
          $this->session->unset_userdata('image');
        
        $this->session->sess_destroy();
        redirect('login');
    }
     public function logoutAgent() {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('emp_id');
        $this->session->unset_userdata('name');
      
        $this->session->sess_destroy();
        redirect('login/agents');
    }
    

}

?>