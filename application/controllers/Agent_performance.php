<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_performance extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Agent_performance_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $bydaterange = $this->input->post('bydaterange', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }else{
           $query= $this->Agent_performance_model->get_max_date();
           $date_fromf=$query->date_of;
           $date_tof=$query->date_of;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        if($this->input->post('delete', TRUE)=='delete'){
          $agent_performance = $this->Agent_performance_model->get_all_bydate_delete($date_fromf,$date_tof);
        }
         if($this->input->post('search', TRUE)=='search'){
           
        }
         $agent_performance = $this->Agent_performance_model->get_all_bydate($date_fromf,$date_tof);

        $data = array(
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'agent_performance/agent_performance_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Agent_performance_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'agents_id' => $row->agents_id,
		'agents_msd' => $row->agents_msd,
		'date_of' => $row->date_of,
		'Inbound_StaffedDuration' => $row->Inbound_StaffedDuration,
		'Inbound_ReadyDuration' => $row->Inbound_ReadyDuration,
		'Inbound_BreakDuration' => $row->Inbound_BreakDuration,
		'Inbound_IdleTime' => $row->Inbound_IdleTime,
		'Inbound_HoldTime' => $row->Inbound_HoldTime,
		'Inbound_NumberOfcallsOffered' => $row->Inbound_NumberOfcallsOffered,
		'Inbound_NumberofCallanswered' => $row->Inbound_NumberofCallanswered,
		'Inbound_AbandonedPer' => $row->Inbound_AbandonedPer,
		'Inbound_AHT' => $row->Inbound_AHT,
		'Outbound_StaffedDuration' => $row->Outbound_StaffedDuration,
		'Outbound_ReadyDuration' => $row->Outbound_ReadyDuration,
		'Outbound_BreakDuration' => $row->Outbound_BreakDuration,
		'Outbound_IdleTime' => $row->Outbound_IdleTime,
		'Outbound_HoldTime' => $row->Outbound_HoldTime,
		'Outbound_NumberOfCallsDial' => $row->Outbound_NumberOfCallsDial,
		'Outbound_NumberofCallsconnected' => $row->Outbound_NumberofCallsconnected,
		'Outbound_ConnectivityPer' => $row->Outbound_ConnectivityPer,
		'Outbound_AHT' => $row->Outbound_AHT,
	    );
             $data['content'] = 'agent_performance/agent_performance_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agent_performance'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('agent_performance/create_action'),
	    'id' => set_value('id'),
	    'agents_id' => set_value('agents_id'),
	    'agents_msd' => set_value('agents_msd'),
	    'date_of' => set_value('date_of'),
	    'Inbound_StaffedDuration' => set_value('Inbound_StaffedDuration'),
	    'Inbound_ReadyDuration' => set_value('Inbound_ReadyDuration'),
	    'Inbound_BreakDuration' => set_value('Inbound_BreakDuration'),
	    'Inbound_IdleTime' => set_value('Inbound_IdleTime'),
	    'Inbound_HoldTime' => set_value('Inbound_HoldTime'),
	    'Inbound_NumberOfcallsOffered' => set_value('Inbound_NumberOfcallsOffered'),
	    'Inbound_NumberofCallanswered' => set_value('Inbound_NumberofCallanswered'),
	    'Inbound_AbandonedPer' => set_value('Inbound_AbandonedPer'),
	    'Inbound_AHT' => set_value('Inbound_AHT'),
	    'Outbound_StaffedDuration' => set_value('Outbound_StaffedDuration'),
	    'Outbound_ReadyDuration' => set_value('Outbound_ReadyDuration'),
	    'Outbound_BreakDuration' => set_value('Outbound_BreakDuration'),
	    'Outbound_IdleTime' => set_value('Outbound_IdleTime'),
	    'Outbound_HoldTime' => set_value('Outbound_HoldTime'),
	    'Outbound_NumberOfCallsDial' => set_value('Outbound_NumberOfCallsDial'),
	    'Outbound_NumberofCallsconnected' => set_value('Outbound_NumberofCallsconnected'),
	    'Outbound_ConnectivityPer' => set_value('Outbound_ConnectivityPer'),
	    'Outbound_AHT' => set_value('Outbound_AHT'),
	);
        $data['content'] = 'agent_performance/agent_performance_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'agents_id' => $this->input->post('agents_id',TRUE),
		'agents_msd' => $this->input->post('agents_msd',TRUE),
		'date_of' => $this->input->post('date_of',TRUE),
		'Inbound_StaffedDuration' => $this->input->post('Inbound_StaffedDuration',TRUE),
		'Inbound_ReadyDuration' => $this->input->post('Inbound_ReadyDuration',TRUE),
		'Inbound_BreakDuration' => $this->input->post('Inbound_BreakDuration',TRUE),
		'Inbound_IdleTime' => $this->input->post('Inbound_IdleTime',TRUE),
		'Inbound_HoldTime' => $this->input->post('Inbound_HoldTime',TRUE),
		'Inbound_NumberOfcallsOffered' => $this->input->post('Inbound_NumberOfcallsOffered',TRUE),
		'Inbound_NumberofCallanswered' => $this->input->post('Inbound_NumberofCallanswered',TRUE),
		'Inbound_AbandonedPer' => $this->input->post('Inbound_AbandonedPer',TRUE),
		'Inbound_AHT' => $this->input->post('Inbound_AHT',TRUE),
		'Outbound_StaffedDuration' => $this->input->post('Outbound_StaffedDuration',TRUE),
		'Outbound_ReadyDuration' => $this->input->post('Outbound_ReadyDuration',TRUE),
		'Outbound_BreakDuration' => $this->input->post('Outbound_BreakDuration',TRUE),
		'Outbound_IdleTime' => $this->input->post('Outbound_IdleTime',TRUE),
		'Outbound_HoldTime' => $this->input->post('Outbound_HoldTime',TRUE),
		'Outbound_NumberOfCallsDial' => $this->input->post('Outbound_NumberOfCallsDial',TRUE),
		'Outbound_NumberofCallsconnected' => $this->input->post('Outbound_NumberofCallsconnected',TRUE),
		'Outbound_ConnectivityPer' => $this->input->post('Outbound_ConnectivityPer',TRUE),
		'Outbound_AHT' => $this->input->post('Outbound_AHT',TRUE),
	    );

            $this->Agent_performance_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('agent_performance'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Agent_performance_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('agent_performance/update_action'),
		'id' => set_value('id', $row->id),
		'agents_id' => set_value('agents_id', $row->agents_id),
		'agents_msd' => set_value('agents_msd', $row->agents_msd),
		'date_of' => set_value('date_of', $row->date_of),
		'Inbound_StaffedDuration' => set_value('Inbound_StaffedDuration', $row->Inbound_StaffedDuration),
		'Inbound_ReadyDuration' => set_value('Inbound_ReadyDuration', $row->Inbound_ReadyDuration),
		'Inbound_BreakDuration' => set_value('Inbound_BreakDuration', $row->Inbound_BreakDuration),
		'Inbound_IdleTime' => set_value('Inbound_IdleTime', $row->Inbound_IdleTime),
		'Inbound_HoldTime' => set_value('Inbound_HoldTime', $row->Inbound_HoldTime),
		'Inbound_NumberOfcallsOffered' => set_value('Inbound_NumberOfcallsOffered', $row->Inbound_NumberOfcallsOffered),
		'Inbound_NumberofCallanswered' => set_value('Inbound_NumberofCallanswered', $row->Inbound_NumberofCallanswered),
		'Inbound_AbandonedPer' => set_value('Inbound_AbandonedPer', $row->Inbound_AbandonedPer),
		'Inbound_AHT' => set_value('Inbound_AHT', $row->Inbound_AHT),
		'Outbound_StaffedDuration' => set_value('Outbound_StaffedDuration', $row->Outbound_StaffedDuration),
		'Outbound_ReadyDuration' => set_value('Outbound_ReadyDuration', $row->Outbound_ReadyDuration),
		'Outbound_BreakDuration' => set_value('Outbound_BreakDuration', $row->Outbound_BreakDuration),
		'Outbound_IdleTime' => set_value('Outbound_IdleTime', $row->Outbound_IdleTime),
		'Outbound_HoldTime' => set_value('Outbound_HoldTime', $row->Outbound_HoldTime),
		'Outbound_NumberOfCallsDial' => set_value('Outbound_NumberOfCallsDial', $row->Outbound_NumberOfCallsDial),
		'Outbound_NumberofCallsconnected' => set_value('Outbound_NumberofCallsconnected', $row->Outbound_NumberofCallsconnected),
		'Outbound_ConnectivityPer' => set_value('Outbound_ConnectivityPer', $row->Outbound_ConnectivityPer),
		'Outbound_AHT' => set_value('Outbound_AHT', $row->Outbound_AHT),
	    );
            $data['content'] = 'agent_performance/agent_performance_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agent_performance'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'agents_id' => $this->input->post('agents_id',TRUE),
		'agents_msd' => $this->input->post('agents_msd',TRUE),
		'date_of' => $this->input->post('date_of',TRUE),
		'Inbound_StaffedDuration' => $this->input->post('Inbound_StaffedDuration',TRUE),
		'Inbound_ReadyDuration' => $this->input->post('Inbound_ReadyDuration',TRUE),
		'Inbound_BreakDuration' => $this->input->post('Inbound_BreakDuration',TRUE),
		'Inbound_IdleTime' => $this->input->post('Inbound_IdleTime',TRUE),
		'Inbound_HoldTime' => $this->input->post('Inbound_HoldTime',TRUE),
		'Inbound_NumberOfcallsOffered' => $this->input->post('Inbound_NumberOfcallsOffered',TRUE),
		'Inbound_NumberofCallanswered' => $this->input->post('Inbound_NumberofCallanswered',TRUE),
		'Inbound_AbandonedPer' => $this->input->post('Inbound_AbandonedPer',TRUE),
		'Inbound_AHT' => $this->input->post('Inbound_AHT',TRUE),
		'Outbound_StaffedDuration' => $this->input->post('Outbound_StaffedDuration',TRUE),
		'Outbound_ReadyDuration' => $this->input->post('Outbound_ReadyDuration',TRUE),
		'Outbound_BreakDuration' => $this->input->post('Outbound_BreakDuration',TRUE),
		'Outbound_IdleTime' => $this->input->post('Outbound_IdleTime',TRUE),
		'Outbound_HoldTime' => $this->input->post('Outbound_HoldTime',TRUE),
		'Outbound_NumberOfCallsDial' => $this->input->post('Outbound_NumberOfCallsDial',TRUE),
		'Outbound_NumberofCallsconnected' => $this->input->post('Outbound_NumberofCallsconnected',TRUE),
		'Outbound_ConnectivityPer' => $this->input->post('Outbound_ConnectivityPer',TRUE),
		'Outbound_AHT' => $this->input->post('Outbound_AHT',TRUE),
	    );

            $this->Agent_performance_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('agent_performance'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Agent_performance_model->get_by_id($id);

        if ($row) {
            $this->Agent_performance_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('agent_performance'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agent_performance'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
	$this->form_validation->set_rules('agents_msd', 'agents msd', 'trim|required');
	$this->form_validation->set_rules('date_of', 'date of', 'trim|required');
	$this->form_validation->set_rules('Inbound_StaffedDuration', 'inbound staffedduration', 'trim|required');
	$this->form_validation->set_rules('Inbound_ReadyDuration', 'inbound readyduration', 'trim|required');
	$this->form_validation->set_rules('Inbound_BreakDuration', 'inbound breakduration', 'trim|required');
	$this->form_validation->set_rules('Inbound_IdleTime', 'inbound idletime', 'trim|required');
	$this->form_validation->set_rules('Inbound_HoldTime', 'inbound holdtime', 'trim|required');
	$this->form_validation->set_rules('Inbound_NumberOfcallsOffered', 'inbound numberofcallsoffered', 'trim|required');
	$this->form_validation->set_rules('Inbound_NumberofCallanswered', 'inbound numberofcallanswered', 'trim|required');
	$this->form_validation->set_rules('Inbound_AbandonedPer', 'inbound abandonedper', 'trim|required|numeric');
	$this->form_validation->set_rules('Inbound_AHT', 'inbound aht', 'trim|required');
	$this->form_validation->set_rules('Outbound_StaffedDuration', 'outbound staffedduration', 'trim|required');
	$this->form_validation->set_rules('Outbound_ReadyDuration', 'outbound readyduration', 'trim|required');
	$this->form_validation->set_rules('Outbound_BreakDuration', 'outbound breakduration', 'trim|required');
	$this->form_validation->set_rules('Outbound_IdleTime', 'outbound idletime', 'trim|required');
	$this->form_validation->set_rules('Outbound_HoldTime', 'outbound holdtime', 'trim|required');
	$this->form_validation->set_rules('Outbound_NumberOfCallsDial', 'outbound numberofcallsdial', 'trim|required');
	$this->form_validation->set_rules('Outbound_NumberofCallsconnected', 'outbound numberofcallsconnected', 'trim|required');
	$this->form_validation->set_rules('Outbound_ConnectivityPer', 'outbound connectivityper', 'trim|required|numeric');
	$this->form_validation->set_rules('Outbound_AHT', 'outbound aht', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "agent_performance.xls";
        $judul = "agent_performance";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Agents Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Agents Msd");
	xlsWriteLabel($tablehead, $kolomhead++, "Date Of");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound StaffedDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound ReadyDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound BreakDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound IdleTime");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound HoldTime");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound NumberOfcallsOffered");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound NumberofCallanswered");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound AbandonedPer");
	xlsWriteLabel($tablehead, $kolomhead++, "Inbound AHT");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound StaffedDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound ReadyDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound BreakDuration");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound IdleTime");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound HoldTime");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound NumberOfCallsDial");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound NumberofCallsconnected");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound ConnectivityPer");
	xlsWriteLabel($tablehead, $kolomhead++, "Outbound AHT");

	foreach ($this->Agent_performance_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->agents_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->agents_msd);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date_of);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_StaffedDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_ReadyDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_BreakDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_IdleTime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_HoldTime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Inbound_NumberOfcallsOffered);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Inbound_NumberofCallanswered);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Inbound_AbandonedPer);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Inbound_AHT);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_StaffedDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_ReadyDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_BreakDuration);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_IdleTime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_HoldTime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Outbound_NumberOfCallsDial);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Outbound_NumberofCallsconnected);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Outbound_ConnectivityPer);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Outbound_AHT);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Agent_performance.php */
/* Location: ./application/controllers/Agent_performance.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2020-04-30 10:48:06 */
