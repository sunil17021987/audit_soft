<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agents extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Agents_model');
        $this->load->model('Employee_model');
        $this->load->model('Shifts_model');
        $this->load->model('Assign_model');
        $this->load->model('Attendance_format_model');
        $this->load->library('form_validation');
        // Load file helper
        $this->load->helper('file');
    }

    public function index() {
        $status = '';
         $status = $this->input->post('status', TRUE);
          if ($status != '') {
            $status = $this->input->post('status', TRUE);
        }else{
           // $status =1;
        }
         $employee_tl = $this->Employee_model->get_all_TL();
        $employee_am = $this->Employee_model->get_all_AM();
        $shifts = $this->Shifts_model->get_all();
        $agents = $this->Agents_model->get_all_by_joins($status);
       // echo $this->db->last_query();

        $data = array(
            'agents_data' => $agents,
             'employee_data_tl' => $employee_tl,
            'employee_data_am' => $employee_am,
            'shifts_data' => $shifts,
            'status'=>$status
        );
      if ($this->uri->segment(1) == 'Agents') {
            $data['Agents'] = "active";
        }
        $data['content'] = 'agents/agents_list';
        $this->load->view('common/master', $data);
    }
    
    public function notLoginToday (){
          $agents = $this->Agents_model->get_all_by_joins_notlogintoday();
          $employee = $this->Employee_model->get_all_notlogintoday();
        $data = array(
            'agents_data' => $agents,
             'employee_data' => $employee
         
        );
         $data['content'] = 'agents/notLoginToday';
        $this->load->view('common/master', $data);
    }

    public function alignment(){
         $data = array(
            'button' => 'Create',
            'action' => site_url('agents/import'),
        );
          $data['content'] = 'agents/alignment';
        $this->load->view('common/master', $data);
    }
  public function import(){
        $data = array();
        $memData = array();
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
          
            if($this->form_validation->run() == true){
                
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                    
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ $rowCount++;
                            
                            // Prepare data for DB insertion
                            $memData = array(
                                'employee_id_TL' => $row['employee_id_TL'],
                                 'employee_id_AM' => $row['employee_id_AM'],
                                 'shift_id' => $row['shift_id'],
                            );
                           
                        $insert = $this->Agents_model->updateAlignment($row['id'],$memData);
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'Members imported successfully. Total Rows ('.$rowCount.') | Inserted ('.$insertCount.') | Updated ('.$updateCount.') | Not Inserted ('.$notAddCount.')';
                        $this->session->set_userdata('success_msg', $successMsg);
                    }
                }else{
                   $this->session->set_flashdata('item', array('message' => 'Invalid file, please select only CSV file.','class' => 'error'));
                }
            }else{
              
                 $this->session->set_flashdata('item', array('message' => 'please try again!','class' => 'error'));
            }
        }
        
        
       
        redirect('agents/alignment');
    }
        public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }
    public function read($id) {
        $row = $this->Agents_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'emp_id' => $row->emp_id,
                'name' => $row->name,
                'employee_id_TL' => $row->employee_id_TL,
                'employee_id_AM' => $row->employee_id_AM,
                'shift_id' => $row->shift_id,
                'DOJ' => $row->DOJ,
                'process' => $row->process,
                'gender' => $row->gender,
                'batch_no' => $row->batch_no,
                'status' => $row->status,
            );
            $data['content'] = 'agents/agents_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }

    public function create() {

        $employee_tl = $this->Employee_model->get_all_TL();
        $employee_am = $this->Employee_model->get_all_AM();
        $shifts = $this->Shifts_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('agents/create_action'),
            'id' => set_value('id'),
            'emp_id' => set_value('emp_id'),
            'name' => set_value('name'),
            'employee_id_TL' => set_value('employee_id_TL'),
            'employee_id_AM' => set_value('employee_id_AM'),
            'shift_id' => set_value('shift_id'),
            'DOJ' => set_value('DOJ'),
            'process' => set_value('process'),
            'gender' => set_value('gender'),
            'batch_no' => set_value('batch_no'),
            'status' => set_value('status'),
            'employee_data_tl' => $employee_tl,
            'employee_data_am' => $employee_am,
            'shifts_data' => $shifts
        );
        $data['content'] = 'agents/agents_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
           
            $data = array(
                'emp_id' => $this->input->post('emp_id', TRUE),
                'name' => $this->input->post('name', TRUE),
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'shift_id' => $this->input->post('shift_id', TRUE),
                'DOJ' => date('Y-m-d', strtotime($this->input->post('DOJ', TRUE))),
                'process' => $this->input->post('process', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'batch_no' => $this->input->post('batch_no', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $insert_id = $this->Agents_model->insert($data);
            if ($insert_id) {
                $data_assgn = array(
                    'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                    'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                    'agents_id' => $insert_id,
                    'date' => date('Y-m-d'),
                    'status' => '1',
                );
                $this->Assign_model->insert($data_assgn);
            }

            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('agents'));
        }
    }

    public function update($id) {
        $row = $this->Agents_model->get_by_id($id);
        $employee_tl = $this->Employee_model->get_all_TL();
        $employee_am = $this->Employee_model->get_all_AM();
        $shifts = $this->Shifts_model->get_all();
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('agents/update_action'),
                'id' => set_value('id', $row->id),
                'emp_id' => set_value('emp_id', $row->emp_id),
                'name' => set_value('name', $row->name),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'shift_id' => set_value('shift_id', $row->shift_id),
                'DOJ' => set_value('DOJ', date('d-m-Y', strtotime($row->DOJ)) ),
                'process' => set_value('process', $row->process),
                'gender' => set_value('gender', $row->gender),
                'batch_no' => set_value('batch_no', $row->batch_no),
                'status' => set_value('status', $row->status),
                'employee_data_tl' => $employee_tl,
                'employee_data_am' => $employee_am,
                'shifts_data' => $shifts
            );
            $data['content'] = 'agents/agents_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'emp_id' => $this->input->post('emp_id', TRUE),
                'name' => $this->input->post('name', TRUE),
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'shift_id' => $this->input->post('shift_id', TRUE),
                'DOJ' => date('Y-m-d', strtotime($this->input->post('DOJ', TRUE))),
                'process' => $this->input->post('process', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'batch_no' => $this->input->post('batch_no', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Agents_model->update($this->input->post('id', TRUE), $data);
//            ******************************insert into assign table new record ****************************************
            $dataupdate = array(
                'status' => '0',
            );

            $this->Assign_model->update_status($this->input->post('id', TRUE), $dataupdate);
            if ($this->input->post('id', TRUE)) {
                $data_assgn = array(
                    'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                    'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                    'agents_id' => $this->input->post('id', TRUE),
                    'date' => date('Y-m-d'),
                    'status' => '1',
                );
                $this->Assign_model->insert($data_assgn);
            }
//            ***********************************************************************************************************            



            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('agents'));
        }
    }
     public function update_action_ajax() {
          $uid = 0;
          $ids =array();
           $ids = $this->input->post('ids', TRUE);
         if(!empty($ids)){
           if(count($ids)>0){
               foreach ($ids as $id) {
                               $data = array(
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'shift_id' => $this->input->post('shift_id', TRUE),
            );
          $uid =  $this->Agents_model->update($id, $data);
               }
         }}
if($uid>0){
    echo"Record Updated successfully";
}else{
    echo"Something is wrong";
}


       
    }

    public function delete($id) {
        $row = $this->Agents_model->get_by_id($id);

        if ($row) {
            $this->Agents_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('agents'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('emp_id', 'emp id', 'trim|required');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
        $this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
        $this->form_validation->set_rules('shift_id', 'shift id', 'trim|required');
        $this->form_validation->set_rules('DOJ', 'doj', 'trim|required');
        $this->form_validation->set_rules('process', 'process', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        $this->form_validation->set_rules('batch_no', 'batch no', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $status='';
        $records=$this->Agents_model->get_all_by_joinsbystatus($status);
//        echo $this->db->last_query();
//        die();
        $this->load->helper('exportexcel');
        $namaFile = "agents.xls";
        $judul = "agents";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Emp Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
        xlsWriteLabel($tablehead, $kolomhead++, "Shift Id");
        xlsWriteLabel($tablehead, $kolomhead++, "DOJ");
        xlsWriteLabel($tablehead, $kolomhead++, "Process");
        xlsWriteLabel($tablehead, $kolomhead++, "Gender");
        xlsWriteLabel($tablehead, $kolomhead++, "Batch No");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");
        
      

        foreach ($records as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->name);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_TL);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_AM);
            xlsWriteLabel($tablebody, $kolombody++, $data->shift_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->DOJ);
            xlsWriteLabel($tablebody, $kolombody++, $data->process);
            xlsWriteLabel($tablebody, $kolombody++, $data->gender);
            xlsWriteNumber($tablebody, $kolombody++, $data->batch_no);
            xlsWriteLabel($tablebody, $kolombody++, $data->statusof);
            

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function ExcelAlignmentFormat1()
    {
         $records=$this->Agents_model->get_all_formar();

        $this->load->helper('exportexcel');
        $namaFile = "AlignmentFormat.xls";
        $judul = "agents";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "id");
        xlsWriteLabel($tablehead, $kolomhead++, "emp_id");
        xlsWriteLabel($tablehead, $kolomhead++, "name");
        xlsWriteLabel($tablehead, $kolomhead++, "employee_id_TL");
        xlsWriteLabel($tablehead, $kolomhead++, "employee_id_AM");
        xlsWriteLabel($tablehead, $kolomhead++, "shift_id");
   
      

        foreach ($records as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $data->id);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->name);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_TL);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_AM);
            xlsWriteLabel($tablebody, $kolombody++, $data->shift_id);
            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
    
    // Export data in CSV format 
  public function ExcelAlignmentFormat(){ 
   // file name 
   $filename = 'alignment_format'.'.csv'; 
   header("Content-Description: File Transfer"); 
   header("Content-Disposition: attachment; filename=$filename"); 
   header("Content-Type: application/csv; ");
   
   // get data 
   $usersData = $this->Agents_model->get_all_formar();

   // file creation 
   $file = fopen('php://output', 'w');
 
   $header = array("id","emp_id","name","employee_id_TL","employee_id_AM","shift_id"); 
   fputcsv($file, $header);
   foreach ($usersData as $key=>$line){ 
     fputcsv($file,$line); 
   }
   fclose($file); 
   exit; 
  }
  
   public function attendance_format(){
         $data = array(
            'button' => 'Create',
            'action' => site_url('agents/importAgentattendance'),
        );
          $data['content'] = 'agents/attendance_format';
        $this->load->view('common/master', $data);
    }
    
     public function importAgentattendance(){
        $data = array();
        $memData = array();
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
          
            if($this->form_validation->run() == true){
                
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                    
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ $rowCount++;
                            
                            // Prepare data for DB insertion
                            $memData = array(
                                'employeeName' => $row['employeeName'],
                                'TLName' => $row['TLName'],
                                'agent_Status' => $row['agent_Status'],
                                'batch' => $row['batch'],
                                'AMName' => $row['AMName'],
                                'Date_of' => $row['Date_of'],
                                'RosterShift' => $row['RosterShift'],
                                 'Login' => $row['Login'],
                                 'Attendance' => $row['Attendance'],
                                 'MSD_ID' => $row['MSD_ID'],
                                'FirstLogin' => $row['FirstLogin'],
                                'ShiftAdherence' => $row['ShiftAdherence'],

                                
                            );
                           
                        $insert = $this->Attendance_format_model->insertagentsperformance($memData);
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'imported successfully. Total Rows ('.$rowCount.')';
                       $this->session->set_flashdata('item', array('message' =>$successMsg,'class' => 'error'));
                    }
                }else{
                   $this->session->set_flashdata('item', array('message' => 'Invalid file, please select only CSV file.','class' => 'error'));
                }
            }else{
              
                 $this->session->set_flashdata('item', array('message' => 'please try again!','class' => 'error'));
            }
        }
        
        
       
        redirect('agents/attendance_format');
    }
    
    
         public function ExcelAgentAttendance_format(){ 
   // file name 
   $filename = 'Attendance_format'.'.csv'; 
   header("Content-Description: File Transfer"); 
   header("Content-Disposition: attachment; filename=$filename"); 
   header("Content-Type: application/csv; ");
   
   // get data 
   //$usersData = $this->Agents_model->get_all_formar();

   // file creation 
   $file = fopen('php://output', 'w');
 
   $header = array("employeeName","TLName","agent_Status","batch","AMName","Date_of","RosterShift","Login","Attendance","MSD_ID","FirstLogin","ShiftAdherence"); 
   fputcsv($file, $header);
//   foreach ($usersData as $key=>$line){ 
//     fputcsv($file,$line); 
//   }
//   fclose($file); 
   exit; 
  }
  
  
  
   public function agentPerformance(){
         $data = array(
            'button' => 'Create',
            'action' => site_url('agents/importAgentPerformance'),
        );
          $data['content'] = 'agents/agentPerformance';
        $this->load->view('common/master', $data);
    }
    
    
     public function ExcelAgentPerformanceFormat(){ 
   // file name 
   $filename = 'AgentPerformanceFormat'.'.csv'; 
   header("Content-Description: File Transfer"); 
   header("Content-Disposition: attachment; filename=$filename"); 
   header("Content-Type: application/csv; ");
   
   // get data 
   //$usersData = $this->Agents_model->get_all_formar();

   // file creation 
   $file = fopen('php://output', 'w');
 
   $header = array("agents_id","agents_msd","date_of","Inbound_StaffedDuration","Inbound_ReadyDuration","Inbound_BreakDuration","Inbound_IdleTime","Inbound_HoldTime","Inbound_NumberOfcallsOffered","Inbound_NumberofCallanswered","Inbound_AbandonedPer","Inbound_AHT","Outbound_StaffedDuration","Outbound_ReadyDuration","Outbound_BreakDuration","Outbound_IdleTime","Outbound_HoldTime","Outbound_NumberOfCallsDial","Outbound_NumberofCallsconnected","Outbound_ConnectivityPer","Outbound_AHT"); 
   fputcsv($file, $header);
//   foreach ($usersData as $key=>$line){ 
//     fputcsv($file,$line); 
//   }
//   fclose($file); 
   exit; 
  }
  
  public function importAgentPerformance(){
        $data = array();
        $memData = array();
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
          
            if($this->form_validation->run() == true){
                
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                    
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ $rowCount++;
                            
                            // Prepare data for DB insertion
                            $memData = array(
                                'agents_id' => $row['agents_id'],
                                'agents_msd' => $row['agents_msd'],
                               
                                 'date_of' => $row['date_of'],
                               
                                 'Inbound_StaffedDuration' => $row['Inbound_StaffedDuration'],
                               
                                 'Inbound_ReadyDuration' => $row['Inbound_ReadyDuration'],
                               
                                 'Inbound_BreakDuration' => $row['Inbound_BreakDuration'],
                               
                                 'Inbound_IdleTime' => $row['Inbound_IdleTime'],
                               
                                 'Inbound_HoldTime' => $row['Inbound_HoldTime'],
                               
                                 'Inbound_NumberOfcallsOffered' => $row['Inbound_NumberOfcallsOffered'],
                               
                                 'Inbound_NumberofCallanswered' => $row['Inbound_NumberofCallanswered'],
                               
                                 'Inbound_AbandonedPer' => $row['Inbound_AbandonedPer'],
                              
                                 'Inbound_AHT' => $row['Inbound_AHT'],
                             
                                 'Outbound_StaffedDuration' => $row['Outbound_StaffedDuration'],
                              
                                 'Outbound_ReadyDuration' => $row['Outbound_ReadyDuration'],
                             
                                 'Outbound_BreakDuration' => $row['Outbound_BreakDuration'],
                               
                                 'Outbound_IdleTime' => $row['Outbound_IdleTime'],
                              
                                 'Outbound_HoldTime' => $row['Outbound_HoldTime'],
                               
                                 'Outbound_NumberOfCallsDial' => $row['Outbound_NumberOfCallsDial'],
                               
                                 'Outbound_NumberofCallsconnected' => $row['Outbound_NumberofCallsconnected'],
                               
                                 'Outbound_ConnectivityPer' => $row['Outbound_ConnectivityPer'],
                               
                                 'Outbound_AHT' => $row['Outbound_AHT'],
                                
                            );
                           
                        $insert = $this->Agents_model->insertagentsperformance($memData);
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'imported successfully. Total Rows ('.$rowCount.')';
                       $this->session->set_flashdata('item', array('message' =>$successMsg,'class' => 'error'));
                    }
                }else{
                   $this->session->set_flashdata('item', array('message' => 'Invalid file, please select only CSV file.','class' => 'error'));
                }
            }else{
              
                 $this->session->set_flashdata('item', array('message' => 'please try again!','class' => 'error'));
            }
        }
        
        
       
        redirect('agents/agentPerformance');
    }
  
    
}

/* End of file Agents.php */
/* Location: ./application/controllers/Agents.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-02 12:35:42 */
