<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calldurationpattern extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Calldurationpattern_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $calldurationpattern = $this->Calldurationpattern_model->get_all();

        $data = array(
            'calldurationpattern_data' => $calldurationpattern
        );

          $data['content'] = 'calldurationpattern/calldurationpattern_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Calldurationpattern_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'start_time' => $row->start_time,
		'end_time' => $row->end_time,
		'target' => $row->target,
		'status' => $row->status,
	    );
             $data['content'] = 'calldurationpattern/calldurationpattern_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('calldurationpattern'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('calldurationpattern/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'start_time' => set_value('start_time'),
	    'end_time' => set_value('end_time'),
	    'target' => set_value('target'),
	    'status' => set_value('status'),
	);
        $data['content'] = 'calldurationpattern/calldurationpattern_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'start_time' => $this->input->post('start_time',TRUE),
		'end_time' => $this->input->post('end_time',TRUE),
		'target' => $this->input->post('target',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Calldurationpattern_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('calldurationpattern'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Calldurationpattern_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('calldurationpattern/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'start_time' => set_value('start_time', $row->start_time),
		'end_time' => set_value('end_time', $row->end_time),
		'target' => set_value('target', $row->target),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'calldurationpattern/calldurationpattern_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('calldurationpattern'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'start_time' => $this->input->post('start_time',TRUE),
		'end_time' => $this->input->post('end_time',TRUE),
		'target' => $this->input->post('target',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Calldurationpattern_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('calldurationpattern'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Calldurationpattern_model->get_by_id($id);

        if ($row) {
            $this->Calldurationpattern_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('calldurationpattern'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('calldurationpattern'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('start_time', 'start time', 'trim|required');
	$this->form_validation->set_rules('end_time', 'end time', 'trim|required');
	$this->form_validation->set_rules('target', 'target', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "calldurationpattern.xls";
        $judul = "calldurationpattern";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Start Time");
	xlsWriteLabel($tablehead, $kolomhead++, "End Time");
	xlsWriteLabel($tablehead, $kolomhead++, "Target");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Calldurationpattern_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->start_time);
	    xlsWriteLabel($tablebody, $kolombody++, $data->end_time);
	    xlsWriteNumber($tablebody, $kolombody++, $data->target);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Calldurationpattern.php */
/* Location: ./application/controllers/Calldurationpattern.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-20 10:53:11 */
