<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_main extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Category_main_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $category_main = $this->Category_main_model->get_all();

        $data = array(
            'category_main_data' => $category_main
        );

          $data['content'] = 'category_main/category_main_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);
        if ($row) {
            $data = array(
		'category_main_id' => $row->category_main_id,
		'category_main_name' => $row->category_main_name,
	    );
             $data['content'] = 'category_main/category_main_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('category_main/create_action'),
	    'category_main_id' => set_value('category_main_id'),
	    'category_main_name' => set_value('category_main_name'),
	);
        $data['content'] = 'category_main/category_main_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'category_main_name' => $this->input->post('category_main_name',TRUE),
	    );

            $this->Category_main_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('category_main'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('category_main/update_action'),
		'category_main_id' => set_value('category_main_id', $row->category_main_id),
		'category_main_name' => set_value('category_main_name', $row->category_main_name),
	    );
            $data['content'] = 'category_main/category_main_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('category_main_id', TRUE));
        } else {
            $data = array(
		'category_main_name' => $this->input->post('category_main_name',TRUE),
	    );

            $this->Category_main_model->update($this->input->post('category_main_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('category_main'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);

        if ($row) {
            $this->Category_main_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('category_main'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('category_main_name', 'category main name', 'trim|required');

	$this->form_validation->set_rules('category_main_id', 'category_main_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "category_main.xls";
        $judul = "category_main";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Name");

	foreach ($this->Category_main_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_name);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Category_main.php */
/* Location: ./application/controllers/Category_main.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2022-01-22 11:44:41 */
