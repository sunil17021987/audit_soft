<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Assign extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Assign_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $assign = $this->Assign_model->get_all();

        $data = array(
            'assign_data' => $assign
        );

          $data['content'] = 'assign/assign_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Assign_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'employee_id_TL' => $row->employee_id_TL,
		'employee_id_AM' => $row->employee_id_AM,
		'agents_id' => $row->agents_id,
		'date' => $row->date,
		'status' => $row->status,
	    );
             $data['content'] = 'assign/assign_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('assign'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('assign/create_action'),
	    'id' => set_value('id'),
	    'employee_id_TL' => set_value('employee_id_TL'),
	    'employee_id_AM' => set_value('employee_id_AM'),
	    'agents_id' => set_value('agents_id'),
	    'date' => set_value('date'),
	    'status' => set_value('status'),
	);
        $data['content'] = 'assign/assign_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'agents_id' => $this->input->post('agents_id',TRUE),
		'date' => $this->input->post('date',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Assign_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('assign'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Assign_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('assign/update_action'),
		'id' => set_value('id', $row->id),
		'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
		'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
		'agents_id' => set_value('agents_id', $row->agents_id),
		'date' => set_value('date', $row->date),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'assign/assign_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('assign'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'agents_id' => $this->input->post('agents_id',TRUE),
		'date' => $this->input->post('date',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Assign_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('assign'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Assign_model->get_by_id($id);

        if ($row) {
            $this->Assign_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('assign'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('assign'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
	$this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
	$this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "assign.xls";
        $judul = "assign";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
	xlsWriteLabel($tablehead, $kolomhead++, "Agents Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Assign_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_TL);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_AM);
	    xlsWriteNumber($tablebody, $kolombody++, $data->agents_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Assign.php */
/* Location: ./application/controllers/Assign.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-05 12:04:09 */
