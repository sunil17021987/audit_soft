<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Employee_model');
        $this->load->model('Category_model');
        $this->load->model('Department_model');
        $this->load->model('Designation_model');
        $this->load->library('form_validation');
        // $this->load->library('Pdf');
        $this->check_auth();
    }

    public function check_auth() {

        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login'));
    }

    public function index() {
        $employee = $this->Employee_model->get_all();

        $data = array(
            'employee_data' => $employee
        );
        if ($this->uri->segment(1) == 'employee') {
            $data['employee'] = "active";
        }
        $data['content'] = 'employee/employee_list';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_list', $data);
    }

    public function save_pdf() {
        //load mPDF library
      //  $this->load->library('m_pdf');
        //now pass the data//
        $employee = $this->Employee_model->get_all();
       $htmlContent='';
        $data = array(
            'employee_data' => $employee
        );
        if ($this->uri->segment(1) == 'employee') {
            $data['employee'] = "active";
        }
        $data['content'] = 'employee/employee_list';
     $html =  $this->load->view('employee/employee_list_1', $data);
      $htmlContent ='<table>
            <thead>
              <tr>
                <th>No</th>
                <th width="20px">MSD ID</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Join Date</th>
<!--                <th>Email</th>-->
                <th>Mobile</th>
             
                  <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
            $start = 0;
            foreach ($employee_data as $employee)
            {
                ?>
              <tr>
                <td><?php echo ++$start ?></td>
                 <td><?php echo $employee->msd_ID ?></td>
                <td><?php echo $employee->emp_name ?></td>
                <td><?php echo $employee->gender ?></td>
                <td><?php echo $employee->join_date ?></td>
<!--                <td><?php echo $employee->email ?></td>-->
                <td><?php echo $employee->mobile ?></td>
               
               
                 <td>aa</td>
                <td style="text-align:center" width="200px"><?php 
			bb
                </td>
              </tr>
              <?php
            }
            ?>
            </tbody>
          </table>';
            $createPDFFile = time().'.pdf';
         
            $this->save_pdf_print($htmlContent);
          //  redirect($createPDFFile);
    }
    
     // create pdf file 
     
public function save_pdf_print($html) 
    {
                // coder for CodeIgniter TCPDF Integration
        // make new advance pdf document
        $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
       
        // set document information
        $tcpdf->SetCreator(PDF_CREATOR);
        $tcpdf->SetAuthor('Muhammad Saqlain Arif');
        $tcpdf->SetTitle('TCPDF Example 001');
        $tcpdf->SetSubject('TCPDF Tutorial');
        $tcpdf->SetKeywords('TCPDF, PDF, example, test, guide');   
       
        //set default header information 
         
        $tcpdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,65,256), array(0,65,127));
        $tcpdf->setFooterData(array(0,65,0), array(0,65,127)); 
       
        //set header  textual styles 
        $tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //set footer textual styles
        $tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
       
        //set default monospaced textual style 
        $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
       
        // set default margins
        $tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // Set Header Margin
        $tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // Set Footer Margin
        $tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
       
        // set auto for page breaks
        $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
       
        // set image for scale factor
        $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
       
        // it is optional :: set some language-dependent strings
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
        {
            // optional
            require_once(dirname(__FILE__).'/lang/eng.php');
            // optional
            $tcpdf->setLanguageArray($l);
        }   
 
       
        // set default font for subsetting mode
        $tcpdf->setFontSubsetting(true);   
       
        // Set textual style 
        // dejavusans is an UTF-8 Unicode textual style, on the off chance that you just need to 
        // print standard ASCII roasts, you can utilize center text styles like 
        // helvetica or times to lessen record estimate. 
        $tcpdf->SetFont('dejavusans', '', 8, '', true);   
       
        // Add a new page 
        // This technique has a few choices, check the source code documentation for more data. 
        $tcpdf->AddPage(); 
       
        // set text shadow for effect
        $tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,197,198), 'opacity'=>1, 'blend_mode'=>'Normal'));    
       
        //Set some substance to print
 
        $set_html =$html;
 
        //Print content utilizing writeHTMLCell() 
        $tcpdf->writeHTMLCell(0, 0, '', '', $set_html, 0, 1, 0, true, '', true); 
 ob_end_clean();
        // Close and yield PDF record 
        // This technique has a few choices, check the source code documentation for more data. 
        $tcpdf->Output('tcpdfexample-onlinecode.pdf', 'I'); 
                // successfully created CodeIgniter TCPDF Integration
          }

    public function read($id) {
        $row = $this->Employee_model->get_by_id($id);
        if ($row) {
            $data = array(
                'emp_id' => $row->emp_id,
                'msd_ID' => $row->msd_ID,
                'emp_name' => $row->emp_name,
                'gender' => $row->gender,
                'dob' => $row->dob,
                'email' => $row->email,
                'mobile' => $row->mobile,
                'alternate_mobile' => $row->alternate_mobile,
                'category' => $row->category,
                'qualification' => $row->qualification,
                'join_date' => $row->join_date,
                'department' => $row->department,
                'designation' => $row->designation,
                'matial_status' => $row->matial_status,
//		'password' => $row->password,
                'experience' => $row->experience,
                'image' => $row->image,
                'created_at' => $row->created_at,
            );
//            $this->load->view('employee/employee_read', $data);
            $data['content'] = 'employee/emp_profile';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function read_details($id) {
//        $data['fees_collect_data'] = $this->Fees_collect_model->get_fees($id);

        $data['row'] = $this->Employee_model->get_by_id1($id);

        $data['content'] = 'employee/emp_profile';
        $this->load->view('common/master', $data);
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('employee/create_action'),
            'emp_id' => set_value('emp_id'),
            'msd_ID' => set_value('msd_ID'),
            'emp_name' => set_value('emp_name'),
            'gender' => set_value('gender'),
            'dob' => set_value('dob'),
            'email' => set_value('email'),
            'mobile' => set_value('mobile'),
            'alternate_mobile' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'address' => set_value('address'),
            'category' => set_value('category'),
            'qualification' => set_value('qualification'),
            'join_date' => set_value('join_date'),
            'department' => set_value('department'),
            'designation' => set_value('designation'),
            'matial_status' => set_value('matial_status'),
            'password' => set_value('password'),
            'experience' => set_value('experience'),
            'image' => set_value('image'),
            'created_at' => set_value('created_at'),
            'status' => set_value('status', 1),
        );
        $data['category_list'] = $this->Category_model->get_category();
        $data['content'] = 'employee/emp_registration';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_form', $data);
    }

    public function create_action() {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'msd_ID' => $this->input->post('msd_ID', TRUE),
                'emp_name' => $this->input->post('emp_name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'dob' => '01/01/2000',
                'email' => $this->input->post('email', TRUE),
                'mobile' => $this->input->post('mobile', TRUE),
                'alternate_mobile' => $this->input->post('alternate_mobile', TRUE),
                'address' => $this->input->post('address', TRUE),
                'category' => $this->input->post('category', TRUE),
                'qualification' => 'NO',
                'join_date' => $this->input->post('join_date', TRUE),
                'department' => $this->input->post('department', TRUE),
                'designation' => $this->input->post('designation', TRUE),
                'matial_status' => 'No',
                'password' => md5($this->input->post('password')),
                'experience' => '0',
                'image' => $this->input->post('image', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Employee_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('employee'));
        }
    }

    public function update($id) {
        $row = $this->Employee_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('employee/update_action'),
                'msd_ID' => set_value('msd_ID', $row->msd_ID),
                'emp_id' => set_value('emp_id', $row->emp_id),
                'emp_name' => set_value('emp_name', $row->emp_name),
                'gender' => set_value('gender', $row->gender),
                'dob' => set_value('dob', $row->dob),
                'email' => set_value('email', $row->email),
                'mobile' => set_value('mobile', $row->mobile),
                'alternate_mobile' => set_value('alternate_mobile', $row->alternate_mobile),
                'address' => set_value('address', $row->address),
                'category' => set_value('category', $row->category),
                'qualification' => set_value('qualification', $row->qualification),
                'join_date' => set_value('join_date', $row->join_date),
                'department' => set_value('department', $row->department),
                'designation' => set_value('designation', $row->designation),
                'matial_status' => set_value('matial_status', $row->matial_status),
                'password' => set_value('password', $row->password),
                'experience' => set_value('experience', $row->experience),
                'image' => set_value('image', $row->image),
                'created_at' => set_value('created_at', $row->created_at),
                'status' => set_value('status', $row->status),
            );
            //$this->load->view('employee/employee_form', $data);
            $data['category_list'] = $this->Category_model->get_category();
            $data['content'] = 'employee/emp_registration';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function update_action() {
        $this->_rules_edit();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('emp_id', TRUE));
        } else {

            $data = array(
                'msd_ID' => $this->input->post('msd_ID', TRUE),
                'emp_name' => $this->input->post('emp_name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'dob' => $this->input->post('dob', TRUE),
                'email' => $this->input->post('email', TRUE),
                'mobile' => $this->input->post('mobile', TRUE),
                'alternate_mobile' => $this->input->post('alternate_mobile', TRUE),
                'address' => $this->input->post('address', TRUE),
                'category' => $this->input->post('category', TRUE),
                'qualification' => $this->input->post('qualification', TRUE),
                'join_date' => $this->input->post('join_date', TRUE),
                'department' => $this->input->post('department', TRUE),
                'designation' => $this->input->post('designation', TRUE),
                'matial_status' => $this->input->post('matial_status', TRUE),
                'experience' => $this->input->post('experience', TRUE),
                'status' => $this->input->post('status', TRUE),
                    //  'image' => $this->input->post('image', TRUE),
                    // 'created_at' => $this->input->post('created_at', TRUE),
//                'password' => $password
            );


            $this->Employee_model->update($this->input->post('emp_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success.');
            redirect(site_url('employee'));
        }
    }

    public function passwordChange($id) {
        $row = $this->Employee_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('employee/update_action'),
                'msd_ID' => set_value('msd_ID', $row->msd_ID),
                'emp_id' => set_value('emp_id', $row->emp_id),
                'emp_name' => set_value('emp_name', $row->emp_name),
                'gender' => set_value('gender', $row->gender),
                'dob' => set_value('dob', $row->dob),
                'email' => set_value('email', $row->email),
                'mobile' => set_value('mobile', $row->mobile),
                'alternate_mobile' => set_value('alternate_mobile', $row->alternate_mobile),
                'address' => set_value('address', $row->address),
                'category' => set_value('category', $row->category),
                'qualification' => set_value('qualification', $row->qualification),
                'join_date' => set_value('join_date', $row->join_date),
                'department' => set_value('department', $row->department),
                'designation' => set_value('designation', $row->designation),
                'matial_status' => set_value('matial_status', $row->matial_status),
                'password' => set_value('password', $row->password),
                'experience' => set_value('experience', $row->experience),
                'image' => set_value('image', $row->image),
                'created_at' => set_value('created_at', $row->created_at),
                'status' => set_value('status', $row->status),
            );
            //$this->load->view('employee/employee_form', $data);
            $data['category_list'] = $this->Category_model->get_category();
            $data['content'] = 'employee/changepassword';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Login/logout'));
        }
    }

    public function password_update_action() {
        $this->_rules_edit_pass();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('emp_id', TRUE));
        } else {

            $data = array(
                'password' => md5($this->input->post('password', TRUE)),
            );


            $this->Employee_model->update($this->input->post('emp_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success.');
            redirect(site_url('employee'));
        }
    }

    public function password_update_action_self() {
        $this->_rules_edit_pass();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('emp_id', TRUE));
        } else {

            $data = array(
                'password' => md5($this->input->post('password', TRUE)),
            );


            $this->Employee_model->update($this->input->post('emp_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success.');
            redirect(site_url('Login/logout'));
        }
    }

    public function delete($id) {
        $row = $this->Employee_model->get_by_id($id);

        if ($row) {
            $this->Employee_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('employee'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('msd_ID', 'msd_ID', 'trim|required');
        $this->form_validation->set_rules('emp_name', 'emp name', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
//        $this->form_validation->set_rules('dob', 'dob', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[employee.email]');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
//        $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
        $this->form_validation->set_rules('join_date', 'join date', 'trim|required');
        $this->form_validation->set_rules('department', 'department', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
//        $this->form_validation->set_rules('matial_status', 'matial status', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
//        $this->form_validation->set_rules('experience', 'experience', 'trim|required');
//        $this->form_validation->set_rules('image', 'image');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('emp_id', 'emp_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_edit() {
        $this->form_validation->set_rules('msd_ID', 'msd_ID', 'trim|required');
        $this->form_validation->set_rules('emp_name', 'emp name', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
//        $this->form_validation->set_rules('dob', 'dob', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
//        $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
        $this->form_validation->set_rules('join_date', 'join date', 'trim|required');
        $this->form_validation->set_rules('department', 'department', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
//        $this->form_validation->set_rules('matial_status', 'matial status', 'trim|required');
        //$this->form_validation->set_rules('password', 'password', 'trim|required');
//        $this->form_validation->set_rules('experience', 'experience', 'trim|required');
//        $this->form_validation->set_rules('image', 'image');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('emp_id', 'emp_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_edit_pass() {
        // $this->form_validation->set_rules('msd_ID', 'msd_ID', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('emp_id', 'emp_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "employee.xls";
        $judul = "employee";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Emp Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Gender");
        xlsWriteLabel($tablehead, $kolomhead++, "Dob");
        xlsWriteLabel($tablehead, $kolomhead++, "Email");
        xlsWriteLabel($tablehead, $kolomhead++, "Mobile");
        xlsWriteLabel($tablehead, $kolomhead++, "Alternate Mobile");
        xlsWriteLabel($tablehead, $kolomhead++, "Category");
        xlsWriteLabel($tablehead, $kolomhead++, "Qualification");
        xlsWriteLabel($tablehead, $kolomhead++, "Join Date");
        xlsWriteLabel($tablehead, $kolomhead++, "Department");
        xlsWriteLabel($tablehead, $kolomhead++, "Designation");
        xlsWriteLabel($tablehead, $kolomhead++, "Matial Status");
//        xlsWriteLabel($tablehead, $kolomhead++, "Nationality");
        xlsWriteLabel($tablehead, $kolomhead++, "Experience");
//        xlsWriteLabel($tablehead, $kolomhead++, "Image");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Employee_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_name);
            xlsWriteLabel($tablebody, $kolombody++, $data->gender);
            xlsWriteLabel($tablebody, $kolombody++, $data->dob);
            xlsWriteLabel($tablebody, $kolombody++, $data->email);
            xlsWriteNumber($tablebody, $kolombody++, $data->mobile);
            xlsWriteNumber($tablebody, $kolombody++, $data->alternate_mobile);
            xlsWriteLabel($tablebody, $kolombody++, $data->category);
            xlsWriteLabel($tablebody, $kolombody++, $data->qualification);
            xlsWriteLabel($tablebody, $kolombody++, $data->join_date);
            xlsWriteNumber($tablebody, $kolombody++, $data->department);
            xlsWriteNumber($tablebody, $kolombody++, $data->designation);
            xlsWriteLabel($tablebody, $kolombody++, $data->matial_status);
//            xlsWriteNumber($tablebody, $kolombody++, $data->password);
            xlsWriteLabel($tablebody, $kolombody++, $data->experience);
//            xlsWriteLabel($tablebody, $kolombody++, $data->image);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}
