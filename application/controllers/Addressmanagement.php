<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Addressmanagement extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Addressmanagement_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $addressmanagement = $this->Addressmanagement_model->get_all();

        $data = array(
            'addressmanagement_data' => $addressmanagement
        );

          $data['content'] = 'addressmanagement/addressmanagement_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Addressmanagement_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'country' => $row->country,
		'state' => $row->state,
		'city' => $row->city,
		'zipcode' => $row->zipcode,
		'street' => $row->street,
		'fullAddress' => $row->fullAddress,
		'status' => $row->status,
	    );
             $data['content'] = 'addressmanagement/addressmanagement_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('addressmanagement'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('addressmanagement/create_action'),
	    'id' => set_value('id'),
	    'country' => set_value('country'),
	    'state' => set_value('state'),
	    'city' => set_value('city'),
	    'zipcode' => set_value('zipcode'),
	    'street' => set_value('street'),
	    'fullAddress' => set_value('fullAddress'),
	    'status' => set_value('status'),
	);
        $data['content'] = 'addressmanagement/addressmanagement_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'country' => $this->input->post('country',TRUE),
		'state' => $this->input->post('state',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'street' => $this->input->post('street',TRUE),
		'fullAddress' => $this->input->post('fullAddress',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Addressmanagement_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('addressmanagement'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Addressmanagement_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('addressmanagement/update_action'),
		'id' => set_value('id', $row->id),
		'country' => set_value('country', $row->country),
		'state' => set_value('state', $row->state),
		'city' => set_value('city', $row->city),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'street' => set_value('street', $row->street),
		'fullAddress' => set_value('fullAddress', $row->fullAddress),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'addressmanagement/addressmanagement_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('addressmanagement'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'country' => $this->input->post('country',TRUE),
		'state' => $this->input->post('state',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'street' => $this->input->post('street',TRUE),
		'fullAddress' => $this->input->post('fullAddress',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Addressmanagement_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('addressmanagement'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Addressmanagement_model->get_by_id($id);

        if ($row) {
            $this->Addressmanagement_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('addressmanagement'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('addressmanagement'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('country', 'country', 'trim|required');
	$this->form_validation->set_rules('state', 'state', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required');
	$this->form_validation->set_rules('street', 'street', 'trim|required');
	$this->form_validation->set_rules('fullAddress', 'fulladdress', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "addressmanagement.xls";
        $judul = "addressmanagement";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Country");
	xlsWriteLabel($tablehead, $kolomhead++, "State");
	xlsWriteLabel($tablehead, $kolomhead++, "City");
	xlsWriteLabel($tablehead, $kolomhead++, "Zipcode");
	xlsWriteLabel($tablehead, $kolomhead++, "Street");
	xlsWriteLabel($tablehead, $kolomhead++, "FullAddress");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Addressmanagement_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->country);
	    xlsWriteLabel($tablebody, $kolombody++, $data->state);
	    xlsWriteLabel($tablebody, $kolombody++, $data->city);
	    xlsWriteNumber($tablebody, $kolombody++, $data->zipcode);
	    xlsWriteLabel($tablebody, $kolombody++, $data->street);
	    xlsWriteLabel($tablebody, $kolombody++, $data->fullAddress);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Addressmanagement.php */
/* Location: ./application/controllers/Addressmanagement.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2021-01-05 14:30:42 */
