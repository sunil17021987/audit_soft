<?php

class Imageupload extends CI_Controller {

   /**
    * Manage __construct
    *
    * @return Response
   */
   public function __construct() { 
      parent::__construct(); 
      $this->load->helper(array('form', 'url')); 
      $this->load->model('Registration_model');
      $this->load->model('Employee_model');
  $this->load->library('form_validation');
   }

   /**
    * Manage index
    *
    * @return Response
   */
   public function index($id) { 
         $data['id']=$id;
         $this->load->view('imageUploadForm',$data); 
   } 
   public function upload($error = null) { 
        $id = $this->uri->segment(3);
        $data = array(
            'button' => 'Create',
            'action' => site_url('Imageupload/uploadImage'),
            'id' => set_value('id'),
            'gfg_events_id' => $id,
            'image' => set_value('image'),
            'date_of' => set_value('date_of'),
            'error' => $error,
        );
        $data['content'] = 'imageUploadForm_1';
        $this->load->view('common/master', $data);
  
   } 

   /**
    * Manage uploadImage
    *
    * @return Response
   */
   public function uploadImage() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->upload();
        } else {
             $this->load->view('slim', $data);
            $images = Slim::getImages();

            $data = array(
              
              
            );
              if ($images != false) {
                foreach ($images as $image) {
                    $file = Slim::saveFile_admin($image['output']['data'], $image['input']['name'], FCPATH . "uploads");
                    $data['image'] = 'uploads/' . $file['name'];
                }
            } else {
                $data['image'] = 'No';
            }

           // $baseurl = base_url() . 'uploads/' . $finfo['file_name'];
          //  $data['image'] = $baseurl;
            $id= $this->input->post('gfg_events_id', TRUE);
            $this->Employee_model->update($id, $data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('employee'));
        }
    }

     public function uploadImage_emp() {

        $config['upload_path'] = '././uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('imageUploadForm', $error);
        } else {
            $id = $this->input->post('emp_id', TRUE);
            $finfo = $this->upload->data();
            $baseurl = base_url() . 'uploads/' . $finfo['file_name'];
            $data['image'] = $baseurl;
            $this->Employee_model->update($id, $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('employee'));
            print_r('Image Uploaded Successfully.');
            exit;
        }
    }
     public function _rules() {
        $this->form_validation->set_rules('gfg_events_id', 'gfg events id', 'trim|required');
        //$this->form_validation->set_rules('image', 'image', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

} 

?>