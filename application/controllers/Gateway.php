<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gateway extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Employee_model');
        $this->load->model('Category_main_model');
        $this->load->model('Call_types_model');
        $this->load->model('Dispositions_model');
        $this->load->model('Communications_model');
        $this->load->model('Audit_model');
        $this->load->model('Category_sub_model');
        $this->load->model('Fatal_reason_model');
        $this->load->model('Calldurationpattern_model');
        $this->load->model('Feedback_status_model');
        $this->load->model('Agents_model');
        $this->load->model('Audit_remarks_model');

        $this->load->library('form_validation');
        $this->check_auth();
    }

    public function check_auth() {

        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login'));
    }

    public function index() {
        $employee = $this->Employee_model->get_all();

        $data = array(
            'employee_data' => $employee
        );
        $data['content'] = 'gateway/gateway_list';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_list', $data);
    }

    public function read($id) {
        $row = $this->Employee_model->get_by_id($id);
        if ($row) {
            $data = array(
                'emp_id' => $row->emp_id,
                'emp_name' => $row->emp_name,
                'gender' => $row->gender,
                'dob' => $row->dob,
                'email' => $row->email,
                'mobile' => $row->mobile,
                'alternate_mobile' => $row->alternate_mobile,
                'category' => $row->category,
                'qualification' => $row->qualification,
                'join_date' => $row->join_date,
                'department' => $row->department,
                'designation' => $row->designation,
                'matial_status' => $row->matial_status,
//		'password' => $row->password,
                'experience' => $row->experience,
                'image' => $row->image,
                'created_at' => $row->created_at,
            );
//            $this->load->view('employee/employee_read', $data);
            $data['content'] = 'employee/emp_profile';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function read_details($id) {
//        $data['fees_collect_data'] = $this->Fees_collect_model->get_fees($id);

        $data['row'] = $this->Employee_model->get_by_id1($id);

        $data['content'] = 'employee/emp_profile';
        $this->load->view('common/master', $data);
    }

    public function login() {
        $category_main = $this->Category_main_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('employee/create_action'),
            'emp_id' => set_value('emp_id'),
            'emp_name' => set_value('emp_name'),
            'gender' => set_value('gender'),
            'dob' => set_value('dob'),
            'email' => set_value('email'),
            'mobile' => set_value('mobile'),
            'alternate_mobile' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'address' => set_value('address'),
            'category' => set_value('category'),
            'qualification' => set_value('qualification'),
            'join_date' => set_value('join_date'),
            'department' => set_value('department'),
            'designation' => set_value('designation'),
            'matial_status' => set_value('matial_status'),
            'password' => set_value('password'),
            'experience' => set_value('experience'),
            'image' => set_value('image'),
            'created_at' => set_value('created_at'),
            'category_main_id' => '',
            'category_main' => $category_main
        );


        $data['content'] = 'gateway/login';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_form', $data);
    }

       public function login_cli() {
        $category_main = $this->Category_main_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('employee/create_action'),
            'emp_id' => set_value('emp_id'),
            'emp_name' => set_value('emp_name'),
            'gender' => set_value('gender'),
            'dob' => set_value('dob'),
            'email' => set_value('email'),
            'mobile' => set_value('mobile'),
            'alternate_mobile' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'address' => set_value('address'),
            'category' => set_value('category'),
            'qualification' => set_value('qualification'),
            'join_date' => set_value('join_date'),
            'department' => set_value('department'),
            'designation' => set_value('designation'),
            'matial_status' => set_value('matial_status'),
            'password' => set_value('password'),
            'experience' => set_value('experience'),
            'image' => set_value('image'),
            'created_at' => set_value('created_at'),
            'category_main_id' => '',
            'category_main' => $category_main
        );


        $data['content'] = 'gateway/login_cli';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_form', $data);
    }
    public function create() {
        if ($this->input->post('id', TRUE)) {
            $emp_id = $this->input->post('emp_id', TRUE);
            $id = $this->input->post('id', TRUE);
            $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $calldurationpattern = $this->Calldurationpattern_model->get_all();
            $feedback_status = $this->Feedback_status_model->get_all();
            $tdaysaudits = $this->Audit_model->count_TodaysAudit_by($id);
            $data = array(
                'agents_id' => set_value('agents_id', $rows->id),
                'agents_id' => set_value('agents_id', $rows->id),
                'TL_id' => set_value('TL_id', $rows->TL_id),
                'AM_id' => set_value('AM_id', $rows->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $rows->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $rows->employee_id_AM),
                'name' => set_value('name', $rows->name),
                'category_main' => $category_main,
                'rows' => $rows,
                'category_main_id' => '',
                'call_types_data' => $call_types,
                'Call_Type' => '',
                'dispositions_data' => $dispositions,
                't_d_b_a' => '',
                'c_t_a_p_p' => '',
                'communications_data' => $communications,
                'Opening' => '',
                'ActiveListening' => '',
                'Probing' => '',
                'Customer_Engagement' => '',
                'Empathy_where_required' => '',
                'Understanding' => '',
                'Professionalism' => '',
                'Politeness' => '',
                'Hold_Procedure' => '',
                'Closing' => '',
                'fatal_reason_data' => $fatal_reason,
                'Fatal_Reason' => '',
                'feedback_status_id' => '',
                'calldurationpattern_data' => $calldurationpattern,
                'feedback_status_data' => $feedback_status,
                'tdaysaudits' => $tdaysaudits
            );

//        *********************************************************************************************    
            $Y = date('Y');
            $m = date('m');
            $date_fromf = $Y . '-' . $m . '-' . '01';
            $date_tof = date('Y-m-d');

            $data['row'] = $this->Agents_model->get_all_by_joins_byID($id);
            $data['agentsDeatils'] = $this->Agents_model->get_audit_byidAll($id, $date_fromf, $date_tof);
            //        *********************************************************************************************           

            $data['call_duration_pattern'] = 'gateway/call_duration_pattern';
            $data['agentPerformanceform'] = 'gateway/agentPerformance';
            $designation = $this->session->userdata('designation');
            if ($designation == '1'){
              $data['content'] = 'gateway/gateway_form_tl';
        }else{
             $data['content'] = 'gateway/gateway_form';
        }
       
    
        $this->load->view('common/master', $data);
        } else {
            redirect(site_url('gateway/login'));
        }
    }

    public function audits_reports() {
        $calldurationpattern = $this->Calldurationpattern_model->get_all();
        $data = array(
            'calldurationpattern_data' => $calldurationpattern
        );
        $data['content'] = 'gateway/audits_reports';
        $data['call_duration_pattern'] = 'gateway/call_duration_pattern';
        $this->load->view('common/master', $data);
    }

    public function create_1() {
        $category_main = $this->Category_main_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('employee/create_action'),
            'emp_id' => set_value('emp_id'),
            'emp_name' => set_value('emp_name'),
            'gender' => set_value('gender'),
            'dob' => set_value('dob'),
            'email' => set_value('email'),
            'mobile' => set_value('mobile'),
            'alternate_mobile' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'address' => set_value('address'),
            'category' => set_value('category'),
            'qualification' => set_value('qualification'),
            'join_date' => set_value('join_date'),
            'department' => set_value('department'),
            'designation' => set_value('designation'),
            'matial_status' => set_value('matial_status'),
            'password' => set_value('password'),
            'experience' => set_value('experience'),
            'image' => set_value('image'),
            'created_at' => set_value('created_at'),
            'category_main_id' => '',
            'category_main' => $category_main
        );


        $data['content'] = 'gateway/gateway_form_1';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_form', $data);
    }

    public function create_action() {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'emp_name' => $this->input->post('emp_name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'dob' => $this->input->post('dob', TRUE),
                'email' => $this->input->post('email', TRUE),
                'mobile' => $this->input->post('mobile', TRUE),
                'alternate_mobile' => $this->input->post('alternate_mobile', TRUE),
                'address' => $this->input->post('address', TRUE),
                'category' => $this->input->post('category', TRUE),
                'qualification' => $this->input->post('qualification', TRUE),
                'join_date' => $this->input->post('join_date', TRUE),
                'department' => $this->input->post('department', TRUE),
                'designation' => $this->input->post('designation', TRUE),
                'matial_status' => $this->input->post('matial_status', TRUE),
                'password' => md5($this->input->post('password')),
                'experience' => $this->input->post('experience', TRUE),
                'image' => $this->input->post('image', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Employee_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('employee'));
        }
    }

    public function update($id) {
        $row = $this->Employee_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('employee/update_action'),
                'emp_id' => set_value('emp_id', $row->emp_id),
                'emp_name' => set_value('emp_name', $row->emp_name),
                'gender' => set_value('gender', $row->gender),
                'dob' => set_value('dob', $row->dob),
                'email' => set_value('email', $row->email),
                'mobile' => set_value('mobile', $row->mobile),
                'alternate_mobile' => set_value('alternate_mobile', $row->alternate_mobile),
                'address' => set_value('address', $row->address),
                'category' => set_value('category', $row->category),
                'qualification' => set_value('qualification', $row->qualification),
                'join_date' => set_value('join_date', $row->join_date),
                'department' => set_value('department', $row->department),
                'designation' => set_value('designation', $row->designation),
                'matial_status' => set_value('matial_status', $row->matial_status),
                'password' => set_value('password', $row->password),
                'experience' => set_value('experience', $row->experience),
                'image' => set_value('image', $row->image),
                'created_at' => set_value('created_at', $row->created_at),
            );
            //$this->load->view('employee/employee_form', $data);
            $data['category_list'] = $this->Category_model->get_category();
            $data['content'] = 'employee/emp_registration';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function update_action() {
        $this->_rules_edit();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('emp_id', TRUE));
        } else {
            $empdetails = $this->Employee_model->get_by_id($this->input->post('emp_id', TRUE));
            $password = "";
            if ($empdetails->password == $this->input->post('password', TRUE)) {
                $password = $this->input->post('password', TRUE);
            } else {
                $password = md5($this->input->post('password', TRUE));
            }
            $data = array(
                'emp_name' => $this->input->post('emp_name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'dob' => $this->input->post('dob', TRUE),
                'email' => $this->input->post('email', TRUE),
                'mobile' => $this->input->post('mobile', TRUE),
                'alternate_mobile' => $this->input->post('alternate_mobile', TRUE),
                'address' => $this->input->post('address', TRUE),
                'category' => $this->input->post('category', TRUE),
                'qualification' => $this->input->post('qualification', TRUE),
                'join_date' => $this->input->post('join_date', TRUE),
                'department' => $this->input->post('department', TRUE),
                'designation' => $this->input->post('designation', TRUE),
                'matial_status' => $this->input->post('matial_status', TRUE),
                'experience' => $this->input->post('experience', TRUE),
                'image' => $this->input->post('image', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
                'password' => $password
            );
//            $msg="";
//            if($this->input->post('password')!='' && $this->input->post('confirm_password')!='')
//            {
//                if($this->input->post('password') == $this->input->post('confirm_password')){
//                    $data['password']=md5($this->input->post('password'));
//                }
//                else
//                $msg="Passwords do not match!";
//            }            

            $this->Employee_model->update($this->input->post('emp_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success.');
            redirect(site_url('employee'));
        }
    }

    public function delete($id) {
        $row = $this->Employee_model->get_by_id($id);

        if ($row) {
            $this->Employee_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('employee'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('employee'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('emp_name', 'emp name', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        $this->form_validation->set_rules('dob', 'dob', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[employee.email]');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
        $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
        $this->form_validation->set_rules('join_date', 'join date', 'trim|required');
        $this->form_validation->set_rules('department', 'department', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
        $this->form_validation->set_rules('matial_status', 'matial status', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('experience', 'experience', 'trim|required');
        $this->form_validation->set_rules('image', 'image');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('emp_id', 'emp_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_edit() {
        $this->form_validation->set_rules('emp_name', 'emp name', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        $this->form_validation->set_rules('dob', 'dob', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
        $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
        $this->form_validation->set_rules('join_date', 'join date', 'trim|required');
        $this->form_validation->set_rules('department', 'department', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
        $this->form_validation->set_rules('matial_status', 'matial status', 'trim|required');
        //$this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('experience', 'experience', 'trim|required');
        $this->form_validation->set_rules('image', 'image');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('emp_id', 'emp_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "employee.xls";
        $judul = "employee";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Emp Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Gender");
        xlsWriteLabel($tablehead, $kolomhead++, "Dob");
        xlsWriteLabel($tablehead, $kolomhead++, "Email");
        xlsWriteLabel($tablehead, $kolomhead++, "Mobile");
        xlsWriteLabel($tablehead, $kolomhead++, "Alternate Mobile");
        xlsWriteLabel($tablehead, $kolomhead++, "Category");
        xlsWriteLabel($tablehead, $kolomhead++, "Qualification");
        xlsWriteLabel($tablehead, $kolomhead++, "Join Date");
        xlsWriteLabel($tablehead, $kolomhead++, "Department");
        xlsWriteLabel($tablehead, $kolomhead++, "Designation");
        xlsWriteLabel($tablehead, $kolomhead++, "Matial Status");
//        xlsWriteLabel($tablehead, $kolomhead++, "Nationality");
        xlsWriteLabel($tablehead, $kolomhead++, "Experience");
//        xlsWriteLabel($tablehead, $kolomhead++, "Image");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Employee_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_name);
            xlsWriteLabel($tablebody, $kolombody++, $data->gender);
            xlsWriteLabel($tablebody, $kolombody++, $data->dob);
            xlsWriteLabel($tablebody, $kolombody++, $data->email);
            xlsWriteNumber($tablebody, $kolombody++, $data->mobile);
            xlsWriteNumber($tablebody, $kolombody++, $data->alternate_mobile);
            xlsWriteLabel($tablebody, $kolombody++, $data->category);
            xlsWriteLabel($tablebody, $kolombody++, $data->qualification);
            xlsWriteLabel($tablebody, $kolombody++, $data->join_date);
            xlsWriteNumber($tablebody, $kolombody++, $data->department);
            xlsWriteNumber($tablebody, $kolombody++, $data->designation);
            xlsWriteLabel($tablebody, $kolombody++, $data->matial_status);
//            xlsWriteNumber($tablebody, $kolombody++, $data->password);
            xlsWriteLabel($tablebody, $kolombody++, $data->experience);
//            xlsWriteLabel($tablebody, $kolombody++, $data->image);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function get_records() {
        $data = array();
        $Login_ID = $this->input->post('Login_ID', TRUE);
        $rows = $this->Employee_model->get_idms($Login_ID);



        if (!empty($rows)) {
            $data['rows'] = $rows;
        } else {
            $data['rows'] = '';
        }

        echo json_encode($data);
    }
     public function get_records_cli() {
        $data = array();
        $Login_ID = $this->input->post('Login_ID', TRUE);
        $rowsQA = $this->Employee_model->get_idms_cli_QA($Login_ID);
//        echo $this->db->last_query();
//        die();
        
        $rowsTL = $this->Employee_model->get_idms_cli_TL($Login_ID);

        if (!empty($rowsQA) && empty($rowsTL)) {
            $data['rows'] = $rowsQA;
            $data['rows_qa'] =  anchor(site_url('Gateway/edit_audit_to_QA/' . $rowsQA->audit_id), '<i class="btn btn-info fa fa-pencil-square-o">Audit By QA Click</i>'); 
            $data['rows_tl'] ='';
            
            
        }
        elseif(empty($rowsQA) && !empty($rowsTL)){
              $data['rows'] = $rowsTL;
              $data['rows_qa'] = '';
              $data['rows_tl'] = anchor(site_url('Gateway/edit_audit_to_TL/' . $rowsTL->audit_id), '<i class="btn btn-info fa fa-pencil-square-o"> Audit By TL Click</i>');
        }
         elseif(!empty($rowsQA) && !empty($rowsTL)){
              $data['rows'] = $rowsQA;
              $data['rows_qa'] = anchor(site_url('Gateway/edit_audit_to_QA/' . $rowsQA->audit_id), '<i class="btn btn-info fa fa-pencil-square-o">Audit By QA Click</i>'); 
             $data['rows_tl'] =  anchor(site_url('Gateway/edit_audit_to_TL/' . $rowsTL->audit_id), '<i class="btn btn-info fa fa-pencil-square-o">Audit By TL Click</i>');;
        }
        else {
            $data['rows'] = '';
             $data['rows_qa'] = '';
             $data['rows_tl'] = '';
        }

        echo json_encode($data);
    }

    public function save_records() {

        $agents_id = $this->input->post('agents_id', TRUE);
        $Opening = implode(",", $this->input->post('Opening', TRUE));
        print_r($Opening);
    }

    public function edit_audit() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
                'agents_id' => set_value('agents_id', $row->id),
                'agents_id' => set_value('agents_id', $row->id),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit';
            $this->load->view('common/master', $data);
        }
    }
    
     public function edit_audit_tl_tl() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid_tl_tl($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
                'agents_id' => set_value('agents_id', $row->id),
                'agents_id' => set_value('agents_id', $row->id),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit_of_tl';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit_of_tl';
            $this->load->view('common/master', $data);
        }
    }
    
    
     public function edit_audit_tl(){

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid($id);
              $employee_tl = $this->Employee_model->get_all_TL();
        $employee_am = $this->Employee_model->get_all_AM();
            $data = array(
                'id' => set_value('id', $id),
                'employee_id_TL' =>$row->TL_id,
                'employee_id_AM' =>$row->AM_id,
                 'employee_data_tl' => $employee_tl,
            'employee_data_am' => $employee_am,
             );

           
            $data['content'] = 'gateway/edit_audit_am_tl';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit_am_tl';
            $this->load->view('common/master', $data);
        }
    }
    public function edit_audit_am_tl(){

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid($id);
              $employee_tl = $this->Employee_model->get_all_TL();
        $employee_am = $this->Employee_model->get_all_AM();
            $data = array(
                'id' => set_value('id', $id),
                'employee_id_TL' =>$row->TL_id,
                'employee_id_AM' =>$row->AM_id,
                 'employee_data_tl' => $employee_tl,
            'employee_data_am' => $employee_am,
             );

           
            $data['content'] = 'gateway/edit_audit_am_tl';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit_am_tl';
            $this->load->view('common/master', $data);
        }
    }
    
     public function edit_audit_am() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid_of_am($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
                'agents_id' => set_value('agents_id', $row->id),
                'agents_id' => set_value('agents_id', $row->id),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit_of_am';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit';
            $this->load->view('common/master', $data);
        }
    }
 public function edit_audit_to_QA() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
               // 'agents_id' => set_value('agents_id', $row->id),
                'agents_id' => set_value('agents_id', $row->agents_id),
                'audit_parent_id' => set_value('audit_parent_id', $row->audit_by),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit_add_AM';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit_add_AM';
            $this->load->view('common/master', $data);
        }
    }
     public function edit_audit_to_TL() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid_TL($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
                'agents_id' => set_value('agents_id', $row->id),
               
                'agents_id' => set_value('agents_id', $row->agents_id),
                'audit_parent_id' => set_value('audit_parent_id', $row->audit_by),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit_add_AM';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit_add_AM';
            $this->load->view('common/master', $data);
        }
    }
    public function get_cliduplicate() {
        $data = array();
        $CLI_Number = $this->input->post('CLI_Number', TRUE);
        $rows = $this->Audit_remarks_model->get_CLI_Number($CLI_Number);
        if (!empty($rows)) {
            $data['rows'] = $rows;
        } else {
            $data['rows'] = '';
        }

        echo json_encode($data);
    }
     public function get_cliduplicate_supervises() {
        $data = array();
        $CLI_Number = $this->input->post('CLI_Number', TRUE);
        $rows = $this->Audit_remarks_model->get_CLI_Number_supervises($CLI_Number);
        if (!empty($rows)) {
            $data['rows'] = $rows;
        } else {
            $data['rows'] = '';
        }

        echo json_encode($data);
    }
    
         public function update_action_ajax_am_tl() {
          $uid = 0;
          $id = $this->input->post('auditid', TRUE);
         if(!empty($id)){
         
           $data = array(
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE)
            );
       
              $uid =   $this->Audit_model->update($id, $data);
         }
if($uid>0){
    echo"Record Updated successfully";
}else{
    echo"Something is wrong";
}
    }
    
    
       public function login_supervises() {
        $category_main = $this->Category_main_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('employee/create_action'),
            'emp_id' => set_value('emp_id'),
            'emp_name' => set_value('emp_name'),
            'gender' => set_value('gender'),
            'dob' => set_value('dob'),
            'email' => set_value('email'),
            'mobile' => set_value('mobile'),
            'alternate_mobile' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'password' => set_value('alternate_mobile'),
            'address' => set_value('address'),
            'category' => set_value('category'),
            'qualification' => set_value('qualification'),
            'join_date' => set_value('join_date'),
            'department' => set_value('department'),
            'designation' => set_value('designation'),
            'matial_status' => set_value('matial_status'),
            'password' => set_value('password'),
            'experience' => set_value('experience'),
            'image' => set_value('image'),
            'created_at' => set_value('created_at'),
            'category_main_id' => '',
            'category_main' => $category_main
        );


        $data['content'] = 'gateway/login_supervises';
        $this->load->view('common/master', $data);
//        $this->load->view('employee/employee_form', $data);
    }
     public function create_supervises() {
        if ($this->input->post('id', TRUE)) {
            $emp_id = $this->input->post('emp_id', TRUE);
            $id = $this->input->post('id', TRUE);
            $rows = $this->Employee_model->get_idms_byid_supervises($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $calldurationpattern = $this->Calldurationpattern_model->get_all();
            $feedback_status = $this->Feedback_status_model->get_all();
            $tdaysaudits = $this->Audit_model->count_TodaysAudit_by_supervises($id);
            $data = array(
                'agents_id' => set_value('agents_id', $rows->id),
                'agents_id' => set_value('agents_id', $rows->id),
                'TL_id' => set_value('TL_id', $rows->TL_id),
                'AM_id' => set_value('AM_id', $rows->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $rows->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $rows->employee_id_AM),
                'name' => set_value('name', $rows->name),
                'category_main' => $category_main,
                'rows' => $rows,
                'category_main_id' => '',
                'call_types_data' => $call_types,
                'Call_Type' => '',
                'dispositions_data' => $dispositions,
                't_d_b_a' => '',
                'c_t_a_p_p' => '',
                'communications_data' => $communications,
                'Opening' => '',
                'ActiveListening' => '',
                'Probing' => '',
                'Customer_Engagement' => '',
                'Empathy_where_required' => '',
                'Understanding' => '',
                'Professionalism' => '',
                'Politeness' => '',
                'Hold_Procedure' => '',
                'Closing' => '',
                'fatal_reason_data' => $fatal_reason,
                'Fatal_Reason' => '',
                'feedback_status_id' => '',
                'calldurationpattern_data' => $calldurationpattern,
                'feedback_status_data' => $feedback_status,
                'tdaysaudits' => $tdaysaudits
            );

//        *********************************************************************************************    
            $Y = date('Y');
            $m = date('m');
            $date_fromf = $Y . '-' . $m . '-' . '01';
            $date_tof = date('Y-m-d');

            $data['row'] = $this->Agents_model->get_all_by_joins_byID_supervises($id);
            $data['agentsDeatils'] = $this->Agents_model->get_audit_byidAll_supervises($id, $date_fromf, $date_tof);
            //        *********************************************************************************************           

            $data['call_duration_pattern'] = 'gateway/call_duration_pattern';
            $data['agentPerformanceform'] = 'gateway/agentPerformance';
            $designation = $this->session->userdata('designation');
            $data['content'] = 'gateway/gateway_form_supervises';
       
    
        $this->load->view('common/master', $data);
        } else {
            redirect(site_url('gateway/login'));
        }
    }
     public function get_records_supervises() {
        $data = array();
        $Login_ID = $this->input->post('Login_ID', TRUE);
        $rows = $this->Employee_model->get_idms_supervises($Login_ID);



        if (!empty($rows)) {
            $data['rows'] = $rows;
        } else {
            $data['rows'] = '';
        }

        echo json_encode($data);
    }
    
    
      public function edit_audit_supervises() {

        $id = $this->uri->segment(3);
        if ($id != '') {
            $row = $this->Audit_model->get_audit_byid_supervises($id);

            $emp_id = $this->input->post('emp_id', TRUE);
            // $id = $this->input->post('id', TRUE);
            // $rows = $this->Employee_model->get_idms_byid($id);
            $category_main = $this->Category_main_model->get_all();
            $call_types = $this->Call_types_model->get_all();
            $dispositions = $this->Dispositions_model->get_all();
            $communications = $this->Communications_model->get_all();
            $fatal_reason = $this->Fatal_reason_model->get_all();
            $subcategory_main = $this->Category_sub_model->get_all_by_category_sub_main_id_all();
            $feedback_status = $this->Feedback_status_model->get_all();

            $data = array(
                'id' => set_value('id', $id),
                'agents_id' => set_value('agents_id', $row->id),
                'agents_id' => set_value('agents_id', $row->id),
                'TL_id' => set_value('TL_id', $row->TL_id),
                'AM_id' => set_value('AM_id', $row->AM_id),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'name' => set_value('name', $row->name),
                'category_main' => $category_main,
                'subcategory_main' => $subcategory_main,
                'call_types_data' => $call_types,
                'dispositions_data' => $dispositions,
                'communications_data' => $communications,
                'score' => set_value('score', $row->score),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'Call_Date' => set_value('Call_Date', date('d-m-Y', strtotime($row->Call_Date))),
                'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
                'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
                'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
                'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
                'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
                'Call_Type' => set_value('Call_Type', $row->Call_Type),
                'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
                'category_main_id' => set_value('category_main_id', $row->category_main_id),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
                'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
                'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
                'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
                'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
                'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
                't_d_b_a' => set_value('t_d_b_a', $row->t_d_b_a),
                'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
                'Opening' => explode(",", $row->Opening),
                'ActiveListening' => explode(",", $row->ActiveListening),
                'Probing' => set_value('Probing', explode(",", $row->Probing)),
                'Customer_Engagement' => set_value('Customer_Engagement', explode(",", $row->Customer_Engagement)),
                'Empathy_where_required' => set_value('Empathy_where_required', explode(",", $row->Empathy_where_required)),
                'Understanding' => set_value('Understanding', explode(",", $row->Understanding)),
                'Professionalism' => set_value('Professionalism', explode(",", $row->Professionalism)),
                'Politeness' => set_value('Politeness', explode(",", $row->Politeness)),
                'Hold_Procedure' => set_value('Hold_Procedure', explode(",", $row->Hold_Procedure)),
                'Closing' => set_value('Closing', explode(",", $row->Closing)),
                'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
                'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
                'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
                'fatal_reason_data' => $fatal_reason,
                'feedback_status_id' => set_value('feedback_status_id', $row->feedback_status_id),
                'feedback_status_data' => $feedback_status
            );


            $data['content'] = 'gateway/edit_audit__supervises';
            $this->load->view('common/master', $data);
        } else {
            $data['content'] = 'gateway/edit_audit__supervises';
            $this->load->view('common/master', $data);
        }
    }
    
    

}
