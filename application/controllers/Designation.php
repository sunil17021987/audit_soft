<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Designation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Designation_model');
        $this->load->library('form_validation');
        $this->check_auth();
    }
 public function check_auth()
    {
      
         $login_type = $this->session->userdata('validated');
         if(!$login_type == TRUE)
              redirect(base_url('login'));
          }
    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'designation/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'designation/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'designation/index.html';
            $config['first_url'] = base_url() . 'designation/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Designation_model->total_rows($q);
        $designation = $this->Designation_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'designation_data' => $designation,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
//        $this->load->view('designation/designation_list', $data);
        $data['content'] = 'designation/designation_list';
        $this->load->view('common/master', $data);
    }
     public function designation_api() {

        $data = $this->Designation_model->get_by_id_json();
        echo json_encode($data);
    }

    public function read($id) 
    {
        $row = $this->Designation_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'created_at' => $row->created_at,
	    );
            $this->load->view('designation/designation_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('designation'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('designation/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'created_at' => set_value('created_at'),
	);
//        $this->load->view('designation/designation_form', $data);
        $data['content'] = 'designation/designation_form';
        $this->load->view('common/master', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Designation_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('designation'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Designation_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('designation/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'created_at' => set_value('created_at', $row->created_at),
	    );
//            $this->load->view('designation/designation_form', $data);
            $data['content'] = 'designation/designation_form';
        $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('designation'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Designation_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('designation'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Designation_model->get_by_id($id);

        if ($row) {
            $this->Designation_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('designation'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('designation'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "designation.xls";
        $judul = "designation";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Designation_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Designation.php */
/* Location: ./application/controllers/Designation.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-10-25 20:42:38 */
/* http://harviacode.com */