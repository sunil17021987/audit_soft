<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Anticipatory extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Anticipatory_model');
        $this->load->library('form_validation');
    }

    public function index() {
//      *******************************************************
         $date_fromf = '';
        $date_tof = '';
        $Calling_Number='';
         $date_fromf = date('Y-m-d');
          $date_tof = date('Y-m-d');
       // $employees = $this->Employee_model->get_all();
        $bydaterange = $this->input->post('bydaterange', TRUE);
         $Calling_Number = $this->input->post('Calling_Number', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }      
//      ******************************************************  
        $anticipatory = $this->Anticipatory_model->get_all_by_agent_id($id = NULL,$date_fromf, $date_tof);
        $data = array(
            'anticipatory_data' => $anticipatory
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['bydaterange'] = date('d/m/Y', strtotime($date_fromf)).'-'.date('d/m/Y', strtotime($date_tof));
        $data['content'] = 'anticipatory/anticipatory_list';
        $this->load->view('common/master', $data);
    }

    public function read($id) {
        $row = $this->Anticipatory_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'agents_id' => $row->agents_id,
                'employee_id_AM' => $row->employee_id_AM,
                'employee_id_TL' => $row->employee_id_TL,
                'mobile_no' => $row->mobile_no,
                'issue_start' => $row->issue_start,
                'issue_end' => $row->issue_end,
                'issue_type' => $row->issue_type,
                'issue_remarks' => $row->issue_remarks,
                'approved_by' => $row->approved_by,
                'is_approved' => $row->is_approved,
                'approved_remarks' => $row->approved_remarks,
                'approved_date_time' => $row->approved_date_time,
                'issue_date_time' => $row->issue_date_time,
            );
            $data['content'] = 'anticipatory/anticipatory_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('anticipatory'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('anticipatory/create_action'),
            'id' => set_value('id'),
            'agents_id' => set_value('agents_id'),
            'employee_id_AM' => set_value('employee_id_AM'),
            'employee_id_TL' => set_value('employee_id_TL'),
            'mobile_no' => set_value('mobile_no'),
            'issue_start' => set_value('issue_start'),
            'issue_end' => set_value('issue_end'),
            'issue_type' => set_value('issue_type'),
            'issue_remarks' => set_value('issue_remarks'),
            'approved_by' => set_value('approved_by'),
            'is_approved' => set_value('is_approved'),
            'approved_remarks' => set_value('approved_remarks'),
            'approved_date_time' => set_value('approved_date_time'),
            'issue_date_time' => set_value('issue_date_time'),
        );
        $data['content'] = 'anticipatory/anticipatory_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'agents_id' => $this->input->post('agents_id', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'mobile_no' => $this->input->post('mobile_no', TRUE),
                'issue_start' => $this->input->post('issue_start', TRUE),
                'issue_end' => $this->input->post('issue_end', TRUE),
                'issue_type' => $this->input->post('issue_type', TRUE),
                'issue_remarks' => $this->input->post('issue_remarks', TRUE),
                'approved_by' => $this->input->post('approved_by', TRUE),
                'is_approved' => $this->input->post('is_approved', TRUE),
                'approved_remarks' => $this->input->post('approved_remarks', TRUE),
                'approved_date_time' => $this->input->post('approved_date_time', TRUE),
                'issue_date_time' => $this->input->post('issue_date_time', TRUE),
            );

            $this->Anticipatory_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('anticipatory'));
        }
    }

    public function update($id) {
        $row = $anticipatory = $this->Anticipatory_model->get_by_id($id);
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('anticipatory/update_action'),
                'id' => set_value('id', $row->id),
                'agents_id' => set_value('agents_id', $row->agents_id),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'mobile_no' => set_value('mobile_no', $row->mobile_no),
                'issue_start' => set_value('issue_start', $row->issue_start),
                'issue_end' => set_value('issue_end', $row->issue_end),
                'issue_type' => set_value('issue_type', $row->issue_type),
                'issue_remarks' => set_value('issue_remarks', $row->issue_remarks),
                'approved_by' => set_value('approved_by', $row->approved_by),
                'is_approved' => set_value('is_approved', $row->is_approved),
                'approved_remarks' => set_value('approved_remarks', $row->approved_remarks),
                'approved_date_time' => date('Y-m-d H:i'),
                'issue_date_time' => set_value('issue_date_time', $row->issue_date_time),
            );
            $data['content'] = 'anticipatory/anticipatory_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('anticipatory'));
        }
    }

    public function update_action() {
       $reg_id = $this->session->userdata('reg_id');
     
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                //'agents_id' => $this->input->post('agents_id',TRUE),
                //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
                //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
                //'mobile_no' => $this->input->post('mobile_no',TRUE),
                //'issue_start' => $this->input->post('issue_start',TRUE),
                //'issue_end' => $this->input->post('issue_end',TRUE),
                //'issue_type' => $this->input->post('issue_type',TRUE),
              //  'issue_remarks' => $this->input->post('issue_remarks', TRUE),
                'approved_by' => $reg_id,
                'is_approved' => $this->input->post('is_approved', TRUE),
                'approved_remarks' => $this->input->post('approved_remarks', TRUE),
                'approved_date_time' => date('Y-m-d H:i'),
                    //'issue_date_time' => $this->input->post('issue_date_time',TRUE),
            );

            $this->Anticipatory_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('anticipatory'));
        }
    }

    public function delete($id) {
        $row = $this->Anticipatory_model->get_by_id($id);

        if ($row) {
            $this->Anticipatory_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('anticipatory'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('anticipatory'));
        }
    }

    public function _rules() {
        //$this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
        //$this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
        //$this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
        //$this->form_validation->set_rules('mobile_no', 'mobile no', 'trim|required');
        //$this->form_validation->set_rules('issue_start', 'issue start', 'trim|required');
        //$this->form_validation->set_rules('issue_end', 'issue end', 'trim|required');
        //$this->form_validation->set_rules('issue_type', 'issue type', 'trim|required');
        //$this->form_validation->set_rules('issue_remarks', 'issue remarks', 'trim|required');
        //$this->form_validation->set_rules('approved_by', 'approved by', 'trim|required');
        $this->form_validation->set_rules('is_approved', 'is approved', 'trim|required');
        $this->form_validation->set_rules('approved_remarks', 'approved remarks', 'trim|required');
        //$this->form_validation->set_rules('approved_date_time', 'approved date time', 'trim|required');
        //$this->form_validation->set_rules('issue_date_time', 'issue date time', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "anticipatory.xls";
        $judul = "anticipatory";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue Date Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Agents");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
        xlsWriteLabel($tablehead, $kolomhead++, "Mobile No");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue Start");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue End");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue Type");
        xlsWriteLabel($tablehead, $kolomhead++, "ExtensionNo");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue Remarks");
        xlsWriteLabel($tablehead, $kolomhead++, "Approved By");
        xlsWriteLabel($tablehead, $kolomhead++, "Is Approved");
        xlsWriteLabel($tablehead, $kolomhead++, "Approved Remarks");
        xlsWriteLabel($tablehead, $kolomhead++, "Approved Date Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Issue Date Time");
        
                $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        
        
        $anticipatory = $this->Anticipatory_model->get_all_by_agent_id($id = NULL,$date_fromf, $date_tof);
        foreach ($anticipatory as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_date_time);
            xlsWriteLabel($tablebody, $kolombody++, $data->name);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_AM);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_TL);
            xlsWriteLabel($tablebody, $kolombody++, $data->mobile_no);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_start);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_end);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_type);
            xlsWriteLabel($tablebody, $kolombody++, $data->extensionNo);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_remarks);
            xlsWriteLabel($tablebody, $kolombody++, $data->approved_by);
            xlsWriteLabel($tablebody, $kolombody++, $data->is_approved);
            xlsWriteLabel($tablebody, $kolombody++, $data->approved_remarks);
            xlsWriteLabel($tablebody, $kolombody++, $data->approved_date_time);
            xlsWriteLabel($tablebody, $kolombody++, $data->issue_date_time);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Anticipatory.php */
/* Location: ./application/controllers/Anticipatory.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2020-08-06 07:41:02 */
