<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attendance_format extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Attendance_format_model');
        $this->load->library('form_validation');
    }

    public function index_1()
    {
        $attendance_format = $this->Attendance_format_model->get_all();

        $data = array(
            'attendance_format_data' => $attendance_format
        );

          $data['content'] = 'attendance_format/attendance_format_list';
        $this->load->view('common/master', $data);    
            
    }
    
     public function index()
    {
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $bydaterange = $this->input->post('bydaterange', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }else{
           $query= $this->Attendance_format_model->get_max_date();
           $date_fromf=$query->date_of;
           $date_tof=$query->date_of;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        if($this->input->post('delete', TRUE)=='delete'){
          $attendance_format_data = $this->Attendance_format_model->get_all_bydate_delete($date_fromf,$date_tof);
        }
         if($this->input->post('search', TRUE)=='search'){
           
        }
         $attendance_format_data = $this->Attendance_format_model->get_all_bydate($date_fromf,$date_tof);

        $data = array(
            'attendance_format_data' => $attendance_format_data,
            'bydaterange' => $bydaterange,
        );

        $data['content'] = 'attendance_format/attendance_format_list';
        $this->load->view('common/master', $data);    
            
    }
    
    

    public function read($id) 
    {
        $row = $this->Attendance_format_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'employeeName' => $row->employeeName,
		'TLName' => $row->TLName,
		'agent_Status' => $row->agent_Status,
		'batch' => $row->batch,
		'AMName' => $row->AMName,
		'Date_of' => $row->Date_of,
		'RosterShift' => $row->RosterShift,
		'Login' => $row->Login,
		'Attendance' => $row->Attendance,
		'MSD_ID' => $row->MSD_ID,
		'created_date' => $row->created_date,
	    );
             $data['content'] = 'attendance_format/attendance_format_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('attendance_format'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('attendance_format/create_action'),
	    'id' => set_value('id'),
	    'employeeName' => set_value('employeeName'),
	    'TLName' => set_value('TLName'),
	    'agent_Status' => set_value('agent_Status'),
	    'batch' => set_value('batch'),
	    'AMName' => set_value('AMName'),
	    'Date_of' => set_value('Date_of'),
	    'RosterShift' => set_value('RosterShift'),
	    'Login' => set_value('Login'),
	    'Attendance' => set_value('Attendance'),
	    'MSD_ID' => set_value('MSD_ID'),
	    'created_date' => set_value('created_date'),
	);
        $data['content'] = 'attendance_format/attendance_format_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'employeeName' => $this->input->post('employeeName',TRUE),
		'TLName' => $this->input->post('TLName',TRUE),
		'agent_Status' => $this->input->post('agent_Status',TRUE),
		'batch' => $this->input->post('batch',TRUE),
		'AMName' => $this->input->post('AMName',TRUE),
		'Date_of' => $this->input->post('Date_of',TRUE),
		'RosterShift' => $this->input->post('RosterShift',TRUE),
		'Login' => $this->input->post('Login',TRUE),
		'Attendance' => $this->input->post('Attendance',TRUE),
		'MSD_ID' => $this->input->post('MSD_ID',TRUE),
		'created_date' => $this->input->post('created_date',TRUE),
	    );

            $this->Attendance_format_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('attendance_format'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Attendance_format_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('attendance_format/update_action'),
		'id' => set_value('id', $row->id),
		'employeeName' => set_value('employeeName', $row->employeeName),
		'TLName' => set_value('TLName', $row->TLName),
		'agent_Status' => set_value('agent_Status', $row->agent_Status),
		'batch' => set_value('batch', $row->batch),
		'AMName' => set_value('AMName', $row->AMName),
		'Date_of' => set_value('Date_of', $row->Date_of),
		'RosterShift' => set_value('RosterShift', $row->RosterShift),
		'Login' => set_value('Login', $row->Login),
		'Attendance' => set_value('Attendance', $row->Attendance),
		'MSD_ID' => set_value('MSD_ID', $row->MSD_ID),
		'created_date' => set_value('created_date', $row->created_date),
	    );
            $data['content'] = 'attendance_format/attendance_format_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('attendance_format'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'employeeName' => $this->input->post('employeeName',TRUE),
		'TLName' => $this->input->post('TLName',TRUE),
		'agent_Status' => $this->input->post('agent_Status',TRUE),
		'batch' => $this->input->post('batch',TRUE),
		'AMName' => $this->input->post('AMName',TRUE),
		'Date_of' => $this->input->post('Date_of',TRUE),
		'RosterShift' => $this->input->post('RosterShift',TRUE),
		'Login' => $this->input->post('Login',TRUE),
		'Attendance' => $this->input->post('Attendance',TRUE),
		'MSD_ID' => $this->input->post('MSD_ID',TRUE),
		'created_date' => $this->input->post('created_date',TRUE),
	    );

            $this->Attendance_format_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('attendance_format'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Attendance_format_model->get_by_id($id);

        if ($row) {
            $this->Attendance_format_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('attendance_format'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('attendance_format'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('employeeName', 'employeename', 'trim|required');
	$this->form_validation->set_rules('TLName', 'tlname', 'trim|required');
	$this->form_validation->set_rules('agent_Status', 'agent status', 'trim|required');
	$this->form_validation->set_rules('batch', 'batch', 'trim|required');
	$this->form_validation->set_rules('AMName', 'amname', 'trim|required');
	$this->form_validation->set_rules('Date_of', 'date of', 'trim|required');
	$this->form_validation->set_rules('RosterShift', 'rostershift', 'trim|required');
	$this->form_validation->set_rules('Login', 'login', 'trim|required');
	$this->form_validation->set_rules('Attendance', 'attendance', 'trim|required');
	$this->form_validation->set_rules('MSD_ID', 'msd id', 'trim|required');
	$this->form_validation->set_rules('created_date', 'created date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "attendance_format.xls";
        $judul = "attendance_format";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "EmployeeName");
	xlsWriteLabel($tablehead, $kolomhead++, "TLName");
	xlsWriteLabel($tablehead, $kolomhead++, "Agent Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Batch");
	xlsWriteLabel($tablehead, $kolomhead++, "AMName");
	xlsWriteLabel($tablehead, $kolomhead++, "Date Of");
	xlsWriteLabel($tablehead, $kolomhead++, "RosterShift");
	xlsWriteLabel($tablehead, $kolomhead++, "Login");
	xlsWriteLabel($tablehead, $kolomhead++, "Attendance");
	xlsWriteLabel($tablehead, $kolomhead++, "MSD ID");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");

	foreach ($this->Attendance_format_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->employeeName);
	    xlsWriteLabel($tablebody, $kolombody++, $data->TLName);
	    xlsWriteLabel($tablebody, $kolombody++, $data->agent_Status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->batch);
	    xlsWriteLabel($tablebody, $kolombody++, $data->AMName);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Date_of);
	    xlsWriteLabel($tablebody, $kolombody++, $data->RosterShift);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Login);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Attendance);
	    xlsWriteLabel($tablebody, $kolombody++, $data->MSD_ID);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Attendance_format.php */
/* Location: ./application/controllers/Attendance_format.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2020-10-16 08:51:16 */
