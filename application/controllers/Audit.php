<?php
ini_set('memory_limit', '-1');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Audit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Audit_model');
        $this->load->model('Audit_remarks_model');
        $this->load->model('Communications_model');
        $this->load->model('Agentfeedback_model');
        $this->load->model('Fatal_reason_model');
        $this->load->model('Employee_model');
         $this->load->model('Agents_model');
          $this->load->model('Agent_performance_model');
           $this->load->helper("sumoftimes_helper");
        
        $this->load->library('form_validation');
    }

    public function index() {
        $audit = $this->Audit_model->get_all();

        $data = array(
            'audit_data' => $audit
        );

        $data['content'] = 'audit/audit_list';
        $this->load->view('common/master', $data);
    }

    public function read($id) {
        $row = $this->Audit_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'agents_id' => $row->agents_id,
                'employee_id_AM' => $row->employee_id_AM,
                'employee_id_TL' => $row->employee_id_TL,
                'score' => $row->score,
                'date' => $row->date,
                'status' => $row->status,
            );
            $data['content'] = 'audit/audit_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('audit/create_action'),
            'id' => set_value('id'),
            'agents_id' => set_value('agents_id'),
            'employee_id_AM' => set_value('employee_id_AM'),
            'employee_id_TL' => set_value('employee_id_TL'),
            'score' => set_value('score'),
            'date' => set_value('date'),
            'status' => set_value('status'),
        );
        $data['content'] = 'audit/audit_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'agents_id' => $this->input->post('agents_id', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'score' => $this->input->post('score', TRUE),
                'date' => $this->input->post('date', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Audit_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('audit'));
        }
    }

    public function update($id) {
        $row = $this->Audit_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('audit/update_action'),
                'id' => set_value('id', $row->id),
                'agents_id' => set_value('agents_id', $row->agents_id),
                'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
                'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
                'score' => set_value('score', $row->score),
                'date' => set_value('date', $row->date),
                'status' => set_value('status', $row->status),
            );
            $data['content'] = 'audit/audit_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'agents_id' => $this->input->post('agents_id', TRUE),
                'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
                'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
                'score' => $this->input->post('score', TRUE),
                'date' => $this->input->post('date', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Audit_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('audit'));
        }
    }

    public function delete($id) {
        $row = $this->Audit_model->get_by_id($id);

        if ($row) {
            $this->Audit_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('audit'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
        $this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
        $this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
        $this->form_validation->set_rules('score', 'score', 'trim|required');
        $this->form_validation->set_rules('date', 'date', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "audit.xls";
        $judul = "audit";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Agents Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
        xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
        xlsWriteLabel($tablehead, $kolomhead++, "Score");
        xlsWriteLabel($tablehead, $kolomhead++, "Date");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");

        foreach ($this->Audit_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->agents_id);
            xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_AM);
            xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_TL);
            xlsWriteNumber($tablebody, $kolombody++, $data->score);
            xlsWriteLabel($tablebody, $kolombody++, $data->date);
            xlsWriteLabel($tablebody, $kolombody++, $data->status);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function intoTime($seconds) {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }

    function secondsToTime($seconds_time) {
        if ($seconds_time < 24 * 60 * 60) {
            return gmdate('H:i:s', $seconds_time);
        } else {
            $hours = floor($seconds_time / 3600);
            $minutes = floor(($seconds_time - $hours * 3600) / 60);
            $seconds = floor($seconds_time - ($hours * 3600) - ($minutes * 60));
            return "$hours:$minutes:$seconds";
        }
    }
    
     public function save_records_am_audit() {
        $audit_time = $this->input->post('audit_time', TRUE);
        $audit_time = time() - $audit_time;

        $callDuration = (($this->input->post('Call_Dur_Min', TRUE) * 60) + $this->input->post('Call_Dur_Sec', TRUE));
        $callDuration = $this->secondsToTime($callDuration);

        $data = array(
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'score' => $this->input->post('score', TRUE),
            'date' => date('Y-m-d'),
            'audit_by' => $this->session->userdata('reg_id'),
            'audit_parent_id' => $this->input->post('audit_parent_id', TRUE),
            'callDuration' => $callDuration,
            'audit_time' => $this->intoTime($audit_time),
            'status' => '1',
        );
        $insert_id = $this->Audit_model->insert_am($data);
        $getstatus = $this->input->post('score', TRUE);
        if ($getstatus < 90) {
            $getstatus = 'Pending';
        } else {
            $getstatus = 'NA';
        }

        $data_rem = array(
            'audit_id' => $insert_id,
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'callDuration' => $callDuration,
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            'date' => date('Y-m-d'),
            'audit_time' => $this->intoTime($audit_time),
            'feedback_status_id' => $getstatus,
            'status' => '1',
        );

        $insert_idr = $this->Audit_remarks_model->insert_am($data_rem);

        if ($insert_idr) {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-green">Success</small>');
            redirect(site_url('Gateway/login_cli'));
        } else {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-red">Success</small>');
            redirect(site_url('Gateway/login_cli'));
        }
    }

    public function save_records() {
          $rows = $this->Audit_model->get_CLI_Number_bycli($this->input->post('Calling_Number', TRUE), $this->input->post('agents_id', TRUE),$this->input->post('CLI_Number', TRUE));
        if (!empty($rows)) {
            redirect(site_url('Gateway/login'));
        }
        
        
        $audit_time = $this->input->post('audit_time', TRUE);
        $audit_time = time() - $audit_time;

        $callDuration = (($this->input->post('Call_Dur_Min', TRUE) * 60) + $this->input->post('Call_Dur_Sec', TRUE));
        $callDuration = $this->secondsToTime($callDuration);

        $data = array(
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'score' => $this->input->post('score', TRUE),
            'date' => date('Y-m-d'),
            'audit_by' => $this->session->userdata('reg_id'),
            'callDuration' => $callDuration,
            'audit_time' => $this->intoTime($audit_time),
            'status' => '1',
        );
        $insert_id = $this->Audit_model->insert($data);
        $getstatus = $this->input->post('score', TRUE);
        if ($getstatus < 90) {
            $getstatus = 'Pending';
        } else {
            $getstatus = 'NA';
        }

        $data_rem = array(
            'audit_id' => $insert_id,
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'callDuration' => $callDuration,
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            'date' => date('Y-m-d'),
            'audit_time' => $this->intoTime($audit_time),
            'feedback_status_id' => $getstatus,
            'status' => '1',
        );

        $insert_idr = $this->Audit_remarks_model->insert($data_rem);

        if ($insert_idr) {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-green">Success</small>');
            redirect(site_url('Gateway/login'));
        } else {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-red">Success</small>');
            redirect(site_url('Gateway/login'));
        }
    }

    public function save_records_tl() {
         
          $rows = $this->Audit_model->get_CLI_Number_tl_bycli($this->input->post('Calling_Number', TRUE), $this->input->post('agents_id', TRUE),$this->input->post('CLI_Number', TRUE));
        if (!empty($rows)) {
            redirect(site_url('Gateway/login'));
        }
        $audit_time = $this->input->post('audit_time', TRUE);
        $audit_time = time() - $audit_time;

        $callDuration = (($this->input->post('Call_Dur_Min', TRUE) * 60) + $this->input->post('Call_Dur_Sec', TRUE));
        $callDuration = $this->secondsToTime($callDuration);

        $data = array(
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'score' => $this->input->post('score', TRUE),
            'date' => date('Y-m-d'),
            'audit_by' => $this->session->userdata('reg_id'),
            'callDuration' => $callDuration,
            'audit_time' => $this->intoTime($audit_time),
            'status' => '1',
        );
        $insert_id = $this->Audit_model->insert_tl($data);
        $getstatus = $this->input->post('score', TRUE);
        if ($getstatus < 90) {
            $getstatus = 'Pending';
        } else {
            $getstatus = 'NA';
        }

        $data_rem = array(
            'audit_id' => $insert_id,
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'callDuration' => $callDuration,
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            'date' => date('Y-m-d'),
            'audit_time' => $this->intoTime($audit_time),
            'feedback_status_id' => $getstatus,
            'status' => '1',
        );

        $insert_idr = $this->Audit_remarks_model->insert_tl($data_rem);

        if ($insert_idr) {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-green">Success</small>');
            redirect(site_url('Gateway/login'));
        } else {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-red">Success</small>');
            redirect(site_url('Gateway/login'));
        }
    }
 public function save_records_AM() {
      $rows = $this->Audit_model->get_CLI_Number_am($this->input->post('Calling_Number', TRUE), $this->input->post('agents_id', TRUE),$this->input->post('CLI_Number', TRUE));
        if (!empty($rows)) {
            redirect(site_url('Gateway/login'));
        }
        $audit_time = $this->input->post('audit_time', TRUE);
        $audit_time = time() - $audit_time;

        $callDuration = (($this->input->post('Call_Dur_Min', TRUE) * 60) + $this->input->post('Call_Dur_Sec', TRUE));
        $callDuration = $this->secondsToTime($callDuration);

        $data = array(
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'score' => $this->input->post('score', TRUE),
            'date' => date('Y-m-d'),
            'audit_by' => $this->session->userdata('reg_id'),
            'callDuration' => $callDuration,
            'audit_time' => $this->intoTime($audit_time),
            'status' => '1',
        );
        $insert_id = $this->Audit_model->insert_tl($data);
        $getstatus = $this->input->post('score', TRUE);
        if ($getstatus < 90) {
            $getstatus = 'Pending';
        } else {
            $getstatus = 'NA';
        }

        $data_rem = array(
            'audit_id' => $insert_id,
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'callDuration' => $callDuration,
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            'date' => date('Y-m-d'),
            'audit_time' => $this->intoTime($audit_time),
            'feedback_status_id' => $getstatus,
            'status' => '1',
        );

        $insert_idr = $this->Audit_remarks_model->insert_am($data_rem);

        if ($insert_idr) {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-green">Success</small>');
            redirect(site_url('Gateway/login'));
        } else {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-red">Success</small>');
            redirect(site_url('Gateway/login'));
        }
    }
    function clean($string) {
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function audits() {
        $date_fromf = '';
        $date_tof = '';
        $Calling_Number='';
       // $employees = $this->Employee_model->get_all();
        $bydaterange = $this->input->post('bydaterange', TRUE);
         $Calling_Number = $this->input->post('Calling_Number', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($Calling_Number != '') {
            $Calling_Number = $this->input->post('Calling_Number', TRUE);
        }

        $designation = $this->session->userdata('designation');
        if ($designation == '1') {
            $audit = $this->Audit_model->get_audit_by_tl($date_fromf, $date_tof,$Calling_Number);
        } else {
            $audit = $this->Audit_model->get_audit_by($date_fromf, $date_tof,$Calling_Number);
            //  echo $this->db->last_query();
        }


        $data = array(
            'audit_data' => $audit,
            'Calling_Number' =>$Calling_Number
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['content'] = 'audit/audits';
        $this->load->view('common/master', $data);
    }

    public function allAudits() {
        $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $Calling_Number='';
        $employees = $this->Employee_model->get_all();


        $bydaterange = $this->input->post('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
         $Calling_Number = $this->input->post('Calling_Number', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }
         if ($Calling_Number != '') {
            $Calling_Number = $this->input->post('Calling_Number', TRUE);
        }

        $designation = $this->session->userdata('designation');
        $audit = $this->Audit_model->get_All_audit_by($date_fromf, $date_tof, $audit_by,$Calling_Number);
        //echo $this->db->last_query();


        $data = array(
            'audit_data' => $audit,
            'bydaterange' => $bydaterange,
            'employees' => $employees,
            'audit_by' => $audit_by,
           'Calling_Number' =>$Calling_Number
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['content'] = 'audit/allAudits';
        $this->load->view('common/master', $data);
    }

    public function allAuditsTl() {
        $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $employees = $this->Employee_model->get_all();


        $bydaterange = $this->input->post('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $designation = $this->session->userdata('designation');
        $audit = $this->Audit_model->get_All_audit_by_tl($date_fromf, $date_tof, $audit_by);


        $data = array(
            'audit_data' => $audit,
            'bydaterange' => $bydaterange,
            'employees' => $employees,
            'audit_by' => $audit_by
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['content'] = 'audit/allAudits_Tl';
        $this->load->view('common/master', $data);
    }
    
      public function allAuditsAM() {
        $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $employees = $this->Employee_model->get_all();


        $bydaterange = $this->input->post('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $designation = $this->session->userdata('designation');
        $audit = $this->Audit_model->get_All_audit_by_AM($date_fromf, $date_tof, $audit_by);
      //  echo$this->db->last_query();
        


        $data = array(
            'audit_data' => $audit,
            'bydaterange' => $bydaterange,
            'employees' => $employees,
            'audit_by' => $audit_by
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['content'] = 'audit/allAudits_AM';
        $this->load->view('common/master', $data);
    }

    public function edit_records() {
        $id = $this->input->post('id', TRUE);
        $data = array(
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            'score' => $this->input->post('score', TRUE),
                //'date' => date('Y-m-d'),
                // 'audit_by'=>$this->session->userdata('reg_id'),
                //'status' => '1',
        );
        $this->Audit_model->update($id, $data);
        $score = $this->input->post('score', TRUE);
        $data_rem = array(
            //'audit_id' => $insert_id,
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            //'date' => date('Y-m-d'),
            //'status' =>'1',
            'feedback_status_id' => ($score < 90) ? $this->input->post('feedback_status_id', TRUE) : 'NA',
        );

        $this->Audit_remarks_model->update_by_audit_id($id, $data_rem);

        if ($id) {
            echo'1';
        } else {
            echo'0';
        }
    }
    
     public function edit_records_of_tl() {
        $id = $this->input->post('id', TRUE);
        $data = array(
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            'score' => $this->input->post('score', TRUE),
                //'date' => date('Y-m-d'),
                // 'audit_by'=>$this->session->userdata('reg_id'),
                //'status' => '1',
        );
        $this->Audit_model->update_of_tl($id, $data);
        $score = $this->input->post('score', TRUE);
        $data_rem = array(
            //'audit_id' => $insert_id,
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            //'date' => date('Y-m-d'),
            //'status' =>'1',
            'feedback_status_id' => ($score < 90) ? $this->input->post('feedback_status_id', TRUE) : 'NA',
        );

        $this->Audit_remarks_model->update_by_audit_id_of_tl($id, $data_rem);

        if ($id) {
            echo'1';
        } else {
            echo'0';
        }
    }
     public function edit_records_of_am() {
        $id = $this->input->post('id', TRUE);
        $data = array(
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            'score' => $this->input->post('score', TRUE),
                //'date' => date('Y-m-d'),
                // 'audit_by'=>$this->session->userdata('reg_id'),
                //'status' => '1',
        );
        $this->Audit_model->update_of_am($id, $data);
        $score = $this->input->post('score', TRUE);
        $data_rem = array(
            //'audit_id' => $insert_id,
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            //'date' => date('Y-m-d'),
            //'status' =>'1',
            'feedback_status_id' => ($score < 90) ? $this->input->post('feedback_status_id', TRUE) : 'NA',
        );

        $this->Audit_remarks_model->update_by_audit_id_of_am($id, $data_rem);

        if ($id) {
            echo'1';
        } else {
            echo'0';
        }
    }

    public function excelExport1() {

        $this->load->library('excel');


        $audit_by = $this->session->userdata('reg_id');
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        $rows = $this->Audit_model->get_audit_byidAll($audit_by, $date_fromf, $date_tof);
        // echo$this->db->last_query();
        // die();
        $this->load->helper('exportexcel');

        $namaFile = date('d-m-Y') . "_audit.xls";
        $judul = "audit";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "SR No.");
        xlsWriteLabel($tablehead, $kolomhead++, "Evaluator Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Login ID");
        xlsWriteLabel($tablehead, $kolomhead++, "EMP-ID");
        xlsWriteLabel($tablehead, $kolomhead++, "CRM-ID");
        xlsWriteLabel($tablehead, $kolomhead++, "Adviser Name");
        xlsWriteLabel($tablehead, $kolomhead++, "TL -Name");
        xlsWriteLabel($tablehead, $kolomhead++, "AM -Name");
        xlsWriteLabel($tablehead, $kolomhead++, "DOP");
        xlsWriteLabel($tablehead, $kolomhead++, "AON");
        xlsWriteLabel($tablehead, $kolomhead++, "Process");
        xlsWriteLabel($tablehead, $kolomhead++, "Call Date");
        xlsWriteLabel($tablehead, $kolomhead++, "Time Of Call");
        xlsWriteLabel($tablehead, $kolomhead++, "Week");
        xlsWriteLabel($tablehead, $kolomhead++, "Calling Number");
        xlsWriteLabel($tablehead, $kolomhead++, "CLI Number");
        xlsWriteLabel($tablehead, $kolomhead++, "Call Duration (Min)");
        xlsWriteLabel($tablehead, $kolomhead++, "Call Duration (Sec)");
        xlsWriteLabel($tablehead, $kolomhead++, "Call duration Bucket");
        xlsWriteLabel($tablehead, $kolomhead++, "Evaluation Date");
        xlsWriteLabel($tablehead, $kolomhead++, "Call Category (As per process)");
        xlsWriteLabel($tablehead, $kolomhead++, "Call Sub Category (As per process)");
        xlsWriteLabel($tablehead, $kolomhead++, "Category As per CCR");
        xlsWriteLabel($tablehead, $kolomhead++, "Sub-Category As per CCR");
        xlsWriteLabel($tablehead, $kolomhead++, "Consumer`s Concern");
        xlsWriteLabel($tablehead, $kolomhead++, "Resolution Given By Adviser");
        xlsWriteLabel($tablehead, $kolomhead++, "Communication Remarks");
        xlsWriteLabel($tablehead, $kolomhead++, "QME Remarks");
        xlsWriteLabel($tablehead, $kolomhead++, "Process Suggestion If Any");
        xlsWriteLabel($tablehead, $kolomhead++, "Fatal Reason");
        xlsWriteLabel($tablehead, $kolomhead++, "Opening");
        xlsWriteLabel($tablehead, $kolomhead++, "Active Listening");
        xlsWriteLabel($tablehead, $kolomhead++, "Probing");
        xlsWriteLabel($tablehead, $kolomhead++, "Customer Engagement/ Rapport Building");
        xlsWriteLabel($tablehead, $kolomhead++, "Empathy where required");
        xlsWriteLabel($tablehead, $kolomhead++, "Understanding (Both CSR & Customer perspective)");
        xlsWriteLabel($tablehead, $kolomhead++, "Professionalism");
        xlsWriteLabel($tablehead, $kolomhead++, "Politeness & Courtesy");
        xlsWriteLabel($tablehead, $kolomhead++, "Hold Procedure");
        xlsWriteLabel($tablehead, $kolomhead++, "Closing");
        xlsWriteLabel($tablehead, $kolomhead++, "Opening(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Active Listening(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Probing(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Customer Engagement/ Rapport Building(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Empathy where required(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Understanding (Both CSR & Customer perspective)(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Professionalism(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Politeness & Courtesy(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Hold Procedure(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Closing(Scored)");
        xlsWriteLabel($tablehead, $kolomhead++, "Correct/Complete Tagging");
        xlsWriteLabel($tablehead, $kolomhead++, "Tagging Done by the adviser");
        xlsWriteLabel($tablehead, $kolomhead++, "correct Tagging as per process");
        xlsWriteLabel($tablehead, $kolomhead++, "Accurate / Complete & Correct Resolution");
        xlsWriteLabel($tablehead, $kolomhead++, "Critical Areas treated as Fatal");
        xlsWriteLabel($tablehead, $kolomhead++, "Audit Count");
        xlsWriteLabel($tablehead, $kolomhead++, "Fatal count");
        xlsWriteLabel($tablehead, $kolomhead++, "Quality scorable");
        xlsWriteLabel($tablehead, $kolomhead++, "Quality Scored");
        xlsWriteLabel($tablehead, $kolomhead++, "Shift");
        xlsWriteLabel($tablehead, $kolomhead++, "Tenure");
        xlsWriteLabel($tablehead, $kolomhead++, "Call type");


        foreach ($rows as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->audit_by);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->name);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_TL);
            xlsWriteLabel($tablebody, $kolombody++, $data->employee_id_AM);
            xlsWriteLabel($tablebody, $kolombody++, $data->DOJ);
            xlsWriteLabel($tablebody, $kolombody++, $data->AON);
            xlsWriteLabel($tablebody, $kolombody++, $data->process);
            xlsWriteLabel($tablebody, $kolombody++, $data->Call_Date);
            xlsWriteLabel($tablebody, $kolombody++, $data->Time_Of_Call);
            xlsWriteLabel($tablebody, $kolombody++, 'week');
            xlsWriteLabel($tablebody, $kolombody++, $data->Calling_Number);
            xlsWriteLabel($tablebody, $kolombody++, $data->CLI_Number);
            xlsWriteLabel($tablebody, $kolombody++, $data->Call_Dur_Min);
            xlsWriteLabel($tablebody, $kolombody++, $data->Call_Dur_Sec);
            xlsWriteLabel($tablebody, $kolombody++, 'Call duration Bucket');
            xlsWriteLabel($tablebody, $kolombody++, $data->date);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_main_name);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_name);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_main_id_crr);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_id_crr);
            xlsWriteLabel($tablebody, $kolombody++, $data->Consumers_Concern);
            xlsWriteLabel($tablebody, $kolombody++, $data->r_g_y_a);
            xlsWriteLabel($tablebody, $kolombody++, $data->r_g_y_a);
            xlsWriteLabel($tablebody, $kolombody++, $data->QME_Remarks);
            xlsWriteLabel($tablebody, $kolombody++, $data->p_s_if_a);
            xlsWriteLabel($tablebody, $kolombody++, $data->Fatal_Reason);
            xlsWriteLabel($tablebody, $kolombody++, $data->Opening);
            xlsWriteLabel($tablebody, $kolombody++, $data->ActiveListening);
            xlsWriteLabel($tablebody, $kolombody++, $data->Probing);
            xlsWriteLabel($tablebody, $kolombody++, $data->Customer_Engagement);
            xlsWriteLabel($tablebody, $kolombody++, $data->Empathy_where_required);
            xlsWriteLabel($tablebody, $kolombody++, $data->Understanding);
            xlsWriteLabel($tablebody, $kolombody++, $data->Professionalism);
            xlsWriteLabel($tablebody, $kolombody++, $data->Politeness);
            xlsWriteLabel($tablebody, $kolombody++, $data->Hold_Procedure);
            xlsWriteLabel($tablebody, $kolombody++, $data->Closing);
            xlsWriteNumber($tablebody, $kolombody++, $data->OpeningScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->ActiveListeningScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->ProbingScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->Customer_EngagementScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->Empathy_where_requiredScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->UnderstandingScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->ProfessionalismScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->PolitenessScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->Hold_ProcedureScored);
            xlsWriteNumber($tablebody, $kolombody++, $data->ClosingScored);
            xlsWriteLabel($tablebody, $kolombody++, $data->Correct_smaller);
            xlsWriteLabel($tablebody, $kolombody++, $data->Correct_smaller);
            xlsWriteLabel($tablebody, $kolombody++, $data->Correct_smaller);
            xlsWriteLabel($tablebody, $kolombody++, $data->Accurate_Complete);
            xlsWriteLabel($tablebody, $kolombody++, $data->Accurate_Complete);
            xlsWriteLabel($tablebody, $kolombody++, $data->Accurate_Complete);
            xlsWriteLabel($tablebody, $kolombody++, 'Audit Count');
            xlsWriteLabel($tablebody, $kolombody++, 100);
            xlsWriteNumber($tablebody, $kolombody++, $data->score);
            xlsWriteLabel($tablebody, $kolombody++, $data->shift_name);
            xlsWriteLabel($tablebody, $kolombody++, 'Tenure');
            xlsWriteLabel($tablebody, $kolombody++, 'Call type');

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function excelExport() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        $audit_by = $this->session->userdata('reg_id');
        // $audit_by ='';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }

        $communications = $this->Communications_model->get_all();


        $designation = $this->session->userdata('designation');
        if ($designation == '1') {
            $rows = $this->Audit_model->get_audit_byidAll_tl($audit_by, $date_fromf, $date_tof);
        } else {
            $rows = $this->Audit_model->get_audit_byidAll($audit_by, $date_fromf, $date_tof);
        }


        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        foreach ($rows as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $sno);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->Call_Date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }

    public function excelExportAll() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll($audit_by, $date_fromf, $date_tof, $audit_by);
//       echo $this->db->last_query();
//       die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }
     public function excelExportAllTL() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data_TL-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll_tl($audit_by, $date_fromf, $date_tof, $audit_by);
//       echo $this->db->last_query();
//       die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }
     public function excelExportAllAM() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data_MOM-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll_AM($audit_by, $date_fromf, $date_tof, $audit_by);
    //  echo $this->db->last_query();
    //  die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BP1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BP1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        $objPHPExcel->getActiveSheet()->SetCellValue('BP1', "Audit parent");
        
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BP' . $rowCount, $data->audit_parent_name);
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 67; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }
   public function excelExportAllRep() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll($audit_by, $date_fromf, $date_tof, $audit_by);
//       echo $this->db->last_query();
//       die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }
    public function excelExportAllRep1() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll($audit_by, $date_fromf, $date_tof, $audit_by);
      // echo $this->db->last_query();
      // die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
//        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
//        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
//        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
//        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->Call_Date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
//            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
//            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
//            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
//            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }

    public function getopenstring($communications, $keydata) {
        $string = '';
        $Opening_arr = explode(",", $communications);
        if ($keydata != '0') {
            $keydata_arr = explode(",", $keydata);
            for ($i = 0; $i <= count($Opening_arr); $i++) {
                if (!empty($keydata_arr[$i])) {
                    $string = $string . $Opening_arr[$keydata_arr[$i] - 1] . ',';
                }
            }
            return $string;
        }
    }

    public function CriticalAreastreatedFatal($Politeness) {
        $keydata_arr = explode(",", $Politeness);
        if ($keydata_arr[0] != 0) {
            if (in_array(3, $keydata_arr)) {
                return"1";
            } else {
                return"2";
            }
        }
        return '1';
    }

    public function week_between_two_dates1($date1, $date2) {
        $first = DateTime::createFromFormat('m/d/Y', $date1);
        $second = DateTime::createFromFormat('m/d/Y', $date2);
        if ($date1 > $date2)
            return $this->week_between_two_dates($date2, $date1);
        return floor($first->diff($second)->days / 7);
    }

    public function week_between_two_dates($date1, $date2) {
        $day = date('d', strtotime($date1));
        switch ($day) {
            case ($day <= 7):
                return "1";
                break;
            case ($day <= 14):
                return "2";
                break;
            case ($day <= 21):
                return "3";
                break;
            case ($day >= 22):
                return "4";
                break;
            default:
                return "0";
        }
    }

    function Tenure($AON) {
        switch ($AON) {
            case ($AON <= 30):
                return "0-30 Days";
                break;
            case ($AON > 30 && $AON <= 60):
                return "31-60 Days";
                break;
            case ($AON > 60 && $AON <= 90):
                return "61-90 Days";
                break;
            case ($AON > 90):
                return "> 90 Days";
                break;
            default:
                return "0";
        }
    }

    public function calldurationBucket($minutes) {
        switch ($minutes) {
            case ($minutes <= 59):
                return "0-60 Sec.";
                break;
            case ($minutes >= 60 && $minutes < 180):
                return "1-3 Min.";
                break;
            case ($minutes >= 180 && $minutes < 240):
                return "3-4 Min.";
                break;
            case ($minutes >= 240 && $minutes < 300):
                return "4-5 Min.";
                break;
            case ($minutes >= 300 && $minutes < 420):
                return "5-7 Min.";
                break;
            case ($minutes >= 420 && $minutes < 480):
                return "7-8 Min.";
                break;
            case ($minutes >= 480 && $minutes < 600):
                return "8-10 Min.";
                break;
            case ($minutes >= 600 && $minutes < 660):
                return "10-11 Min.";
                break;
            case ($minutes >= 660 && $minutes < 720):
                return "11-12 Min.";
                break;
            case ($minutes >= 720):
                return "More than 12 Min";
                break;
            default:
                return "No";
        }
    }
    
    
     public function save_records_supervises() {
        $audit_time = $this->input->post('audit_time', TRUE);
        $audit_time = time() - $audit_time;

        $callDuration = (($this->input->post('Call_Dur_Min', TRUE) * 60) + $this->input->post('Call_Dur_Sec', TRUE));
        $callDuration = $this->secondsToTime($callDuration);

        $data = array(
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'score' => $this->input->post('score', TRUE),
            'date' => date('Y-m-d'),
            'audit_by' => $this->session->userdata('reg_id'),
            'callDuration' => $callDuration,
            'audit_time' => $this->intoTime($audit_time),
            'status' => '1',
        );
        $insert_id = $this->Audit_model->insert_supervises($data);
        $getstatus = $this->input->post('score', TRUE);
        if ($getstatus < 90) {
            $getstatus = 'Pending';
        } else {
            $getstatus = 'NA';
        }

        $data_rem = array(
            'audit_id' => $insert_id,
            'agents_id' => $this->input->post('agents_id', TRUE),
            'employee_id_TL' => $this->input->post('employee_id_TL', TRUE),
            'employee_id_AM' => $this->input->post('employee_id_AM', TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'callDuration' => $callDuration,
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            'date' => date('Y-m-d'),
            'audit_time' => $this->intoTime($audit_time),
            'feedback_status_id' => $getstatus,
            'status' => '1',
        );

        $insert_idr = $this->Audit_remarks_model->insert_supervises($data_rem);

        if ($insert_idr) {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-green">Success</small>');
            redirect(site_url('Gateway/login_supervises'));
        } else {
            $this->session->set_flashdata('message', '<small class="label pull-right bg-red">Success</small>');
            redirect(site_url('Gateway/login_supervises'));
        }
    }
    
    
     public function allAudits_supervises() {
        $date_fromf = '';
        $date_tof = '';
        $bydaterange = '';
        $audit_by = '';
        $Calling_Number='';
        $employees = $this->Employee_model->get_all();


        $bydaterange = $this->input->post('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
         $Calling_Number = $this->input->post('Calling_Number', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }
         if ($Calling_Number != '') {
            $Calling_Number = $this->input->post('Calling_Number', TRUE);
        }

        $designation = $this->session->userdata('designation');
        $audit = $this->Audit_model->get_All_audit_by_supervises($date_fromf, $date_tof, $audit_by,$Calling_Number);
        //echo $this->db->last_query();


        $data = array(
            'audit_data' => $audit,
            'bydaterange' => $bydaterange,
            'employees' => $employees,
            'audit_by' => $audit_by,
           'Calling_Number' =>$Calling_Number
        );
        $data['date_fromf'] = $date_fromf;
        $data['date_tof'] = $date_tof;
        $data['content'] = 'audit/allAudits_supervises';
        $this->load->view('common/master', $data);
    }
    
    
     public function excelExportAllSupervises() {

        $this->load->library('excel');
        // create file name
        $fileName = 'AUDIT_data_Supervises-' . date('d-m-Y') . time() . '.xlsx';

        // load excel library
        $this->load->library('excel');
        // $audit_by = $this->session->userdata('reg_id');
        $audit_by = '';
        $audit_by = '';
        $date_fromf = '';
        $date_tof = '';
        if ($this->uri->segment(3)) {
            $date_fromf = $this->uri->segment(3);
            $date_tof = $this->uri->segment(4);
        }
        $bydaterange = $this->input->get('bydaterange', TRUE);
        $audit_by = $this->input->post('audit_by', TRUE);
        if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
        }
        if ($audit_by != '') {
            $audit_by = $this->input->post('audit_by', TRUE);
        }

        $communications = $this->Communications_model->get_all();



        $rows = $this->Audit_model->get_audit_byidAll_Supervises($audit_by, $date_fromf, $date_tof, $audit_by);
//       echo $this->db->last_query();
//       die();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BO1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:BO1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF6347');


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "Eval_MSD");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "Evaluator Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "Login ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMP-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "CRM-ID");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "Adviser Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "TL -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "AM -Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "DOP");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "AON");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "Call Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "Time Of Call");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "Week");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "Calling Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "CLI Number");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "Call Duration (Min)");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "Call Duration (Sec)");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "Call duration Bucket");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "Evaluation Date");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "Call Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "Call Sub Category (As per process)");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "Sub-Category As per CCR");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "Consumer`s Concern");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "Resolution Given By Adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "Communication Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "QME Remarks");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "Process Suggestion If Any");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "Fatal Reason");
        $objPHPExcel->getActiveSheet()->SetCellValue('AF1', "Opening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AG1', "Active Listening");
        $objPHPExcel->getActiveSheet()->SetCellValue('AH1', "Probing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AI1', "Customer Engagement/ Rapport Building");
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', "Empathy where required");
        $objPHPExcel->getActiveSheet()->SetCellValue('AK1', "Understanding (Both CSR & Customer perspective)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AL1', "Professionalism");
        $objPHPExcel->getActiveSheet()->SetCellValue('AM1', "Politeness & Courtesy");
        $objPHPExcel->getActiveSheet()->SetCellValue('AN1', "Hold Procedure");
        $objPHPExcel->getActiveSheet()->SetCellValue('AO1', "Closing");
        $objPHPExcel->getActiveSheet()->SetCellValue('AP1', "Opening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', "Active Listening(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AR1', "Probing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AS1', "Customer Engagement/ Rapport Building(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AT1', "Empathy where required(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AU1', "Understanding (Both CSR & Customer perspective)(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AV1', "Professionalism(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AW1', "Politeness & Courtesy(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AX1', "Hold Procedure(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AY1', "Closing(Scored)");
        $objPHPExcel->getActiveSheet()->SetCellValue('AZ1', "Correct/Complete Tagging");
        $objPHPExcel->getActiveSheet()->SetCellValue('BA1', "Tagging Done by the adviser");
        $objPHPExcel->getActiveSheet()->SetCellValue('BB1', "correct Tagging as per process");
        $objPHPExcel->getActiveSheet()->SetCellValue('BC1', "Accurate / Complete & Correct Resolution");
        $objPHPExcel->getActiveSheet()->SetCellValue('BD1', "Critical Areas treated as Fatal");
        $objPHPExcel->getActiveSheet()->SetCellValue('BE1', "Audit Count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BF1', "Fatal count");
        $objPHPExcel->getActiveSheet()->SetCellValue('BG1', "Quality scorable");
        $objPHPExcel->getActiveSheet()->SetCellValue('BH1', "Quality Scored");
        $objPHPExcel->getActiveSheet()->SetCellValue('BI1', "Shift");
        $objPHPExcel->getActiveSheet()->SetCellValue('BJ1', "Tenure");
        $objPHPExcel->getActiveSheet()->SetCellValue('BK1', "Call type");
        $objPHPExcel->getActiveSheet()->SetCellValue('BL1', "Audit_Time");
        $objPHPExcel->getActiveSheet()->SetCellValue('BM1', "FeedBack Status");
        $objPHPExcel->getActiveSheet()->SetCellValue('BN1', "Auditor");
        $objPHPExcel->getActiveSheet()->SetCellValue('BO1', "Accepted");
        // set Row
        $rowCount = 2;
        $sno = 1;
        $comm_rem = '';
        foreach ($rows as $data) {
            $comm_rem = ($data->Opening == 'GREEN') ? '' : $this->getopenstring($communications[0]->options, $data->Opening . ',');
            $comm_rem .= ($data->ActiveListening == 'GREEN') ? '' : $this->getopenstring($communications[1]->options, $data->ActiveListening . ',');
            $comm_rem .= ($data->Probing == 'GREEN') ? '' : $this->getopenstring($communications[2]->options, $data->Probing . ',');
            $comm_rem .= ($data->Customer_Engagement == 'GREEN') ? '' : $this->getopenstring($communications[3]->options, $data->Customer_Engagement . ',');
            $comm_rem .= ($data->Empathy_where_required == 'GREEN') ? '' : $this->getopenstring($communications[4]->options, $data->Empathy_where_required . ',');
            $comm_rem .= ($data->Understanding == 'GREEN') ? '' : $this->getopenstring($communications[5]->options, $data->Understanding . ',');
            $comm_rem .= ($data->Professionalism == 'GREEN') ? '' : $this->getopenstring($communications[6]->options, $data->Professionalism . ',');
            $comm_rem .= ($data->Politeness == 'GREEN') ? '' : $this->getopenstring($communications[7]->options, $data->Politeness . ',');
            $comm_rem .= ($data->Hold_Procedure == 'GREEN') ? '' : $this->getopenstring($communications[8]->options, $data->Hold_Procedure . ',');
            $comm_rem .= ($data->Closing == 'GREEN') ? '' : $this->getopenstring($communications[9]->options, $data->Closing . ',');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data->id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data->empMSDID);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data->audit_by);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data->emp_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data->employee_id_TL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data->employee_id_AM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-Y', strtotime($data->DOJ)));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data->AON);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data->process . '_' . $data->batch_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($data->Call_Date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data->Time_Of_Call);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, 'week-' . $this->week_between_two_dates(date('m/d/Y', strtotime($data->date)), date('m/d/Y')));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data->Calling_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data->CLI_Number);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data->Call_Dur_Min);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data->Call_Dur_Sec);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $this->calldurationBucket((($data->Call_Dur_Min * 60) + $data->Call_Dur_Sec)));
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, date('d-M-Y', strtotime($data->date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data->category_main_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data->category_sub_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data->category_main_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data->category_sub_id_crr);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data->Consumers_Concern);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data->r_g_y_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $comm_rem);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data->QME_Remarks);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data->p_s_if_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, ($data->Fatal_Reason == '0') ? 'No-Fatal' : $this->Fatal_reason_model->getFatalReason($data->Fatal_Reason));
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, ($data->Opening == 'GREEN') ? $data->Opening : $this->getopenstring($communications[0]->options, $data->Opening));
            if ($data->Opening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AF' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, ($data->ActiveListening == 'GREEN') ? $data->ActiveListening : $this->getopenstring($communications[1]->options, $data->ActiveListening));
            if ($data->ActiveListening == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AG' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, ($data->Probing == 'GREEN') ? $data->Probing : $this->getopenstring($communications[2]->options, $data->Probing));
            if ($data->Probing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AH' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, ($data->Customer_Engagement == 'GREEN') ? $data->Customer_Engagement : $this->getopenstring($communications[3]->options, $data->Customer_Engagement));
            if ($data->Customer_Engagement == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AI' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, ($data->Empathy_where_required == 'GREEN') ? $data->Empathy_where_required : $this->getopenstring($communications[4]->options, $data->Empathy_where_required));
            if ($data->Empathy_where_required == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AJ' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, ($data->Understanding == 'GREEN') ? $data->Understanding : $this->getopenstring($communications[5]->options, $data->Understanding));
            if ($data->Understanding == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AK' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, ($data->Professionalism == 'GREEN') ? $data->Professionalism : $this->getopenstring($communications[6]->options, $data->Professionalism));
            if ($data->Professionalism == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AL' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, ($data->Politeness == 'GREEN') ? $data->Politeness : $this->getopenstring($communications[7]->options, $data->Politeness));
            if ($data->Politeness == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AM' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, ($data->Hold_Procedure == 'GREEN') ? $data->Hold_Procedure : $this->getopenstring($communications[8]->options, $data->Hold_Procedure));
            if ($data->Hold_Procedure == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AN' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, ($data->Closing == 'GREEN') ? $data->Closing : $this->getopenstring($communications[9]->options, $data->Closing));
            if ($data->Closing == 'GREEN') {
                $objPHPExcel->getActiveSheet()->getStyle('AO' . $rowCount)->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('228B22');
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $data->OpeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $data->ActiveListeningScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $data->ProbingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $rowCount, $data->Customer_EngagementScored);





            $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $rowCount, $data->Empathy_where_requiredScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $rowCount, $data->UnderstandingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $rowCount, $data->ProfessionalismScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $rowCount, $data->PolitenessScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $rowCount, $data->Hold_ProcedureScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AY' . $rowCount, $data->ClosingScored);
            $objPHPExcel->getActiveSheet()->SetCellValue('AZ' . $rowCount, ($data->Correct_smaller == '3' || $data->Correct_smaller == '6') ? '10' : '0');
            $objPHPExcel->getActiveSheet()->SetCellValue('BA' . $rowCount, $data->t_d_b_a);
            $objPHPExcel->getActiveSheet()->SetCellValue('BB' . $rowCount, $data->c_t_a_p_p);
            $objPHPExcel->getActiveSheet()->SetCellValue('BC' . $rowCount, ($data->Accurate_Complete == 2) ? '0' : '15');
            $objPHPExcel->getActiveSheet()->SetCellValue('BD' . $rowCount, $this->CriticalAreastreatedFatal($data->Politeness));
            $objPHPExcel->getActiveSheet()->SetCellValue('BE' . $rowCount, '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BF' . $rowCount, ($data->Accurate_Complete == 1) ? '0' : '1');
            $objPHPExcel->getActiveSheet()->SetCellValue('BG' . $rowCount, '100');
            $objPHPExcel->getActiveSheet()->SetCellValue('BH' . $rowCount, $data->score);
            $objPHPExcel->getActiveSheet()->SetCellValue('BI' . $rowCount, $data->shift_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('BJ' . $rowCount, $this->Tenure($data->AON));
            $objPHPExcel->getActiveSheet()->SetCellValue('BK' . $rowCount, $data->call_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('BL' . $rowCount, $data->audit_time);
            $objPHPExcel->getActiveSheet()->SetCellValue('BM' . $rowCount, $data->feedback_status_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('BN' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Auditor : '');
            $objPHPExcel->getActiveSheet()->SetCellValue('BO' . $rowCount, ($agentfeedback = $this->Agentfeedback_model->get_by_audit_id($data->id)) ? $agentfeedback->Accepted : '');
            $rowCount++;
            $sno++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $nCols = 66; //set the number of columns

        foreach (range(0, $nCols) as $col) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        }




        $objWriter->save("././uploads/" . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "uploads/" . $fileName);
    }
    
    
     public function edit_records_supervises() {
        $id = $this->input->post('id', TRUE);
        $data = array(
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            'score' => $this->input->post('score', TRUE),
                //'date' => date('Y-m-d'),
                // 'audit_by'=>$this->session->userdata('reg_id'),
                //'status' => '1',
        );
        $this->Audit_model->update_supervises($id, $data);
        $score = $this->input->post('score', TRUE);
        $data_rem = array(
            //'audit_id' => $insert_id,
            //'agents_id' => $this->input->post('agents_id',TRUE),
            //'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
            //'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
            'Call_Date' => date('Y-m-d', strtotime($this->input->post('Call_Date', TRUE))),
            'Time_Of_Call' => $this->input->post('Time_Of_Call', TRUE),
            'Calling_Number' => $this->clean($this->input->post('Calling_Number', TRUE)),
            'CLI_Number' => $this->input->post('CLI_Number', TRUE),
            'Call_Dur_Min' => $this->input->post('Call_Dur_Min', TRUE),
            'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec', TRUE),
            'Call_Type' => $this->input->post('Call_Type', TRUE),
            'Todays_Audit_Count' => '0',
            'category_main_id' => $this->input->post('category_main_id', TRUE),
            'category_sub_id' => $this->input->post('category_sub_id', TRUE),
            'category_main_id_crr' => $this->input->post('category_main_id_crr', TRUE),
            'category_sub_id_crr' => $this->input->post('category_sub_id_crr', TRUE),
            'Consumers_Concern' => $this->input->post('Consumers_Concern', TRUE),
            'r_g_y_a' => $this->input->post('r_g_y_a', TRUE),
            'QME_Remarks' => $this->input->post('QME_Remarks', TRUE),
            'p_s_if_a' => $this->input->post('p_s_if_a', TRUE),
            't_d_b_a' => $this->input->post('t_d_b_a', TRUE),
            'c_t_a_p_p' => $this->input->post('c_t_a_p_p', TRUE),
            'Opening' => (!empty($_POST['Opening'])) ? implode(",", $this->input->post('Opening', TRUE)) : '0',
            'ActiveListening' => (!empty($_POST['ActiveListening'])) ? implode(",", $this->input->post('ActiveListening', TRUE)) : '0',
            'Probing' => (!empty($_POST['Probing'])) ? implode(",", $this->input->post('Probing', TRUE)) : '0',
            'Customer_Engagement' => (!empty($_POST['Customer_Engagement'])) ? implode(",", $this->input->post('Customer_Engagement', TRUE)) : '0',
            'Empathy_where_required' => (!empty($_POST['Empathy_where_required'])) ? implode(",", $this->input->post('Empathy_where_required', TRUE)) : '0',
            'Understanding' => (!empty($_POST['Understanding'])) ? implode(",", $this->input->post('Understanding', TRUE)) : '0',
            'Professionalism' => (!empty($_POST['Professionalism'])) ? implode(",", $this->input->post('Professionalism', TRUE)) : '0',
            'Politeness' => (!empty($_POST['Politeness'])) ? implode(",", $this->input->post('Politeness', TRUE)) : '0',
            'Hold_Procedure' => (!empty($_POST['Hold_Procedure'])) ? implode(",", $this->input->post('Hold_Procedure', TRUE)) : '0',
            'Closing' => (!empty($_POST['Closing'])) ? implode(",", $this->input->post('Closing', TRUE)) : '0',
            'Correct_smaller' => (!empty($_POST['Correct_smaller'])) ? $this->input->post('Correct_smaller', TRUE) : '0',
            'Accurate_Complete' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Accurate_Complete', TRUE) : '0',
            'Fatal_Reason' => (!empty($_POST['Accurate_Complete'])) ? $this->input->post('Fatal_Reason', TRUE) : '0',
            //'date' => date('Y-m-d'),
            //'status' =>'1',
            'feedback_status_id' => ($score < 90) ? $this->input->post('feedback_status_id', TRUE) : 'NA',
        );

        $this->Audit_remarks_model->update_by_audit_id_supervises($id, $data_rem);

        if ($id) {
            echo'1';
        } else {
            echo'0';
        }
    }
    
      public function dashboard_supervisers() {
        $Y = date('Y');
        $m = date('m');
        $date_fromf = $Y . '-' . $m . '-' . '01';
        $date_tof = date('Y-m-d');
        $msd_ID= $this->session->userdata('msd_ID');
        
          $supervises_id = $this->Agents_model->get_ID_supervises($msd_ID);
          $id=$supervises_id->id;
        $data['row'] = $this->Agents_model->get_all_by_joins_byID_supervises_dash($id);
         $data['monthlycount'] = $this->Agents_model->countMonthly_supervisess($id);
       //  print_r($data['monthlycount']);
        //  echo $this->db->last_query();
        $data['agentsDeatils'] = $this->Agents_model->get_audit_byidAll_supervisess($id, $date_fromf, $date_tof);
         
        if ($this->uri->segment(1) == 'DashboardAgent') {
            $data['admission'] = "active";
        }
        $data['id'] =$id;
        $data['addmodel'] = 'audit/add_weapon_model_supervisers';
        $data['content'] = 'audit/homeAgent_supervisers';
        $this->load->view('common/master', $data);
    }
    
     public function agentPerformancebyTL1()
    {
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $bydaterange = $this->input->post('bydaterange', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        //get all agents of TL
          $id = $this->session->userdata('reg_id');
          $date_of='';
           $agentsids = $this->Agents_model->get_all_agents_by_tl($id);
           $tags = implode(',',$agentsids);
          $date_of = $this->Agent_performance_model->get_max_date();
           // echo $this->db->last_query();
       //  die();
         $agent_performance = $this->Agent_performance_model->agentPerformancebyQA_AVG_agentswise($date_fromf = '',$date_tof = '',$date_of='',$employee_id_TL='' );
    //   echo $this->db->last_query();
    //   die();
        $data = array(
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list';
        $this->load->view('common/master', $data);    
            
    }
    
     public function agentPerformancebyAM()
    {
         $agent_performance='';
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $employee_id_TL='';
           $bydaterange = $this->input->post('bydaterange', TRUE);
           $employee_id_TL = $this->input->post('employee_id_TL', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        //get all agents of TL
          $id = $this->session->userdata('reg_id');
          $tl_lists=$this->Agents_model->get_all_tl_of_AM($id);//get all TL of AM
          $date_of='';
          if($employee_id_TL){
           $agentsids = $this->Agents_model->get_all_agents_by_tl($employee_id_TL);
           $tags = implode(',',$agentsids);
          $date_of = $this->Agent_performance_model->get_max_date();
           // echo $this->db->last_query();
       //  die();
         $agent_performance = $this->Agent_performance_model->get_all_bydateagent_TL($date_fromf,$date_tof,$tags,$date_of->date_of);
//        echo $this->db->last_query();
//         die();
          }
        $data = array(
            'tl_lists'=>$tl_lists,
            'employee_id_TL'=>$employee_id_TL,
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list_am';
        $this->load->view('common/master', $data);    
            
    }
      public function agentPerformancebyQA()
    {
          $am_lists='';
         $agent_performance='';
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $employee_id_TL='';
           $employee_id_AM='';
           $bydaterange = $this->input->post('bydaterange', TRUE);
           $employee_id_TL = $this->input->post('employee_id_TL', TRUE);
            $employee_id_AM = $this->input->post('employee_id_AM', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        //get all agents of TL
          $id = $employee_id_AM;
           $am_lists=$this->Agents_model->get_all_am_of_AM();//get all TL of AM
           // echo $this->db->last_query();
         //die();
          $tl_lists=$this->Agents_model->get_all_tl_of_AM($id);//get all TL of AM
          $date_of='';
          if($employee_id_TL){
           $agentsids = $this->Agents_model->get_all_agents_by_tl($employee_id_TL);
           $tags = implode(',',$agentsids);
          $date_of = $this->Agent_performance_model->get_max_date();
           // echo $this->db->last_query();
       //  die();
         $agent_performance = $this->Agent_performance_model->get_all_bydateagent_TL($date_fromf,$date_tof,$tags,$date_of->date_of);
//        echo $this->db->last_query();
//         die();
          }
        $data = array(
            'am_lists'=>$am_lists,
            'tl_lists'=>$tl_lists,
            'employee_id_TL'=>$employee_id_TL,
            'employee_id_AM'=>$employee_id_AM,
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list_qa';
        $this->load->view('common/master', $data);    
            
    }
    
    
    function get_tl_listby_amid(){
 
        $id=$this->input->post('employee_id_AM', TRUE);
         $tl_lists=$this->Agents_model->get_all_tl_of_AM($id);//get all TL of AM
         
           $strig = '<option value="">--Select--</option>';
        if ($tl_lists) {

            foreach ($tl_lists as $row) {
                $strig = $strig . '<option value="' . $row->emp_id . '">' . $row->emp_name . '</option>';
            }
        } else {
            
        }
        echo $strig;
    }
    
    
     public function agentPerformancebyQA_AVG_AM()
    {
          $am_lists='';
         $agent_performance='';
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $employee_id_TL='';
           $employee_id_AM='';
           $bydaterange = $this->input->post('bydaterange', TRUE);
           $employee_id_TL = $this->input->post('employee_id_TL', TRUE);
            $employee_id_AM = $this->input->post('employee_id_AM', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        //get all agents of TL
          $id = $employee_id_AM;
           $am_lists=$this->Agents_model->get_all_am_of_AM();//get all TL of AM
           // echo $this->db->last_query();
         //die();
          $tl_lists=$this->Agents_model->get_all_tl_of_AM($id);//get all TL of AM
          $date_of='';
       
            $date_of = $this->Agent_performance_model->get_max_date();
            $agent_performance = $this->Agent_performance_model->agentPerformancebyQA_AVG_AM($date_fromf,$date_tof,$date_of->date_of);
     // echo $this->db->last_query();
       //  die();
    //  print_r($agent_performance);
        $data = array(
            'am_lists'=>$am_lists,
            'tl_lists'=>$tl_lists,
            'employee_id_TL'=>$employee_id_TL,
            'employee_id_AM'=>$employee_id_AM,
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list_qa_avg_am';
        $this->load->view('common/master', $data);    
            
    }
    
     public function agentPerformancebyQA_AVG_TL()
    {
          $am_lists='';
         $agent_performance='';
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $employee_id_TL='';
           $employee_id_AM='';
           $bydaterange = $this->input->post('bydaterange', TRUE);
           $employee_id_TL = $this->input->post('employee_id_TL', TRUE);
            $employee_id_AM = $this->input->post('employee_id_AM', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if($this->session->userdata('type')== 4){
            $employee_id_AM=$this->session->userdata('reg_id');
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
        //get all agents of TL
          $id = $employee_id_AM;
           $am_lists=$this->Agents_model->get_all_am_of_AM();//get all TL of AM
           // echo $this->db->last_query();
         //die();
          $tl_lists=$this->Agents_model->get_all_tl_of_AM($id);//get all TL of AM
          $date_of='';
       
            $date_of = $this->Agent_performance_model->get_max_date();
            $agent_performance = $this->Agent_performance_model->agentPerformancebyQA_AVG_TL($date_fromf,$date_tof,$date_of->date_of,$employee_id_AM);
     // echo $this->db->last_query();
       //  die();
    //  print_r($agent_performance);
        $data = array(
            'am_lists'=>$am_lists,
            'tl_lists'=>$tl_lists,
            'employee_id_TL'=>$employee_id_TL,
            'employee_id_AM'=>$employee_id_AM,
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list_qa_avg_tl';
        $this->load->view('common/master', $data);    
            
    }
    
      public function agentPerformancebyTL()
    {
          $am_lists='';
         $agent_performance='';
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $employee_id_TL='';
           $employee_id_AM='';
           $bydaterange = $this->input->post('bydaterange', TRUE);
           $employee_id_TL = $this->input->post('employee_id_TL', TRUE);
            $employee_id_AM = $this->input->post('employee_id_AM', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
        if($this->session->userdata('type')== 1){
            $employee_id_TL=$this->session->userdata('reg_id');
        }
     
        
         
       
          $date_of='';
       
            $date_of = $this->Agent_performance_model->get_max_date();
            $agent_performance = $this->Agent_performance_model->agentPerformancebyQA_AVG_agentswise($date_fromf,$date_tof,$date_of->date_of,$employee_id_TL);
     // echo $this->db->last_query();
       //  die();
    //  print_r($agent_performance);
        $data = array(
          
           
            'employee_id_TL'=>$employee_id_TL,
            'employee_id_AM'=>$employee_id_AM,
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'audit/agent_performance_list_qa_avg_agentwise';
        $this->load->view('common/master', $data);    
            
    }
    
    

}

/* End of file Audit.php */
/* Location: ./application/controllers/Audit.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-06 08:47:06 */
