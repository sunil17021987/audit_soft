<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Outbound_callings extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
         $this->check_auth();
        $this->load->model('Outbound_callings_model');
        $this->load->model('Call_types_model');
        $this->load->model('Category_main_model');
        $this->load->library('form_validation');
    }
  public function check_auth() {

        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login'));
    }
    public function index()
    {
       // $outbound_callings = $this->Outbound_callings_model->get_all();
        $date_fromf = '';
        $date_tof = '';
        $audit_by = $login_type = $this->session->userdata('id');
        $outbound_callings =$this->Outbound_callings_model->getRecordsByaAgent($date_fromf = '', $date_tof = '', $audit_by = '');

        $data = array(
            'outbound_callings_data' => $outbound_callings
        );

          $data['content'] = 'outbound_callings/outbound_callings_list';
            $this->load->view('common/masterAgent', $data);
       
            
    }

    public function read($id) 
    {
        $row = $this->Outbound_callings_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'agents_id' => $row->agents_id,
		'patient_full_name' => $row->patient_full_name,
		'patient_mobile_number' => $row->patient_mobile_number,
		'address' => $row->address,
		'district' => $row->district,
		'block' => $row->block,
		'call_status' => $row->call_status,
		'q1' => $row->q1,
		'q2' => $row->q2,
		'q3' => $row->q3,
		'q4' => $row->q4,
		'q5' => $row->q5,
		'q6' => $row->q6,
		'q7' => $row->q7,
		'q8' => $row->q8,
		'q9' => $row->q9,
		'created_date' => $row->created_date,
		'updated_date' => $row->updated_date,
	    );
             $data['content'] = 'outbound_callings/outbound_callings_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('outbound_callings'));
        }
    }

    public function create() 
    {
        $call_types = $this->Call_types_model->get_all();
        $category_main = $this->Category_main_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('outbound_callings/create_action'),
	    'id' => set_value('id'),
	    'agents_id' => set_value('agents_id'),
	    'patient_full_name' => set_value('patient_full_name'),
	    'patient_mobile_number' => set_value('patient_mobile_number'),
	    'address' => set_value('address'),
	    'district' => set_value('district'),
	    'block' => set_value('block'),
	    'call_status' => set_value('call_status'),
	    'q1' => set_value('q1'),
	    'q2' => set_value('q2'),
	    'q3' => set_value('q3'),
	    'q4' => set_value('q4'),
	    'q5' => set_value('q5'),
	    'q6' => set_value('q6'),
	    'q7' => set_value('q7'),
	    'q8' => set_value('q8'),
	    'q9' => set_value('q9'),
	    'created_date' => set_value('created_date'),
	    'updated_date' => set_value('updated_date'),
            'call_types_data' => $call_types,
             'category_main' => $category_main
	);
        $data['content'] = 'outbound_callings/outbound_callings_form';
         $this->load->view('common/masterAgent', $data);      
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'agents_id' =>$this->session->userdata('id'),
		'patient_full_name' => $this->input->post('patient_full_name',TRUE),
		'patient_mobile_number' => $this->input->post('patient_mobile_number',TRUE),
		'address' => $this->input->post('address',TRUE),
		'district' => $this->input->post('district',TRUE),
		'block' => $this->input->post('block',TRUE),
		'call_status' => $this->input->post('call_status',TRUE),
		'q1' => $this->input->post('q1',TRUE),
		'q2' => $this->input->post('q2',TRUE),
		'q3' => $this->input->post('q3',TRUE),
		'q4' => $this->input->post('q4',TRUE),
		'q5' => $this->input->post('q5',TRUE),
		'q6' => $this->input->post('q6',TRUE),
		'q7' => $this->input->post('q7',TRUE),
		'q8' => $this->input->post('q8',TRUE),
		'q9' => $this->input->post('q9',TRUE),
		'created_date' => $this->input->post('created_date',TRUE),
		'updated_date' => $this->input->post('updated_date',TRUE),
	    );

            $this->Outbound_callings_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('outbound_callings'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Outbound_callings_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('outbound_callings/update_action'),
		'id' => set_value('id', $row->id),
		'agents_id' => set_value('agents_id', $row->agents_id),
		'patient_full_name' => set_value('patient_full_name', $row->patient_full_name),
		'patient_mobile_number' => set_value('patient_mobile_number', $row->patient_mobile_number),
		'address' => set_value('address', $row->address),
		'district' => set_value('district', $row->district),
		'block' => set_value('block', $row->block),
		'call_status' => set_value('call_status', $row->call_status),
		'q1' => set_value('q1', $row->q1),
		'q2' => set_value('q2', $row->q2),
		'q3' => set_value('q3', $row->q3),
		'q4' => set_value('q4', $row->q4),
		'q5' => set_value('q5', $row->q5),
		'q6' => set_value('q6', $row->q6),
		'q7' => set_value('q7', $row->q7),
		'q8' => set_value('q8', $row->q8),
		'q9' => set_value('q9', $row->q9),
		'created_date' => set_value('created_date', $row->created_date),
		'updated_date' => set_value('updated_date', $row->updated_date),
	    );
            $data['content'] = 'outbound_callings/outbound_callings_form';
             $this->load->view('common/masterAgent', $data);    
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('outbound_callings'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'agents_id' => $this->input->post('agents_id',TRUE),
		'patient_full_name' => $this->input->post('patient_full_name',TRUE),
		'patient_mobile_number' => $this->input->post('patient_mobile_number',TRUE),
		'address' => $this->input->post('address',TRUE),
		'district' => $this->input->post('district',TRUE),
		'block' => $this->input->post('block',TRUE),
		'call_status' => $this->input->post('call_status',TRUE),
		'q1' => $this->input->post('q1',TRUE),
		'q2' => $this->input->post('q2',TRUE),
		'q3' => $this->input->post('q3',TRUE),
		'q4' => $this->input->post('q4',TRUE),
		'q5' => $this->input->post('q5',TRUE),
		'q6' => $this->input->post('q6',TRUE),
		'q7' => $this->input->post('q7',TRUE),
		'q8' => $this->input->post('q8',TRUE),
		'q9' => $this->input->post('q9',TRUE),
		'created_date' => $this->input->post('created_date',TRUE),
		'updated_date' => $this->input->post('updated_date',TRUE),
	    );

            $this->Outbound_callings_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('outbound_callings'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Outbound_callings_model->get_by_id($id);

        if ($row) {
            $this->Outbound_callings_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('outbound_callings'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('outbound_callings'));
        }
    }

    public function _rules() 
    {
//	$this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
	$this->form_validation->set_rules('patient_full_name', 'patient full name', 'trim|required');
	$this->form_validation->set_rules('patient_mobile_number', 'patient mobile number', 'trim|required');
	$this->form_validation->set_rules('address', 'address', 'trim|required');
	$this->form_validation->set_rules('district', 'district', 'trim|required');
	$this->form_validation->set_rules('block', 'block', 'trim|required');
	$this->form_validation->set_rules('call_status', 'call status', 'trim|required');
	$this->form_validation->set_rules('q1', 'q1', 'trim|required');
	$this->form_validation->set_rules('q2', 'q2', 'trim|required');
	$this->form_validation->set_rules('q3', 'q3', 'trim|required');
	$this->form_validation->set_rules('q4', 'q4', 'trim|required');
	$this->form_validation->set_rules('q5', 'q5', 'trim|required');
	$this->form_validation->set_rules('q6', 'q6', 'trim|required');
	$this->form_validation->set_rules('q7', 'q7', 'trim|required');
	$this->form_validation->set_rules('q8', 'q8', 'trim|required');
	$this->form_validation->set_rules('q9', 'q9', 'trim|required');
//	$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
//	$this->form_validation->set_rules('updated_date', 'updated date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "outbound_callings.xls";
        $judul = "outbound_callings";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Agents Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Patient Full Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Patient Mobile Number");
	xlsWriteLabel($tablehead, $kolomhead++, "Address");
	xlsWriteLabel($tablehead, $kolomhead++, "District");
	xlsWriteLabel($tablehead, $kolomhead++, "Block");
	xlsWriteLabel($tablehead, $kolomhead++, "Call Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Q1");
	xlsWriteLabel($tablehead, $kolomhead++, "Q2");
	xlsWriteLabel($tablehead, $kolomhead++, "Q3");
	xlsWriteLabel($tablehead, $kolomhead++, "Q4");
	xlsWriteLabel($tablehead, $kolomhead++, "Q5");
	xlsWriteLabel($tablehead, $kolomhead++, "Q6");
	xlsWriteLabel($tablehead, $kolomhead++, "Q7");
	xlsWriteLabel($tablehead, $kolomhead++, "Q8");
	xlsWriteLabel($tablehead, $kolomhead++, "Q9");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Date");

	foreach ($this->Outbound_callings_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->agents_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->patient_full_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->patient_mobile_number);
	    xlsWriteLabel($tablebody, $kolombody++, $data->address);
	    xlsWriteNumber($tablebody, $kolombody++, $data->district);
	    xlsWriteNumber($tablebody, $kolombody++, $data->block);
	    xlsWriteNumber($tablebody, $kolombody++, $data->call_status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q1);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q2);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q3);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q4);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q5);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q6);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q7);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q8);
	    xlsWriteLabel($tablebody, $kolombody++, $data->q9);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Outbound_callings.php */
/* Location: ./application/controllers/Outbound_callings.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2022-01-22 09:33:52 */
