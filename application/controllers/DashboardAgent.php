<?php
if (!defined('BASEPATH'))
    exit('No Direct Script Access Allowed');

class DashboardAgent extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Ftp_model');
        $this->load->model('Ftp_agents_model');
        $this->load->model('Anticipatory_model');
        $this->load->model('Agents_model');
        $this->load->model('Anticipatory_status_model');
        $this->load->model('Attendance_format_model');
        
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        
        $this->load->library('xmlrpc');
        $this->load->library('xmlrpcs');
        
        $this->load->helper("sumoftimes_helper");
         $this->load->helper("avgtime_helper");
        
        $this->load->library('upload');
        $this->load->model('Agents_model');
        $this->load->model('Agent_performance_model');
        $this->check_auth();
    }

    public function check_auth() {

        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login/agents'));
    }

    public function index() {
        $Y = date('Y');
        $m = date('m');
        $date_fromf = $Y . '-' . $m . '-' . '01';
        $date_tof = date('Y-m-d');
        $id = $this->session->userdata('id');
        $data['row'] = $this->Agents_model->get_all_by_joins_byID($id);
         $data['monthlycount'] = $this->Agents_model->countMonthly($id);
       //  print_r($data['monthlycount']);
        //  echo $this->db->last_query();
        $data['agentsDeatils'] = $this->Agents_model->get_audit_byidAll($id, $date_fromf, $date_tof);
         
        if ($this->uri->segment(1) == 'DashboardAgent') {
            $data['admission'] = "active";
        }
        $data['addmodel'] = 'dash/add_weapon_model';
        $data['content'] = 'dash/homeAgent';
        $this->load->view('common/masterAgent', $data);
    }
    
      public function tl_audits() {
        $Y = date('Y');
        $m = date('m');
        $date_fromf = $Y . '-' . $m . '-' . '01';
        $date_tof = date('Y-m-d');
        $id = $this->session->userdata('id');
        $data['row'] = $this->Agents_model->get_all_by_joins_byID($id);
         $data['monthlycount'] = $this->Agents_model->countMonthly_tl($id);
       //  print_r($data['monthlycount']);
        //  echo $this->db->last_query();
        $data['agentsDeatils'] = $this->Agents_model->get_audit_byidAll_tl($id, $date_fromf, $date_tof);
         
        if ($this->uri->segment(1) == 'tl_audits') {
            $data['admission'] = "active";
        }
        $data['addmodel'] = 'dash/add_weapon_model';
        $data['content'] = 'dash/homeAgent_tl';
        $this->load->view('common/masterAgent', $data);
    }

    public function ftpDownload() {
        $ftp = $this->Ftp_model->get_all_by_join();

        $data = array(
            'ftp_data' => $ftp
        );

        $data['content'] = 'dash/ftp_list';
        $this->load->view('common/masterAgent', $data);
    }

    public function ftpUpload() {
        $id = $this->session->userdata('id');
        $ftp_agents = $this->Ftp_agents_model->get_all_by_join_id($id);

        $data = array(
            'button' => 'Create',
            'action' => site_url('DashboardAgent/create_action'),
            'id' => set_value('id'),
            'upload_by' => set_value('upload_by'),
            'file_name' => set_value('file_name'),
            'filename' => set_value('filename'),
            'fileurl' => set_value('fileurl'),
            'dateof' => set_value('dateof'),
            'status' => set_value('status'),
        );
        $data['ftp_agents_data'] = $ftp_agents;
        $data['content'] = 'dash/ftp_agents_form';
        $this->load->view('common/masterAgent', $data);
    }

    public function create_action() {
        $this->_rules_new();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path'] = '././uploads/ftp_agents/';
            $config['allowed_types'] = 'xls|xlsx|csv|pdf|txt|ods|doc|docx';
            // $config['detect_mime']          = false;

            $this->upload->initialize($config);
            // $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $udata = array('upload_data' => $this->upload->data());
                $reg_id = $this->session->userdata('id');
                $data = array(
                    'upload_by' => $reg_id,
                    'file_name' => 'uploads/ftp_agents/' . $udata['upload_data']['file_name'],
                    'filename' => $udata['upload_data']['file_name'],
                    'fileurl' => 'uploads/ftp_agents/' . $udata['upload_data']['file_name'],
                );

                $this->Ftp_agents_model->insert($data);
            } else {
                $error = array('error' => $this->upload->display_errors());


                $this->session->set_flashdata('message', 'File Type Not Allowed please Try Again...');
                redirect(site_url('ftp'));
            }
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('DashboardAgent/ftpUpload'));
        }
    }

    public function _rules_new() {
        // $this->form_validation->set_rules('upload_by', 'upload by', 'trim|required');
        //  $this->form_validation->set_rules('file', 'file name', 'trim|required');
        // $this->form_validation->set_rules('dateof', 'dateof', 'trim|required');
        // $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    
     public function agentPerformance()
    {
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $bydaterange = $this->input->post('bydaterange', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
         $agent_performance = $this->Agent_performance_model->get_all_bydateagent($date_fromf,$date_tof);

        $data = array(
            'agent_performance_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'dash/agent_performance_list';
        $this->load->view('common/masterAgent', $data);    
            
    }
    
    
   
   
   public function agentPerformanceByCRM()
    {
        $msdid = $this->session->userdata('emp_id');
        /* API URL */
        $url = 'http://172.16.10.39/mppkvvcl/index.php/Json/getAgentPerformanceBYMSD';
             
        /* Init cURL resource */
        $ch = curl_init($url);
            
        /* Array Parameter Data */
        $data = ['id'=>$msdid];
            
        /* pass encoded JSON string to the POST fields */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
           
        /* set the content type json */
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json'
                  
                ));
            
        /* set return type json */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
        /* execute request */
        $result = curl_exec($ch);
             $transaction = json_decode($result, TRUE);
        /* close cURL resource */
        curl_close($ch);
          var_dump($transaction['record']);
          
          $tr='';
          if($transaction['status']){
          foreach ($transaction['record'] as $value) {
            
              $tr .="<tr>
                   <td><span class='badge bg-green-active'>".$value['attenddate']."</span></td>
		    <td>".$value['totalaction']."</td>
		    <td>".$value['closed']."</td>
                   <td>".$value['forceclose']."</td>
                        <td>".$value['remonder']."</td>
                             <td>".$value['opned']."</td>
                                  <td>".($value['noconnected']+$value['noconnected1'])."</td>
                                       <td>".($value['notpickedbyofficer']+$value['notpickedbyofficer1'])."</td>
                                            <td>".$value['reopen']."</td>
		  </tr>";
              
          }
          
         
          }else{
               $tr .="<tr>
                   <td><span class='badge bg-green-active'>0</span></td>
		    <td>0</td>
		    <td>0</td>
                   <td>0</td>
                        <td>0</td>
                             <td>0</td>
                                  <td>0</td>
                                       <td>0</td>
                                            <td>0</td>
		  </tr>";
               
          }
           echo $tr;
          
    }
    
    
     public function anticipatory()
    {
        
          $id = $this->session->userdata('id');
         $anticipatory_data=array();
          $anticipatory_status = $this->Anticipatory_status_model->get_all();
         $anticipatory_data = $this->Anticipatory_model->get_all_by_agent_id($id);
        $data = array(
            'id'=>$id ,
            'anticipatory_data'=>$anticipatory_data,
            'button' => 'Create',
            'action' => site_url('DashboardAgent/anticipatory_create_action'),
	    'id' => set_value('id'),
	    'agents_id' => set_value('agents_id'),
	    'employee_id_AM' => set_value('employee_id_AM'),
	    'employee_id_TL' => set_value('employee_id_TL'),
	    'mobile_no' => set_value('mobile_no'),
	    'issue_start' => set_value('issue_start'),
	    'issue_end' => set_value('issue_end'),
	    'issue_type' => set_value('issue_type'),
            'extensionNo' => set_value('extensionNo'),
             'issue_remarks' => set_value('issue_remarks'),
	    'approved_by' => set_value('approved_by'),
	    'is_approved' => set_value('is_approved'),
	    'approved_remarks' => set_value('approved_remarks'),
	    'approved_date_time' => set_value('approved_date_time'),
	    'issue_date_time' => set_value('issue_date_time'),
            'anticipatory_status'=>$anticipatory_status
        );

        $data['content'] = 'dash/anticipatory';
        $this->load->view('common/masterAgent', $data);    
    }
    
     public function anticipatory_create_action() 
    {
       $id = $this->session->userdata('id');
       $rows = $this->Agents_model->get_am_tl_by_agent_id($id);
       $issue_start=date('Y-m-d', strtotime($this->input->post('issue_start',TRUE)));
       $start_Time_Of_Call=$this->input->post('start_Time_Of_Call',TRUE);
       $issue_end= date('Y-m-d', strtotime($this->input->post('issue_end',TRUE)));
       $end_Time_Of_Call=$this->input->post('end_Time_Of_Call',TRUE);
       $response=array();
           $data = array(
		'agents_id' =>$rows->id,
		'employee_id_AM' => $rows->employee_id_AM,
		'employee_id_TL' => $rows->employee_id_TL,
		'mobile_no' => $this->input->post('mobile_no',TRUE),
		'issue_start' => $issue_start.' '.date("H:i", strtotime($start_Time_Of_Call)),
		'issue_end' => $issue_end.' '.date("H:i", strtotime($end_Time_Of_Call)),
		'issue_type' => $this->input->post('issue_type',TRUE),
                'extensionNo' => $this->input->post('extensionNo',TRUE),
		'issue_remarks' => $this->input->post('issue_remarks',TRUE),
		'approved_by' =>0,
		'is_approved' =>'0',
		'approved_remarks' =>'',
		'approved_date_time' =>'0000-00-00 00-00-00',
		'issue_date_time' => date('Y-m-d H:i'),
	    );

            $lastId = $this->Anticipatory_model->insert($data);
           if($lastId > 0)
          {
                 $anticipatory_data = $this->Anticipatory_model->get_all_by_agent_id_inserted_id($lastId,$id);
                 $response['anticipatory_data']=$anticipatory_data;
                  $response['status']='1';
           }else{
                $response['anticipatory_data']='';
                $response['status']='0';
          }
          echo json_encode($response);
          
    }
    function get_pendings_count(){
         $rows = $this->Anticipatory_model->get_pendings_count();
       
         $response['anticipatory_data']=$rows;
           $response['status']='1';
          echo json_encode($response);
    }
    
    
    
     public function attendance()
    {
          $date_fromf = '';
          $date_tof = '';
           $bydaterange = '';
           $bydaterange = $this->input->post('bydaterange', TRUE);
                if ($bydaterange != '') {
            $datearr = explode('-', $bydaterange);
            $date_from = str_replace("/", "-", $datearr[0]);
            $datef = explode('-', $date_from);
            $datef1 = $datef[0] . '-' . $datef[1] . '-' . $datef[2];
            $date_fromf = date('Y-m-d', strtotime($datef1));

            $date_to = str_replace("/", "-", $datearr[1]);
            $date_to = str_replace(" ", "", $date_to);
            $datet = explode('-', $date_to);
            $datet1 = $datet[0] . '-' . $datet[1] . '-' . $datet[2];
            $date_tof = date('Y-m-d', strtotime($datet1));
            $bydaterange = $bydaterange;
        }
       // $agent_performance = $this->Agent_performance_model->get_all();
         $agent_performance = $this->Attendance_format_model->get_all_bydateagent($date_fromf,$date_tof);

        $data = array(
            'attendance_format_data' => $agent_performance,
            'bydaterange' => $bydaterange,
        );

          $data['content'] = 'dash/attendance';
        $this->load->view('common/masterAgent', $data);    
            
    }
    
    
    
     

}

?>
