<?php

class Imageupload extends CI_Controller {

   /**
    * Manage __construct
    *
    * @return Response
   */
   public function __construct() { 
      parent::__construct(); 
      $this->load->helper(array('form', 'url')); 
      $this->load->model('Registration_model');
      $this->load->model('Employee_model');

   }

   /**
    * Manage index
    *
    * @return Response
   */
   public function index($id) { 
         $data['id']=$id;
         $this->load->view('imageUploadForm',$data); 
   } 
   public function upload($id) { 
         $data['id']=$id;
         $this->load->view('imageUploadForm_1',$data); 
   } 

   /**
    * Manage uploadImage
    *
    * @return Response
   */
   public function uploadImage() {

        $config['upload_path'] = '././uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('imageUploadForm', $error);
        } else {
            $id = $this->input->post('id', TRUE);
            $finfo = $this->upload->data();
            $baseurl = base_url() . 'uploads/' . $finfo['file_name'];
            $data['image'] = $baseurl;
            $this->Registration_model->update($id, $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('registration'));

            print_r('Image Uploaded Successfully.');
            exit;
        }
    }

     public function uploadImage_emp() {

        $config['upload_path'] = '././uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('imageUploadForm', $error);
        } else {
            $id = $this->input->post('emp_id', TRUE);
            $finfo = $this->upload->data();
            $baseurl = base_url() . 'uploads/' . $finfo['file_name'];
            $data['image'] = $baseurl;
            $this->Employee_model->update($id, $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('employee'));
            print_r('Image Uploaded Successfully.');
            exit;
        }
    }

} 

?>