<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>First Name</b></td><td><?php echo $first_name; ?></td></tr>
	    <tr><td><b>Last Name</b></td><td><?php echo $last_name; ?></td></tr>
	    <tr><td><b>Email</b></td><td><?php echo $email; ?></td></tr>
	    <tr><td><b>Password</b></td><td><?php echo $password; ?></td></tr>
	    <tr><td><b>Phone</b></td><td><?php echo $phone; ?></td></tr>
	    <tr><td><b>Created</b></td><td><?php echo $created; ?></td></tr>
	    <tr><td><b>Modified</b></td><td><?php echo $modified; ?></td></tr>
	    <tr><td><b>Status</b></td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>