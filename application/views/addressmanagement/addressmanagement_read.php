<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Addressmanagement Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Country</b></td><td><?php echo $country; ?></td></tr>
	    <tr><td><b>State</b></td><td><?php echo $state; ?></td></tr>
	    <tr><td><b>City</b></td><td><?php echo $city; ?></td></tr>
	    <tr><td><b>Zipcode</b></td><td><?php echo $zipcode; ?></td></tr>
	    <tr><td><b>Street</b></td><td><?php echo $street; ?></td></tr>
	    <tr><td><b>FullAddress</b></td><td><?php echo $fullAddress; ?></td></tr>
	    <tr><td><b>Status</b></td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('addressmanagement') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>