<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Addressmanagement <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Country <?php echo form_error('country') ?></label>
            <input type="text" class="form-control" name="country" id="country" placeholder="Country" value="<?php echo $country; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">State <?php echo form_error('state') ?></label>
            <input type="text" class="form-control" name="state" id="state" placeholder="State" value="<?php echo $state; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">City <?php echo form_error('city') ?></label>
            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $city; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Zipcode <?php echo form_error('zipcode') ?></label>
            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zipcode" value="<?php echo $zipcode; ?>" />
        </div>
	    <div class="form-group">
            <label for="street">Street <?php echo form_error('street') ?></label>
            <textarea class="form-control" rows="3" name="street" id="street" placeholder="Street"><?php echo $street; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="fullAddress">FullAddress <?php echo form_error('fullAddress') ?></label>
            <textarea class="form-control" rows="3" name="fullAddress" id="fullAddress" placeholder="FullAddress"><?php echo $fullAddress; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('addressmanagement') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>