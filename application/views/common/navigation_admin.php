<?php
$permissionArray = array();
$permissionss = $this->Rbac_model->get_by_employee_id($this->session->userdata('reg_id'));
if(!empty($permissionss)){
    $permissionArray = explode(",", $permissionss->permission);
}else{
    $permissionss ='';
}

?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"> <img src="<?php echo base_url().($this->session->userdata('image')?$this->session->userdata('image'):'assets/admin/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image"> </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('emp_name'); ?> </p>
                <a href="#"><i class="fa fa-circle text-success"></i>Online</a> </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li class="treeview <?php
            if (isset($admission)) {
                echo$admission;
            }
            ?>">
                <a href="<?php echo base_url('dashboard'); ?>"> <i class="glyphicon glyphicon-home"></i> <span>Home</span> <span class="pull-right-container"> <span class="label label-primary pull-right"></span> </span> </a> </li>
            <li>
                <a href="<?php echo base_url('dashboard/dashboard_outbound'); ?>"> <i class="glyphicon glyphicon-home"></i> <span>Home</span> <span class="pull-right-container"> <span class="label label-primary pull-right"></span> </span> </a> </li>

            <?php if (in_array('1', $permissionArray)) { ?>
                <li class="treeview">

                    <a href="#">
                        <i class="glyphicon glyphicon-cog"></i> <span>Settings</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        
                         <li><a href="<?php echo base_url('Category_main'); ?>"><i class="fa fa-circle-o"></i> Category_main </a></li>
                           <li><a href="<?php echo base_url('Category_sub'); ?>"><i class="fa fa-circle-o"></i>Sub Category</a></li>
<!--                        <li><a href="<?php echo base_url('Call_types'); ?>"><i class="fa fa-circle-o"></i> Call Types </a></li>
                        <li><a href="<?php echo base_url('Category'); ?>"><i class="fa fa-circle-o"></i> Category </a></li>
                        <li><a href="<?php echo base_url('Department'); ?>"><i class="fa fa-circle-o"></i> Department </a></li>
                        <li><a href="<?php echo base_url('Designation'); ?>"><i class="fa fa-circle-o"></i> Designation </a></li>
                        <li><a href="<?php echo base_url('Shifts'); ?>"><i class="fa fa-circle-o"></i> Shift </a></li>
                        <li><a href="<?php echo base_url('Dispositions'); ?>"><i class="fa fa-circle-o"></i>Dispositions</a></li>
                        <li><a href="<?php echo base_url('Communications'); ?>"><i class="fa fa-circle-o"></i>Communications</a></li>
                        <li><a href="<?php echo base_url('Fatal_reason'); ?>"><i class="fa fa-circle-o"></i>Fatal Reason</a></li>
                        <li><a href="<?php echo base_url('Calldurationpattern'); ?>"><i class="fa fa-circle-o"></i>Call duration pattern</a></li>
                        <li><a href="<?php echo base_url('Feedback_status'); ?>"><i class="fa fa-circle-o"></i>Feedback Status</a></li>-->
                        <li><a href="<?php echo base_url('Menubar'); ?>"><i class="fa fa-circle-o"></i>Menubar Names</a></li>
                        <li><a href="<?php echo base_url('Rbac'); ?>"><i class="fa fa-circle-o"></i>Set Permissions</a></li>
                        <li><a href="<?php echo base_url('agents/alignment'); ?>"><i class="fa fa-circle-o"></i>Update Alignment</a></li>
                        <li><a href="<?php echo base_url('agents/notLoginToday'); ?>"><i class="fa fa-circle-o"></i>Not Login Today</a></li>
<!--                         <li><a href="<?php echo base_url('agents/agentPerformance'); ?>"><i class="fa fa-circle-o"></i>Upload Agent Performance</a></li>-->
<!--                          <li><a href="<?php echo base_url('Anticipatory_status'); ?>"><i class="fa fa-circle-o"></i>Anticipatory status</a></li>-->
<!--                           <li><a href="<?php echo base_url('agents/attendance_format'); ?>"><i class="fa fa-circle-o"></i>Upload Agent Attendance</a></li>-->


                    </ul>
                </li>
            <?php }
            ?>

            <?php if (in_array('2', $permissionArray)) { ?>

                <li class="<?php
                if (isset($employee)) {
                    echo$employee;
                }
                ?>">
                    <a href="<?php echo base_url('employee'); ?>">
                        <i class="fa fa-th"></i> <span>Staff</span>

                    </a>
                </li> 

            <?php } ?>

            <?php if (in_array('3', $permissionArray)) { ?>

                <li class="<?php
                
                ?>">
                    <a href="<?php echo base_url('Agents'); ?>">
                        <i class="fa fa-th"></i> <span>Agents</span>

                    </a>
                </li>

         <?php } ?>

            <?php if (in_array('5', $permissionArray)) { ?>

                <li>
                    <a href="<?php echo base_url('Gateway/audits_reports'); ?>">
                        <i class="fa fa-th"></i> <span>Reports</span>

                    </a>
                </li>

            <?php } ?>


            <?php if (in_array('6', $permissionArray)) { ?>

                <li>
                    <a href="<?php echo base_url('Audit/audits'); ?>">
                        <i class="fa fa-th"></i> <span>Audit's</span>

                    </a>
                </li>

            <?php } ?>
                
                  <?php if (in_array('7', $permissionArray)) { ?>

                <li>
                    <a href="<?php echo base_url('Audit/allAudits'); ?>">
                        <i class="fa fa-th"></i> <span>All Audit's</span>

                    </a>
                </li>
           

            <?php } ?>
          
  
 
               
                
           






        </ul>
    </section>

</aside>