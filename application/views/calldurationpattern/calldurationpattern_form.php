<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Calldurationpattern <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Start Time(hh:ii:ss) <?php echo form_error('start_time') ?></label>
            <input type="text" class="form-control" name="start_time" id="start_time" placeholder="hh:ii:ss" value="<?php echo $start_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">End Time(hh:ii:ss) <?php echo form_error('end_time') ?></label>
            <input type="text" class="form-control" name="end_time" id="end_time" placeholder="hh:ii:ss" value="<?php echo $end_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Target <?php echo form_error('target') ?></label>
            <input type="number" class="form-control" name="target" id="target" placeholder="Target" value="<?php echo $target; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('calldurationpattern') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>