<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Department
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <!--<h2 style="margin-top:0px">Department <?php echo $button ?></h2>-->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Created At <?php echo form_error('created_at') ?></label>
            <input type="text"  readonly class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo date('d/m/Y'); ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('department') ?>" class="btn btn-default">Cancel</a>
	</form>
    <!-- ******************/master footer ****************** -->
                    </div>
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>