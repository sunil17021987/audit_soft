<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agents Performance Upload CSV File        
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <?php
if(!empty($this->session->flashdata('item'))) {
  $message = $this->session->flashdata('item');
  ?>
                        <div lass="<?php echo $message['class']?>"> <?php echo $message['message']; ?></div>
<?php 
}
?>
                        
                        <!-- ******************/master header end ****************** -->
                         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                     <table>
                       <tr>
                            <td>
                                 <input type="file" name="file">
                            </td>
                             <td>
                               
                              <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                             </td>
                             <td>Export Format</td>
                             <td> 
                              <?php echo anchor(site_url('agents/ExcelAgentPerformanceFormat'), 'Export Format', 'class="btn btn-primary"'); ?>
                             </td>

                       </tr>
                     </table>
        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>