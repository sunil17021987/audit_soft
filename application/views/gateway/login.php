<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-md-8"> <div>  GateWay Login: </div> </div>
            <div class="col-md-4">
                <?php
                if($this->session->flashdata('message')){ 
                    echo $this->session->flashdata('message');
                }
               ?> 
            </div>

        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <form role="form" id="idForm" action="<?php echo site_url('Gateway/create') ?>" method="post" autocomplete="off">
            <div class="row">
                <div class="col-md-12">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="form-group">
                                <label>Agent MSD ID</label>
                                <input type="text" name="emp_id" class="form-control input-sm" placeholder="MSD ID" id="Login_ID">
                                <input type="hidden" name="id" class="form-control input-sm" placeholder="ID" id="agents_id">
                            </div>
                         
                            <div class="box-footer" id="hideshow">
                               
                                <button type="submit" class="btn btn-info ">Start Audit</button>
                            </div>






                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->

                    <!-- /.box -->
                </div>
                <!-- /.col -->

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </form>
    </section>
    <!-- /.content -->
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#hideshow').hide();
        $('#quality_scored').val(100);
        $('#Login_ID').on('keyup', function () {
            var Login_ID = this.value;

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Gateway/get_records') ?>",
                //dataType : "JSON",
                data: {Login_ID: Login_ID},
                success: function (data) {

                    var json = JSON.parse(data);
                    if (json.rows !== '') {
                        //alert(json.rows.employee_id_TL);
                       $('#hideshow').show();
                        $('span#TL_Name').html('').append(json.rows.employee_id_TL);
                        $('span#AM_Name').html('').append(json.rows.employee_id_AM);
                        $('span#CCR_Name').html('').append(json.rows.name);
                        $('#agents_id').val(json.rows.id);
                        $('#TL_id').val(json.rows.TL_id);
                        $('#AM_id').val(json.rows.AM_id);
                    } else {
                         $('#hideshow').hide();
                        $('#EMP_ID').val();
                        $('#CRM_ID').val();
                        $('span#TL_Name').html('').append();
                        $('span#AM_Name').html('').append();
                        $('span#CCR_Name').html('').append();
                        $('#agents_id').val();
                        $('#TL_id').val();
                        $('#AM_id').val();
                    }
                }
            });
            return false;
        });





    });



</script>
