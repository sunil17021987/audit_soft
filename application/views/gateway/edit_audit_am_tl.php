<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Audits TL / AM         
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
         
                 
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>

                                    <select id="employee_id_TL" class="form-control" name="employee_id_TL">
                                        <option value="">---  Select TL ---</option>
                                        <?php
                                        if (!empty($employee_data_tl)) {
                                            foreach ($employee_data_tl as $rl) {
                                                ?>
                                                <option  <?php if ($employee_id_TL == $rl->emp_id) echo 'selected'; ?> value="<?php echo $rl->emp_id ?>"><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>

                                    <select id="employee_id_AM" class="form-control" name="employee_id_AM" id="employee_id_AM">
                                        <option value="">---  Select AM ---</option>
                                        <?php
                                        if (!empty($employee_data_am)) {
                                            foreach ($employee_data_am as $rl) {
                                                ?>
                                                <option <?php if ($employee_id_AM == $rl->emp_id) echo 'selected'; ?> value="<?php echo $rl->emp_id ?>"><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 
                       
                            <div class="col-xs-3">
                                <br>
                                <input type="hidden" id="auditid" name="id" value="<?php echo $id; ?>" /> 
                                <button type="button" id="Updatetlam" class="btn btn-primary">Update</button> 

                            </div>
                        </div>
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                                 
                            $('#Updatetlam').click(function (e) {
                                e.preventDefault();

//                                var ids = new Array();
//                                ;
//                                $('.casebox:checked').each(function () {
//                                    var values = $(this).val();
//
//                                    ids.push(values);
//                                });

                                var employee_id_TL = $("#employee_id_TL").val();
                                var employee_id_AM = $("#employee_id_AM").val();
                                var auditid = $("#auditid").val();


                                $.ajax({
                                    url: "<?php echo base_url('Gateway/update_action_ajax_am_tl')?>",
                                    type: 'POST',
                                    data: {employee_id_TL: employee_id_TL, employee_id_AM: employee_id_AM, auditid: auditid},
                                    error: function () {
                                        alert('Something is wrong');
                                    },
                                    success: function (data) {

                                        alert(data);
                                        window.location.href = "<?php echo base_url('Audit/allAudits')?>";

                                       // location.reload(true);
                                    }
                                });


                            });


                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>