<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>  Details <small>List</small> </h1>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <!-- /.box-header -->
      <div class="box-body">
        <!-- ******************/master header end ****************** -->
        <div class="row" style="margin-bottom: 10px">
          <div class="col-md-4">
            <!--<h2 style="margin-top:0px">Employee List</h2>-->
          </div>
          <div class="col-md-4 text-center">
            <div style="margin-top: 4px"  id="message"> <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> </div>
          </div>
          <div class="col-md-4 text-right"> <?php echo anchor(site_url('gateway/create'), 'Create', 'class="btn btn-primary"'); ?> <?php echo anchor(site_url('employee/excel'), 'Excel', 'class="btn btn-primary"'); ?> </div>
        </div>
        <div class="box-body table-responsive">
          <table class="table table-bordered table-striped" id="mytable">
            <thead>
              <tr>
                <th width="20px">No</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Dob</th>
                <th>Email</th>
                <th>Mobile</th>
                <!--		    <th>Category</th>
		    <th>Qualification</th>
		    <th>Join Date</th>-->
                <th>Department</th>
                <th>Designation</th>
                <!--		    <th>Matial Status</th>
		    <th>Nationality</th>
		    <th>Experience</th>
		    <th>Image</th>
		    <th>Created At</th>-->
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
            $start = 0;
            foreach ($employee_data as $employee)
            {
                ?>
              <tr>
                <td><?php echo ++$start ?></td>
                <td><?php echo $employee->emp_name ?></td>
                <td><?php echo $employee->gender ?></td>
                <td><?php echo $employee->dob ?></td>
                <td><?php echo $employee->email ?></td>
                <td><?php echo $employee->mobile ?></td>
                <!--		    <td><?php echo $employee->category ?></td>
		    <td><?php echo $employee->qualification ?></td>
		    <td><?php echo $employee->join_date ?></td>-->
                <td><?php echo $employee->department; //$row = $this->Department_model->get_by_id($employee->department); if($row){echo  $row->name; }else{} ?></td>
                <td>1</td>
                <!--		    <td><?php echo $employee->matial_status ?></td>
		    <td><?php echo $employee->nationality ?></td>
		    <td><?php echo $employee->experience ?></td>
		    <td><?php echo $employee->image ?></td>
		    <td><?php echo $employee->created_at ?></td>-->
                <td style="text-align:center" width="200px"><?php 
			echo anchor(site_url('employee/read_details/'.$employee->emp_id),'<i class="fa fa-eye" aria-hidden="true"></i>'); 
			echo ' | '; 
			echo anchor(site_url('employee/update/'.$employee->emp_id),'<i class="fa fa-pencil-square-o"></i>'); 
			echo ' | '; 
			echo anchor(site_url('employee/delete/'.$employee->emp_id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
                </td>
              </tr>
              <?php
            }
            ?>
            </tbody>
          </table>
        </div>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
        <!-- ******************/master footer ****************** -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
