    <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tbody><tr>
                                            <th>Sr.</th>
                                            <th>Audit Date</th>
                                            <th>Call Date</th>
                                            <th>Score</th>
                                            <th>Fatal</th>
                                            <th>Evaluator Name</th>
                                            <th>QME Remarks</th>
                                        </tr>

                                        <?php
                                        $count = 0;
                                        if (!empty($agentsDeatils)) {
                                            foreach ($agentsDeatils as $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo ++$count; ?></td>
                                                    <td><?php echo date('d-M-Y', strtotime($value->date)); ?></td>
                                                    <td><?php echo date('d-M-Y', strtotime($value->Call_Date)); ?></td>
                                                    <td><span class="label label-success"><?php echo $value->score; ?></span></td>
                                                    <td><?php echo ($value->Fatal_Reason == '0') ? '0' : '1'; ?></td>
                                                    <td><?php echo $value->audit_by ?></td>
                                                     <td><?php echo $value->QME_Remarks ?></td>
                                                </tr>
                                            <?php
                                            }
                                        }
                                        ?>




                                    </tbody></table>


                            </div>