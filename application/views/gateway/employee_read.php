<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Employee Read</h2>
        <table class="table">
	    <tr><td>Emp Name</td><td><?php echo $emp_name; ?></td></tr>
	    <tr><td>Gender</td><td><?php echo $gender; ?></td></tr>
	    <tr><td>Dob</td><td><?php echo $dob; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Mobile</td><td><?php echo $mobile; ?></td></tr>
	    <tr><td>Category</td><td><?php echo $category; ?></td></tr>
	    <tr><td>Qualification</td><td><?php echo $qualification; ?></td></tr>
	    <tr><td>Join Date</td><td><?php echo $join_date; ?></td></tr>
	    <tr><td>Department</td><td><?php echo $department; ?></td></tr>
	    <tr><td>Designation</td><td><?php echo $designation; ?></td></tr>
	    <tr><td>Matial Status</td><td><?php echo $matial_status; ?></td></tr>
	    <tr><td>Nationality</td><td><?php echo $nationality; ?></td></tr>
	    <tr><td>Experience</td><td><?php echo $experience; ?></td></tr>
	    <tr><td>Image</td><td><?php echo $image; ?></td></tr>
	    <tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('employee') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>