<style>
    table {
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid orange;
    }

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid orange;
    }
    .tabletd{
        background-color:#f39c12;
        color: #FFF;
        font-size: 16px;
        font-weight: 700;
    }
    #quality_scored{
        width: 25%;
    }
    .error{
        color: red;
    }
</style>


<div class="content-wrapper" style="min-height: 946px;" id="auditForm">
    <form role="form" id="idForm" action="<?php echo site_url('Audit/save_records') ?>" method="post" autocomplete="off">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="row">
                <div class="col-md-2"> GateWay:</div>
                <div class="col-md-2">
                    <small class="label  bg-blue">Agent : &nbsp;&nbsp;<?php echo $name; ?></small>

                </div>
                <div class="col-md-2">
                    <small class="label  bg-yellow">TL : &nbsp;&nbsp;<?php echo $employee_id_TL; ?></small>
                </div>
                <div class="col-md-3">
                    <small class="label  bg-yellow"> AM : &nbsp;&nbsp;<?php echo $employee_id_AM; ?></small>

                </div>
                <div class="col-md-3">
                    <input type="text" name="score" readonly class="form-control input-sm" placeholder="Enter ..." id="quality_scored">
                    <input type="hidden" name="audit_time" value="<?php echo time() ?>" id="audit_time">
                </div>
            </div>




        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">

                <!-- /.col -->
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Audit</a></li>
                            <li><a href="#timeline" data-toggle="tab">Call duration</a></li>
                            <li><a href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="active tab-pane" id="activity">
                                <!--***********************************************-->

                                <div class="row">

                                    <div class="box box-warning">

                                        <div class="box-body">

                                            <!-- text input -->

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Call Date</label>
                                                        <div class="input-group date">
                                                            <!--                                                            <div class="input-group-addon">
                                                                                                                            <i class="fa fa-calendar"></i>
                                                                                                                        </div>-->
                                                            <input type="text" readonly name="Call_Date" class="form-control pull-right" id="Call_Date">
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-md-2">

                                                    <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                            <label>Time Of Call:</label>

                                                            <div class="input-group">
                                                                <input type="text" name="Time_Of_Call" id="Time_Of_Call" class="form-control input-sm timepicker">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Calling Number</label>

                                                        <div class="input-group">

                                                            <input type="text" name="Calling_Number" id="Calling_Number" class="form-control input-sm">
                                                        </div>

                                                    </div>




                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>CLI Number</label>
                                                        <input type="text" name="CLI_Number" id="CLI_Number" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>Call(M)</label>
                                                        <input type="text" name="Call_Dur_Min" id="Call_Dur_Min" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>Sec.</label>
                                                        <input type="text" name="Call_Dur_Sec" id="Call_Dur_Sec" value="" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Call Type</label>

                                                        <select name="Call_Type" class=" form-control select1 input-sm" id="Call_Type">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($call_types_data)) {
                                                                foreach ($call_types_data as $call_types) {
                                                                    ?>
                                                                    <option <?php echo ($call_types->id == $Call_Type) ? 'selected' : ''; ?>  value="<?php echo $call_types->id ?>"><?php echo $call_types->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>




                                            </div>



                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Category As Per Process</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_main_id" class=" form-control select1 input-sm" id="category_main_id">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($category_main)) {
                                                                foreach ($category_main as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->category_main_id == $category_main_id) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->category_main_id ?>"><?php echo $downloadtype->category_main_name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_sub_id" class=" form-control select1 input-sm" id="category_sub_id">
                                                            <option value="">Select Sub Category</option>

                                                        </select>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Category As Per CCR</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_main_id_crr" class=" form-control select1 input-sm" id="category_main_id_crr">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($category_main)) {
                                                                foreach ($category_main as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->category_main_id == $category_main_id) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->category_main_id ?>"><?php echo $downloadtype->category_main_name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_sub_id_crr" class=" form-control select1 input-sm" id="category_sub_id_crr">
                                                            <option value="">Select Sub Category</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Consumer's Concern</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" name="Consumers_Concern" id="Consumers_Concern" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Resolution Given By Adviser</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="r_g_y_a" id="r_g_y_a" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>QME Remarks</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="QME_Remarks" id="QME_Remarks" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Process Suggestion If Any</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="p_s_if_a" id="p_s_if_a" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Disposition</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">


                                                        <select name="t_d_b_a" class=" form-control select1 input-sm" id="t_d_b_a">
                                                            <option value="">Select Dispositions</option>
                                                            <?php
                                                            if (!empty($dispositions_data)) {
                                                                foreach ($dispositions_data as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->id == $t_d_b_a) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->name ?>"><?php echo $downloadtype->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <label> Correct Disposition</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select name="c_t_a_p_p" class=" form-control select1 input-sm" id="c_t_a_p_p">
                                                            <option value="">Select Dispositions</option>
                                                            <?php
                                                            if (!empty($dispositions_data)) {
                                                                foreach ($dispositions_data as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->id == $c_t_a_p_p) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->name ?>"><?php echo $downloadtype->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </div>



                                            </div>


                                            <div class="row-border"></div>


                                        </div>
                                        <!-- /.box-body -->
                                    </div>

                                </div>



                                <!-- /.box-body -->






                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[0]->title ?></h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">
                                                    <?php
                                                    $Opening = explode(",", $communications_data[0]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Opening) ? 'checked' : ''; ?> name="Opening[]" class="Opening" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>
                                                </ul>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[1]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">

                                                    <?php
                                                    $Opening = explode(",", $communications_data[1]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $ActiveListening) ? 'checked' : ''; ?> name="ActiveListening[]" class="ActiveListening" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>

                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[2]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">
                                                    <?php
                                                    $Opening = explode(",", $communications_data[2]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Probing) ? 'checked' : ''; ?> name="Probing[]" class="Probing" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>

                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[3]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">

                                                    <?php
                                                    $Opening = explode(",", $communications_data[3]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Customer_Engagement) ? 'checked' : ''; ?> name="Customer_Engagement[]" class="Customer_Engagement" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>

                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[4]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">
                                                    <?php
                                                    $Opening = explode(",", $communications_data[4]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Empathy_where_required) ? 'checked' : ''; ?> name="Empathy_where_required[]" class="Empathy_where_required" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>
                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[5]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">

                                                    <?php
                                                    $Opening = explode(",", $communications_data[5]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Understanding) ? 'checked' : ''; ?> name="Understanding[]" class="Understanding" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>



                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[6]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">

                                                    <?php
                                                    $Opening = explode(",", $communications_data[6]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Professionalism) ? 'checked' : ''; ?> name="Professionalism[]" class="Professionalism" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>



                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[7]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">

                                                    <?php
                                                    $Opening = explode(",", $communications_data[7]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Politeness) ? 'checked' : ''; ?> name="Politeness[]" class="Politeness" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>



                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[8]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">
                                                    <?php
                                                    $Opening = explode(",", $communications_data[8]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Hold_Procedure) ? 'checked' : ''; ?> name="Hold_Procedure[]" class="Hold_Procedure" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>



                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="box box-warning box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo$communications_data[9]->title ?></h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body" style="">
                                                <ul class="nav nav-stacked">
                                                    <?php
                                                    $Opening = explode(",", $communications_data[9]->options);
                                                    $val = 0;
                                                    foreach ($Opening as $key => $value) {
                                                        $val = $key + 1;
                                                        ?>
                                                        <li>
                                                            <input type="checkbox" <?php echo ($val == $Closing) ? 'checked' : ''; ?> name="Closing[]" class="Closing" value="<?php echo$val; ?>"/>
                                                            <span> <?php echo $value; ?></span>
                                                        </li>
                                                    <?php }
                                                    ?>



                                                </ul>


                                            </div>
                                            <!-- /.box-body -->
                                        </div>

                                    </div>

                                </div>
                                <div class="row">

                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td style="width:203px" class="tabletd">Correct/Complete Tagging</td>
                                                <td>
                                                <td class="tabletd" style="background-color:#7E5109"> <label>For < 30 Days Adviser</label></td>
                                                </td>
                                                <td>
                                                    <div class="form-group">

                                                        <input type="hidden" name="check_true" value="T" id="check_true"/>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller1" value="1">
                                                                Wrong tagging done as per call scenario
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller2" value="2">
                                                                No Tagging.
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller3" value="3">
                                                                NA
                                                            </label>
                                                        </div>


                                                    </div>
                                                </td>


                                                <td class="tabletd" style="background-color:#7E5109"> <label>For > 30 Days Adviser</label></td>
                                                <td>  
                                                    <div class="form-group">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller4" value="4">
                                                                Wrong tagging done as per call scenario
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller5" value="5">
                                                                No Tagging.
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Correct_smaller" class="Correct_smaller" id="Correct_smaller6" value="6">
                                                                NA
                                                            </label>
                                                        </div>


                                                    </div>
                                                </td>
                                            </tr>


                                        </tbody></table>

                                    <div class="box-header">
                                        <h3 class="box-title">Accurate/Complete & Correct Resolution</h3>
                                    </div>
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td style="width:203px" class="tabletd">Fatal Status</td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="check_fatal" value="0" id="check_fatal"/>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Accurate_Complete" class="Accurate_Complete" id="Accurate_Complete" value="1" checked="">
                                                                Non - Fatal

                                                            </label>
                                                        </div>

                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="Accurate_Complete" class="Accurate_Complete" id="Accurate_Complete" value="2">
                                                                Fatal
                                                            </label>
                                                        </div>

                                                    </div>
                                                </td>

                                                <td class="tabletd"> <label>Fatal Reason</label></td>
                                                <td>  
                                                    <div class="form-group">
                                                        <select name="Fatal_Reason" class=" form-control select1 input-sm" id="Fatal_Reason">
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($fatal_reason_data)) {
                                                                foreach ($fatal_reason_data as $call_types) {
                                                                    ?>
                                                                    <option <?php echo ($call_types->id == $Fatal_Reason) ? 'selected' : ''; ?>  value="<?php echo $call_types->id ?>"><?php echo $call_types->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </td>
                                                
                                                 <td class="tabletd"> <label>Feedback Status</label></td>
                                                <td>  
                                                    <div class="form-group">
                                                        <select name="feedback_status_id" class=" form-control select1 input-sm" id="feedback_status_id" required>
                                                           
                                                            <?php
                                                            if (!empty($feedback_status_data)) {
                                                                foreach ($feedback_status_data as $feedbackstatus) {
                                                                    ?>
                                                                    <option <?php echo ($feedbackstatus->name == $feedback_status_id) ? 'selected' : ''; ?>  value="<?php echo $feedbackstatus->name ?>"><?php echo $feedbackstatus->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </td>
                                                
                                            </tr>


                                        </tbody></table>

                                </div>
                                <div class="row">
                                    <input type="hidden" name="agents_id" value="<?php echo$agents_id; ?>" id="agents_id">
                                    <input type="hidden" name="employee_id_TL" value="<?php echo$TL_id; ?>" id="TL_id"/>
                                    <input type="hidden" name="employee_id_AM" value="<?php echo$AM_id; ?>" id="AM_id"/>

                                    <button type="submit"  class="btn btn-primary">Save</button> 

                                </div>



                                <!--***********************************************-->
                            </div>

                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <!------>

                                <!------>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                cc
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </form>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/timepicker/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/timepicker/js/timepicki.js') ?>"></script>


<script type="text/javascript" src="<?php echo base_url('assets/dist/jquery.validate.js') ?>"></script>

<script type="text/javascript">

   



    $(document).ready(function () {

        $('#quality_scored').val(100);
        $('#Login_ID').on('keyup', function () {
            var Login_ID = this.value;

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Gateway/get_records') ?>",
                //dataType : "JSON",
                data: {Login_ID: Login_ID},
                success: function (data) {

                    var json = JSON.parse(data);
                    if (json.rows != '') {
                        // alert(json.rows.Login_ID);
                        $('#EMP_ID').val(json.rows.EMP_ID);
                        $('#CRM_ID').val(json.rows.CRM_ID);
                        $('span#TL_Name').html('').append(json.rows.employee_id_TL);
                        $('span#AM_Name').html('').append(json.rows.employee_id_AM);
                        $('span#CCR_Name').html('').append(json.rows.name);
                        $('#agents_id').val(json.rows.id);
                        $('#TL_id').val(json.rows.TL_id);
                        $('#AM_id').val(json.rows.AM_id);


                    } else {
                        $('#EMP_ID').val();
                        $('#CRM_ID').val();
                        $('span#TL_Name').html('').append();
                        $('span#AM_Name').html('').append();
                        $('span#CCR_Name').html('').append();
                        $('#agents_id').val();
                        $('#TL_id').val();
                        $('#AM_id').val();
                    }
                }
            });
            return false;
        });


        $(".Opening").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Opening[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".ActiveListening").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='ActiveListening[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });

        $(".Probing").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Probing[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".Customer_Engagement").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Customer_Engagement[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });

        $(".Empathy_where_required").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Empathy_where_required[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 5);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 5);
                    }
                }
            }
        });

        $(".Understanding").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Understanding[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });
        $(".Professionalism").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Professionalism[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".Politeness").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Politeness[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });


        $(".Hold_Procedure").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Hold_Procedure[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 5);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 5);
                    }
                }
            }
        });

        $(".Closing").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Closing[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 7);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 7);
                    }
                }
            }
        });

        $(".Correct_smaller").click(function () {

            var quality_scored = parseInt($('#quality_scored').val());

            if ($(this).val() === '3' || $(this).val() === '6') {
                var check_true = $('#check_true').val();
                if (check_true === 'F') {
                    $('#check_true').val('T');
                    $('#quality_scored').val(quality_scored + 10);
                }
            } else {
                var check_true = $('#check_true').val();
                if (check_true === 'T') {
                    $('#check_true').val('F');
                    $('#quality_scored').val(quality_scored - 10);
                }

            }
        });

        $(".Accurate_Complete").click(function () {

            var quality_scored = parseInt($('#quality_scored').val());

            if ($(this).val() === '2') {
                $('#check_fatal').val(quality_scored);
                $('#quality_scored').val(0);

            } else {
                var check_fatal = $('#check_fatal').val();
                quality_scored = check_fatal;
                $('#check_true').val('F');
                $('#quality_scored').val(quality_scored);


            }
        });


        $('#category_main_id').on('change', function () {
            var category_main_id = $(this).val();

            if (category_main_id) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                    data: {category_main_id: category_main_id},
                    success: function (html) {
                        $('#category_sub_id').html(html);
                    }
                });


            }

        });
        $('#category_main_id_crr').on('change', function () {
            var category_main_id_crr = $(this).val();

            if (category_main_id_crr) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                    data: {category_main_id: category_main_id_crr},
                    success: function (html) {
                        $('#category_sub_id_crr').html(html);
                    }
                });


            }

        });


        
        
$('#idForm').validate({

   rules: {
    Call_Date: "required",
    Time_Of_Call:"required",
    Calling_Number: {
      required: true,
      number: true,
      minlength: 10,
      maxlength: 12
     },
      CLI_Number: {
      required: true
     },
     Call_Dur_Min:{
      required: true,
      number: true,
      minlength: 1,
      maxlength: 2
     },
      Call_Dur_Sec:{
       required: true,
      number: true,
      minlength: 1,
      maxlength: 2
    },
    Call_Type:{
      required: true  
    },
    category_main_id:{
        required: true  
    },
     category_sub_id:{
        required: true  
    },
    category_main_id_crr:{
        required: true  
    },
     category_sub_id_crr:{
        required: true  
    },
    Consumers_Concern:{
        required: true   
    },
    r_g_y_a:{
        required: true   
    },
     QME_Remarks:{
        required: true   
    },
     p_s_if_a:{
        required: true   
    },
     t_d_b_a:{
        required: true   
    },
     c_t_a_p_p:{
        required: true   
    }
    
  },
  messages: {
    Call_Date: "Please Enter Date",
     Time_Of_Call: "Please Enter Time",
    Calling_Number: {
      required: "Mobile Number 10-12 Digits",
      number:"Mobile Number 10-12 Digits",
       minlength: "Mobile Number 10-12 Digits",
      maxlength: "Mobile Number 10-12 Digits"
     },
       CLI_Number: {
      required:"CLI Number"
     },
      Call_Dur_Min:{
      required:"Min",
      number:"Min"
    },
    Call_Dur_Sec:{
        required:"Min",
      number:"Min"
    },
     Call_Type:{
      required:"Call Type"  
    },
     category_main_id:{
        required:"category main"  
    },
     category_sub_id:{
       required:"category Sub"
    },
     category_main_id_crr:{
        required:"category Main"
    },
     category_sub_id_crr:{
        required:"category Sub"
    },
     Consumers_Concern:{
       required:"Consumer's Concern" 
    },
     r_g_y_a:{
       required:"Resolution Given By Adviser" 
    },
      QME_Remarks:{
       required:"QME Remarks"  
    },
     p_s_if_a:{
       required:"Process Suggestion If Any"   
    },
     t_d_b_a:{
        required:"Disposition"   
    },
     c_t_a_p_p:{
        required:"Correct Disposition" 
    }
  },

    submitHandler: function(form) {
  
    form.preventDefault(); // avoid to execute the actual submit of the form.
            $.ajax({
                url: form.action,
                type: form.method,
                data: form.serialize(), // serializes the form's elements.
                success: function (data)
                {
                    if (data === '1') {
                        alert('Audit Done....');
                        window.location.href = "<?php echo base_url('Gateway/login') ?>";
                    } else {
                        alert('Error...! ');
                    }
                }
            });
    }
});


     


    });
    $('#timepicker1').timepicki();
$('#form').submit(function(e){     

            e.preventDefault();
            var $form = $(this);

          // check if the input is valid
            if(! $form.valid()) return false;

           $.ajax({
                type: 'POST',
                url: 'add.php',
                data: $('#form').serialize(),
                success: function(response) {
                    $("#answers").html(response);
                }

            });
        });

</script>
<script src="<?php echo base_url('assets/timepicker/js/bootstrap.min.js') ?>"></script>
