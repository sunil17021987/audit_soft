<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Anticipatory <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post">
                            <!--	    <div class="form-group">
                                        <label for="int">Agents Id <?php echo form_error('agents_id') ?></label>
                                        <input type="text" class="form-control" name="agents_id" id="agents_id" placeholder="Agents Id" value="<?php echo $agents_id; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>
                                        <input type="text" class="form-control" name="employee_id_AM" id="employee_id_AM" placeholder="Employee Id AM" value="<?php echo $employee_id_AM; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>
                                        <input type="text" class="form-control" name="employee_id_TL" id="employee_id_TL" placeholder="Employee Id TL" value="<?php echo $employee_id_TL; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="varchar">Mobile No <?php echo form_error('mobile_no') ?></label>
                                        <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile No" value="<?php echo $mobile_no; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="datetime">Issue Start <?php echo form_error('issue_start') ?></label>
                                        <input type="text" class="form-control" name="issue_start" id="issue_start" placeholder="Issue Start" value="<?php echo $issue_start; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="datetime">Issue End <?php echo form_error('issue_end') ?></label>
                                        <input type="text" class="form-control" name="issue_end" id="issue_end" placeholder="Issue End" value="<?php echo $issue_end; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="int">Issue Type <?php echo form_error('issue_type') ?></label>
                                        <input type="text" class="form-control" name="issue_type" id="issue_type" placeholder="Issue Type" value="<?php echo $issue_type; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="varchar">Issue Remarks <?php echo form_error('issue_remarks') ?></label>
                                        <input type="text" class="form-control" name="issue_remarks" id="issue_remarks" placeholder="Issue Remarks" value="<?php echo $issue_remarks; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="int">Approved By <?php echo form_error('approved_by') ?></label>
                                        <input type="text" class="form-control" name="approved_by" id="approved_by" placeholder="Approved By" value="<?php echo $approved_by; ?>" />
                                    </div>-->
                            <div class="form-group">
                                <label for="enum">Is Approved <?php echo form_error('is_approved') ?></label>
                                <?php
                                 
                                $statusList = array(
                                    '1' => 'No',
                                    '2' => 'Yes'
                                );
                                ?>
                                <select name="is_approved" class=" form-control select1 input-sm" id="is_approved" required>

                                    <?php
                                    if (!empty($statusList)) {
                                        foreach ($statusList as $key=>$value) {
                                            ?>
                                            <option <?php echo ($key == $is_approved) ? 'selected' : ''; ?>  value="<?php echo $key ?>"><?php echo $value ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>


                             
                            </div>
                            <div class="form-group">
                                <label for="varchar">Approved Remarks <?php echo form_error('approved_remarks') ?></label>
                                <input type="text" class="form-control" name="approved_remarks" id="approved_remarks" placeholder="Approved Remarks" value="<?php echo $approved_remarks; ?>" required/>
                            </div>
                            <!--
                                        <div class="form-group">
                                        <label for="datetime">Approved Date Time <?php echo form_error('approved_date_time') ?></label>
                                        <input type="text" class="form-control" name="approved_date_time" id="approved_date_time" placeholder="Approved Date Time" value="<?php echo $approved_date_time; ?>" />
                                    </div>
                                        <div class="form-group">
                                        <label for="datetime">Issue Date Time <?php echo form_error('issue_date_time') ?></label>
                                        <input type="text" class="form-control" name="issue_date_time" id="issue_date_time" placeholder="Issue Date Time" value="<?php echo $issue_date_time; ?>" />
                                    </div>-->
                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('anticipatory') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>
        
       
        