<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
         <div class="row">
             <div class="col-xs-6">
            Anticipatory List          
           </div>
             
             <div class="col-xs-6">
                 <small class="label pull-right bg-yellow" id="result"></small>
                 
             </div>
         </div>
      
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                         <?php echo form_open(base_url() . 'Anticipatory/index'); ?>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-6">
                     
                <div class="form-group">
                  

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="bydaterange" class="form-control pull-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>

            
            </div>
            <div class="col-md-2 text-center">
                  <button type="submit" class="btn btn-info" style="width: 100%;">Search</button>
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
           <div class="col-md-4 text-right">
                <?php
                $string = '';
                if ($date_tof != '' && $date_fromf != '') {
                    $string = "/$date_fromf/$date_tof";
                }
                ?>
                <a class="btn btn-info" href="<?php echo base_url() . 'anticipatory/excel' . $string ?>">Excel</a>
               <?php //echo anchor(site_url('anticipatory/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php //echo anchor(site_url('anticipatory/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
             <?php echo form_close(); ?>
        </div>
                        
                        
                        <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
                     <th>Issue Date Time</th>
		    <th>Agents Id</th>
                   
		    <!--<th>Employee Id AM</th>-->
		    <!--<th>Employee Id TL</th>-->
		    <th>Mobile No</th>
		    <th>Issue Start</th>
		    <th>Issue End</th>
		    <th>Issue Type</th>
                    <th>ExtensionNo</th>
		    <th>Issue Remarks</th>
		    <th>Approved By</th>
		    <th>Is Approved</th>
		    <!--<th>Approved Remarks</th>-->
		    <!--<th>Approved Date Time</th>-->
		    
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($anticipatory_data as $anticipatory)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
                     <td><?php echo $anticipatory->issue_date_time ?></td>
		     <td><?php echo $anticipatory->name ?></td>
                    
		    <!--<td><?php echo $anticipatory->employee_id_AM ?></td>-->
		    <!--<td><?php echo $anticipatory->employee_id_TL ?></td>-->
		    <td><?php echo $anticipatory->mobile_no ?></td>
		    <td><?php echo $anticipatory->issue_start ?></td>
		    <td><?php echo $anticipatory->issue_end ?></td>
		    <td><?php echo $anticipatory->issue_type ?></td>
                     <td><?php echo $anticipatory->extensionNo ?></td>
		    <td><?php echo $anticipatory->issue_remarks ?></td>
		    <td><?php echo $anticipatory->approved_by ?></td>
		    <td><?php echo $anticipatory->is_approved ?></td>
		    <!--<td><?php echo $anticipatory->approved_remarks ?></td>-->
		    <!--<td><?php echo $anticipatory->approved_date_time ?></td>-->
		   
		    <td style="text-align:center" width="200px">
			<?php 
			//echo anchor(site_url('anticipatory/read/'.$anticipatory->id),'<i class="fa fa-eye"></i>'); 
			//echo ' | '; 
			echo anchor(site_url('anticipatory/update/'.$anticipatory->id),'<i class="fa fa-pencil-square-o"></i>'); 
			echo ' | '; 
			echo anchor(site_url('anticipatory/delete/'.$anticipatory->id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
                            </div>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
               
                 
                $("#mytable").dataTable();
                setInterval(function(){
                 get_pendings_count();
             }, 6000);
                
            });
              function get_pendings_count(){
                $.ajax({

            type: "GET",

            url: "<?php echo site_url('DashboardAgent/get_pendings_count'); ?>",
            //data: dataString,
            datatype: 'json',
            success: function (data) {
                var obj = jQuery.parseJSON(data);
              //  alert("Total Pendings complaints : "+obj.anticipatory_data);
                if (obj.status === '1') {
                   // $("tbody").append("<tr><td>" + obj.anticipatory_data.issue_date_time + "</td><td>" + obj.anticipatory_data.mobile_no + "</td><td>" + obj.anticipatory_data.issue_start + "</td><td>" + obj.anticipatory_data.issue_end + "</td><td>" + obj.anticipatory_data.issue_type + "</td><td>" + obj.anticipatory_data.issue_remarks + "</td><td>" + obj.anticipatory_data.approved_by + "</td><td>" + obj.anticipatory_data.is_approved + "</td><td>" + obj.anticipatory_data.approved_remarks + "</td><td>" + obj.anticipatory_data.approved_date_time + "</td><td>" + obj.anticipatory_data.issue_date_time + "</td></tr>");
    $("#result").html('');             
    $("#result_pendings").html('');    
    $("#result").html("Pendings: "+obj.anticipatory_data);
   $("#result_pendings").html(obj.anticipatory_data); 
    
                  //  $("#result").addClass("alert alert-success");
                  //  location.reload();
                } else {
                    $("#result").html('Error');
                    $("#result").addClass("alert alert-danger");
                  //  location.reload();
                }

                //$("#antiForm")[0].reset();
            }

        });
    }
        </script>
    <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>    
    </section>
    </div>
        
 