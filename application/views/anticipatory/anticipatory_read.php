<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Anticipatory Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Agents Id</b></td><td><?php echo $agents_id; ?></td></tr>
	    <tr><td><b>Employee Id AM</b></td><td><?php echo $employee_id_AM; ?></td></tr>
	    <tr><td><b>Employee Id TL</b></td><td><?php echo $employee_id_TL; ?></td></tr>
	    <tr><td><b>Mobile No</b></td><td><?php echo $mobile_no; ?></td></tr>
	    <tr><td><b>Issue Start</b></td><td><?php echo $issue_start; ?></td></tr>
	    <tr><td><b>Issue End</b></td><td><?php echo $issue_end; ?></td></tr>
	    <tr><td><b>Issue Type</b></td><td><?php echo $issue_type; ?></td></tr>
	    <tr><td><b>Issue Remarks</b></td><td><?php echo $issue_remarks; ?></td></tr>
	    <tr><td><b>Approved By</b></td><td><?php echo $approved_by; ?></td></tr>
	    <tr><td><b>Is Approved</b></td><td><?php echo $is_approved; ?></td></tr>
	    <tr><td><b>Approved Remarks</b></td><td><?php echo $approved_remarks; ?></td></tr>
	    <tr><td><b>Approved Date Time</b></td><td><?php echo $approved_date_time; ?></td></tr>
	    <tr><td><b>Issue Date Time</b></td><td><?php echo $issue_date_time; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('anticipatory') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>