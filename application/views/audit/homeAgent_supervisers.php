<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">


                        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/admin/dist/img/avatar.png" alt="User profile picture">



                        <p class="text-muted text-center"></p></br>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Employee ID</b> <a class="pull-right"><?php echo $row->emp_id; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Name</b> <a class="pull-right"><?php echo $row->name; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>DOP</b> <a class="pull-right"><?php echo $row->DOJ; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Batch No.</b> <a class="pull-right"><?php echo $row->batch_no; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Shift</b> <a class="pull-right"><?php echo $row->shift_id; ?></a>
                            </li>

                            <li class="list-group-item">
                                <b>AM</b> <a class="pull-right"><?php echo$row->employee_id_AM ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>TL</b> <a class="pull-right"><?php echo$row->employee_id_TL ?></a>
                            </li>
                        </ul>


                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            <!--/.col--> 
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">This Month</a></li>
                        <li><a href="#fees" data-toggle="tab">Monthly</a></li>
                        <li><a href="#settings" data-toggle="tab">Graph</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">


                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tbody><tr>
                                            <th>Sr.</th>
                                            <th>Audit Date</th>
                                            <th>Call Date</th>
                                            <th>Time Of Call</th> 
                                            <th>Calling Number</th>
                                            <th>CLI Number</th>
                                            <th>Score</th>
                                            <th>Fatal</th>
                                            <th>Evaluator Name</th>
                                            <th>QME Remarks</th>
                                            <th>Action</th>
                                        </tr>

                                        <?php
                                        $count = 0;
                                        if (!empty($agentsDeatils)) {
                                            foreach ($agentsDeatils as $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo ++$count; ?></td>
                                                    <td><?php echo date('d-M-Y', strtotime($value->date)); ?></td>
                                                    <td><?php echo date('d-M-Y', strtotime($value->Call_Date)); ?></td>
                                                    <td><?php echo $value->Time_Of_Call; ?></td>
                                                    <td><?php echo $value->Calling_Number; ?></td>
                                                    <td><?php echo $value->CLI_Number; ?></td>
                                                    <td>
                                                         <?php
                                                                                        $colour='';
                                                                                        if($value->score >= 90)
                                                                                        {
                                                                                        $colour='success';    
                                                                                        }else if($value->score > 1 && $value->score < 90 )
                                                                                        {
                                                                                            $colour='warning'; 
                                                                                        }
                                                                                        else if($value->score == 0)
                                                                                        {
                                                                                            $colour='danger'; 
                                                                                        }
                                                                                        ?>    
                                                                                            <span class="label label-<?=$colour?>"><?php echo $value->score; ?></span>
                                                        
                                                     </td>
                                                    <td><?php echo ($value->Fatal_Reason == '0') ? '0' : '1'; ?></td>
                                                    <td><?php echo $value->audit_by ?></td>
                                                    <td><?php echo $value->QME_Remarks ?></td>
                                                    <td>
                                                        <?php if (($value->score === 0) || ($value->score < 90)) { ?>
                                                            <!--<a href="JavaScript:Void(0);" id="<?php echo$value->id; ?>" onclick="callModel(this)" > <span title="Add Remarks" class="glyphicon glyphicon-tags"></span> </a>-->
                                                        <?php }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>




                                    </tbody></table>
                                <?php $this->load->view($addmodel); ?>

                            </div>


                        </div>

                        <div class="tab-pane" id="fees">

                            <div class="col-md-12">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Monthly Report</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-group" id="accordion">
                                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                            <?php
                                            $cointid = 0;
                                            $months=array();
                                            $scores=array();
                                            if (!empty($monthlycount)) {
                                                foreach ($monthlycount as $value) {
                                                  $months[]=   date('M-Y', strtotime($value->date));
                                                  $scores[]=   ceil($value->score);
                                                    ?>
                                                    <div class="panel box box-primary">
                                                        <div class="box-header with-border">
                                                            <h6 class="box-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo$cointid; ?>" class="collapsed" aria-expanded="false">
                                                                    <small class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo date('M-Y', strtotime($value->date)).'      AVG SCORE- '. ceil($value->score) ; ?></small>  
                                                                </a>
                                                            </h6>
                                                        </div>
                                                        <div id="collapse<?php echo$cointid; ?>" class="panel-collapse collapse" aria-expanded="false">
                                                            <div class="box-body">
                                                                <div class="box-body table-responsive no-padding">
                                                                    <table class="table table-hover">
                                                                        <tbody><tr>
                                                                                <th>Sr.</th>
                                                                                <th>Audit Date</th>
                                                                                <th>Call Date</th>
                                                                                <th>Time Of Call</th> 
                                                                                <th>Calling Number</th>
<!--                                                                                <th>CLI Number</th>-->
                                                                                <th>Score</th>
                                                                                <th>Fatal</th>
<!--                                                                                <th>Evaluator Name</th>
                                                                                <th>QME Remarks</th>-->
                                                                                <th>Action</th>
                                                                            </tr>

                                                                            <?php
                                                                            $count = 0;
                                                                            $agentsDeatils = $this->Agents_model->get_audit_byidAllBymonth_supervisess($value->date,$id);
                                                                          
                                                                            
                                                                            
                                                                             
                                                                             
                                                                             
                                                                            if (!empty($agentsDeatils)) {
                                                                                foreach ($agentsDeatils as $value) {
                                                                                   //  $agentfb = $this->Agents_model->get_fb_status($value->id);
                                                                                     $agentfb ='';
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td><?php echo ++$count; ?></td>
                                                                                        <td><?php echo date('d-M-Y', strtotime($value->date)); ?></td>
                                                                                        <td><?php echo date('d-M-Y', strtotime($value->Call_Date)); ?></td>
                                                                                        <td><?php echo $value->Time_Of_Call; ?></td>
                                                                                        <td><?php echo $value->Calling_Number; ?></td>
<!--                                                                                        <td><?php echo $value->CLI_Number; ?></td>-->
                                                                                        <td>
                                                                                        <?php
                                                                                        $colour='';
                                                                                        if($value->score >= 90)
                                                                                        {
                                                                                        $colour='success';    
                                                                                        }else if($value->score > 1 && $value->score < 90 )
                                                                                        {
                                                                                            $colour='warning'; 
                                                                                        }
                                                                                        else if($value->score == 0)
                                                                                        {
                                                                                            $colour='danger'; 
                                                                                        }
                                                                                        ?>    
                                                                                            <span class="label label-<?=$colour?>"><?php echo $value->score; ?></span>
                                                                                        
                                                                                        </td>
                                                                                        <td><?php echo ($value->Fatal_Reason == '0') ? '0' : '1'; ?></td>
<!--                                                                                        <td><?php echo $value->audit_by ?></td>
                                                                                        <td><?php echo $value->QME_Remarks ?></td>-->
                                                                                        <td>
                                                                                            
                                                                                            <?php
                                                                                        if(!empty($agentfb)){
                                                                                echo"<span class='label label-success'>    By Auditor: ". $agentfb->Auditor." Accepted: ".$agentfb->Accepted." </span> ";
                                                                             }
                                                                             ?><br>
                                                                                          
                                                                                            <?php if (($value->score === 0) || ($value->score < 90)) { ?>
                                                                                                <!--<a href="JavaScript:Void(0);" id="<?php echo$value->id; ?>" onclick="showModel(this.id)"> <span title="Add Remarks" class="glyphicon glyphicon-tags"></span> </a>-->
                                                                                            <?php }
                                                                                            ?>
                                                                                            <div class="modal modal-warning fade" id="modal-warning_<?= $value->id; ?>">
                                                                                                <div class="modal-dialog">
                                                                                                    <div class="modal-content">
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                                <span aria-hidden="true">&times;</span></button>
                                                                                                            <h4 class="modal-title">Add FeedBack.. Multi<?php echo$value->id; ?></h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="form-group">
                                                                                                                <label for="weapon_primary_cat">FeedBack Given By Auditor</label>            
                                                                                                                <select class="form-control" name="Auditor1" id="Auditor_<?= $value->id;?>">
                                                                                                                    <option value="Yes" >Yes</option>
                                                                                                                    <option value="No">No</option>                                
                                                                                                                </select>
                                                                                                            </div>

                                                                                                            <div class="form-group">
                                                                                                                <label for="weapon_primary_cat">FeedBack Accepted</label>            
                                                                                                                <select class="form-control" name="Accepted1" id="Accepted_<?= $value->id; ?>">
                                                                                                                    <option value="Yes" >Yes</option>
                                                                                                                    <option value="No">No</option>                                
                                                                                                                </select>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="modal-footer">
                                                                                                            <input type="hidden" name="audit_id1" id="audit_id_<?= $value->id;?>" value="<?php echo$value->id; ?>"/>
                                                                                                            <button type="button" id="closemodel" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                                                                            <button type="button" class="btn btn-outline" id="<?php echo$value->id; ?>" onclick="saveshowModel(this.id)">Save</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!-- /.modal-content -->
                                                                                                </div>
                                                                                                <!-- /.modal-dialog -->
                                                                                            </div>






                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>




                                                                        </tbody>
                                                                    </table>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $cointid++;
                                                }
                                            }
                                            ?>



                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>

                        </div>
                        <div class="tab-pane" id="settings">
                            
                        
<canvas id="bar-chart" width="800" height="450"></canvas>
       

                        </div>


                    </div>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<script src="<?php echo base_url() ?>assets/js/3.4.1_jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/Chart.min.js"></script>

<!-- /.content-wrapper -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
<script>
                                                                                                function showModel(id) {

                                                                                                    $('#modal-warning_' + id).modal('show');

                                                                                                }

                                                                                                function saveshowModel(id) {
                                                                                                   
                                                                                                    var audit_id = $("#audit_id_"+id).val();
                                                                                                    var Auditor = $("#Auditor_"+id).val();
                                                                                                    var Accepted = $("#Accepted_"+id).val();
                                                                                               
                                                                                                    var url = '<?php echo site_url(); ?>/Agentfeedback/create_action_ajax';
                                                                                                    $.post(url, {audit_id: audit_id, Auditor: Auditor, Accepted: Accepted}, function (result) {
                                                                                                        //alert(result=='result');
                                                                                                        if (result === 'Success') {
                                                                                                            alert(result);
                                                                                                            location.reload();
                                                                                                        } else {
                                                                                                            alert(result);
                                                                                                        }


                                                                                                    });

                                                                                                }


                                                                                                function callModel(e) {
                                                                                                    $('#audit_id').val(e.id);
                                                                                                    $('#modal-warning').modal('show');
                                                                                                }
                                                                                                function callModel1(e) {

                                                                                                    $('#audit_id').val(e.id);
                                                                                                    $('#modal-warning').modal('show');
                                                                                                }
                                                                                                $(document).ready(function () {

                                                                                                });

                                                                                                $("#add_weapon").click(function () {
                                                                                                    var audit_id = $("#audit_id").val();
                                                                                                    var Auditor = $("#Auditor").val();
                                                                                                    var Accepted = $("#Accepted").val();

                                                                                                    var url = '<?php echo site_url(); ?>/Agentfeedback/create_action_ajax';
                                                                                                    $.post(url, {audit_id: audit_id, Auditor: Auditor, Accepted: Accepted}, function (result) {
                                                                                                        //alert(result=='result');
                                                                                                        if (result === 'Success') {
                                                                                                            alert(result);
                                                                                                            setTimeout(function () {
                                                                                                                $('#closemodel').trigger('click');
                                                                                                            }, 1000);
                                                                                                        } else {
                                                                                                            alert(result);
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                                
                                                                                              

new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: [<?php echo  $newarray='"'.implode('", "', $months).'"';?>],
      datasets: [
        {
          label: "Score",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          data: [<?php echo implode(',', $scores);?>]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Monthly Avg Score'
      }
    }
});
</script>