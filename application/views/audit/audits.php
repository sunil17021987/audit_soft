<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Self Audit's List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open(base_url() . 'Audit/audits'); ?>
        <div class="row">



            <div class="col-md-6">
                <div class="form-group">
                    <label>Date range:</label>

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="bydaterange"  class="form-control pull-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>

            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label> Mobile / CLI</label>
                    <input type="text" name="Calling_Number" value="<?php echo$Calling_Number; ?>"  class="form-control pull-right" >

                </div>
            </div> 





            <div class="col-md-2" style="margin-top: 20px;">
                <button type="submit" class="btn btn-info" style="width: 100%;">
                    Search</button>
            </div>
            <div class="col-md-2" style="margin-top: 20px;">
                <?php
                $string = '';
                if ($date_tof != '' && $date_fromf != '') {
                    $string = "/$date_fromf/$date_tof";
                }
                ?>
                <a class="btn btn-info" href="<?php echo base_url() . 'Audit/excelExport' . $string ?>">Excel</a>
            </div>

        </div>
        <?php echo form_close(); ?>


        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4 text-center">
                                <div style="margin-top: 4px"  id="message">
                                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                <?php //echo anchor(site_url('audit/create'), 'Create', 'class="btn btn-primary"'); ?>
                                <?php //echo anchor(site_url('audit/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th width="80px">No</th>
                                    <th>Agent</th>
                                    <th> AM</th>
                                    <th> TL</th>
                                    <th>Score</th>
                                    <th>status</th>
                                    <th>Date</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $start = 0;
                                $feedback_status_id='';
                                foreach ($audit_data as $audit) {
                                    ?>
                                    <tr>
                                        <td><?php echo ++$start ?></td>
                                        <td><?php echo $audit->name ?></td>
                                        <td><?php echo $audit->employee_id_AM ?></td>
                                        <td><?php echo $audit->employee_id_TL ?></td>
                                        <td><?php echo $audit->score ?></td>
                                        <td><?php $feedback_status_id = $this->Audit_remarks_model->get_by_audit_id($audit->id);
                                        if($feedback_status_id!=''){
                                        echo $feedback_status_id->feedback_status_id;
                                        }
                                        ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($audit->date)); ?></td>

                                        <td style="text-align:center" width="200px">
                                            <?php
                                            //echo anchor(site_url('audit/read/'.$audit->id),'<i class="fa fa-eye"></i>'); 
                                            //echo ' | '; 
                                            echo anchor(site_url('Gateway/edit_audit/' . $audit->id), '<i class="fa fa-pencil-square-o"></i>');
                                            //echo ' | '; 
                                            //echo anchor(site_url('audit/delete/'.$audit->id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#mytable").dataTable();
                            });
                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>