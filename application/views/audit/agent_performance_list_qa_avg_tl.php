<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent performance List TL AVG        
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <!--        <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-4">
                                      
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <div style="margin-top: 4px"  id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                        <?php echo anchor(site_url('agent_performance/create'), 'Create', 'class="btn btn-primary"'); ?>
                        <?php echo anchor(site_url('agent_performance/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                                    </div>
                                </div>-->

                        <?php echo form_open(base_url() . 'Audit/agentPerformancebyQA_AVG_TL'); ?>
                        <div class="row">



                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date range:</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">
                                    </div>
                                    <!-- /.input group -->
                                </div>

                            </div>
                            
                            <?php
                             if($this->session->userdata('type')!= 4){?>
                                  <div class="col-md-3">
                                <div class="form-group">
                                    <label>AM Name:</label>
                                    <select id="employee_id_AM" class="form-control" name="employee_id_AM">
                                        <option value="">---  Select Name ---</option>
                                        <?php
                                        if (!empty($am_lists)) {
                                            foreach ($am_lists as $rl) {
                                                ?>
                                                <option value="<?php echo $rl->emp_id ?>" <?php if ($employee_id_AM == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>  

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>TL Name:</label>
                                    <select id="employee_id_TL" class="form-control" name="employee_id_TL">
                                        <option value="">---  Select Name ---</option>
                                        <?php
                                        if (!empty($tl_lists)) {
                                            foreach ($tl_lists as $rl) {
                                                ?>
                                                <option value="<?php echo $rl->emp_id ?>" <?php if ($employee_id_TL == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>  

                                </div>
                            </div>
                             <?php }
                            
                            ?>
                            
                           







                            <div class="col-md-2" style="margin-top: 20px;">
                                <button type="submit" class="btn btn-info" style="width: 100%;">
                                    Search</button>
                            </div>


                        </div>
                        <?php echo form_close(); ?>


                         <div class="row">

                            <div class="col-md-12">

                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Over All Performance</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-bordered">
                                                <tbody><tr>
                                                        <th>S.no.</th>
                                                        <th>MSD ID</th>


                                                        <th>Staffed Duration</th>
                                                        <th>Ready Duration</th>
                                                        <th>Break Duration</th>
                                                       
                                                        <th>Hold Time</th>
                                                       <th>Abandoned %</th>
                                                        <th>Connectivity %</th>

                                                    </tr>

                                                    <?php
                                                    $start = 0;
                                                    if ($agent_performance_data) {
                                                        foreach ($agent_performance_data as $agent_performance) {
                                                           // $agentname = $this->Agents_model->get_agents_namebyid($agent_performance->agents_id);
                                                            $StaffedDuration = 0;
                                                            $ReadyDuration = 0;
                                                            ?>
                                                            <tr>
                                                                <td><?php echo ++$start ?></td>
                                                               <td><?php echo $agent_performance->agents_msd . "(" . $agent_performance->msd . ")" ?></td>


                                                                <td><?php
                                                               
                                                                echo $StaffedDuration = sum_the_time($agent_performance->Inbound_StaffedDuration, $agent_performance->Outbound_StaffedDuration); ?></td>
                                                                <td><?php echo $ReadyDuration = sum_the_time($agent_performance->Inbound_ReadyDuration, $agent_performance->Outbound_ReadyDuration); ?></td>
                                                                <td><?php echo sum_the_time($agent_performance->Inbound_BreakDuration, $agent_performance->Outbound_BreakDuration); ?></td>
                                                               
                                                                <td><?php echo sum_the_time($agent_performance->Inbound_HoldTime, $agent_performance->Outbound_HoldTime) ?></td>
                                                                 <td><span class="badge bg-red"><?php echo $agent_performance->Inbound_AbandonedPer ?>%</span></td>
                                                                 <td><span class="badge bg-red"><?php echo $agent_performance->Outbound_ConnectivityPer ?>%</td>
                                                                  
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>





                                                </tbody></table>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>  
                        
                        
                        
                        

                        <div class="row">

                            <div class="col-md-6">

                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Inbound Performance</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-bordered">
                                                <tbody><tr>
                                                        <th>S.no.</th>
                                                        <th>MSD ID</th>
<!--                                                        <th>Date</th>-->
                                                        <th>Staffed Duration</th>
                                                        <th>Ready Duration</th>
                                                        <th>Break Duration</th>
                                                        <th>Occupancy %</th>
                                                        <th>Hold Time</th>
                                                        <th>calls Offered</th>
                                                        <th>Call answered</th>
                                                        <th>Abandoned %</th>
                                                        <th>AHT</th>
                                                    </tr>

                                                    <?php
                                                    $start = 0;
                                                    if ($agent_performance_data) {
                                                        foreach ($agent_performance_data as $agent_performance) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo ++$start ?></td>
                                                                  <td><?php echo $agent_performance->agents_msd . "(" . $agent_performance->msd . ")" ?></td>
                                                              
<!--                                                                <td><?php echo date('d-M-Y', strtotime($agent_performance->date_of)) ?></td>-->
                                                                <td><?php echo $agent_performance->Inbound_StaffedDuration ?></td>
                                                                <td><?php echo $agent_performance->Inbound_ReadyDuration ?></td>
                                                                <td><?php echo $agent_performance->Inbound_BreakDuration ?></td>
                                                                <td><?php echo $agent_performance->Inbound_IdleTime."%" ?></td>
                                                                <td><?php echo $agent_performance->Inbound_HoldTime ?></td>
                                                                <td><?php echo $agent_performance->Inbound_NumberOfcallsOffered ?></td>
                                                                <td><?php echo $agent_performance->Inbound_NumberofCallanswered ?></td>
                                                                <td><span class="badge bg-red"><?php echo $agent_performance->Inbound_AbandonedPer ?>%</span></td>
                                                                <td><?php echo $agent_performance->Inbound_AHT ?></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>





                                                </tbody></table>
                                        </div>
                                    </div>

                                </div>









                            </div>

                            <div class="col-md-6">


                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Outbound Performance</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-bordered">
                                                <tbody><tr>
                                                        <th>S.no.</th>
                                                        <th>MSD ID</th>
<!--                                                        <th>Date</th>-->

                                                        <th>Staffed Duration</th>
                                                        <th>Ready Duration</th>
                                                        <th>Break Duration</th>
                                                        <th>Occupancy %</th>
                                                        <th>Hold Time</th>
                                                        <th>Calls Dial</th>
                                                        <th>Calls connected</th>
                                                        <th>Connectivity %</th>
                                                        <th>AHT</th>
                                                    </tr>

                                                    <?php
                                                    $start = 0;
                                                    if ($agent_performance_data) {
                                                        foreach ($agent_performance_data as $agent_performance) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo ++$start ?></td>
                                                                <td><?php echo $agent_performance->agents_msd . "(" . $agent_performance->msd . ")" ?></td>
<!--                                                                <td><?php echo date('d-M-Y', strtotime($agent_performance->date_of)) ?></td>-->
                                                                <td><?php echo $agent_performance->Outbound_StaffedDuration ?></td>
                                                                <td><?php echo $agent_performance->Outbound_ReadyDuration ?></td>
                                                                <td><?php echo $agent_performance->Outbound_BreakDuration ?></td>
                                                                <td><?php echo $agent_performance->Outbound_IdleTime.'%' ?></td>
                                                                <td><?php echo $agent_performance->Outbound_HoldTime ?></td>
                                                                <td><?php echo $agent_performance->Outbound_NumberOfCallsDial ?></td>
                                                                <td><?php echo $agent_performance->Outbound_NumberofCallsconnected ?></td>

                                                                <td><span class="badge bg-red"><?php echo $agent_performance->Outbound_ConnectivityPer ?>%</span></td>
                                                                <td><?php echo $agent_performance->Outbound_AHT ?></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>





                                                </tbody></table>
                                        </div>
                                    </div>

                                </div>






                            </div>
                        </div>  




                        <!--                        <div class="row">
                                                    <hr>
                                                    <div class="col-md-12">
                        
                                                        <div class="box">
                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">Performance as per CRM</h3>
                                                                <a class="btn bg-olive margin" id="refreshdata">
                                                                    <i class="fa fa-repeat"></i>
                                                                </a>
                                                            </div>
                                                             /.box-header 
                                                            <div class="box-body">
                                                                <div class="box-body table-responsive no-padding">
                                                                    <div id="wait" style="display:none;width:69px;height:89px;border:0px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src=<?php echo base_url() . 'assets/images/35.gif' ?> width="64" height="64" /><br>Loading..</div>
                        
                        
                                                                    <table class="table table-bordered" id="crmdata">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th>Date</th>
                                                                                <th>Total Action In CRM</th>
                                                                                <th>Complaint Close</th>
                                                                                <th>Force Close</th>
                                                                                <th>Reminder</th>
                                                                                <th>New Complaint Registered</th>
                                                                                <th>Call not connected</th>
                                                                                <th>Not Pick</th>
                                                                                <th>Reopen</th>
                                                                            </tr>
                        
                        
                        
                        
                        
                        
                        
                                                                        </tbody></table>
                                                                </div>
                                                            </div>
                        
                                                        </div>
                        
                                                    </div>
                        
                        
                                                </div>  -->

                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                            $('#employee_id_AM').on('change', function () {
                                var employee_id_AM = $(this).val();

                                if (employee_id_AM) {
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?php echo site_url(); ?>/Audit/get_tl_listby_amid',
                                        data: {employee_id_AM: employee_id_AM},
                                        success: function (html) {
                                            $('#employee_id_TL').html(html);
                                        }
                                    });


                                }

                            });
                            $(document).ready(function () {
                                //  $("#mytable").dataTable();
                                $(document).ajaxStart(function () {
                                    $("#wait").css("display", "block");
                                });


                                var category_main_id = '123';

                                if (category_main_id) {
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?php echo site_url(); ?>/DashboardAgent/agentPerformanceByCRMM',
                                        data: {category_main_id: category_main_id},
                                        success: function (html) {
                                            // alert(html);
                                            //  $('#crmdata').html(html);
                                            $("#crmdata").last().append(html);
                                            $(document).ajaxComplete(function () {
                                                $("#wait").css("display", "none");
                                            });
                                        }
                                    });


                                }


                            });


                            $("#refreshdata").click(function () {
                                location.reload();
                            });
                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>