<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Audit <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Agents Id <?php echo form_error('agents_id') ?></label>
            <input type="text" class="form-control" name="agents_id" id="agents_id" placeholder="Agents Id" value="<?php echo $agents_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>
            <input type="text" class="form-control" name="employee_id_AM" id="employee_id_AM" placeholder="Employee Id AM" value="<?php echo $employee_id_AM; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>
            <input type="text" class="form-control" name="employee_id_TL" id="employee_id_TL" placeholder="Employee Id TL" value="<?php echo $employee_id_TL; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Score <?php echo form_error('score') ?></label>
            <input type="text" class="form-control" name="score" id="score" placeholder="Score" value="<?php echo $score; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('audit') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>