<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Attendance_format List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $attributes = array('class' => 'testform', 'id' => 'myform');
        echo form_open(base_url() . 'attendance_format/index', $attributes);
        ?>
        <div class="row">



            <div class="col-md-6">
                <div class="form-group">
                    <label>Date range:</label>

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">

                    </div>
                    <!-- /.input group -->
                </div>

            </div>







            <div class="col-md-2" style="margin-top: 20px;">
                <button type="submit" name="search" value="search" class="btn btn-info" style="width: 100%;">
                    Search</button>
            </div>

            <div class="col-md-2" style="margin-top: 20px;">


                <button type="submit" name="delete" value="delete" class="btn btn-danger" onclick="if (!confirm('Are you sure?')) {
                                            return false
                                        }"><span>Delete</span></button>
            </div>
            <div class="col-md-2" style="margin-top: 20px;">
                <?php echo anchor(site_url('attendance_format/excel'), 'Excel', 'class="btn btn-primary"'); ?>
            </div>



        </div>
        <?php echo form_close(); ?>



        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->

                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th width="80px">No</th>
                                    <th>Employee Name</th>
                                    <th>TL Name</th>
                                    <th>Agent Status</th>
                                    <th>Batch</th>
                                    <th>AM Name</th>
                                    <th>Date Of</th>
                                    <th>Roster Shift</th>
                                    <th>Login</th>
                                    <th>Attendance</th>
                                    <th>First Login</th>
                                    <th>Shift Adherence</th>

                                    <th>MSD ID</th>

                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $start = 0;
                                foreach ($attendance_format_data as $attendance_format) {
                                    ?>
                                    <tr>
                                        <td><?php echo ++$start ?></td>
                                        <td><?php echo $attendance_format->employeeName ?></td>
                                        <td><?php echo $attendance_format->TLName ?></td>
                                        <td><?php echo $attendance_format->agent_Status ?></td>
                                        <td><?php echo $attendance_format->batch ?></td>
                                        <td><?php echo $attendance_format->AMName ?></td>
                                        <td><?php echo $attendance_format->Date_of ?></td>
                                        <td><?php echo $attendance_format->RosterShift ?></td>
                                        <td><?php echo $attendance_format->Login ?></td>
                                        <td><?php echo $attendance_format->Attendance ?></td>
                                        <td><?php echo $attendance_format->MSD_ID ?></td>
                                         <td><?php echo $attendance_format->FirstLogin ?></td>
                                          <td><?php echo $attendance_format->ShiftAdherence?></td>
                                        <td><?php echo $attendance_format->created_date ?></td>
                                        <td style="text-align:center" width="200px">
                                            <?php
                                            echo anchor(site_url('attendance_format/read/' . $attendance_format->id), '<i class="fa fa-eye"></i>');
                                            echo ' | ';
                                            echo anchor(site_url('attendance_format/update/' . $attendance_format->id), '<i class="fa fa-pencil-square-o"></i>');
                                            echo ' | ';
                                            echo anchor(site_url('attendance_format/delete/' . $attendance_format->id), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#mytable").dataTable();
                                                    });
                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>