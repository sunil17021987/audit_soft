<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Attendance_format Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>EmployeeName</b></td><td><?php echo $employeeName; ?></td></tr>
	    <tr><td><b>TLName</b></td><td><?php echo $TLName; ?></td></tr>
	    <tr><td><b>Agent Status</b></td><td><?php echo $agent_Status; ?></td></tr>
	    <tr><td><b>Batch</b></td><td><?php echo $batch; ?></td></tr>
	    <tr><td><b>AMName</b></td><td><?php echo $AMName; ?></td></tr>
	    <tr><td><b>Date Of</b></td><td><?php echo $Date_of; ?></td></tr>
	    <tr><td><b>RosterShift</b></td><td><?php echo $RosterShift; ?></td></tr>
	    <tr><td><b>Login</b></td><td><?php echo $Login; ?></td></tr>
	    <tr><td><b>Attendance</b></td><td><?php echo $Attendance; ?></td></tr>
	    <tr><td><b>MSD ID</b></td><td><?php echo $MSD_ID; ?></td></tr>
	    <tr><td><b>Created Date</b></td><td><?php echo $created_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('attendance_format') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>