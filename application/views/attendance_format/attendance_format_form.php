<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Attendance_format <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">EmployeeName <?php echo form_error('employeeName') ?></label>
            <input type="text" class="form-control" name="employeeName" id="employeeName" placeholder="EmployeeName" value="<?php echo $employeeName; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">TLName <?php echo form_error('TLName') ?></label>
            <input type="text" class="form-control" name="TLName" id="TLName" placeholder="TLName" value="<?php echo $TLName; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Agent Status <?php echo form_error('agent_Status') ?></label>
            <input type="text" class="form-control" name="agent_Status" id="agent_Status" placeholder="Agent Status" value="<?php echo $agent_Status; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Batch <?php echo form_error('batch') ?></label>
            <input type="text" class="form-control" name="batch" id="batch" placeholder="Batch" value="<?php echo $batch; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">AMName <?php echo form_error('AMName') ?></label>
            <input type="text" class="form-control" name="AMName" id="AMName" placeholder="AMName" value="<?php echo $AMName; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Date Of <?php echo form_error('Date_of') ?></label>
            <input type="text" class="form-control" name="Date_of" id="Date_of" placeholder="Date Of" value="<?php echo $Date_of; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">RosterShift <?php echo form_error('RosterShift') ?></label>
            <input type="text" class="form-control" name="RosterShift" id="RosterShift" placeholder="RosterShift" value="<?php echo $RosterShift; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Login <?php echo form_error('Login') ?></label>
            <input type="text" class="form-control" name="Login" id="Login" placeholder="Login" value="<?php echo $Login; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Attendance <?php echo form_error('Attendance') ?></label>
            <input type="text" class="form-control" name="Attendance" id="Attendance" placeholder="Attendance" value="<?php echo $Attendance; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">MSD ID <?php echo form_error('MSD_ID') ?></label>
            <input type="text" class="form-control" name="MSD_ID" id="MSD_ID" placeholder="MSD ID" value="<?php echo $MSD_ID; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Created Date <?php echo form_error('created_date') ?></label>
            <input type="text" class="form-control" name="created_date" id="created_date" placeholder="Created Date" value="<?php echo $created_date; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('attendance_format') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>