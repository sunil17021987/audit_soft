<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           photo <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
<!--                                <label for="int">Gfg Events Id <?php echo form_error('gfg_events_id') ?></label>-->
                                <input type="hidden" class="form-control" name="gfg_events_id" id="gfg_events_id" placeholder="Gfg Events Id" value="<?php echo $gfg_events_id; ?>" />
                            </div>
                             <div class="form-group">  
                                <div style="padding-bottom:15px;">

                                    <div class="slim img-responsive" style="width:200px; height:230px;margin:0 auto;" data-label="Drop your image here" data-size="200,230">

                                        <style type="text/css">
                                            .slim .slim-file-hopper {
                                                background:url("images/364x209.jpg")!important;
                                            }
                                        </style>
                                        <input type="file"/>
                                    </div>


                                </div>
                            </div>

                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('gfg_events_photos') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>