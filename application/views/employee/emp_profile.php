<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employee Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">User profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="row">
            <div class="col-md-3">
                <?php
                $start = 0;
                foreach ($row as $r) {
                    ?>
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <a href="<?php echo base_url(); ?>Imageupload/upload/<?php echo $r->emp_id; ?>">

                                <?php
                                if ($r->image != '') {
                                    ?>
                                <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url($r->image)?>" alt="User profile picture">
                                <?php } else { ?>
                                    <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/admin/dist/img/avatar.png" alt="User profile picture">

    <?php }
    ?>

                            </a>
                            <!--<h3 class="profile-username text-center">Nina Mcintire</h3>-->

                            <p class="text-muted text-center"></p></br>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Employee ID</b> <a class="pull-right"><?php echo $r->emp_id; ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Name</b> <a class="pull-right"><?php echo $r->emp_name; ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Designation</b> <a class="pull-right"><?php  $row = $this->Designation_model->get_by_id($r->designation); echo  $row->name; ?></a>
                                </li>
                            </ul>

                            <a href="<?php echo base_url(); ?>Imageupload/upload/<?php echo $r->emp_id; ?>" class="btn btn-primary btn-block"><b>Upload Image</b></a>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                 <!--/.col--> 
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab"><i class="fa fa-street-view"> Personal</i></a></li>
                            <!--<li><a href="#fees" data-toggle="tab">Fees</a></li>
                            <li><a href="#settings" data-toggle="tab">Settings</a></li>-->
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">

                                <!--<div class="tab-pane" id="settings">-->
                                <form class="form-horizontal">
                                    <!-- /*****************form part start******************-->    
                                    <div class="box box-info">
                                        <div class="box-header">
                                            <h3 class="box-title">Personal Details</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped" >
                                                <tbody>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td><b>Date Of Birth</b></td>
                                                        <td>
    <?php echo $r->dob; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>2.</td>
                                                        <td><b>Mobile No.</b></td>
                                                        <td>
    <?php echo $r->mobile; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>3.</td>
                                                        <td><b>Email</b></td>
                                                        <td>
    <?php echo $r->email; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>4.</td>
                                                        <td><b>Address</b></td>
                                                        <td>
    <?php echo $r->address; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>5.</td>
                                                        <td><b>Qualification</b></td>
                                                        <td>
    <?php echo $r->qualification; ?>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>6.</td>
                                                        <td><b>Department</b></td>
                                                        <td>
    <?php echo $r->department; ?>
                                                            
                                                        </td>

                                                    </tr>
                                                    <tr><td></td><td></td><td style="float: right"><a href="<?php echo site_url('employee') ?>" class="btn btn-warning">Cancel</a></td></tr>
                                                </tbody></table>


<?php } ?>

                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <!-- /***********.form part********** -->    

                            </form>
                            <!--</div>-->
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
<!--                        <div class="tab-pane" id="fees">
                            ---Start current batch fees details---

                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="border-bottom-warning page-header profile-title-1">	
                                        <i class="fa fa-inr"></i> Current Fees Details	</h4>
                                </div> /.col 
                            </div>

                            <div class="box box-solid">
                                <div class="box-body table-responsive no-padding">
                                    <div id="w0" class="grid-view"><table class="table table-striped table-bordered" ><thead>
                                                <tr><th>#</th><th>Name</th><th>Start Date</th><th>End Date</th><th>Due Date</th><th>Fees Details</th><th>Total Amount</th><th>Total Paid Fees</th></tr>
                                            </thead>
                                            <tbody>
                                                <tr data-key="1"><td>1</td><td>Tuition Fees</td><td>01-06-2015</td><td>30-06-2015</td><td>01-07-2015</td><td><ol><li>Tuition Fees-2015 (25000) </li><li>Extra Misc Fees (1500) </li></ol></td><td>26500</td><td>0</td></tr>
                                            </tbody></table>
                                    </div>    </div>
                            </div>
                            ---End current batch fees details---

                            ---Start student payment history block---

                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="edusec-border-bottom-warning page-header edusec-profile-title-1">	
                                        <i class="fa fa-inr"></i> Student Payment History	</h4>
                                </div> /.col 
                            </div>

                            <div class="box box-solid">
                                <div class="box-body table-responsive no-padding">
                                    <div id="w1"><div id="w2" class="grid-view">
                                            <table class="table table-striped table-bordered"><thead>
                                                    <tr><th>#</th><th>Receipt No.</th><th>Payment Date</th><th>Fees Collect Name</th><th>Payment Mode</th><th>Cheque No</th><th>Amount</th></tr>
                                                </thead>
                                                <tbody>
<?php
$start = 0;
foreach ($fees_collect_data as $fees_collect) {
    ?>
                                                        <tr>
                                                            <td><?php echo ++$start ?></td>
                                                            <td><?php echo $fees_collect->s_id ?></td>
                                                            <td><?php echo $fees_collect->date ?></td>
                                                            <td><?php echo $fees_collect->paid_amount ?></td>
                                                            <td><?php echo $fees_collect->due_amount ?></td>
                                                            <td><?php echo $fees_collect->created_at ?></td>

                                                        </tr>
<?php } ?>
                                                    <tr><td colspan="7"><div class="empty">No fees results found.</div></td></tr>
                                                </tbody></table>
                                        </div></div>     </div>
                            </div>
                            ---End student payment history block---

                        </div>-->
                        <!-- /.tab-pane -->

                    </div>
                    <!-- /.nav-tabs-custom -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->