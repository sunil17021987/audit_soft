<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> New Employee </h1>
    <?php echo validation_errors(); ?>
  </section>
  <div ng-app="myApp" ng-controller="courseCtrl"> 
    <!-- Main content -->
    <section class="content">
      <div class="row">
      
      <!-- /.box-header -->
      <div class="box-body"> 
        <!-- ******************/master header end ****************** -->
        <div class="col-xs-12 col-lg-12">
          <div class="box-success box view-item col-xs-12 col-lg-12">
            <div class="stu-master-form">
              <p class="note">Fields with <span class="required"> <b style="color:red;">*</b></span> are required.</p>
              <form action="<?php echo $action; ?>" method="post" id="frmemp" autocomplete="off">
                <!--<input type="hidden" name="_csrf" value="Z0lZUnFXSUEIexAnRDwzICUGFioIYC4UFB0qKwhkJgQ4fD8GGTgFBw==">-->
                
                <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                  <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> Personal Details</h4>
                  </div>
                  <div class="box-body">
                    <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>" />
                    <input type="hidden" name="created_at" value="<?php echo date('d/m/Y'); ?>" />
                    <!--<div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group field-stuinfo-stu_title">
                          <label class="control-label" for="stuinfo-stu_title">Title</label>
                          <select id="title" class="form-control" name="title">
                            <option value="">--- Select Title ---</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Ms.">Ms.</option>
                          </select>
                          <div class="help-block"></div>
                        </div>
                      </div>
                    </div>-->
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Name <?php echo form_error('name') ?></label>
                          <input type="text" class="form-control" name="emp_name" id="name" placeholder="Name" value="<?php echo $emp_name; ?>" />
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group has-feedback">
                          <label for="varchar">Gender <?php echo form_error('gender') ?></label>
                          <select name="gender" class="form-control select">
                            <option value="">--- Select Gender ---</option>
                            <option value="MALE" <?php if($gender=='MALE') echo 'selected';  ?>>MALE</option>
                            <option value="FEMALE" <?php if($gender=='FEMALE') echo 'selected';  ?>>FEMALE</option>
                          </select>
                          </select>
                        </div>
                      </div>
<!--                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Date Of Birth<?php echo form_error('dob') ?></label>
                          <input type="text" class="form-control" name="dob" id="dob" placeholder="Dob" value="<?php echo $dob; ?>" />
                        </div>
                      </div>-->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Email ID<?php echo form_error('email') ?></label>
                          <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Mobile No. <?php echo form_error('mobile') ?></label>
                          <input type="text" class="form-control" name="mobile" id="mob" placeholder="Mobile No." value="<?php echo $mobile; ?>" />
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Alternate No. <?php echo form_error('alternate_mobile') ?></label>
                          <input type="text" class="form-control" name="alternate_mobile" id="alternate_mobile" placeholder="Alternate No." value="<?php echo$alternate_mobile; ?>" />
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group field-stumaster-stu_master_category_id">
                          <label class="control-label" for="stumaster-stu_master_category_id">Category</label>
                          <select id="category" class="form-control" name="category">
                            <option value="">---  Select Category ---</option>
                            <?php
							if (!empty($category_list)) {
								foreach ($category_list as $rl) {
									?>
                                    <option value="<?php echo $rl->category_id ?>" <?php if($category== $rl->category_id) echo 'selected';  ?>><?php echo $rl->category_name ?></option>
                                    <?php
								}
							}
							?>
                          </select>
                          <div class="help-block"></div>
                        </div>
                      </div>
<!--                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <label class="control-label" for="stumaster-stu_master_nationality_id">Qualification<?php echo form_error('qualification') ?></label>
                        <select name="qualification" class="form-control">
                          <option value="">Select Qualification</option>
                          <option value="10th" <?php if($qualification=='10th') echo 'selected';  ?>>10th</option>
                          <option value="12th" <?php if($qualification=='12th') echo 'selected';  ?>>12th</option>
                          <option value="Graduation" <?php if($qualification=='Graduation') echo 'selected';  ?>>Graduation</option>
                          <option value="PG" <?php if($qualification=='PG') echo 'selected';  ?>>Post Graduation</option>
                        </select>
                      </div>-->
                          <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Address <?php echo form_error('address') ?></label>
                          <textarea type="text" class="form-control" name="address" id="address" placeholder="Address" value="" ><?php echo $address; ?></textarea>
                        </div>
                      </div>
                    
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                         <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="msd_ID">MSD ID<?php echo form_error('msd_ID') ?></label>
                          <input type="text" class="form-control" name="msd_ID" id="password" placeholder="msd ID" value="<?php echo$msd_ID; ?>" />
                        </div>
                      </div>
                          <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="password">Password<?php echo form_error('password') ?></label>
                          <input type="password" class="form-control" name="password" id="password" placeholder="New Password" value="<?php echo$password; ?>" />
                        </div>
                      </div>
                    
                    
                    </div>
                  </div>
                  <!---./end box-body---> 
                </div>
                <!---./end box--->
                
                <div class="box box-solid box-warning col-xs-12 col-lg-12 no-padding">
                  <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> Academic Details</h4>
                  </div>
                  <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="varchar">Joining Date <?php echo form_error('join_date') ?></label>
                          <input type="text" class="form-control" name="join_date" id="admission_date" placeholder="Admission Date" value="<?php echo $join_date; ?>" />
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <label for="varchar">Department<?php echo form_error('department') ?></label>
                        
                        <select name="department" class="form-control" ng-model="myData.countryId">
                          <option value="">Select department </option>
                          <option ng-repeat="country in myData.countries" value="{{country.id}}">{{country.name}}</option>
                        </select>
                        <!--<input type="hidden" name="department" value="{{myData.countryId}}" />-->
                      </div>
                       <div class="col-xs-12 col-sm-4 col-lg-4">
                        <label for="varchar">Designation<?php echo form_error('designation') ?></label>
                        <select class="form-control" name="designation" ng-model="design.countryId">
                          <option value="">Select designation </option>
                           <option ng-repeat="deg in design.countries" value="{{deg.id}}">{{deg.name}}</option>
                        </select>
<!--                        <input type="hidden" name="designation" value="{{design.countryId}}" />-->
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
<!--                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group field-stumaster-stu_master_stu_status_id">
                          <label class="control-label" for="stumaster-stu_master_stu_status_id">Martial Status</label>
                          <select id="status" class="form-control" name="matial_status">
                            <option value="UNMARRIED" <?php if($matial_status=='UNMARRIED') echo 'selected';  ?>>UNMARRIED</option>
                            <option value="MARRIED" <?php if($matial_status=='MARRIED') echo 'selected';  ?>>MARRIED</option>
                            <option value="DIVORCED" <?php if($matial_status=='DIVORCED') echo 'selected';  ?>>DIVORCED</option>
                          </select>
                          <div class="help-block"></div>
                        </div>
                      </div>-->
<!--                      <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group field-stumaster-stu_master_stu_status_id">
                          <label class="control-label" for="stumaster-stu_master_stu_status_id">Experience</label>
                          <select id="experience" class="form-control" name="experience">
                            <option value="0" <?php if($experience=='0') echo 'selected';  ?>>0-1 year</option>
                            <option value="1" <?php if($experience=='1') echo 'selected';  ?>>1-2 years</option>
                            <option value="2" <?php if($experience=='2') echo 'selected';  ?>>2-3 years</option>
                            <option value="3" <?php if($experience=='3') echo 'selected';  ?>>3-4 years</option>
                            <option value="4" <?php if($experience=='4') echo 'selected';  ?>>5-6 years</option>
                            <option value="5" <?php if($experience=='5') echo 'selected';  ?>>6-7 years</option>
                            <option value="6" <?php if($experience=='6') echo 'selected';  ?>>More than 10  years</option>
                          </select>
                          <div class="help-block"></div>
                        </div>
                      </div>-->
                        
                         <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group has-feedback">
                          <label for="varchar">Status <?php echo form_error('status') ?></label>
                          <select name="status" class="form-control select">
                            <option value="">--- Select Gender ---</option>
                            <option value="1" <?php if($status=='1') echo 'selected';  ?>>Active</option>
                            <option value="0" <?php if($status=='0') echo 'selected';  ?>>Close</option>
                          </select>
                          </select>
                        </div>
                      </div>
                        
                        
                      <!--                                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                                    <div class="form-group">
                                                        <label for="varchar">Batch Timing <?php echo form_error('time') ?></label>
                                                        <input type="text" class="form-control"  name="time" id="time" placeholder="Batch Time" value="<?php echo $time; ?>" />
                                                    </div>    </div>--> 
                    </div>
                  </div>
                  <!---./end box-body---> 
                </div>
                <!---./end box--->
                
                <div class="form-group col-xs-6 col-sm-6 col-lg-4 no-padding edusecArLangCss">
                  <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                  </div>
                  <div class="col-xs-6"> <a href="<?php echo site_url('employee') ?>" class="btn btn-default">Cancel</a> </div>
                </div>
              </form>
              <?php
              if($emp_id!=''){?>
                    <div class="box box-solid box-danger col-xs-12 col-lg-12 no-padding">
                     <form action="<?php echo base_url('employee/password_update_action'); ?>" method="post" id="frmemppass" autocomplete="off">
                  <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>" />
                         <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> Update Password</h4>
                  </div>
                  <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                 <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                         <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="msd_ID">MSD ID<?php echo form_error('msd_ID') ?></label>
                          <input type="text" readonly class="form-control" name="msd_ID" id="password" placeholder="msd ID" value="<?php echo$msd_ID; ?>" />
                        </div>
                      </div>
                          <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="password">Password<?php echo form_error('password') ?></label>
                          <input type="password" class="form-control" name="password" id="password" placeholder="New Password" value="" required />
                        </div>
                      </div>
                    
                    
                    </div>
                    </div>
                 
                  </div>
                      <div class="form-group col-xs-6 col-sm-6 col-lg-4 no-padding edusecArLangCss">
                  <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                  </div>
                  <div class="col-xs-6"> <a href="<?php echo site_url('employee') ?>" class="btn btn-default">Cancel</a> </div>
                </div>
                         </form>
                  <!---./end box-body---> 
                </div>
              <?php }
              ?>
               
              
              
            </div>
          </div>
        </div>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script> 
        <script>
    $(document).ready(function () {

    $("#dob").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
//    $("#datepicker1").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    $('#admission_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#datepicker2').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $(".timepicker").timepicker({
                    showInputs: false
                });
});                    
    var app = angular.module('myApp', []);

                                app.controller('courseCtrl', function ($scope, $http) {
                //    $http.get("http://testdemosite.ml/hotel/room/room_api").then(function (response) {
				$scope.myData = {};
        $scope.myData.countryId = "<?php echo $department; ?>";
                                    $http.get("<?php echo base_url()?>department/department_api").then(function (response) {
                                        $scope.myData.countries = response.data;
										//$scope.myData = $scope.options[1];
                //        $scope.room = $scope.myData[1].id;

                                    });
                                    $scope.design = {};
									 $scope.design.countryId = "<?php echo $designation; ?>";
                                    $http.get("<?php echo base_url()?>designation/designation_api").then(function (response) {
                                        $scope.design.countries = response.data;
                //        $scope.room = $scope.myData[1].id;

                                    });
                                    $scope.myFunc = function ($scope) {

                                        $http.get("<?php echo base_url()?>designation/designation_api" + $scope.course_id)
                                                .then(function (response) {
                                                    $scope.related = response.data;
                                                });
                                    };
                                });</script> 
        <!-- ******************/master footer ****************** --> 
      </div>
      <!--                </div>
                                 /.col 
                            </div>--> 
      
      <!-- /.row --> 
      </div>
    </section>
    <!-- /.content --> 
  </div>
</div>

<script>
//    $(document).ready(function(e) {
//        $('#frmemp').submit(function(e) {
//            if($('#password').val()!="" && $('#confirm_password').val()=="")
//			{
//				alert('Please fill Confirm Password!');
//				e.stopPropagation();
//				e.preventDefault();
//			}
//			else if($('#password').val()!="" && $('#confirm_password').val()!="")
//			{
//				if($('#password').val()!=$('#confirm_password').val())
//				{
//					alert('Passwords do not match!');
//					e.stopPropagation();
//					e.preventDefault();
//				}
//			}
//        });
//    });
</script>