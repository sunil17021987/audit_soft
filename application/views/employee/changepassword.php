<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Update Password </h1>
    <?php echo validation_errors(); ?>
  </section>
  <div ng-app="myApp" ng-controller="courseCtrl"> 
    <!-- Main content -->
    <section class="content">
      <div class="row">
      
      <!-- /.box-header -->
      <div class="box-body"> 
        <!-- ******************/master header end ****************** -->
        <div class="col-xs-12 col-lg-12">
              <form action="<?php echo base_url('employee/password_update_action_self'); ?>" method="post" id="frmemppass" autocomplete="off">
         <div class="box box-solid box-danger col-xs-12 col-lg-12 no-padding">
                   
                  <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>" />
                         <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> Update Password</h4>
                  </div>
                  <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                 <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                         <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="msd_ID">MSD ID<?php echo form_error('msd_ID') ?></label>
                          <input type="text" readonly class="form-control" name="msd_ID" id="password" placeholder="msd ID" value="<?php echo$msd_ID; ?>" />
                        </div>
                      </div>
                          <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="form-group">
                          <label for="password">Password<?php echo form_error('password') ?></label>
                          <input type="password" class="form-control" name="password" id="password" placeholder="New Password" value="" required />
                        </div>
                      </div>
                    
                    
                    </div>
                    </div>
                 
                  </div>
                      <div class="form-group col-xs-6 col-sm-6 col-lg-4 no-padding edusecArLangCss">
                  <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                  </div>
                  <div class="col-xs-6"> <a href="<?php echo site_url('dashboard') ?>" class="btn btn-default">Cancel</a> </div>
                </div>
                        
                  <!---./end box-body---> 
                </div>
             </form>
        </div>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script> 
        <script>
    $(document).ready(function () {

    $("#dob").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
//    $("#datepicker1").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    $('#admission_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#datepicker2').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $(".timepicker").timepicker({
                    showInputs: false
                });
});                    
    var app = angular.module('myApp', []);

                                app.controller('courseCtrl', function ($scope, $http) {
                //    $http.get("http://testdemosite.ml/hotel/room/room_api").then(function (response) {
				$scope.myData = {};
        $scope.myData.countryId = "<?php echo $department; ?>";
                                    $http.get("<?php echo base_url()?>department/department_api").then(function (response) {
                                        $scope.myData.countries = response.data;
										//$scope.myData = $scope.options[1];
                //        $scope.room = $scope.myData[1].id;

                                    });
                                    $scope.design = {};
									 $scope.design.countryId = "<?php echo $designation; ?>";
                                    $http.get("<?php echo base_url()?>designation/designation_api").then(function (response) {
                                        $scope.design.countries = response.data;
                //        $scope.room = $scope.myData[1].id;

                                    });
                                    $scope.myFunc = function ($scope) {

                                        $http.get("<?php echo base_url()?>designation/designation_api" + $scope.course_id)
                                                .then(function (response) {
                                                    $scope.related = response.data;
                                                });
                                    };
                                });</script> 
        <!-- ******************/master footer ****************** --> 
      </div>
      <!--                </div>
                                 /.col 
                            </div>--> 
      
      <!-- /.row --> 
      </div>
    </section>
    <!-- /.content --> 
  </div>
</div>

<script>
//    $(document).ready(function(e) {
//        $('#frmemp').submit(function(e) {
//            if($('#password').val()!="" && $('#confirm_password').val()=="")
//			{
//				alert('Please fill Confirm Password!');
//				e.stopPropagation();
//				e.preventDefault();
//			}
//			else if($('#password').val()!="" && $('#confirm_password').val()!="")
//			{
//				if($('#password').val()!=$('#confirm_password').val())
//				{
//					alert('Passwords do not match!');
//					e.stopPropagation();
//					e.preventDefault();
//				}
//			}
//        });
//    });
</script>