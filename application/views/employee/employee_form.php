<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Employee <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Emp Name <?php echo form_error('emp_name') ?></label>
            <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Emp Name" value="<?php echo $emp_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Gender <?php echo form_error('gender') ?></label>
            <input type="text" class="form-control" name="gender" id="gender" placeholder="Gender" value="<?php echo $gender; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Dob <?php echo form_error('dob') ?></label>
            <input type="text" class="form-control" name="dob" id="dob" placeholder="Dob" value="<?php echo $dob; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Mobile <?php echo form_error('mobile') ?></label>
            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $mobile; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Category <?php echo form_error('category') ?></label>
            <input type="text" class="form-control" name="category" id="category" placeholder="Category" value="<?php echo $category; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Qualification <?php echo form_error('qualification') ?></label>
            <input type="text" class="form-control" name="qualification" id="qualification" placeholder="Qualification" value="<?php echo $qualification; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Join Date <?php echo form_error('join_date') ?></label>
            <input type="text" class="form-control" name="join_date" id="join_date" placeholder="Join Date" value="<?php echo $join_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Department <?php echo form_error('department') ?></label>
            <input type="text" class="form-control" name="department" id="department" placeholder="Department" value="<?php echo $department; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Designation <?php echo form_error('designation') ?></label>
            <input type="text" class="form-control" name="designation" id="designation" placeholder="Designation" value="<?php echo $designation; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Matial Status <?php echo form_error('matial_status') ?></label>
            <input type="text" class="form-control" name="matial_status" id="matial_status" placeholder="Matial Status" value="<?php echo $matial_status; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nationality <?php echo form_error('nationality') ?></label>
            <input type="text" class="form-control" name="nationality" id="nationality" placeholder="Nationality" value="<?php echo $nationality; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Experience <?php echo form_error('experience') ?></label>
            <input type="text" class="form-control" name="experience" id="experience" placeholder="Experience" value="<?php echo $experience; ?>" />
        </div>
	    <div class="form-group">
            <label for="image">Image <?php echo form_error('image') ?></label>
            <textarea class="form-control" rows="3" name="image" id="image" placeholder="Image"><?php echo $image; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('employee') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>