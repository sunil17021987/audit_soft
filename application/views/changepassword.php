<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Change Password <small></small> </h1>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary"> 
        <!-- /.box-header -->
        <div class="box-body"> 
          <!-- ******************/master header end ****************** -->
          <?php if($this->session->flashdata('msg')) echo $this->session->flashdata('msg');?>
          <form action="<?php echo $action; ?>" method="post">
            <div class="form-group">
              <label for="varchar">Current Password <?php echo form_error('curr_password') ?></label>
              <input type="text" class="form-control" name="curr_password" id="curr_password" placeholder="Current Password" />
            </div>
            <div class="form-group">
              <label for="bigint">New Password <?php echo form_error('new_password') ?></label>
              <input type="text" class="form-control" name="new_password" id="new_password" placeholder="New Password" />
            </div>
            <div class="form-group">
              <label for="bigint">Re-type Password <?php echo form_error('retype_password') ?></label>
              <input type="text" class="form-control" name="retype_password" id="retype_password" placeholder="Re-type Password" />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php //echo site_url('kit_inventory') ?>" class="btn btn-default">Cancel</a>
          </form>
          <!-- ******************/master footer ****************** --> 
        </div>
      </div>
    </div>
  </section>
</div>
