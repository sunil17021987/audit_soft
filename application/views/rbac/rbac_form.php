<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rbac <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Employee Id <?php echo form_error('employee_id') ?></label>
          
              <select id="category" class="form-control" name="employee_id">
                                            <option value="">---  Select Name ---</option>
                                            <?php
                                            if (!empty($employees)) {
                                                foreach ($employees as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->emp_id ?>" <?php if ($employee_id == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
            </select>
            
        </div>
	    <div class="form-group">
            <label for="permission">Permission <?php echo form_error('permission') ?></label>
          <?php
                                                   
          ?>
            <select name="permission[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                        style="width: 100%;">
                      <?php
                                            if (!empty($menubar_data)) {
                                                foreach ($menubar_data as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>" <?php if(in_array($rl->id,$permission)) echo 'selected'; ?>><?php echo $rl->name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
           
                </select>
            
            </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('rbac') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>