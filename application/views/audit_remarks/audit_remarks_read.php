<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Audit_remarks Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Audit Id</b></td><td><?php echo $audit_id; ?></td></tr>
	    <tr><td><b>Agents Id</b></td><td><?php echo $agents_id; ?></td></tr>
	    <tr><td><b>Employee Id TL</b></td><td><?php echo $employee_id_TL; ?></td></tr>
	    <tr><td><b>Employee Id AM</b></td><td><?php echo $employee_id_AM; ?></td></tr>
	    <tr><td><b>Call Date</b></td><td><?php echo $Call_Date; ?></td></tr>
	    <tr><td><b>Time Of Call</b></td><td><?php echo $Time_Of_Call; ?></td></tr>
	    <tr><td><b>Calling Number</b></td><td><?php echo $Calling_Number; ?></td></tr>
	    <tr><td><b>CLI Number</b></td><td><?php echo $CLI_Number; ?></td></tr>
	    <tr><td><b>Call Dur Min</b></td><td><?php echo $Call_Dur_Min; ?></td></tr>
	    <tr><td><b>Call Dur Sec</b></td><td><?php echo $Call_Dur_Sec; ?></td></tr>
	    <tr><td><b>Call Type</b></td><td><?php echo $Call_Type; ?></td></tr>
	    <tr><td><b>Todays Audit Count</b></td><td><?php echo $Todays_Audit_Count; ?></td></tr>
	    <tr><td><b>Category Main Id</b></td><td><?php echo $category_main_id; ?></td></tr>
	    <tr><td><b>Category Sub Id</b></td><td><?php echo $category_sub_id; ?></td></tr>
	    <tr><td><b>Category Main Id Crr</b></td><td><?php echo $category_main_id_crr; ?></td></tr>
	    <tr><td><b>Category Sub Id Crr</b></td><td><?php echo $category_sub_id_crr; ?></td></tr>
	    <tr><td><b>Consumers Concern</b></td><td><?php echo $Consumers_Concern; ?></td></tr>
	    <tr><td><b>R G Y A</b></td><td><?php echo $r_g_y_a; ?></td></tr>
	    <tr><td><b>QME Remarks</b></td><td><?php echo $QME_Remarks; ?></td></tr>
	    <tr><td><b>P S If A</b></td><td><?php echo $p_s_if_a; ?></td></tr>
	    <tr><td><b>C T A P P</b></td><td><?php echo $c_t_a_p_p; ?></td></tr>
	    <tr><td><b>Opening</b></td><td><?php echo $Opening; ?></td></tr>
	    <tr><td><b>ActiveListening</b></td><td><?php echo $ActiveListening; ?></td></tr>
	    <tr><td><b>Probing</b></td><td><?php echo $Probing; ?></td></tr>
	    <tr><td><b>Customer Engagement</b></td><td><?php echo $Customer_Engagement; ?></td></tr>
	    <tr><td><b>Empathy Where Required</b></td><td><?php echo $Empathy_where_required; ?></td></tr>
	    <tr><td><b>Understanding</b></td><td><?php echo $Understanding; ?></td></tr>
	    <tr><td><b>Professionalism</b></td><td><?php echo $Professionalism; ?></td></tr>
	    <tr><td><b>Politeness</b></td><td><?php echo $Politeness; ?></td></tr>
	    <tr><td><b>Hold Procedure</b></td><td><?php echo $Hold_Procedure; ?></td></tr>
	    <tr><td><b>Closing</b></td><td><?php echo $Closing; ?></td></tr>
	    <tr><td><b>Correct Smaller</b></td><td><?php echo $Correct_smaller; ?></td></tr>
	    <tr><td><b>Accurate Complete</b></td><td><?php echo $Accurate_Complete; ?></td></tr>
	    <tr><td><b>Fatal Reason</b></td><td><?php echo $Fatal_Reason; ?></td></tr>
	    <tr><td><b>Date</b></td><td><?php echo $date; ?></td></tr>
	    <tr><td><b>Status</b></td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('audit_remarks') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>