<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent performance List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <!--        <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-4">
                                      
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <div style="margin-top: 4px"  id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                        <?php echo anchor(site_url('agent_performance/create'), 'Create', 'class="btn btn-primary"'); ?>
                        <?php echo anchor(site_url('agent_performance/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                                    </div>
                                </div>-->

                        <?php
                        $attributes = array('class' => 'testform', 'id' => 'myform');
                        echo form_open(base_url() . 'agent_performance/index', $attributes);
                        ?>
                        <div class="row">



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date range:</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">
                                        
                                    </div>
                                    <!-- /.input group -->
                                </div>

                            </div>







                            <div class="col-md-2" style="margin-top: 20px;">
                                <button type="submit" name="search" value="search" class="btn btn-info" style="width: 100%;">
                                    Search</button>
                            </div>

                            <div class="col-md-2" style="margin-top: 20px;">
                             
                                
                                <button type="submit" name="delete" value="delete" class="btn btn-danger" onclick="if (!confirm('Are you sure?')) { return false }"><span>Delete</span></button>
                            </div>
                            <div class="col-md-2" style="margin-top: 20px;">
                                <?php echo anchor(site_url('agent_performance/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                            </div>
                              


                        </div>
<?php echo form_close(); ?>


                        <div class="box-body table-responsive no-padding">


                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th width="80px">No</th>
<!--                                        <th>Agents Id</th>-->
                                        <th>Agents Msd</th>
                                        <th>Date Of</th>
                                        <th>Inbound StaffedDuration</th>
                                        <th>Inbound ReadyDuration</th>
                                        <th>Inbound BreakDuration</th>
                                        <th>Inbound IdleTime</th>
                                        <th>Inbound HoldTime</th>
                                        <th>Inbound NumberOfcallsOffered</th>
                                        <th>Inbound NumberofCallanswered</th>
                                        <th>Inbound AbandonedPer</th>
                                        <th>Inbound AHT</th>
                                        <th>Outbound StaffedDuration</th>
                                        <th>Outbound ReadyDuration</th>
                                        <th>Outbound BreakDuration</th>
                                        <th>Outbound IdleTime</th>
                                        <th>Outbound HoldTime</th>
                                        <th>Outbound NumberOfCallsDial</th>
                                        <th>Outbound NumberofCallsconnected</th>
                                        <th>Outbound ConnectivityPer</th>
                                        <th>Outbound AHT</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($agent_performance_data as $agent_performance) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$start ?></td>
<!--                                            <td><?php echo $agent_performance->agents_id ?></td>-->
                                            <td><?php echo $agent_performance->agents_msd ?></td>
                                            <td><span class="label label-primary pull-right"><?php echo date('d-M-Y', strtotime($agent_performance->date_of)) ?></span></td>
                                            <td><?php echo $agent_performance->Inbound_StaffedDuration ?></td>
                                            <td><?php echo $agent_performance->Inbound_ReadyDuration ?></td>
                                            <td><?php echo $agent_performance->Inbound_BreakDuration ?></td>
                                            <td><?php echo $agent_performance->Inbound_IdleTime ?></td>
                                            <td><?php echo $agent_performance->Inbound_HoldTime ?></td>
                                            <td><?php echo $agent_performance->Inbound_NumberOfcallsOffered ?></td>
                                            <td><?php echo $agent_performance->Inbound_NumberofCallanswered ?></td>
                                            <td><?php echo $agent_performance->Inbound_AbandonedPer ?></td>
                                            <td><?php echo $agent_performance->Inbound_AHT ?></td>
                                            <td><?php echo $agent_performance->Outbound_StaffedDuration ?></td>
                                            <td><?php echo $agent_performance->Outbound_ReadyDuration ?></td>
                                            <td><?php echo $agent_performance->Outbound_BreakDuration ?></td>
                                            <td><?php echo $agent_performance->Outbound_IdleTime ?></td>
                                            <td><?php echo $agent_performance->Outbound_HoldTime ?></td>
                                            <td><?php echo $agent_performance->Outbound_NumberOfCallsDial ?></td>
                                            <td><?php echo $agent_performance->Outbound_NumberofCallsconnected ?></td>
                                            <td><?php echo $agent_performance->Outbound_ConnectivityPer ?></td>
                                            <td><?php echo $agent_performance->Outbound_AHT ?></td>
                                            <td style="text-align:center" width="200px">
                                                <?php
                                                echo anchor(site_url('agent_performance/read/' . $agent_performance->id), '<i class="fa fa-eye"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('agent_performance/update/' . $agent_performance->id), '<i class="fa fa-pencil-square-o"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('agent_performance/delete/' . $agent_performance->id), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                                    $(document).ready(function () {
                                        $("#mytable").dataTable();
                                    });
                                    $(".testform1").click(function (event) {
                                     var choice = this.getAttribute('name');
                                     alert(choice);
                                        if (!confirm('Are you sure that you want to submit the form'))
                                            event.preventDefault();
                                    });

                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>