<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent_performance <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Agents Id <?php echo form_error('agents_id') ?></label>
            <input type="text" class="form-control" name="agents_id" id="agents_id" placeholder="Agents Id" value="<?php echo $agents_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Agents Msd <?php echo form_error('agents_msd') ?></label>
            <input type="text" class="form-control" name="agents_msd" id="agents_msd" placeholder="Agents Msd" value="<?php echo $agents_msd; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Date Of <?php echo form_error('date_of') ?></label>
            <input type="text" class="form-control" name="date_of" id="date_of" placeholder="Date Of" value="<?php echo $date_of; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound StaffedDuration <?php echo form_error('Inbound_StaffedDuration') ?></label>
            <input type="text" class="form-control" name="Inbound_StaffedDuration" id="Inbound_StaffedDuration" placeholder="Inbound StaffedDuration" value="<?php echo $Inbound_StaffedDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound ReadyDuration <?php echo form_error('Inbound_ReadyDuration') ?></label>
            <input type="text" class="form-control" name="Inbound_ReadyDuration" id="Inbound_ReadyDuration" placeholder="Inbound ReadyDuration" value="<?php echo $Inbound_ReadyDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound BreakDuration <?php echo form_error('Inbound_BreakDuration') ?></label>
            <input type="text" class="form-control" name="Inbound_BreakDuration" id="Inbound_BreakDuration" placeholder="Inbound BreakDuration" value="<?php echo $Inbound_BreakDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound IdleTime <?php echo form_error('Inbound_IdleTime') ?></label>
            <input type="text" class="form-control" name="Inbound_IdleTime" id="Inbound_IdleTime" placeholder="Inbound IdleTime" value="<?php echo $Inbound_IdleTime; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound HoldTime <?php echo form_error('Inbound_HoldTime') ?></label>
            <input type="text" class="form-control" name="Inbound_HoldTime" id="Inbound_HoldTime" placeholder="Inbound HoldTime" value="<?php echo $Inbound_HoldTime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Inbound NumberOfcallsOffered <?php echo form_error('Inbound_NumberOfcallsOffered') ?></label>
            <input type="text" class="form-control" name="Inbound_NumberOfcallsOffered" id="Inbound_NumberOfcallsOffered" placeholder="Inbound NumberOfcallsOffered" value="<?php echo $Inbound_NumberOfcallsOffered; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Inbound NumberofCallanswered <?php echo form_error('Inbound_NumberofCallanswered') ?></label>
            <input type="text" class="form-control" name="Inbound_NumberofCallanswered" id="Inbound_NumberofCallanswered" placeholder="Inbound NumberofCallanswered" value="<?php echo $Inbound_NumberofCallanswered; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Inbound AbandonedPer <?php echo form_error('Inbound_AbandonedPer') ?></label>
            <input type="text" class="form-control" name="Inbound_AbandonedPer" id="Inbound_AbandonedPer" placeholder="Inbound AbandonedPer" value="<?php echo $Inbound_AbandonedPer; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Inbound AHT <?php echo form_error('Inbound_AHT') ?></label>
            <input type="text" class="form-control" name="Inbound_AHT" id="Inbound_AHT" placeholder="Inbound AHT" value="<?php echo $Inbound_AHT; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound StaffedDuration <?php echo form_error('Outbound_StaffedDuration') ?></label>
            <input type="text" class="form-control" name="Outbound_StaffedDuration" id="Outbound_StaffedDuration" placeholder="Outbound StaffedDuration" value="<?php echo $Outbound_StaffedDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound ReadyDuration <?php echo form_error('Outbound_ReadyDuration') ?></label>
            <input type="text" class="form-control" name="Outbound_ReadyDuration" id="Outbound_ReadyDuration" placeholder="Outbound ReadyDuration" value="<?php echo $Outbound_ReadyDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound BreakDuration <?php echo form_error('Outbound_BreakDuration') ?></label>
            <input type="text" class="form-control" name="Outbound_BreakDuration" id="Outbound_BreakDuration" placeholder="Outbound BreakDuration" value="<?php echo $Outbound_BreakDuration; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound IdleTime <?php echo form_error('Outbound_IdleTime') ?></label>
            <input type="text" class="form-control" name="Outbound_IdleTime" id="Outbound_IdleTime" placeholder="Outbound IdleTime" value="<?php echo $Outbound_IdleTime; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound HoldTime <?php echo form_error('Outbound_HoldTime') ?></label>
            <input type="text" class="form-control" name="Outbound_HoldTime" id="Outbound_HoldTime" placeholder="Outbound HoldTime" value="<?php echo $Outbound_HoldTime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Outbound NumberOfCallsDial <?php echo form_error('Outbound_NumberOfCallsDial') ?></label>
            <input type="text" class="form-control" name="Outbound_NumberOfCallsDial" id="Outbound_NumberOfCallsDial" placeholder="Outbound NumberOfCallsDial" value="<?php echo $Outbound_NumberOfCallsDial; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Outbound NumberofCallsconnected <?php echo form_error('Outbound_NumberofCallsconnected') ?></label>
            <input type="text" class="form-control" name="Outbound_NumberofCallsconnected" id="Outbound_NumberofCallsconnected" placeholder="Outbound NumberofCallsconnected" value="<?php echo $Outbound_NumberofCallsconnected; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Outbound ConnectivityPer <?php echo form_error('Outbound_ConnectivityPer') ?></label>
            <input type="text" class="form-control" name="Outbound_ConnectivityPer" id="Outbound_ConnectivityPer" placeholder="Outbound ConnectivityPer" value="<?php echo $Outbound_ConnectivityPer; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Outbound AHT <?php echo form_error('Outbound_AHT') ?></label>
            <input type="text" class="form-control" name="Outbound_AHT" id="Outbound_AHT" placeholder="Outbound AHT" value="<?php echo $Outbound_AHT; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('agent_performance') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>