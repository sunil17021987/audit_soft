
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard OutBound

        </h1>

    </section>
    <?php
    $login_type = $this->session->userdata('type');
    if ($login_type != '') {
        ?>
        <section class="content">
            <?php echo form_open(base_url() . 'Dashboard/dashboard_outbound'); ?>
            <div class="row">



                <div class="col-md-8">
                    <div class="form-group">
                        <label>Date range:</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>

                <!--                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <select id="category" class="form-control" name="audit_by">
                                            <option value="">---  Select Name ---</option>
                <?php
                if (!empty($employees)) {
                    foreach ($employees as $rl) {
                        ?>
                                                                                                                                            <option value="<?php echo $rl->emp_id ?>" <?php if ($audit_by == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                        <?php
                    }
                }
                ?>
                                        </select>  
                
                                    </div>
                                </div>-->






                <div class="col-md-2" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-info" style="width: 100%;">
                        Search</button>
                </div>

            </div>
            <?php echo form_close(); ?>
         
           <div class="row">
               <div class="col-xs-6">
                   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Dashboard</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                 
                  <th>Date</th>
                  <th>CONNECTED</th>
                  <th>%</th>
                  <th>NOT CONNECTED</th>
                  <th>%</th>
                 
                </tr>
                
                <?php
                if($audits){
                    
                    foreach ($audits as $audit) {?>
                                        <tr>
                  <td><?php echo $audit->createdDate;?></td>
                  <td><?php echo $audit->connected;?></td>
                  <td><span class="badge bg-red"><?php echo $audit->connectedPer;?></span></td>
              
                   <td><?php echo $audit->notconnected;?></td>
                 
                  <td><span class="badge bg-red"><?php echo $audit->notConnectedPer;?></span></td>
                </tr>
                    <?php }
                    
                }else{
                    
                }
                ?>
                

              
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
                   
               </div>
               
           </div>    

            
            
            
            
             <div class="row">
               <div class="col-xs-12">
                   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Area Wise Segregation Of Outbound Positive Calling</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                    <tr>
                 
                  <th>Block</th>
                  <th>Patient in Hospital</th>
                  <th>Patient in Home Isolation</th>
                  <th>Patient in Quarantine Center</th>
                  <th>Need Doctor Assistance</th>
                  <th>Need Medicines</th>
                  <th>Patient has Recovered</th>
                  <th>Mobile Switched off</th>
                  <th>Wrong number</th>
                  <th>Out of Network</th>
                  <th>Total</th>
                 
                </tr>
                
                <?php
                if($blockwises){
                    
                    foreach ($blockwises as $blockwise) {?>
                                        <tr>
                  <td><?php echo $blockwise->block_name;?></td>
                  <td><?php echo $blockwise->PatientInHospital;?></td>
                  <td><?php echo $blockwise->PatientInHomeIsolation;?></td>
                  <td><?php echo $blockwise->PatientInQuarantineCenter;?></td>
                  <td><?php echo $blockwise->NeedDoctorAssistance;?></td>
                  <td><?php echo $blockwise->NeedMedicines;?></td>
                  <td><?php echo $blockwise->PatientHasRecovered;?></td>
                  <td><?php echo $blockwise->MobileSwitchedOff;?></td>
                  <td><?php echo $blockwise->WrongNumber;?></td>
                  <td><?php echo $blockwise->OutOfNetwork;?></td>
                  <td><?php echo $blockwise->PatientInHospital+$blockwise->PatientInHomeIsolation+$blockwise->PatientInQuarantineCenter+$blockwise->NeedDoctorAssistance+$blockwise->NeedMedicines+$blockwise->PatientHasRecovered+$blockwise->MobileSwitchedOff+$blockwise->WrongNumber+$blockwise->OutOfNetwork;?></td>
                </tr>
                    <?php }
                    
                }else{
                    
                }
                ?>
                

              
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
                   
               </div>
               
           </div>   
        </section>

        <?php
    } else {
        echo"some thing wrong!";
    }
    ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   