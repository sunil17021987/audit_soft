<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Anticipatory <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-8">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4 text-center">
                                <div style="margin-top: 4px"  id="message">
                                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">

                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th width="80px">Issue Date</th>

                                        <th>Mobile No</th>
                                        <th>Issue Start</th>
                                        <th>Issue End</th>
                                        <th>Issue Type</th>
                                        <th>Issue Remarks</th>
                                        <th>Approved By</th>
                                        <th>Is Approved</th>
                                        <th>Approved Remarks</th>
                                        <th>Approved Date Time</th>
                                        <th>Issue Date Time</th>
<!--                                        <th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($anticipatory_data as $anticipatory) {
                                        ?>
                                        <tr>
                                            <td><?php echo $anticipatory->issue_date_time ?></td>

                                            <td><?php echo $anticipatory->mobile_no ?></td>
                                            <td><?php echo $anticipatory->issue_start ?></td>
                                            <td><?php echo $anticipatory->issue_end ?></td>
                                            <td><?php echo $anticipatory->issue_type ?></td>
                                            <td><?php echo $anticipatory->issue_remarks ?></td>
                                            <td><?php echo $anticipatory->approved_by ?></td>
                                            <td><?php echo $anticipatory->is_approved ?></td>
                                            <td><?php echo $anticipatory->approved_remarks ?></td>
                                            <td><?php echo $anticipatory->approved_date_time ?></td>
                                            <td><?php echo $anticipatory->issue_date_time ?></td>
<!--                                            <td style="text-align:center" width="200px">
                                                <?php
                                                echo anchor(site_url('anticipatory/read/' . $anticipatory->id), '<i class="fa fa-eye"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('anticipatory/update/' . $anticipatory->id), '<i class="fa fa-pencil-square-o"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('anticipatory/delete/' . $anticipatory->id), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                                ?>
                                            </td>-->
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
            <!-------------------------Col 2 start-------------------->
            <div class="col-xs-4">
                <div class="box box-danger">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <center><div id="result"></div></center>
                        <form action="<?php echo $action; ?>" id="antiForm" method="post">

                            <div class="form-group">
                                <label for="varchar">Add called Mobile Numbers with comma's <?php echo form_error('mobile_no') ?></label>
                                <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile No" value="<?php echo $mobile_no; ?>" required="" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="datetime">Issue Start Date: <?php echo form_error('issue_start') ?></label>
                                    <input type="text" class="form-control" name="issue_start" id="Call_Date" placeholder="Issue Start" value="<?php echo $issue_start; ?>" required="" />
                                </div>
                                <div class="col-md-6">

                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <label>Time:</label>

                                            <div class="input-group">
                                                <input type="text" name="start_Time_Of_Call" id="Time_Of_Call" class="form-control input-sm timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                <label for="datetime">Issue End Date <?php echo form_error('issue_end') ?></label>
                                <input type="text" class="form-control" name="issue_end" id="Call_Date_end" placeholder="Issue End" value="<?php echo $issue_end; ?>" required="" />
                                </div>
                                                                <div class="col-md-6">

                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <label>Time:</label>

                                            <div class="input-group">
                                                <input type="text" name="end_Time_Of_Call" id="Time_Of_Call" class="form-control input-sm timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="int">Issue Type <?php echo form_error('issue_type') ?></label>
                               
                                <select name="issue_type" class=" form-control select1 input-sm" id="issue_type" required="">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($anticipatory_status)) {
                                                                foreach ($anticipatory_status as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->id == $issue_type) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->id ?>"><?php echo $downloadtype->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                            </div>
                            
                             <div class="form-group">
                                <label for="varchar">Extension No. <?php echo form_error('extensionNo') ?></label>
                                <input type="text" class="form-control" name="extensionNo" id="extensionNo" placeholder="extension No" value="<?php echo $extensionNo; ?>" required="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Issue Remarks <?php echo form_error('issue_remarks') ?></label>
                                <input type="text" class="form-control" name="issue_remarks" id="issue_remarks" placeholder="Issue Remarks" value="<?php echo $issue_remarks; ?>" required="" />
                            </div>

                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('DashboardAgent/anticipatory') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
<script src="<?php echo base_url() ?>assets/js/3.4.1_jquery.min.js"></script>

<script>
    $(document).ready(function () {
        //  alert('ok');
        //  $("#mytable").dataTable();



    });

    $("#antiForm").submit(function (e) {
        e.preventDefault();
        dataString = $("#antiForm").serialize();

        $.ajax({

            type: "POST",

            url: "<?php echo site_url('DashboardAgent/anticipatory_create_action'); ?>",
            data: dataString,
            datatype: 'json',
            success: function (data) {
                var obj = jQuery.parseJSON(data);
              //  alert(obj.anticipatory_data.id);
                if (obj.status === '1') {
                    $("tbody").append("<tr><td>" + obj.anticipatory_data.issue_date_time + "</td><td>" + obj.anticipatory_data.mobile_no + "</td><td>" + obj.anticipatory_data.issue_start + "</td><td>" + obj.anticipatory_data.issue_end + "</td><td>" + obj.anticipatory_data.issue_type + "</td><td>" + obj.anticipatory_data.issue_remarks + "</td><td>" + obj.anticipatory_data.approved_by + "</td><td>" + obj.anticipatory_data.is_approved + "</td><td>" + obj.anticipatory_data.approved_remarks + "</td><td>" + obj.anticipatory_data.approved_date_time + "</td><td>" + obj.anticipatory_data.issue_date_time + "</td></tr>");
                    $("#result").html('Successfully Add Record!');
                    $("#result").addClass("alert alert-success");
                    location.reload();
                } else {
                    $("#result").html('Error');
                    $("#result").addClass("alert alert-danger");
                    location.reload();
                }

                //$("#antiForm")[0].reset();
            }

        });

        return false;  //stop the actual form post !important!

    });
</script>