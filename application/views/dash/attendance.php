<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent Attendance List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
<!--        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
              
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('agent_performance/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('agent_performance/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>-->

 <?php echo form_open(base_url() . 'DashboardAgent/attendance'); ?>
        <div class="row">



            <div class="col-md-6">
                <div class="form-group">
                    <label>Date range:</label>

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>

            </div>







            <div class="col-md-2" style="margin-top: 20px;">
                <button type="submit" class="btn btn-info" style="width: 100%;">
                    Search</button>
            </div>
        

        </div>
        <?php echo form_close(); ?>

  <div class="row">
      <div class="col-md-12">
     <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Employee Name</th>
		    <th>TL Name</th>
		    <th>Agent Status</th>
		    <th>Batch</th>
		    <th>AM Name</th>
		    <th>Date Of</th>
		    <th>Roster Shift</th>
		    <th>Login</th>
		    <th>Attendance</th>
                     <th>First Login</th>
                     <th>Shift Adherence</th>
                    
		  
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($attendance_format_data as $attendance_format)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $attendance_format->employeeName ?></td>
		    <td><?php echo $attendance_format->TLName ?></td>
		    <td><?php echo $attendance_format->agent_Status ?></td>
		    <td><?php echo $attendance_format->batch ?></td>
		    <td><?php echo $attendance_format->AMName ?></td>
                    <td><?php echo date('d-M-Y', strtotime($attendance_format->Date_of)) ?></td>
		    <td><?php echo $attendance_format->RosterShift ?></td>
		    <td><?php echo $attendance_format->Login ?></td>
		    <td><?php echo $attendance_format->Attendance ?></td>
                     <td><?php echo $attendance_format->FirstLogin ?></td>
                     <td><?php echo $attendance_format->ShiftAdherence?></td>
		  
		   
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
      </div>

    
  </div>  








        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
              //  $("#mytable").dataTable();
              $(document).ajaxStart(function(){
    $("#wait").css("display", "block");
  });
 
              
                          var category_main_id = '123';

            if (category_main_id) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/DashboardAgent/agentPerformanceByCRM',
                    data: {category_main_id: category_main_id},
                    success: function (html) {
                       // alert(html);
                      //  $('#crmdata').html(html);
                        $("#crmdata").last().append(html);
                         $(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
  });
                    }
                });


            }
              
              
            });
            
            
             $("#refreshdata").click(function () {
          location.reload();
               });
        </script>
    <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>