<style>
    .modal-header {
        background-color: #3c8dbc;
        color: #FFF;
    }
    .modal-body {

        color: #000;
    }
</style>
<div class="modal fade" id="modal-warning_feedbackpending">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pending List</h4>
            </div>
            <div class="modal-body">
             <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                         <th>MSD ID</th>
                                          <th>Agent</th>
                                        <th>TL Name</th>
                                        <th>AM Name</th>
                                        <th>Audit By</th>
                                        <th>Call Date</th>
                                        <th>Audit Date</th>
                                        <th>Score</th>
                                    </tr>
                                    <?php
                                    if (!empty($allPengings)) {
                                        foreach ($allPengings as $key=>$rl) {
                                            ?>
                                            <tr>
                                                <td><?php echo++$key?></td>
                                                <td><?php echo$rl->emp_id?></td>
                                                <td><?php echo$rl->name?></td>
                                                <td><?php echo$rl->employee_id_TL?></td>
                                                <td><?php echo$rl->employee_id_AM?></td>
                                                <td><?php echo$rl->auditByName?></td>
                                                <td><?php echo date('d-M-Y', strtotime($rl->Call_Date)) ?></td>
                                                 <td><?php echo date('d-M-Y', strtotime($rl->date)) ?></td>
                                               <td><?php echo$rl->score?></td>
                                            </tr> 
                                            <?php
                                        }
                                    }
                                    ?>




                                </tbody></table>
	 
             </div>
          
            <div class="modal-footer">
               
                <button type="button" id="closemodel" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
<!--                <button type="button" class="btn btn-outline" id="add_weapon">Save</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>