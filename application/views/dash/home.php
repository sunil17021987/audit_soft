<style>
    table.redTable {
        border: 2px solid #0B6FA4;
        background-color: #EEE7DB;
        width: 100%;
        text-align: center;
        border-collapse: collapse;
    }
    table.redTable td, table.redTable th {
        border: 1px solid #AAAAAA;
        padding: 3px 2px;
    }
    table.redTable tbody td {
        font-size: 14px;
    }
    table.redTable tr:nth-child(even) {
        background: #FFF8F9;
    }
    table.redTable thead {
        background: #0B6FA4;
    }
    table.redTable thead th {
        font-size: 12px;
        font-weight: bold;
        color: #FFFFFF;
        text-align: center;
        border-left: 2px solid #0B6FA4;
    }
    table.redTable thead th:first-child {
        border-left: none;
    }

    table.redTable tfoot {
        font-size: 13px;
        font-weight: bold;
        color: #FFFFFF;
        background: #0B6FA4;
    }
    table.redTable tfoot td {
        font-size: 13px;
    }
    table.redTable tfoot .links {
        text-align: right;
    }
    table.redTable tfoot .links a{
        display: inline-block;
        background: #FFFFFF;
        color: #0B6FA4;
        padding: 2px 8px;
        border-radius: 5px;
    }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard

        </h1>

    </section>
    <?php
    $login_type = $this->session->userdata('type');
    if ($login_type != '') {
        ?>
        <section class="content">
            <?php echo form_open(base_url() . 'Dashboard/index'); ?>
            <div class="row">



                <div class="col-md-8">
                    <div class="form-group">
                        <label>Date range:</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="bydaterange" value="<?php echo$bydaterange; ?>"  class="form-control pull-right" id="reservation">
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>

                <!--                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <select id="category" class="form-control" name="audit_by">
                                            <option value="">---  Select Name ---</option>
                <?php
                if (!empty($employees)) {
                    foreach ($employees as $rl) {
                        ?>
                                                                                                                                            <option value="<?php echo $rl->emp_id ?>" <?php if ($audit_by == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                        <?php
                    }
                }
                ?>
                                        </select>  
                
                                    </div>
                                </div>-->






                <div class="col-md-2" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-info" style="width: 100%;">
                        Search</button>
                </div>

            </div>
            <?php echo form_close(); ?>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo $audits; ?></h3>

                            <p>Total Audits</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelTotalAudits(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($callModelTotalAudits); ?>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php echo $fatals; ?><sup style="font-size: 20px"></sup></h3>

                            <p>Total Fatals</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>

                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelTotalFatals(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($callModelTotalFatals); ?>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo ($score->t_score != 0) ? number_format((($score->t_score / $score->score_able) * 100),2): ''; ?></h3>

                            <p>Quality score</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo$feedbackpending; ?></h3>

                            <p>feedback Pending | Close</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModel(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($addmodel); ?>

                    </div>
                </div>
                <!-- ./col -->
            </div>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo$fnotackbyCSA; ?></h3>

                            <p>FeedBack Not Acknologed By CSA</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelNotAcknologed(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($modelnotAcknologed); ?>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo$notAccepted; ?></h3>

                            <p>FeedBack Not Accepted By CSA</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelNotAccepted(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($modelnotAccepted); ?>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo$noActin; ?></h3>

                            <p>No Action</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelTotalNoActin(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($callModelTotalNoActin); ?>

                    </div>
                </div>


                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo$agents->totalactive . '|' . $agents->totalclose; ?></h3>

                            <p>Active/Close</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" >Total Agents  </a>
                        <?php //$this->load->view($callModelTotalNoActin); ?>

                    </div>
                </div>

            </div>
             <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo $hundredperscre; ?></h3>

                            <p>100% Score List</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="JavaScript:Void(0);" class="small-box-footer" id="1" onclick="callModelTotalAudits_hundred(this)" >More info <i class="fa fa-arrow-circle-right"></i> </a>
                        <?php $this->load->view($totalAuditshundred); ?>

                    </div>
                </div>
             </div>


            <!--            <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">AM Audits</h3>
                                    </div>
                                     /.box-header 
                                    <div class="box-body no-padding">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>AM Name</th>
            
                                                    <th style="width: 40px">Total</th>
                                                    <th style="width: 40px">Fatal</th>
                                                    <th style="width: 40px">Score</th>
                                                </tr>
            <?php
            if (!empty($auditsAm)) {
                foreach ($auditsAm as $key => $rl) {
                    ?>
                                                                                                                <tr>
                                                                                                                    <td><?php echo ++$key ?></td>
                                                                                                                    <td><?php echo$rl->emp_name ?></td>
                                                                    
                                                                                                                    <td><span class="badge bg-green"><?php echo$rl->AM_total ?></span></td>
                                                                                                                    <td><span class="badge bg-red"><?php echo$rl->fatal ?></span></td>
                                                                                                                    <td><span class="badge bg-aqua"><?php echo number_format($rl->score, 0) ?></span></td>
                                                                                                                </tr> 
                    <?php
                }
            }
            ?>
            
            
            
            
                                            </tbody></table>
                                    </div>
                                     /.box-body 
                                </div>
            
                            </div>
            
                            <div class="col-lg-6 col-xs-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">TL Audits</h3>
                                    </div>
                                     /.box-header 
                                    <div class="box-body no-padding">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>TL Name</th>
            
                                                    <th style="width: 40px">Total</th>
                                                    <th style="width: 40px">Fatal</th>
                                                    <th style="width: 40px">Score</th>
                                                </tr>
            <?php
            if (!empty($auditsTl)) {
                foreach ($auditsTl as $key => $rl) {
                    ?>
                                                                                                                <tr>
                                                                                                                    <td><?php echo ++$key ?></td>
                                                                                                                    <td><?php echo$rl->emp_name ?></td>
                                                                    
                                                                                                                    <td><span class="badge bg-green"><?php echo$rl->AM_total ?></span></td>
                                                                                                                    <td><span class="badge bg-red"><?php echo$rl->fatal ?></span></td>
                                                                                                                    <td><span class="badge bg-aqua"><?php echo number_format($rl->score, 0) ?></span></td>
                                                                                                                </tr> 
                    <?php
                }
            }
            ?>
            
            
            
            
                                            </tbody></table>
                                    </div>
                                     /.box-body 
                                </div>
            
                            </div>
            
            
            
                        </div>-->

            <div class="row">
                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Week wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Week</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            $count = 0;
                            if (!empty($week_wises)) {
                                foreach ($week_wises as $week_wise) {
                                    ?>
                                    <tr>
                                        <td>Month:<?php echo$week_wise->monthof; ?> Week <?php echo$week_wise->week; ?></td><td><?php echo$week_wise->total ?></td><td><?php echo$week_wise->fatal ?></td><td><?php echo ceil((($week_wise->fatal / $week_wise->total) * 100)) . '%'; ?></td><td><?php echo $week_wise->av . '%' ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $score_able = 0;
                                    $count = count($week_wises);
                                    foreach ($week_wises as $amwisee) {
                                        $amtotal += $amwisee->total;
                                        $amtotalfc += $amwisee->fatal;
                                        $amtotalfp += (($amwisee->fatal / $amwisee->total) * 100);
                                        $amtotalfq += $amwisee->t_score;
                                        $score_able += $amwisee->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfc != 0) ? ceil(($amtotalfc / $amtotal) * 100) . '%' : '0%'; ?></td>
                                <td><?php echo ($amtotalfq != 0) ? number_format((($amtotalfq / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>

                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">AM Wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>AM</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($amwises)) {
                                foreach ($amwises as $amwise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$amwise->emp_name ?></td><td><?php echo$amwise->total ?></td><td><?php echo$amwise->fatal ?></td><td><?php echo number_format((($amwise->fatal / $amwise->total) * 100), 0) . '%'; ?></td><td><?php echo $amwise->av . '%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($amwises);
                                    foreach ($amwises as $amwisee) {
                                        $amtotal += $amwisee->total;
                                        $amtotalfc += $amwisee->fatal;
                                        $amtotalfp += number_format((($amwisee->fatal / $amwisee->total) * 100), 0);
                                        $t_score += $amwisee->t_score;
                                        $score_able += $amwisee->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                        </tr>
                    </table>


                </div>


                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Shift wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Shift</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($shiftwises)) {
                                foreach ($shiftwises as $shiftwise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$shiftwise->shift_name ?></td><td><?php echo$shiftwise->total ?></td><td><?php echo$shiftwise->fatal ?></td><td><?php echo number_format((($shiftwise->fatal / $shiftwise->total) * 100), 0) . '%'; ?></td><td><?php echo $shiftwise->av.'%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($shiftwises);
                                    foreach ($shiftwises as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>

                    </table>


                </div>


            </div>   

            <br>
            <div class="row">
                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Category</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($count_Category_wise)) {
                                foreach ($count_Category_wise as $date_wise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$date_wise->category_main_name; ?></td><td><?php echo$date_wise->total ?></td><td><?php echo$date_wise->fatal ?></td><td><?php echo number_format((($date_wise->fatal / $date_wise->total) * 100), 0) . '%'; ?></td><td><?php echo $date_wise->av . '%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfqcat = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($count_Category_wise);
                                    foreach ($count_Category_wise as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $amtotalfqcat += $amwisees->av;
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : ''; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>


                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">TL Wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>TL</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($tlwises)) {
                                foreach ($tlwises as $tlwise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$tlwise->emp_name ?></td><td><?php echo$tlwise->total ?></td><td><?php echo$tlwise->fatal ?></td><td><?php echo number_format((($tlwise->fatal / $tlwise->total) * 100), 0) . '%'; ?></td><td><?php echo number_format($tlwise->score, 2) . '%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($tlwises);
                                    foreach ($tlwises as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>


                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Tenure wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Tenure</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>> 90 Days </td><td><?php echo$count_Tenure_wise_90->total ?></td><td><?php echo$count_Tenure_wise_90->fatal ?></td><td><?php echo ($count_Tenure_wise_90->fatal != '') ? number_format((($count_Tenure_wise_90->fatal / $count_Tenure_wise_90->total) * 100), 0) . '%' : '0'; ?></td><td><?php echo $count_Tenure_wise_90->av . '%'; ?></td>
                            </tr>
                            <tr>
                                <td>61-90 Days</td><td><?php echo$count_Tenure_wise_60_90->total ?></td><td><?php echo$count_Tenure_wise_60_90->fatal ?></td><td><?php echo ($count_Tenure_wise_60_90->fatal != '') ? number_format((($count_Tenure_wise_60_90->fatal / $count_Tenure_wise_60_90->total) * 100), 0) . '%' : '0'; ?></td><td><?php echo $count_Tenure_wise_60_90->av . '%'; ?></td>
                            </tr>

                            <tr>
                                <td>31-60 Days</td><td><?php echo$count_Tenure_wise_30_60->total ?></td><td><?php echo$count_Tenure_wise_30_60->fatal ?></td><td><?php echo ($count_Tenure_wise_30_60->fatal != '') ? number_format((($count_Tenure_wise_30_60->fatal / $count_Tenure_wise_30_60->total) * 100), 0) . '%' : '0'; ?></td><td><?php echo $count_Tenure_wise_30_60->av . '%'; ?></td>
                            </tr>
                            <tr>
                                <td>0-30 Days</td>  <td><?php echo$count_Tenure_wise_0_30->total ?></td><td><?php echo$count_Tenure_wise_0_30->fatal ?></td><td><?php echo$count_Tenure_wise_0_30->fatal . '%'; ?></td><td><?php echo $count_Tenure_wise_0_30->av . '%' ?></td>
                            </tr>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able=0;
                                    $count = count($tlwises);

                                    echo $amtotal = $count_Tenure_wise_60_90->total + $count_Tenure_wise_90->total + $count_Tenure_wise_30_60->total + $count_Tenure_wise_0_30->total;
                                    $amtotalfc = $count_Tenure_wise_60_90->fatal + $count_Tenure_wise_90->fatal + $count_Tenure_wise_30_60->fatal + $count_Tenure_wise_0_30->fatal;
                                    $t_score = $count_Tenure_wise_60_90->t_score + $count_Tenure_wise_90->t_score + $count_Tenure_wise_30_60->t_score + $count_Tenure_wise_0_30->t_score;
                                     $score_able = $count_Tenure_wise_60_90->score_able + $count_Tenure_wise_90->score_able + $count_Tenure_wise_30_60->score_able + $count_Tenure_wise_0_30->score_able;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfc != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score /$score_able)*100),2) . '%' : '';?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>


            </div> 
            <br> 
            <div class="row">
                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Audit date wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Audit date</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($date_wises)) {
                                foreach ($date_wises as $date_wise) {
                                    ?>
                                    <tr>
                                        <td><?php echo date('d-M', strtotime($date_wise->day)) ?></td><td><?php echo$date_wise->total ?></td><td><?php echo$date_wise->fatal ?></td><td><?php echo number_format((($date_wise->fatal / $date_wise->total) * 100), 0) . '%'; ?></td><td><?php echo number_format($date_wise->score, 2) . '%' ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($date_wises);
                                    foreach ($date_wises as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>

                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Auditor Wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Auditor</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($count_Auditor_Wise)) {
                                foreach ($count_Auditor_Wise as $tlwise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$tlwise->emp_name ?></td><td><?php echo$tlwise->total ?></td><td><?php echo$tlwise->fatal ?></td><td><?php echo number_format((($tlwise->fatal / $tlwise->total) * 100), 0) . '%'; ?></td><td><?php echo number_format($tlwise->score, 2) . '%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($count_Auditor_Wise);
                                    foreach ($count_Auditor_Wise as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>


                <div class="col-lg-4 col-xs-6">

                    <table class="redTable">
                        <thead>
                            <tr>
                                <th colspan="5">Call type wise</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>Call type</th>
                                <th>Audit Count</th>
                                <th>Fatal count</th>
                                <th>Fatal%</th>
                                <th>Quality%</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($count_Call_type_wise)) {
                                foreach ($count_Call_type_wise as $tlwise) {
                                    ?>
                                    <tr>
                                        <td><?php echo$tlwise->name ?></td><td><?php echo$tlwise->total ?></td><td><?php echo$tlwise->fatal ?></td><td><?php echo number_format((($tlwise->fatal / $tlwise->total) * 100), 0) . '%'; ?></td><td><?php echo $tlwise->av . '%'; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>
                                    <?php
                                    $amtotal = 0;
                                    $amtotalfc = 0;
                                    $amtotalfp = 0;
                                    $amtotalfq = 0;
                                    $t_score = 0;
                                    $score_able = 0;
                                    $count = count($count_Call_type_wise);
                                    foreach ($count_Call_type_wise as $amwisees) {
                                        $amtotal += $amwisees->total;
                                        $amtotalfc += $amwisees->fatal;
                                        $amtotalfp += number_format((($amwisees->fatal / $amwisees->total) * 100), 0);
                                        $t_score += $amwisees->t_score;
                                        $score_able += $amwisees->score_able;
                                    }
                                    echo $amtotal;
                                    ?>
                                </td>
                                <td><?php echo$amtotalfc; ?></td>
                                <td><?php echo ($amtotalfp != 0) ? number_format(($amtotalfc / $amtotal) * 100, 0) . '%' : '0%'; ?></td>
                                <td><?php echo ($t_score != 0) ? number_format((($t_score / $score_able) * 100),2) . '%' : '0%'; ?></td>
                            </tr>
                        </tfoot>
                    </table>


                </div>


            </div>  
        </section>

        <?php
    } else {
        echo"some thing wrong!";
    }
    ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
                        function callModel(e) {

                            $('#audit_id').val(e.id);
                            $('#modal-warning_feedbackpending').modal('show');
                        }
                        function callModelTotalAudits(e) {

                            $('#audit_id').val(e.id);
                            $('#modal-default').modal('show');
                        }
                         function callModelTotalAudits_hundred(e) {

                            $('#audit_id').val(e.id);
                            $('#modal-default_hundred').modal('show');
                        }
                        function callModelTotalFatals(e) {
                            $('#audit_id').val(e.id);
                            $('#modal-default_fatal').modal('show');
                        }
                        function callModelTotalNoActin(e) {
                            $('#audit_id').val(e.id);
                            $('#modal-successnoaction').modal('show');
                        }
                        function callModelNotAccepted(e) {
                            $('#audit_id').val(e.id);
                            $('#modal-warningNotAccepted').modal('show');
                        }
                        function callModelNotAcknologed(e) {
                            $('#audit_id').val(e.id);
                            $('#modal-warningNotAcknologed').modal('show');
                        }
</script>    