<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Outbound_callings Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Agents Id</b></td><td><?php echo $agents_id; ?></td></tr>
	    <tr><td><b>Patient Full Name</b></td><td><?php echo $patient_full_name; ?></td></tr>
	    <tr><td><b>Patient Mobile Number</b></td><td><?php echo $patient_mobile_number; ?></td></tr>
	    <tr><td><b>Address</b></td><td><?php echo $address; ?></td></tr>
	    <tr><td><b>District</b></td><td><?php echo $district; ?></td></tr>
	    <tr><td><b>Block</b></td><td><?php echo $block; ?></td></tr>
	    <tr><td><b>Call Status</b></td><td><?php echo $call_status; ?></td></tr>
	    <tr><td><b>Q1</b></td><td><?php echo $q1; ?></td></tr>
	    <tr><td><b>Q2</b></td><td><?php echo $q2; ?></td></tr>
	    <tr><td><b>Q3</b></td><td><?php echo $q3; ?></td></tr>
	    <tr><td><b>Q4</b></td><td><?php echo $q4; ?></td></tr>
	    <tr><td><b>Q5</b></td><td><?php echo $q5; ?></td></tr>
	    <tr><td><b>Q6</b></td><td><?php echo $q6; ?></td></tr>
	    <tr><td><b>Q7</b></td><td><?php echo $q7; ?></td></tr>
	    <tr><td><b>Q8</b></td><td><?php echo $q8; ?></td></tr>
	    <tr><td><b>Q9</b></td><td><?php echo $q9; ?></td></tr>
	    <tr><td><b>Created Date</b></td><td><?php echo $created_date; ?></td></tr>
	    <tr><td><b>Updated Date</b></td><td><?php echo $updated_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('outbound_callings') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>