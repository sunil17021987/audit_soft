<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Outbound_callings List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4 text-center">
                                <div style="margin-top: 4px"  id="message">
                                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                <?php echo anchor(site_url('outbound_callings/create'), 'Create', 'class="btn btn-primary"'); ?>
                                <?php echo anchor(site_url('outbound_callings/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">             
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th width="80px">No</th>
                                        <th>Agents Id</th>
                                        <th>Patient Full Name</th>
                                        <th>Patient Mobile Number</th>
                                        <th>Address</th>
                                        <th>District</th>
                                        <th>Block</th>
                                        <th>Call Status</th>
                                        <th>Q1</th>
                                        <th>Q2</th>
                                        <th>Q3</th>
                                        <th>Q4</th>
                                        <th>Q5</th>
                                        <th>Q6</th>
                                        <th>Q7</th>
                                        <th>Q8</th>
                                        <th>Q9</th>
                                        <th>Created Date</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($outbound_callings_data as $outbound_callings) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$start ?></td>
                                            <td><?php echo $outbound_callings->agent_name ?></td>
                                            <td><?php echo $outbound_callings->patient_full_name ?></td>
                                            <td><?php echo $outbound_callings->patient_mobile_number ?></td>
                                            <td><?php echo $outbound_callings->address ?></td>
                                            <td><?php echo $outbound_callings->district_name ?></td>
                                            <td><?php echo $outbound_callings->block_name ?></td>
                                            <td><?php echo $outbound_callings->call_type_name ?></td>
                                            <td><?php echo $outbound_callings->q1 ?></td>
                                            <td><?php echo $outbound_callings->q2 ?></td>
                                            <td><?php echo $outbound_callings->q3 ?></td>
                                            <td><?php echo $outbound_callings->q4 ?></td>
                                            <td><?php echo $outbound_callings->q5 ?></td>
                                            <td><?php echo $outbound_callings->q6 ?></td>
                                            <td><?php echo $outbound_callings->q7 ?></td>
                                            <td><?php echo $outbound_callings->q8 ?></td>
                                            <td><?php echo $outbound_callings->q9 ?></td>
                                            <td><?php echo $outbound_callings->created_date ?></td>

                                            <td style="text-align:center" width="200px">
                                                <?php
                                                echo anchor(site_url('outbound_callings/read/' . $outbound_callings->id), '<i class="fa fa-eye"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('outbound_callings/update/' . $outbound_callings->id), '<i class="fa fa-pencil-square-o"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('outbound_callings/delete/' . $outbound_callings->id), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>  
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#mytable").dataTable();
                            });
                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>