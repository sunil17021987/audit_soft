<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Outbound callings <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="varchar">Patient Full Name <?php echo form_error('patient_full_name') ?></label>
                                        <input type="text" class="form-control" name="patient_full_name" id="patient_full_name" placeholder="Patient Full Name" value="<?php echo $patient_full_name; ?>" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="varchar">Patient Mobile Number <?php echo form_error('patient_mobile_number') ?></label>
                                        <input type="text" class="form-control" name="patient_mobile_number" id="patient_mobile_number" placeholder="Patient Mobile Number" value="<?php echo $patient_mobile_number; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="address">Address <?php echo form_error('address') ?></label>
                                        <textarea class="form-control" rows="3" name="address" id="address" placeholder="Address"><?php echo $address; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="int">District <?php echo form_error('district') ?></label>
                                             <select name="district" class=" form-control select1 input-sm" id="category_main_id">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($category_main)) {
                                                                foreach ($category_main as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->category_main_id == $district) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->category_main_id ?>"><?php echo $downloadtype->category_main_name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                       
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="int">Block <?php echo form_error('block') ?></label>
                                         <select name="block" class=" form-control select1 input-sm" id="category_sub_id">
                                                            <option value="">Select Sub Category</option>

                                                        </select>
                                      
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="int">Call Status <?php echo form_error('call_status') ?></label>
                                         <select name="call_status" class=" form-control select1 input-sm" id="call_status">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($call_types_data)) {
                                                                foreach ($call_types_data as $call_types) {
                                                                    ?>
                                                                    <option <?php echo ($call_types->id == $call_status) ? 'selected' : ''; ?>  value="<?php echo $call_types->id ?>"><?php echo $call_types->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                      
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">
                                        How is your health now ?
                                        <?php echo form_error('q1') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="q1" value="GOOD" class="flat-red" checked> GOOD
                                        </label>
                                        <label>
                                            <input type="radio" name="q1" value="BETTER" class="flat-red"> BETTER
                                        </label>
                                        <label>
                                            <input type="radio" name="q1" value="WORSENED" class="flat-red"> WORSENED
                                        </label>
                                        <label>
                                            <input type="radio" name="q1" value="STABLE" class="flat-red"> STABLE
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">
                                        Are you aware of the home isolation guidelines issued by the administration ?

                                        <?php echo form_error('q2') ?>
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="q2" value="Yes" class="flat-red" checked> Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="q2" value="No" class="flat-red"> No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">
                                        Have you received your medicines?

                                        <?php echo form_error('q3') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="q3" value="Yes" class="flat-red" checked> Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="q3" value="No" class="flat-red"> No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">Are you taking the medicines as per the prescription?
                                        <?php echo form_error('q4') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="q4" value="Yes" class="flat-red" checked> Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="q4" value="No" class="flat-red"> No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">Are you facing any concerns right now?
                                        <?php echo form_error('q5') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <label>
                                                <input type="radio" name="q5" value="Yes" class="flat-red" checked> Yes
                                            </label>
                                            <label>
                                                <input type="radio" name="q5" value="No" class="flat-red"> No
                                            </label>
                                        </div>




                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">Has the call centre no 7440203333 shared with you incase of any emergency?
                                        <?php echo form_error('q6') ?></label>
                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>
                                                <input type="radio" name="q6" value="Yes" class="flat-red" checked> Yes
                                            </label>
                                            <label>
                                                <input type="radio" name="q6" value="No" class="flat-red"> No
                                            </label>
                                        </div>

                                    </div>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">Are any one else in your family has any symptoms of Covid?
                                        <?php echo form_error('q7') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>
                                                <input type="radio" name="q7" value="Yes" class="flat-red" checked> Yes
                                            </label>
                                            <label>
                                                <input type="radio" name="q7" value="No" class="flat-red"> No
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">Are the rest of your family members have undergone the sampling test for Covid?
                                        <?php echo form_error('q8') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="q8" value="Yes" class="flat-red" checked> Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="q8" value="No" class="flat-red"> No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="enum">This Contact center from District Administration can be accessed for any problems related to Covid. Please do not share any personal details like adhar card. Do not share any other information apart from your health related topics.
                                        <?php echo form_error('q9') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">

                                        <label>
                                            <input type="radio" name="q9" value="Yes" class="flat-red" checked> Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="q9" value="No" class="flat-red"> No
                                        </label>



                                    </div>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-xs-6">
                                </div>
                                <div class="col-xs-6">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                                    <a href="<?php echo site_url('outbound_callings') ?>" class="btn btn-default">Cancel</a>
                                </div>
                            </div>

                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>
 <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<!--<script src="<?php echo base_url('assets/timepicker/js/jquery.min.js') ?>"></script>-->
<!--<script src="<?php echo base_url('assets/timepicker/js/timepicki.js') ?>"></script>-->
      
<script type="text/javascript" src="<?php echo base_url('assets/dist/jquery.validate.js') ?>"></script>       
        <script>
           $('#category_main_id').on('change', function () {
            var category_main_id = $(this).val();

            if (category_main_id) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                    data: {category_main_id: category_main_id},
                    success: function (html) {
                        $('#category_sub_id').html(html);
                    }
                });


            }

        });
        </script>        