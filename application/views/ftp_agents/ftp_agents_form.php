<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Ftp_agents <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Upload By <?php echo form_error('upload_by') ?></label>
            <input type="text" class="form-control" name="upload_by" id="upload_by" placeholder="Upload By" value="<?php echo $upload_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">File Name <?php echo form_error('file_name') ?></label>
            <input type="text" class="form-control" name="file_name" id="file_name" placeholder="File Name" value="<?php echo $file_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Filename <?php echo form_error('filename') ?></label>
            <input type="text" class="form-control" name="filename" id="filename" placeholder="Filename" value="<?php echo $filename; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Fileurl <?php echo form_error('fileurl') ?></label>
            <input type="text" class="form-control" name="fileurl" id="fileurl" placeholder="Fileurl" value="<?php echo $fileurl; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Dateof <?php echo form_error('dateof') ?></label>
            <input type="text" class="form-control" name="dateof" id="dateof" placeholder="Dateof" value="<?php echo $dateof; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('ftp_agents') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>